@echo off
@echo ----- START %~f0 -----
title %~nx0

setlocal ENABLEDELAYEDEXPANSION
set MY_HOME=%~dp0
set MY_NAME=%~nx0

set ECHO_SET=off

set RCODE=1

:: 環境設定ファイル
set MY_RUBY_ENV=%MY_HOME%_RubyEnv.bat
if exist "%MY_RUBY_ENV%" (
    call "%MY_RUBY_ENV%"
)
@echo %ECHO_SET%

cd /d %MY_HOME%

@echo on
TECHINFO_Analysis_TypeS.exe %*
@set RCODE=%ERRORLEVEL%
@echo %ECHO_SET%

:: -- 終了処理 ---
:USAGE_ERROR

:DONE
@echo.
@echo ----- END %MY_NAME% by %RCODE% -----
@echo.
if %RCODE% == 0 (
    echo 正常終了しました。しばらくお待ち下さい。
    timeout 2 > nul
) else if %RCODE% == 2 (
    echo 処理を終了します。しばらくお待ち下さい。
    timeout 2 > nul
) else (
    echo 実行中にエラーが発生しました。処理を中止します。
    echo.
    echo ★★★ 何かキーを押して下さい。★★★
    pause > nul
)

:END
exit /b %RCODE%