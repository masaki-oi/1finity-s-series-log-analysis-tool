#coding: windows-31j
#/****************************************************************************
# *  Module  : AC_log_DropFileViewerForm
# *
# *                   All rights reserved, Copyright (C) FUJITSU LIMITED 2013
# *---------------------------------------------------------------------------
# *  処理概要: TECHINFO 内 PUFC_UD_sig.LOG の解析を行い ActionCode の
# *            実行シーケンスを CSVファイルに出力する。
# *  引数    : TECHINFO
# *  復帰値  : csv ファイル
# *---------------------------------------------------------------------------
# *  変更履歴: 2013.08.26 両備）柳本    新規作成(Version 1.0)
# *  変更履歴: 2013.10.17 QNET）児玉    エンディアン対応
# *  変更履歴: 2013.10.24 QNET）児玉    TECHINFOファイル名から抽出したフォルダを作成し、
# *                                     そのフォルダにAC解析結果ファイルを全て格納
# *                                     デフォルトで解析ログを出力するように変更
# *  変更履歴: 2013.11.08 QNET）吉村    エンディアン対応の自動判別対応を無しに変更
# *  変更履歴: 2014.04.14 QNET）野田    #17152 フォルダ構成変更対応
# *  変更履歴: 2014.06.12 QNET）野田    #18275 STEP3 TEHCINFO解析ツール  AC解析機能追加
# *  変更履歴: 2014.06.17 QNET）出合    #18283 STEP3 TECHINFO解析ツール パス系の記述を変数に一本化
# ****************************************************************************/
require 'rubygems'
require 'nkf'
require 'date'
require 'time'

#----------------------------------------------------------#
# ActionCode定義データ生成 クラス
#----------------------------------------------------------#
class AC_log_Analyze_Cls < Common_Analyze_Cls

  #----------------------------------------------------------#
  # コンストラクタ
  #----------------------------------------------------------#
  def initialize(label, prog_label, aclogobj, ac_path)
    @logobj = Log_class.instance
    @aclogobj = aclogobj

    log_write(sprintf("AC_log_Analyze_Cls::initialize Start\n"))
    @label = label
    @prog_label = prog_label
    @ac_path = ac_path
    log_write(sprintf("AC_log_Analyze_Cls::initialize End\n"))
  end

  #----------------------------------------------------------#
  #  ログ書き込み
  #----------------------------------------------------------#
  def log_write(msg, level=0)
    file, line, func = parse_caller(caller.first)
    @logobj.write(msg, level, file, line)
    @aclogobj.write(msg, level, file, line)
  end

  #----------------------------------------------------------#
  #  setting_status_logout
  #----------------------------------------------------------#
  def setting_status_logout()
    #設定情報をログに出力
    output_ary = [
                  ["要求 (IPPシグナル番号:0x60e00000)", $chk_req],
                  ["応答 (IPPシグナル番号:0x62e00000)", $chk_rsp],
                  ["通知 (IPPシグナル番号:0x60e00001)", $chk_info],
                  ["解析対象ActionCode", $ver_cmbx],
                  ["Endian変換モード", $endian_cmbx],
                  ["ACのpayloadサイズが0x300を超えた場合、そのACの解析をSkip", $payload],
                  ["解析ログを出力する", $chk_aclog],
                  ["解析対象期間を指定する", $chk_anatime],
                  ["解析開始時刻", $start_time],
                  ["解析終了時刻", $end_time]
                 ]

    output_ary.each{|output|
      output_disp = sprintf("%s = %s\n", output[0], output[1])
      # puts output_disp
      log_write(output_disp, 0)
    }
  end

  #-----------------------------------------------------------------#
  # TECHINFO.tgz 解析関数                                           #
  #-----------------------------------------------------------------#
  def techinfo_parse(techinfo)
    log_write(sprintf("AC_log_Analyze_Cls::techinfo_parse Start\n"))
    @label.text = "解析開始"

    # 設定情報をログに出力
    setting_status_logout()

    #14973 ac_tbl内にActionCode一覧＆詳細_X.XX版.gz解凍
    expand_check = 0
    #ディレクトリ名取得(パスなし)
    log_write(sprintf("%s\n", $ver_cmbx))
    dirname = $ver_cmbx
    expand_check = tar_expand(dirname)

    if expand_check == 0 #解凍ファイル有り(正常)
      @label.text = "解凍: ac_tbl/" + dirname + ".gz"

    elsif expand_check == 1 #解凍ファイル無し(処理終了)
      log_write(sprintf("expand_check error\n"))
      return
    end

    # TECHINFO の中身分繰り返す。
    Zlib::GzipReader.open(techinfo) do |tgz|
      Minitar::Reader.open(tgz).each do |entry|

        # TECHINFO の中身のファイル名取得。
        fname = entry.name.upcase

        # TGZ のファイルならば…
        if fname =~ /TGZ/ 
           
           # TGZ ファイル名取得
           $g_tgz_fname = entry.name.upcase
           $g_tgz_fname = $g_tgz_fname.encode(Encoding::Windows_31J)

           # TGZ ラベル更新
           @label.text = $g_tgz_fname + "解析中"
           
           # TGZ の解析開始
           tgzfile_parse(entry)
        end
        
      end  # tgz
    end  # entry

    #14973 ac_tbl内のD_XXXXファイル削除
    log_write(sprintf("dirname = %s\n", dirname))
    tar_delete(dirname)
    @label.text = "   削除: ac_tbl/D_XXXXファイル"

    # ActionCode 解析中にエラーを検出していた場合
    # エラーメッセージ出力ファイルを close。
    if ($g_errfile != 0)
      $g_errfile.close
      $g_errfile = 0
    end
    
    log_write(sprintf("AC_log_Analyze_Cls::techinfo_parse Analyze End\n"))
    @label.text = "-- 解析終了 --"
    puts "###########  処理終了 ###############"
    
    # 終了ダイアログ表示
    msg = "解析完了しました。\n解析結果：#{@ac_path}"
    wsh = WIN32OLE.new('WScript.Shell')
    wsh.Popup(msg,0,"完了")

    log_write(sprintf("AC_log_Analyze_Cls::techinfo_parse End\n"))

  end

  #-----------------------------------------------------------------#
  # TECHINFO 内 ****.tgz 解析関数                                   #
  #-----------------------------------------------------------------#
  def tgzfile_parse(entry)
    log_write(sprintf("AC_log_Analyze_Cls::tgzfile_parse Start\n"))

    # TGZ内の中身分繰り返す。
    fpl = StringIO.new(entry.read)
    Zlib::GzipReader.wrap(fpl) do |tgz|
      # TGZ のファイルを取り出す
      Minitar::Reader.open(tgz).each do |entry|

        # 名前が ActionCode を格納したログ(*_UD_sig.LOG)なら解析開始。
        if entry.name =~ /UD_sig.LOG/
        
          # 解析ログファイルの名前退避
          $g_ud_ac_fname = String.new(entry.name)
          
          # Fileオブジェクト取得
          ac_ud_log  = StringIO.new(entry.read)
          
          # ACログ解析スタート！！
          ud_ac_log_analyze(ac_ud_log)

          # 進捗ラベルクリア
          @prog_label.text = ""

        end  # if
      end  # entry
    end  # tgz

    log_write(sprintf("AC_log_Analyze_Cls::tgzfile_parse End\n"))

  end
  #----------------------------------------------------------#
  #  解析 Error メッセージログ出力関数                       #
  #----------------------------------------------------------#
  def err(msg)
    log_write(sprintf("AC_log_Analyze_Cls::err Start\n"), 2)

    # エラー検出フラグをセット
    # (このフラグはログ毎にクリアされる）
    if ($g_error_detected == 0)
      $g_error_detected = 1
    end

    # エラー出力ファイルを OPEN する
    if($g_errfile == 0)
      $g_errfile = File.open(@ac_path + "/" + ERR_FILE, "w")    # 2013.10.24 Kodama
    end

    # error.txt へ出力
    $g_errfile.puts "#{$g_tgz_fname}: #{$g_param_data[CSV_TIME]}: #{msg}"

    # コンソールへ出力
    puts "#{msg}"

    # 解析ログを採取している場合は解析ログへも出力
    if $chk_aclog
      $g_aclogfile.puts "#{msg}"
    end

    log_write(sprintf("AC_log_Analyze_Cls::err End\n"), 2)

  end
  #----------------------------------------------------------#
  #  解析 information メッセージログ出力関数                 #
  #----------------------------------------------------------#
  def info(msg)
    log_write(sprintf("AC_log_Analyze_Cls::info Start\n"))

    if($g_errfile == 0)
      $g_errfile = File.open(@ac_path + "/" + ERR_FILE, "w")  # 2013.10.24 Kodama
    end

    # error.txt へ出力
    $g_errfile.puts "#{$g_tgz_fname}: #{$g_param_data[CSV_TIME]}: #{msg}"

    # コンソールへ出力
    puts "#{msg}"

    # 解析ログを採取している場合は解析ログへも出力
    if $chk_aclog
      $g_aclogfile.puts "#{msg}"
    end

    log_write(sprintf("AC_log_Analyze_Cls::info End\n"))

  end
  #----------------------------------------------------------#
  #   解析 debug メッセージログ出力関数                      #
  #----------------------------------------------------------#
  def dbg(msg)
    log_write(sprintf("AC_log_Analyze_Cls::dbg Start\n"), 2)

    # puts "#{msg}"
    if $chk_aclog
      $g_aclogfile.puts "#{msg}"
    end

    log_write(sprintf("AC_log_Analyze_Cls::dbg End\n"), 2)

  end
  #----------------------------------------------------------#
  #   $g_param_data に最後に格納した値を出力                 #
  #----------------------------------------------------------#
  def dbg_last_param(msg)
    log_write(sprintf("AC_log_Analyze_Cls::dbg_last_param Start\n"), 2)

    if $chk_aclog
      last_num = $g_param_data.size - 1
      $g_aclogfile.puts "   => #{msg}: #{$g_param_data[last_num]}"
    end

    log_write(sprintf("AC_log_Analyze_Cls::dbg_last_param End\n"), 2)

  end
  #----------------------------------------------------------#
  #   SIGLOGをダンプする関数                                 #
  #----------------------------------------------------------#
  def dbg_dump_log(ud_ac_log, start_offset)
    log_write(sprintf("AC_log_Analyze_Cls::dbg_dump_log Start\n"))

    d = Array.new(16, "**")
    ud_ac_log.pos = start_offset
    i = start_offset % 16
    
    dbg("<LOG DUMP>")
    
    loop {
      val1 = getc_param(ud_ac_log, 1)
      if (val1 == "2d")
        val2 = getc_param(ud_ac_log, 1)
        if (val2 == "2d")
          val3 = getc_param(ud_ac_log, 1)
          if (val3 == "2d")
            val4 = getc_param(ud_ac_log, 1)
            if (val4 == "3e")
              dbg("0x#{(ud_ac_log.pos - i - 4).to_s(16)}: "+
                   " #{d[0]}#{d[1]}#{d[2]}#{d[3]}"+
                   " #{d[4]}#{d[5]}#{d[6]}#{d[7]}"+
                   " #{d[8]}#{d[9]}#{d[10]}#{d[11]}"+
                   " #{d[12]}#{d[13]}#{d[14]}#{d[15]}")

              log_write(sprintf("AC_log_Analyze_Cls::dbg_dump_log End\n"))
              return 
            end
            ud_ac_log.pos -= 1
          end
          ud_ac_log.pos -= 1
        end
        ud_ac_log.pos -= 1
      end
      d[i] = val1
      
      if (i == 15)
        dbg("0x#{(ud_ac_log.pos - 16).to_s(16)}: "+
             " #{d[0]}#{d[1]}#{d[2]}#{d[3]}"+
             " #{d[4]}#{d[5]}#{d[6]}#{d[7]}"+
             " #{d[8]}#{d[9]}#{d[10]}#{d[11]}"+
             " #{d[12]}#{d[13]}#{d[14]}#{d[15]}")
        i = 0
        d = Array.new(16, "**")
      else
        i += 1
      end
      
      # ファイル終端(EOF)?
      if ud_ac_log.eof?
        log_write(sprintf("AC_log_Analyze_Cls::dbg_dump_log End\n"))
        return
      end
    }

    log_write(sprintf("AC_log_Analyze_Cls::dbg_dump_log End\n"))

  end
  #----------------------------------------------------------#
  #  PUFC_UD_sig.LOG ファイルの解析                          #
  #----------------------------------------------------------#
  def ud_ac_log_analyze(ud_ac_log)
    log_write(sprintf("AC_log_Analyze_Cls::ud_ac_log_analyze Start\n"))

    #バイナリモードへ
    ud_ac_log.binmode

    # 変数初期化
    log_skip = 0              # 解析対象ログを判断
    log_num = 0               # ログ数をカウントする変数
    $g_endian = BIG_ENDIAN    # エンディアンのタイプを格納する変数
    $g_log_rotate = 0         # ログローテートの発生を判断
    $in_param_analyze = 0     # パラメタ部解析中フラグ
    last_timestamp = 0        # ひとつ前のログのタイムスタンプ
    last_timestamp_micro = 0  # ひとつ前のログのタイムスタンプ(マイクロ秒)

    # エンディアンチェックする。
    # 実機(BIG)とシミュレータ(LITTLE)でエンディアンが違う。
    endian_check(ud_ac_log)

    # 解析ログ採取のチェックがついていれば
    # 解析ログファイルをオープンし、解析ログを採取。
    if $chk_aclog
      # 解析ログファイルの名前設定
      logfile_name = @ac_path + $g_tgz_fname.gsub("TGZ", "txt") # 2013.10.24 Kodama

      # 解析ログファイルオープン
      log_write(sprintf("logfile_name = %s\n", logfile_name))
      $g_aclogfile = File.open(logfile_name, "w")
    end
    
    dbg("=============================================")
    dbg("   #{$g_tgz_fname}  (size #{ud_ac_log.size}) ")
    dbg("=============================================")
    # 2013.10.17 Kodama
    dbg("[Endian変換モード] #{$endian_cmbx}")

    # csv ファイルの名前設定
    csvfile_name = @ac_path + $g_tgz_fname.gsub("TGZ", "csv")   # 2013.10.24 Kodama

    # csv ファイルオープン
    csvfile = File.open(csvfile_name, "w")

    # csv ヘッダ部作成
    make_csv_header(csvfile)

    # 解析対象期間を取得(オプション機能)
    if $chk_anatime
      start_time = get_check_time($start_time)
      end_time   = get_check_time($end_time)
    end
    
    # 先頭の読み飛ばし
    get_param(ud_ac_log, 4)  # SIGLOG Manage Data
    
    #===================================================================#
    #  *_UD_sig.LOG 解析開始                                            #
    #===================================================================#
    while true

      # ログ番号インクリメント
      log_num = log_num + 1

      # データ格納配列の初期化(グローバル変数)
      # 本配列のデータが csv ファイルに出力される。
      $g_param_data.clear
      
      # "Result" カラムの領域確保
      $g_param_data << ""

      # ファイル終端(EOF)?
      if ud_ac_log.eof?
        @prog_label.text = "100%"
        dbg("[EOF DETECTED]")
        break
      end

      start_offset = ud_ac_log.pos
      dbg("---------------------------------------------")
      dbg("[START No.#{log_num}]")
      dbg("[LOG] offset : #{start_offset}(0x#{start_offset.to_s(16)})")

      # 進捗ラベル更新(GUI)
      @prog_label.text = "#{(ud_ac_log.pos * 100)/ud_ac_log.size}%"

      # シーケンス番号の取得
      sequence_no = get_param(ud_ac_log, 4) 
      
      # ログローテートされたログの終端かどうかチェック。
      if (sequence_no == 0x20202020)
        dbg("[ROTATE] ログローテートされたログの終端を検出しました")
        dbg_dump_log(ud_ac_log, start_offset)
        dbg("[END   No.#{log_num}]")
        next
      end
      
      # シーケンス番号を格納
      $g_param_data << sequence_no
      dbg_last_param("Sequence No.")
      
      # Timestamp 取得 
      timestamp       = get_param(ud_ac_log, 4)
      timestamp_micro = get_param(ud_ac_log, 4)

      get_param(ud_ac_log, 32) # src_name[32] の読み飛ばし
      get_param(ud_ac_log, 32) # dest_name[32]の読み飛ばし

      # SIGNAL 情報取得
      sig_num = get_param(ud_ac_log, 4)
      sig_len = get_param(ud_ac_log, 4)
      dbg("[Signal Info] sig_len #{sig_len}")

      # 解析対象ログかどうかの確認
      log_skip  = sig_num_check(sig_num)
      log_skip |= timestamp_check(timestamp, start_time, end_time)

      # 解析対象外の処理
      if (log_skip == 1)
        if($g_dbg_dump_log == 1)
          dbg_dump_log(ud_ac_log, start_offset)
          $g_dbg_dump_log = 0
        else
          # 対象外の場合ファイルの OFFSET を次のログまで進める。
          # Current + sig_len + 8バイト
          # 8バイトの内訳 : part_num(2), extra_info(2), 区切り文字(4)
          ud_ac_log.pos = ud_ac_log.pos + sig_len + 8
        end
        dbg("[END   No.#{log_num}]")
        log_skip = 0
        next
      end

      # タイムスタンプの格納
      $g_param_data << "#{Time.at(timestamp).strftime("%Y/%m/%d %H:%M:%S")} #{timestamp_micro}"
      dbg_last_param("Timestamp")

      # 前回ログとの時間差分(usec)を計算
      if (last_timestamp == 0)
        # 初回時は前回のデータが無いので今回と同じものを利用(差分0とする)
        last_timestamp       = timestamp
        last_timestamp_micro = timestamp_micro
      end
      diff = ((timestamp - last_timestamp) * 1000000) + timestamp_micro - last_timestamp_micro
      last_timestamp       = timestamp
      last_timestamp_micro = timestamp_micro

      if (diff < 0)
        # ログサイズは MAX_SIGLOG_SIZE(512KB)以上か？
        if (ud_ac_log.size >= MAX_SIGLOG_SIZE)
          info("[INFO] ログローテートを検出しました。")
          $g_log_rotate = 1 
        else
          info("[INFO] 時間差分(us)がマイナスになりました。(Wait(us) = #{diff})")
        end
      end 

      # 時間の差分情報(usec)格納
      $g_param_data << "#{diff}"
      dbg_last_param("Wait(us)")

      # シグナル種別格納
      sig_name = get_sig_name(sig_num)
      $g_param_data << "#{sig_name}"
      dbg_last_param("REQ/RSP")

      get_param(ud_ac_log, 2)   # part_num
      get_param(ud_ac_log, 2)   # extra_info
      get_param(ud_ac_log, 4)   # type
      get_param(ud_ac_log, 4)   # length
      get_param(ud_ac_log, 2)   # qualifier
      get_param(ud_ac_log, 2)   # tag
      get_param(ud_ac_log, 4)   # src_card
      get_param(ud_ac_log, 4)   # dest_card
      get_param(ud_ac_log, 32)  # src_name[32]
      get_param(ud_ac_log, 32)  # dest_name[32]
      
      # AC情報(ID、サイズ)の読込み
      ac_name = get_ac_info(ud_ac_log)

      get_param(ud_ac_log, 2)   # tlv_header_ac_t->tlv_parts_num
      get_param(ud_ac_log, 2)   # tlv_header_ac_t->reserve[2]
      get_param(ud_ac_log, 4)   # tlv_parts_header_t->type
      get_param(ud_ac_log, 4)   # tlv_parts_header_t->length
      
      # スロット(カード名)情報の読込み
      card_name = get_card_info(ud_ac_log)

      get_param(ud_ac_log, 32)  # tlv_parts_ac_header_t->rsp_card_name[32]
      get_param(ud_ac_log, 32)  # tlv_parts_ac_header_t->rsp_thread_name[32]
      get_param(ud_ac_log, 4)   # tlv_parts_ac_header_t->result

      # Paramater Type 取得(文字列として取得)
      tlv_cmn_header_type = getc_param(ud_ac_log, 4)
      
      # heder_type が "2d2d2d3e"(--->) ならパラメタ無しの AC
      if ("2d2d2d3e".casecmp(tlv_cmn_header_type) == 0)
        # CSV ファイルへデータ書き込み
        output_csv(csvfile)
        dbg("[ActionCode] No Parameter")
        dbg("[END   No.#{log_num}]")
        next
      end
      
      tlv_cmn_length    = get_param(ud_ac_log, 4)
      tlv_cmn_parts_num = get_param(ud_ac_log, 2)
      tlv_cmn_reserve   = get_param(ud_ac_log, 2)
      
      tlv_total_parts_num = tlv_cmn_parts_num
      $total_param_size = tlv_cmn_length - 4
      dbg("  [TLV] Total parameter size = #{$total_param_size}")
      dbg("  [TLV] Total parameter num = #{tlv_total_parts_num}")
      
      # ACのpayloadサイズが0x300超えた場合、そのACの解析をSkip
      if $payload
         # 合計パラメタサイズチェック(MAX_TOTAL_PARAM_SIZE(664)制限は下の方で説明)
         if (0 < $total_param_size && $total_param_size <= MAX_TOTAL_PARAM_SIZE)
          #=======================#
          # 各パラメタの解析開始  #
          #=======================#
          analyze_paramater(ud_ac_log, tlv_cmn_parts_num, tlv_total_parts_num)
          
         elsif ($total_param_size > MAX_TOTAL_PARAM_SIZE)
          #========================================================================#
          # ペイロード(commonヘッダ部/パラメタ部)はサイズ 768(0x300)バイトを       #
          # 超えた場合、768(0x300)バイト以降は切り捨てられる(つまり最大768バイト)。#
          #     ⇒ common ヘッダ部のサイズは固定 92 バイト。                       #
          #     ⇒ よって、パラメタ部の最大サイズは 676 バイト。(768 - 92 バイト)  #
          # なお、パラメタのサイズは以下の内訳である。                             #
          #                                                                        #
          #  ＜パラメタ部のサイズ(676バイト) の内訳＞                              #
          #            - type (4バイト)                                            #
          #            - length (4バイト)                                          #
          #            - tlv_parts_num (2バイト)                                   #
          #            - reserved (2バイト)                                        #
          #            - lengthの中身                                              # 
          #                                                                        #
          # 上記より、最大 "lengthの中身" は、 676 - 4 - 4 - 2 - 2 = 664 バイト    #
          # よって、664 バイトを超えている場合、パラメタ部は切り捨てられている。   #
          #========================================================================#
          dbg("[EXCESS] ペイロードサイズ 768(0x300)バイト以上のログを検出しました。")
          $g_error_detected = 2
          ud_ac_log.pos += MAX_TOTAL_PARAM_SIZE

        elsif ($total_param_size <= 0)
          #===============================================================#
          # *** Work Around ***                                           #
          # パラメタが無いのに TLV ヘッダがある AC の対処 (UD の Bug?)。  #
          # 例えば "UD_AC_FPG_INITIAL_CHECK"                              #
          # カテゴリNo-015_ActionCode_FPGA データ更新(CPU).xlsx           #
          # シート（FPGA初期化チェック）                                  #
          #===============================================================#
          info("[INFO] パラメタの合計サイズが 0 のログを検出したため、パラメタ解析はスキップしました。")
        end
      else
        if (0 < $total_param_size)
          #=======================#
          # 各パラメタの解析開始  #
          #=======================#
          # $total_param_sizeは、analyze_paramater2にて更新されるため
          # 768(0x300)バイト以上のデータ時用に値を保持しておく
          bak_total_param_size = $total_param_size
          analyze_paramater2(ud_ac_log, tlv_cmn_parts_num, tlv_total_parts_num)

          # dbg(sprintf("  [CHK] bak_total_param_size = %X", $bak_total_param_size))
          # dbg(sprintf("  [CHK] MAX_TOTAL_PARAM_SIZE = %X", MAX_TOTAL_PARAM_SIZE))
          if (bak_total_param_size > MAX_TOTAL_PARAM_SIZE)
            dbg("[EXCESS] ペイロードサイズ 768(0x300)バイト以上のデータは破棄されます。")
            $g_error_detected = 2
            # ud_ac_log.pos += MAX_TOTAL_PARAM_SIZE
          end
        else
          #===============================================================#
          # *** Work Around ***                                           #
          # パラメタが無いのに TLV ヘッダがある AC の対処 (UD の Bug?)。  #
          # 例えば "UD_AC_FPG_INITIAL_CHECK"                              #
          # カテゴリNo-015_ActionCode_FPGA データ更新(CPU).xlsx           #
          # シート（FPGA初期化チェック）                                  #
          #===============================================================#
          info("[INFO] パラメタの合計サイズが 0 のログを検出したため、パラメタ解析はスキップしました。")

        end
      end

      # SIGLOG終端文字(--->)取得
      end_char = getc_param(ud_ac_log, 4)
      
      # 終端文字チェック
      if ("2d2d2d3e".casecmp(end_char) != 0)
        # 何らかの問題により終端文字ではなかった。
        show_something_wrong(ac_name)
        
        # 次のログまでオフセットを進める
        ud_ac_log.pos += $total_param_size
      end
      
      # データを CSV に書き込み
      output_csv(csvfile)
      dbg("[END   No.#{log_num}]")
      
    end # End of while No1

    # csv ファイルクローズ
    csvfile.close

    # ログローテートが行われていた場合は
    # csv ファイルのデータを並び替える。
    if ($g_log_rotate == 2)
      sort_csvfile(csvfile_name)
    end

    # 解析ログを採取していたならばクローズする
    if $chk_aclog
      $g_aclogfile.close
    end
    log_write(sprintf("AC_log_Analyze_Cls::ud_ac_log_analyze End\n"))
  end

  #---------------------------------------------------------------#
  #  エンディアンをチェックする関数。                             #
  #  以下を判断する（ -> get_param() の動作を変える)              #
  #   - UDシミュレータは Intel アーキ(little endian)              #
  #   - 実機は PPC（big endian）                                  #
  #                                                               #
  # <チェック方法>                                                #
  # GUIの『Endian変換モード』のメニューで変換方法を切り替える     #
  # (1)変換をシグナルログで自動判別                               #
  #    ud_ac_log の offset 96 (メッセージヘッダ部のlength)には    #
  #    固定値として 76 が入っている。この値を読んで判断する。     #
  # (2)変換しない（実機ログ）                                     #
  #    Big Endianのまま                                           #
  # (3)変換する（シミュレーションログ）                           #
  #    Little Endian -> Big Endian                                #
  #---------------------------------------------------------------#
  def endian_check(ud_ac_log)
    log_write(sprintf("AC_log_Analyze_Cls::endian_check Start\n"))

    # オフセット保存
    oldval = ud_ac_log.pos
    
    # length へ
    ud_ac_log.pos = 96

    val = get_param(ud_ac_log, 4)

    # 2013.10.17 Kodama
    endian_mode = $endian_cmbx
    puts "[解析ファイル] #{$g_tgz_fname}"
    puts "[Endian変換モード] #{endian_mode}"

    # 2013.10.17 Kodama
    if endian_mode =~ /自動判別/
      # 上位16ビットに値があれば Little Endian(シミュレータ環境のログ)
      if (val != 76)
        $g_endian = LITTLE_ENDIAN
      end
    elsif endian_mode =~ /変換する/
      $g_endian = LITTLE_ENDIAN
    end

    # オフセット復元
    ud_ac_log.pos = oldval

    log_write(sprintf("AC_log_Analyze_Cls::endian_check End\n"))

  end

  #---------------------------------------------------------------#
  #  ローテート発生時にログ情報並び替える関数                     #
  #---------------------------------------------------------------#
  def sort_csvfile(csvfile_name)
    log_write(sprintf("AC_log_Analyze_Cls::sort_csvfile Start\n"))

    find = 0
    last_line = ""
    tmpfile_name = $D_RUN_PATH + "/tmp_for_rotate.txt"
    log_write(sprintf("tmpfile_name path = %s\n", tmpfile_name))

    # csv ファイルオープン
    File.open(csvfile_name, "r") do |csvfile|
      # tmp ファイル作成
      File.open(tmpfile_name, "w") do |tmpfile|

        # tmp ファイルにヘッダ作成
        make_csv_header(tmpfile)

        # csv ファイルの先頭から読んでいく
        while line = csvfile.gets

          # last_line には最終的に最後ログが入って While を抜ける。
          last_line = String.new(line)

          # csv ファイル内の ROTATE を検出した以降は、
          # tmp ファイルにそのままデータを出力する。
          if (find == 1)
            tmpfile.print line
            next
          end

          # ROTATE の文字列があるかどうか？
          if line =~ /ROTATE/

            # ROTATE検出
            find = 1

            # ROTATE の文字を削除して代わりに
            # 0(先頭なので時間差分なし)を入れる。
            line.gsub!("ROTATE", "0")

            # tmpファイルに書き込み
            tmpfile.print line
          end
        end

        #--------------------------------#
        # ここまでで ROTATE 以降の行が   #
        # tmp ファイルに記載されている。 #
        #--------------------------------#

        # 先頭にオフセットを戻す。
        csvfile.rewind

        # ヘッダ分(頭３行)を読み飛ばす。
        3.times {
          csvfile.gets
        }

        # 最初のログ行を取り出す。
        first_line = csvfile.gets

        # 最初のログを分割し、timestamp を取り出す。
        first_line_array = first_line.split(",")
        first_timestamp = String.new(first_line_array[CSV_TIME])

        # 最後のログを分割し、timestamp を取り出す。
        last_line_array = last_line.split(",")
        last_timestamp = String.new(last_line_array[CSV_TIME])

        # 二つの時間差分を求める。
        diff = get_timediff_from_csv(first_timestamp, last_timestamp)
        first_line_array[CSV_WAIT] = sprintf("'%d", diff)

        # 最後の改行を削除
        first_line_array.pop

        # tmpline を tmp ファイルに出力
        first_line_array.each do |param_data|
          tmpfile.print param_data, ","
        end
        tmpfile.print "\n"

        # 以降は、ROTATE を検出するまで単純に順番通り出力する。
        while line = csvfile.gets
          # ROTATE の文字を見つけたら終了
          if line =~ /ROTATE/
            break
          end
          tmpfile.print line
        end
        
      end # tmpfile.close
    end # csvfile.close

    # 元のファイルを削除
    File.delete(csvfile_name)
    # ファイル名変更
    File.rename(tmpfile_name, csvfile_name)

    log_write(sprintf("AC_log_Analyze_Cls::sort_csvfile End\n"))

  end
  #---------------------------------------------------------------#
  #  CSV に格納した形式のタイムスタンプからエポックタイムを求める #
  #---------------------------------------------------------------#
  def get_timestamp_from_csv(timestamp)
    log_write(sprintf("AC_log_Analyze_Cls::get_timestamp_from_csv Start\n"))

    # "'" を消す
    timestamp.gsub!("'", "")

    # スペースで区切る
    tmp = timestamp.split(" ")

    # parse メソッドで処理
    time = Time.parse("#{tmp[0]} #{tmp[1]}")

    # UNXI タイムに変換して返す。]
    log_write(sprintf("AC_log_Analyze_Cls::get_timestamp_from_csv End\n"))
    return time.to_i

  end
  #---------------------------------------------------------------#
  #  CSV に格納した形式のタイムスタンプからマイクロ秒を求める     #
  #---------------------------------------------------------------#
  def get_timestamp_micro_from_csv(timestamp)
    log_write(sprintf("AC_log_Analyze_Cls::get_timestamp_micro_from_csv Start\n"))

    # CSV のフォーマットは "'YYYY/MM/DD HH:MM:SS <MICRO>"
    # よってスペース区切り、その3番目の要素がマイクロ秒。
    tmp = timestamp.split(" ")
    
    # 文字列なので数値に直して復帰
    log_write(sprintf("AC_log_Analyze_Cls::get_timestamp_micro_from_csv End\n"))
    return tmp[2].to_i

  end

  #---------------------------------------------------------------#
  #  CSV に格納した形式のタイムスタンプデータから差分を求める     #
  #---------------------------------------------------------------#
  def get_timediff_from_csv(timestamp1, timestamp2)
    log_write(sprintf("AC_log_Analyze_Cls::get_timediff_from_csv Start\n"))

    timestamp      = get_timestamp_from_csv(timestamp1)
    last_timestamp = get_timestamp_from_csv(timestamp2)
    timestamp_micro      = get_timestamp_micro_from_csv(timestamp1)
    last_timestamp_micro = get_timestamp_micro_from_csv(timestamp2)

    diff = ((timestamp - last_timestamp) * 1000000) + timestamp_micro - last_timestamp_micro

    log_write(sprintf("AC_log_Analyze_Cls::get_timediff_from_csv End\n"))
    return diff
  end
  #---------------------------------------------------------------#
  #  SIGLOGの終端情報異常時のエラー出力                           #
  #---------------------------------------------------------------#
  def show_something_wrong(ac_name)
    log_write(sprintf("AC_log_Analyze_Cls::show_something_wrong Start\n"))

    # 全パラメタを解析しても区切り文字(--->)が現れない場合に出力
    # - 解析ルーチンが間違っている(本プログラムのバグ)
    #  - ログの格納方法が間違っている(UDのbug)
    #  - フォーマットファイル自体が間違っている
    #     ・AC仕様書.xlsx 解析アプリのバグ
    #     ・AC仕様書のバグ
    err("[ERROR] ActionCode「#{ac_name}」 解析中に問題を検出しました。")
    err("        - SIGLOG に異常がある、または、")
    err("        - 解析フォーマットファイルの記載ミス(情報が足りない等)、または、")
    err("        - 解析フォーマットの版数と SIGLOG の版数が合っていない、などが考えられます。")
    
    log_write(sprintf("AC_log_Analyze_Cls::show_something_wrong End\n"))

  end
  #---------------------------------------------------------------#
  #  IPP シグナル種類名を取得する関数                             #
  #---------------------------------------------------------------#
  def get_sig_name(sig_num)
    log_write(sprintf("AC_log_Analyze_Cls::get_sig_name Start\n"), 2)

    # IPP シグナル番号からどの種類の AC かを判断
    #  - 要求 (REQ)
    #  - 応答 (RSP)
    #  - 通知 (INFO)
    if (sig_num == 0x60E00000)
      name = "REQ"
    elsif (sig_num == 0x62E00000)
      name = "RSP"
    elsif (sig_num == 0x60E00001)
      name = "INFO"
    else
      name = "UNKNOWN"
    end

    log_write(sprintf("AC_log_Analyze_Cls::get_sig_name End\n"), 2)
    return name

  end
  #---------------------------------------------------------------#
  #  Card(slot)情報の取得(slot番号、カード名)                     #
  #---------------------------------------------------------------#
  def get_card_info(ud_ac_log)
    log_write(sprintf("AC_log_Analyze_Cls::get_card_info Start\n"), 2)

    # スロット番号取得
    slot_no = get_param(ud_ac_log, 4)

    # スロット番号格納
    $g_param_data << sprintf("%08x", slot_no)
    dbg_last_param("Slot NO")

    # スロット番号からカード名取得
    card_name = get_card_name(slot_no)

    # カード名格納
    $g_param_data << "#{card_name}"
    dbg_last_param("Card name")

    log_write(sprintf("AC_log_Analyze_Cls::get_card_info End\n"), 2)
    return card_name

  end

  #---------------------------------------------------------------#
  #  ActionCode 情報の取得(ID、ペイロードサイズ)                  #
  #---------------------------------------------------------------#
  def get_ac_info(ud_ac_log)
    log_write(sprintf("AC_log_Analyze_Cls::get_ac_info Start\n"), 2)

    # ActionCode ID 取得
    ac_id     = get_param(ud_ac_log, 4)
    ac_length = get_param(ud_ac_log, 4)

    # ActionCode の名前をリストから取得
    ac_name = get_ac_name(ac_id)

    # ActionCode ID 格納
    $g_param_data << sprintf("%08x", ac_id)
    dbg_last_param("AC ID")

    # ActionCode 名前格納
    $g_param_data << "#{ac_name}"
    dbg_last_param("AC Name")

    # debug
    dbg("[ActionCode] #{ac_name} (0x#{ac_id.to_s(16)}) length #{ac_length}\n")

    log_write(sprintf("AC_log_Analyze_Cls::get_ac_info End\n"), 2)
    return ac_name

  end

  #---------------------------------------------------------------#
  #  各パラメタの解析                                             #
  #---------------------------------------------------------------#
  def analyze_paramater(ud_ac_log, tlv_cmn_parts_num, tlv_total_parts_num)
    log_write(sprintf("AC_log_Analyze_Cls::analyze_paramater Start\n"), 2)

    # パラメタ解析中のフラグを立てる
    $in_param_analyze = 1
    
    param_no = 0
    # パラメタの数だけ繰り返す
    while tlv_cmn_parts_num > 0 do

      param_no += 1
      dbg("  [TLV] Parameter #{param_no} / #{tlv_total_parts_num}")

      # Paramater Type 取得
      tlv_parts_header_type   = get_param(ud_ac_log, 4)
              
      # パラメタタイプ格納
      $g_param_data << sprintf("type=%08x", tlv_parts_header_type)
      dbg_last_param("TLV")

      # Paramater length 取得
      tlv_parts_header_length = get_param(ud_ac_log, 4)
      
      # length 取得時のオフセットを覚えておく(エラー時に使用)
      tmp_pos = ud_ac_log.pos

      # パラメタ長格納
      $g_param_data  << sprintf("length=%08x", tlv_parts_header_length)
      dbg_last_param("TLV")

      # TLV 名取得
      tlv_name = get_tlv_name(tlv_parts_header_type)

      # TLV 名が取得出来なければスキップ
      if ("NOT_FOUND".casecmp(tlv_name) == 0)
         # エラー時は次のパラメタまでオフセットを進める
         ud_ac_log.pos = tmp_pos + tlv_parts_header_length
         tlv_cmn_parts_num = tlv_cmn_parts_num - 1
         next
      end

      dbg("  [TLV] #{tlv_name} (0x#{tlv_parts_header_type.to_s(16)}) length #{tlv_parts_header_length}")

      # パラメタの取得
      ret = get_tlv_paramater(tlv_name, ud_ac_log)
      if (ret < 0)
        # エラー時は次のパラメタまでオフセットを進める
        ud_ac_log.pos = tmp_pos + tlv_parts_header_length
      end

      tlv_cmn_parts_num = tlv_cmn_parts_num - 1

    end # while
      
    # パラメタ解析中のフラグを落とす
    $in_param_analyze = 0

    log_write(sprintf("AC_log_Analyze_Cls::analyze_paramater End\n"), 2)

  end

  #---------------------------------------------------------------#
  #  各パラメタの解析                                             #
  #---------------------------------------------------------------#
  def analyze_paramater2(ud_ac_log, tlv_cmn_parts_num, tlv_total_parts_num)
    log_write(sprintf("AC_log_Analyze_Cls::analyze_paramater2 Start\n"), 2)

    payload_now_length = 0

    # パラメタ解析中のフラグを立てる
    $in_param_analyze = 1
    
    start_address = ud_ac_log.pos
    end_address = start_address + MAX_TOTAL_PARAM_SIZE
    # dbg(sprintf("  [BIN] start_address=%d(0x%X)", start_address, start_address))

    param_no = 0
    # パラメタの数だけ繰り返す
    while tlv_cmn_parts_num > 0 do
      param_no += 1
      dbg("  [TLV] Parameter #{param_no} / #{tlv_total_parts_num}")
      # dbg(sprintf("  [BIN] now_address=%d(0x%X), payload_address=%d(0x%X)", ud_ac_log.pos, ud_ac_log.pos, payload_now_length, payload_now_length))

      # Paramater Type 取得
      tlv_parts_header_type   = get_param(ud_ac_log, 4)
      payload_now_length = payload_now_length + 4

      # パラメタタイプ格納
      $g_param_data << sprintf("type=%08x", tlv_parts_header_type)
      dbg_last_param("TLV")

      # Paramater length 取得
      tlv_parts_header_length   = get_param(ud_ac_log, 4)
      payload_now_length = payload_now_length + 4

      # length 取得時のオフセットを覚えておく(エラー時に使用)
      tmp_pos = ud_ac_log.pos

      # パラメタ長格納
      $g_param_data  << sprintf("length=%08x", tlv_parts_header_length)
      dbg_last_param("TLV")
      if payload_now_length + tlv_parts_header_length > MAX_TOTAL_PARAM_SIZE
        ud_ac_log.pos = ud_ac_log.pos + (MAX_TOTAL_PARAM_SIZE - payload_now_length)
        # dbg(sprintf("  [BIN] TLV Anlyze Break Hit ud_ac_log.pos=%d(0x%X), end address=%d(0x%X)", ud_ac_log.pos, ud_ac_log.pos, end_address, end_address))
        # dbg(sprintf("  [BIN] payload_now_lengths=%d(0x%X), tlv_parts_header_length=%d(0x%X)", payload_now_length, payload_now_length, tlv_parts_header_length, tlv_parts_header_length))
        break
      else
        payload_now_length = payload_now_length + tlv_parts_header_length
      end

      # TLV 名取得
      tlv_name = get_tlv_name(tlv_parts_header_type)

      # TLV 名が取得出来なければスキップ
      if ("NOT_FOUND".casecmp(tlv_name) == 0)
         # エラー時は次のパラメタまでオフセットを進める
         ud_ac_log.pos = tmp_pos + tlv_parts_header_length
         tlv_cmn_parts_num = tlv_cmn_parts_num - 1
         next
      end

      dbg("  [TLV] #{tlv_name} (0x#{tlv_parts_header_type.to_s(16)}) length #{tlv_parts_header_length}")

      # パラメタの取得
      ret = get_tlv_paramater(tlv_name, ud_ac_log)
      if (ret < 0)
        # エラー時は次のパラメタまでオフセットを進める
        ud_ac_log.pos = tmp_pos + tlv_parts_header_length
      end

      tlv_cmn_parts_num = tlv_cmn_parts_num - 1

    end # while
      
    # パラメタ解析中のフラグを落とす
    $in_param_analyze = 0

    log_write(sprintf("AC_log_Analyze_Cls::analyze_paramater2 End\n"), 2)

  end
  #----------------------------------------------------------#
  #  CSV ファイルのヘッダ部作成                              #
  #----------------------------------------------------------#
  def make_csv_header(f)
    log_write(sprintf("AC_log_Analyze_Cls::make_csv_header Start\n"))

    # 現在時刻取得
    day = Time.now

    # 解析に使用するフォーマットバージョンを取得
    # (プルダウンメニューよりユーザが選択)
    ver = $ver_cmbx
    
    # ヘッダ情報書き込み
    f.print "Date, File Name, AC VERSION, CONVERT VERSION\n"

    # Date
    f.print day.strftime("%a %b %d %H:%M:%S %z %Y"), "," 

    # File Name
    f.print $g_drop_fname.gsub(/.TGZ/, ""),    "/"   # TECHINFO.TGZ   -> TECHINFO
    f.print $g_tgz_fname.gsub(/.\/|.TGZ/, ""), "/"   # ./*****.TGZ    -> *****
    f.print $g_ud_ac_fname.gsub(/.\//, ""),    ","   # ./*_UD_sig.LOG -> *_UD_sig.LOG
    
    # AC VERSION
    f.print ver, ","
    
    # CONVERT VERSION
    f.print $log_analyeze_version, "\n"
    
    # ヘッダ情報書き込み
    f.print "Result, Sequence No, Timestamp, Wait(us), REQ/RSP, AC#, AC Name, Card#, Card Name, Parameter\n"

    log_write(sprintf("AC_log_Analyze_Cls::make_csv_header End\n"))

  end
  #----------------------------------------------------------#
  #  CSVファイルへの書き込み                                 #
  #----------------------------------------------------------#
  def output_csv(f)
    log_write(sprintf("AC_log_Analyze_Cls::output_csv Start\n"), 2)

    # 解析中にエラーが発生していた場合 "Result" 列に以下を表示
    #  - "ERROR"  : 解析エラー等
    #  - "EXCESS" : ペイロードサイズが 0x300 を超えていた場合
    if ($g_error_detected == 1) 
      $g_param_data[CSV_RESULT] = "ERROR"
    elsif ($g_error_detected == 2)
      $g_param_data[CSV_RESULT] = "EXCESS"
    end
    
    # ログローテートが行われていたら、その ROTATE を付加。
    if ($g_log_rotate == 1)
      $g_param_data[CSV_WAIT] = "ROTATE"
      $g_log_rotate = 2
    end
    
    # エラー検出フラグをクリア
    $g_error_detected = 0
     
    # 配列の数だけ繰り返す
    $g_param_data.each do |param_data|
      # データ出力
      # (文字列認識させるため先頭にアロストロフィを付加)
      f.print "'", param_data, ","
    end
    
    f.print "\n"

    log_write(sprintf("AC_log_Analyze_Cls::output_csv End\n"), 2)

  end
  #----------------------------------------------------------#
  #  SIGNAL番号が解析対象タイプかチェック                    #
  #----------------------------------------------------------#
  def sig_num_check(sig_num)
    log_write(sprintf("AC_log_Analyze_Cls::sig_num_check Start\n"), 2)

    skip = 0
    name = get_sig_name(sig_num)

    if (name == "UNKNOWN")
      info("[INFO] AC 要求/応答/通知 以外の IPP シグナル番号 0x#{sig_num.to_s(16)} を検出したためログ解析をスキップしました。")
      skip = 1
      $g_dbg_dump_log = 1
    elsif (name == "REQ" && $chk_req == false)
      skip = 1
      $g_dbg_dump_log = 1
    elsif (name == "RSP" && $chk_rsp == false)
      skip = 1
      $g_dbg_dump_log = 1
    elsif (name == "INFO" && $chk_info == false)
      skip = 1
      $g_dbg_dump_log = 1
    end

    if (skip == 0)
      dbg("[ActionCode] #{name} Action Code (0x#{sig_num.to_s(16)})")
    else
      dbg("[SKIP] #{name} Action Code (0x#{sig_num.to_s(16)})")
    end

    log_write(sprintf("AC_log_Analyze_Cls::sig_num_check End\n"), 2)
    return skip
    
  end
  #----------------------------------------------------------#
  #  タイムスタンプが解析開始＆終了時間内かどうかをチェック  #
  #----------------------------------------------------------#
  def timestamp_check(timestamp, start_time, end_time)
    log_write(sprintf("AC_log_Analyze_Cls::timestamp_check Start\n"), 2)

    dbg("[ActionCode] #{Time.at(timestamp).strftime("%a %b %d %H:%M:%S %z %Y")}")
    if $chk_anatime
      if (timestamp < start_time || end_time < timestamp)
        dbg("[SKIP] Out of check time")
        log_write(sprintf("AC_log_Analyze_Cls::timestamp_check(SKIP) End\n"))
        return 1
      end
    end

    log_write(sprintf("AC_log_Analyze_Cls::timestamp_check End\n"), 2)
    return 0

  end
  #----------------------------------------------------------#
  #  解析開始＆終了時間を取得                                #
  #----------------------------------------------------------#
  def get_check_time(time)
    log_write(sprintf("AC_log_Analyze_Cls::get_check_time Start\n"))

    # "YYYYMMDDhhmmss" 形式の文字列を分解
    tmp = time.scanf '%4d%2d%2d%2d%2d%2d'
    
    # Timeオブジェクトへ変換
    t = Time.local(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5])
      dbg("[TARGET TIME] #{tmp[0]}/#{tmp[1]}/#{tmp[2]} #{tmp[3]}:#{tmp[4]}:#{tmp[5]}")

    log_write(sprintf("AC_log_Analyze_Cls::get_check_time End\n"))
    return t.to_i
    
  end

  #----------------------------------------------------------#
  #  TLVフォーマット解析部 & 値取得                          #
  #----------------------------------------------------------#
  def analyze_tlv_fmt(f, ud_ac_log, dynamic_check, dynamic_size, tlv_name)
    log_write(sprintf("AC_log_Analyze_Cls::analyze_tlv_fmt Start\n"), 2)

    # 変数初期化
    array_name = ""
    array_num  = 0
    array_num2 = 0

    while line = f.gets
      # dbg("      (format) #{line}")

      # 空白＆タブ削除、かつ、コメント以外を抽出
      tmp  = line.gsub(/\s|\t/, "").split("#")
      line = tmp[0]

      # 空行チェック
      if (line.nil?)
        next
      end

      #====================#
      # 二次元配列         #
      #====================#
      if line =~ /\[\d*\]\[\d*\]/
        # 2013.10.11 Kodama
        tmp = line.split(/,|\[/)
        array_type = tmp[0]
        array_name = tmp[1]
        array_num  = tmp[2].to_i
        array_num2 = tmp[3].to_i

        # エラーチェック
        # 2013.10.11 Kodama
        if !(array_type =~ /char|short|long|int|struct/)
          err("[ERROR] <解析エラー> 不明な型名です。フォーマット('#{line}')")
          return -1
        end
        if (array_name.nil?)
          err("[ERROR] <解析エラー> 変数名が取得できませんでした。 フォーマット('#{line}')")
          return -1
        end
        if (array_num.nil? || array_num == 0)
          err("[ERROR] <解析エラー> 配列サイズが 0 または NULLです。フォーマット('#{line}')")
          return -1
        end
        if (array_num2.nil? || array_num2 == 0)
          err("[ERROR] <解析エラー> 配列サイズが 0 または NULLです。フォーマット('#{line}')")
          return -1
        end

        # 普通の2次元配列(構造体配列ではない)を処理
        if line =~ /=/
          tmp = line.split("=")
          array_total_size = tmp[1].to_i

          # エラーチェック
          if (array_total_size.nil? || array_total_size == 0)
            err("[ERROR] <解析エラー> 配列サイズが 0 または NULLです。フォーマット('#{line}')")
            return -1
          end
          
          # 配列数と全体サイズから単体のサイズを求める
          array_each_size = array_total_size / (array_num * array_num2)
          
          # エラーチェック
          if (array_each_size == 0)
            err("[ERROR] <解析エラー> 配列サイズが 0 または NULL です。フォーマット('#{line}')")
            return -1
          end

          # char 型 (1byte)なら連続で出力
          if (array_each_size == 1)

            # 配列名を一時退避
            array_name_tmp = String.new(array_name)

            array_no = 0
            while array_no < array_num do

              # 退避しておいた配列名を代入
              array_name = String.new(array_name_tmp)

              # 格納
              array_name << "[#{array_no}][#{array_num2}]"
              array_no += 1
              data = getc_param(ud_ac_log, array_num2)
              data = conv_endian(data, array_type, array_num2, array_name)  #2013.10.11 Kodama
              set_param(array_name, data)
            end
          else
            # 配列名を一時退避
            array_name_tmp = String.new(array_name)

            array_no = 0
            while array_no < array_num  do
              array_no2 = 0
              while array_no2 < array_num2 do
              
                # 退避しておいた配列名を代入
                array_name = String.new(array_name_tmp)

                # 格納
                array_name << "[#{array_no}][#{array_no2}]"
                array_no2 += 1
                data = getc_param(ud_ac_log, array_each_size)
                data = conv_endian(data, array_type, array_each_size, array_name) #2013.10.11 Kodama
                set_param(array_name, data)
              end
              array_no += 1
            end
          end
          array_name = ""
          array_num  = 0
          array_num2 = 0
        end
      #====================#
      # １次元配列         #
      #====================#
      elsif line =~ /\[\d*\]/

        # 2013.10.11 Kodama
        tmp = line.split(/,|\[/)
        array_type = tmp[0]
        array_name = tmp[1]
        array_num  = tmp[2].to_i
        
        # エラーチェック
        # 2013.10.11 Kodama
        if !(array_type =~ /char|short|long|int|struct/)
          err("[ERROR] <解析エラー> 不明な型名です。フォーマット('#{line}')")
          return -1
        end
        if (array_name.nil?)
          err("[ERROR] <解析エラー> 変数名が取得できませんでした。 フォーマット('#{line}')")
          return -1
        end
        if (array_num.nil? || array_num == 0)
          err("[ERROR] <解析エラー> 配列サイズが 0 または NULLです。フォーマット('#{line}')")
          return -1
        end

        #15156 動的バッファ対応
        dynamic_change = 0
        if (dynamic_size != 0 && dynamic_check == 1)
          case array_name
          when /^lbm_data/
            array_num = dynamic_size
            dynamic_change = 1
          when /^lbr_data/
            array_num = dynamic_size
            dynamic_change = 1
          when /^dmm_data/
            array_num = dynamic_size
            dynamic_change = 1
          when /^lmm_data/
            array_num = dynamic_size
            dynamic_change = 1
          else
          end
        end

        # 普通の配列(構造体配列ではない)を処理
        if line =~ /=/
          tmp = line.split("=")
          array_total_size = tmp[1].to_i

          # エラーチェック
          if (array_total_size.nil? || array_total_size == 0)
            err("[ERROR] <解析エラー> 配列サイズが 0 または NULLです。フォーマット('#{line}')")
            return -1
          end
          
          #15156 動的バッファ対応
          if dynamic_change == 1
            array_total_size = dynamic_size
          end

          # 配列数と全体サイズから単体のサイズを求める
          array_each_size = array_total_size / array_num
          
          # エラーチェック
          if (array_each_size == 0)
            err("[ERROR] <解析エラー> 配列サイズが 0 または NULL です。フォーマット('#{line}')")
            return -1
          end
          
          # char 型 (1byte)なら連続で出力
          if (array_each_size == 1)
            # 格納
            array_name << "[#{array_total_size}]"
            data = getc_param(ud_ac_log, array_total_size)
            data = conv_endian(data, array_type, array_total_size, array_name)  #2013.10.11 Kodama
            set_param(array_name, data)
          else
            # 配列名を一時退避
            array_name_tmp = String.new(array_name)

            array_no = 0
            while array_no < array_num  do
              
              # 退避しておいた配列名を代入
              array_name = String.new(array_name_tmp)

              # 格納
              array_name << "[#{array_no}]"
              array_no += 1
              data = getc_param(ud_ac_log, array_each_size)
              data = conv_endian(data, array_type, array_each_size, array_name) #2013.10.11 Kodama
              set_param(array_name, data)
            end
          end
          array_name = ""
          array_num  = 0
        end
      #========================================#
      # 構造体記号の処理 (再帰呼び出しを行う)  #
      #========================================#
      elsif line =~ /\{/

        # 現在のファイルオフセットを保存
        return_pos = f.pos
        
        #====================#
        # 1次元配列の構造体  #
        #====================#
        if (array_num2 == 0)
          # 構造体の配列数だけ繰り返す
          array_no = 0
          while array_num > array_no do
        
            # 保存しておいたオフセットへ戻る
            f.pos = return_pos
        
            dbg("  struct #{array_name}[#{array_no}]")
          
            # 構造体内は再帰呼び出しで解析する
            ret = analyze_tlv_fmt(f, ud_ac_log, dynamic_check, dynamic_size, tlv_name)
            if (ret == -1)
              return -1
            end
            array_no += 1
          end
          #====================#
          # 2次元配列の構造体  #
          #====================#
        else
          # 構造体の配列数だけ繰り返す
          array_no = 0
          while array_num > array_no do
            array_no2 = 0
            while array_num2 > array_no2 do
              # 保存しておいたオフセットへ戻る
              f.pos = return_pos

              dbg("  struct #{array_name}[#{array_no}][#{array_no2}]")

              # 構造体内は再帰で解析
              ret = analyze_tlv_fmt(f, ud_ac_log, dynamic_check, dynamic_size, tlv_name)
              if (ret == -1)
                log_write(sprintf("AC_log_Analyze_Cls::analyze_tlv_fmt(Ret Error) End\n"), 2)
                return -1
              end
              array_no2 += 1
            end  # while array_num2
            array_no += 1
          end # while array_num
          array_num  = 0
          array_num2 = 0
        end # if (array_num2 == 0)
        
      #========================================#
      # 構造体の終端処理                       #
      #========================================#
      elsif line =~ /\}/
        log_write(sprintf("AC_log_Analyze_Cls::analyze_tlv_fmt End\n"), 2)
        return 0

      #========================================#
      # 普通の変数                             #
      #========================================#
      elsif line =~ /=/
          # 2013.10.11 Kodama
          tmp = line.split(/,|=/)
          var_type = tmp[0]
          var_name = tmp[1]
          var_size = tmp[2].to_i

          # エラーチェック
          # 2013.10.11 Kodama
          if !(var_type =~ /char|short|long|int|struct/)
            err("[ERROR] <解析エラー> 不明な型名です。フォーマット('#{line}')")
            return -1
          end
          if (var_name.nil?)
            err("[ERROR] <解析エラー> 変数名が取得できませんでした。 フォーマット('#{line}')")
            return -1
          end
          if (var_size.nil? || var_size == 0)
            err("[ERROR] <解析エラー> 変数サイズが正しく取得できませんでした。フォーマット('#{line}')")
            return -1
          end

          # 格納
          data = getc_param(ud_ac_log, var_size)
          #15156 動的バッファ対応
          if (dynamic_check == 1 && var_name =~ /size/)
            dynamic_size = data.hex
          end
          data = conv_endian(data, var_type, var_size, var_name)  #2013.10.11 Kodama
          set_param(var_name, data)
      end
    end

    log_write(sprintf("AC_log_Analyze_Cls::analyze_tlv_fmt End\n"), 2)
    return 0
    
  end
  #----------------------------------------------------------#
  #  CSVへ出力する配列へバイナリ値を文字列として格納         #
  #----------------------------------------------------------#
  def set_param(name, data)
    log_write(sprintf("AC_log_Analyze_Cls::set_param Start\n"), 2)

    # 指定フォーマットで格納
    $g_param_data << "#{name}%data=#{data}"
    dbg_last_param("Data")

    log_write(sprintf("AC_log_Analyze_Cls::set_param End\n"), 2)

  end
  #-----------------------------------------------------------#
  # ファイルやパラメタ合計サイズを超えて読み込まないかを確認  #
  #-----------------------------------------------------------#
  def param_size_check(bin, size)
    log_write(sprintf("AC_log_Analyze_Cls::param_size_check Start\n"), 2)

    # ファイルサイズを超えて無いかチェック。
    if (bin.pos + size > bin.size)
      log_write(sprintf("file size error\n"))
      log_write(sprintf("bin.pos = %d, size = %d, bin.size = %d\n", bin.pos, size, bin.size))
      err("[ERROR] ファイルサイズを超えて読み込みを行おうとしました。")
      bin.pos = bin.size
      return -1
    end

    # パラメタ解析中の処理
    if ($in_param_analyze == 1)

      # パラメタ合計サイズからこれから読み込むサイズを引く。
      $total_param_size -= size
      
      # パラメタ合計サイズを超えて読み込もうとしてないか？
      if ($total_param_size < 0)
        log_write(sprintf("paramater size error\n"))
        log_write(sprintf("$total_param_size = %d\n", $total_param_size))
        err("[ERROR] パラメタの合計サイズ以上にデータ読み込みが発生しました。")
        return -1
      end
    end

    log_write(sprintf("AC_log_Analyze_Cls::param_size_check End\n"),2 )
    return 0
  end
  #----------------------------------------------------------#
  #  パラメータの型名からエンディアン変換                    #
  #  2013.10.11 追加 Kodama                                  #
  #----------------------------------------------------------#
  def conv_endian(data, type, size, name)
    log_write(sprintf("AC_log_Analyze_Cls::conv_endian Start\n"), 2)

    convd = ""

    # シミュレータ環境のログの場合はエンディアン変換を実施
    if ($g_endian == LITTLE_ENDIAN)
      # 型名 "char" 以外は変換
      if (type =~ /int|long/)
        (0..size-4).step(4) do |i|
          convd = convd + data[i+6,2] + data[i+4,2] + data[i+2,2] + data[i,2]
        end
      elsif (type =~ /short/)
        (0..size-2).step(2) do |i|
          convd = convd + data[i+2,2] + data[i,2]
        end
      else  # charの場合は変換なし
        convd = data
      end
    else # 実機環境のログの場合は変換なし
      convd = data
    end

    dbg("  [Endian変換] 型名=#{type} 変数名=#{name} サイズ=#{size} データ=#{data}->#{convd}")

    log_write(sprintf("AC_log_Analyze_Cls::conv_endian End\n"), 2)
    return convd

  end
  #----------------------------------------------------------#
  #  サイズを判断し、バイナリから文字列として取得            #
  #----------------------------------------------------------#
  def getc_param(bin, size)
    log_write(sprintf("AC_log_Analyze_Cls::getc_param Start\n"), 2)

    # ファイルやパラメタ合計サイズを超えて読み込まないか？
    ret = param_size_check(bin, size)
    if (ret < 0)
      log_write(sprintf("AC_log_Analyze_Cls::getc_param param_size_check error\n"), 0)
      return "[SIZE OVER]"
    end

    # 文字列として読み込み。
    val = bin.read(size).unpack("H*")

    log_write(sprintf("AC_log_Analyze_Cls::getc_param End\n"), 2)
    return val[0]

  end

  #----------------------------------------------------------#
  #  サイズを判断し、バイナリからデータ取得                  #
  #----------------------------------------------------------#
  def get_param(bin, size)
    log_write(sprintf("AC_log_Analyze_Cls::get_param Start\n"), 2)

    # ファイルやパラメタ合計サイズを超えて読み込まないか？
    ret = param_size_check(bin, size)
    if (ret < 0)
      log_write(sprintf("AC_log_Analyze_Cls::get_param param_size_check error\n"), 0)
      return -1
    end

    # エンディアンの違いで unpack 方法を変更
    if ($g_endian == BIG_ENDIAN)
      #---------------------------------#
      # BIG_ENDIAN (実機で作成したログ) #
      #---------------------------------#
      if (size == 1)
        val = bin.read(1).unpack("C*")
      elsif (size == 2)
        val = bin.read(2).unpack("n*")
      elsif (size == 4)
        val = bin.read(4).unpack("N*")
      else
        val = bin.read(size).unpack("N*")
      end
    else
      #--------------------------------------------#
      # LITTLE_ENDIAN (シミュレータで作成したログ) #
      #--------------------------------------------#
      if (size == 1)
        val = bin.read(1).unpack("C*")
      elsif (size == 2)
        val = bin.read(2).unpack("v*")
      elsif (size == 4)
        val = bin.read(4).unpack("V*")
      else
        val = bin.read(size).unpack("V*")
      end
    end

    log_write(sprintf("AC_log_Analyze_Cls::get_param End\n"), 2)
    return val[0]

  end

  #----------------------------------------------------------#
  #  データフォーマット呼び出し ＆解析ルーチン呼び出し       #
  #----------------------------------------------------------#
  def get_tlv_paramater(tlv_name, ud_ac_log)
    log_write(sprintf("AC_log_Analyze_Cls::get_tlv_paramater Start\n"), 2)

    ret = 0

    # ACバージョンディレクトリ取得
    ac_ver_dir = $ver_cmbx

    # フォーマットファイル名の設定
    fmtfile_name = $D_ACTBL_PATH+"/"+ac_ver_dir+"/"+tlv_name

    # フォーマットファイルの存在確認
    if (!FileTest.exist?(fmtfile_name))
      # 存在しなければ終了
      err("[ERROR]「#{tlv_name}」の解析フォーマットファイルがありません。")
      return -1
    end

    #15156 動的バッファ対応
    dynamic_check = 0
    dynamic_size = 0
    dynamic_ret = 0
    dynamic_ret = dynamic_judge(tlv_name)
    if (dynamic_ret == 1)
      dynamic_check = 1
    end
    # 存在すればフォーマットファイルをOPENして解析
    File.open(fmtfile_name, "r") do |f|
      dbg("  [FMT] #{tlv_name} analyze")
      #15156 動的バッファ対応(引数dynamic_check, dynamic_size, tlv_name追加)
      ret = analyze_tlv_fmt(f, ud_ac_log, dynamic_check, dynamic_size, tlv_name)
    end
    
    # 解析中にエラーが発生した場合
    if (ret != 0)
      err("[ERROR] <解析エラー>「#{tlv_name}」の解析フォーマットファイルに問題があります。")
      return -1
    end

    log_write(sprintf("AC_log_Analyze_Cls::get_tlv_paramater End\n"), 2)
    return ret

  end
  #----------------------------------------------------------#
  #  ファイルからIDにマッチする文字列を返す                  #
  #----------------------------------------------------------#
  def get_id_match_string(id, file)
    log_write(sprintf("AC_log_Analyze_Cls::get_id_match_string Start\n"), 2)

    # ACバージョンディレクトリ取得
    ac_ver_dir = $ver_cmbx

    # 変換テーブル名
    tblfile_name = $D_ACTBL_PATH+"/"+ac_ver_dir+"/"+file

    # 変換テーブル存在確認
    if (!FileTest.exist?(tblfile_name))
      # 存在しなければ終了
      err("[ERROR] #{file} ファイルが存在しません。")
      return -1
    end

    File.open(tblfile_name, "r") do |f|
      while line = f.gets
      
        # "#" が存在したらコメント行と判断する。
        if line =~ /#/
          next
        end
        
        data = line.split(/\s+/)
        if (id == data[1].hex)
          log_write(sprintf("AC_log_Analyze_Cls::get_id_match_string End\n"), 2)
          return data[0]
        end
      end
    end

    log_write(sprintf("AC_log_Analyze_Cls::get_id_match_string End\n"), 2)
    return "NOT_FOUND"
  end
  #----------------------------------------------------------#
  #  Card 名取得                                             #
  #----------------------------------------------------------#
  def get_card_name(id)
    log_write(sprintf("AC_log_Analyze_Cls::get_card_name Start\n"), 2)

    ret = get_id_match_string(id, CARD_TBL)
    
    if (ret == "NOT_FOUND")
      err("[ERROR] Card ID「0x#{id.to_s(16)}」に対応する Card 名が #{CARD_TBL} にありません。")
    end

    log_write(sprintf("AC_log_Analyze_Cls::get_card_name End\n"), 2)
    return ret
  end
  #----------------------------------------------------------#
  #  ActionCode 名取得                                       #
  #----------------------------------------------------------#
  def get_ac_name(id)
    log_write(sprintf("AC_log_Analyze_Cls::get_ac_name Start\n"), 2)

    ret = get_id_match_string(id, ACID_TBL)
    
    if (ret == "NOT_FOUND")
      err("[ERROR] ActionCode ID「0x#{id.to_s(16)}」に対応する ActionCode 名が #{ACID_TBL} にありません。")
    end

    log_write(sprintf("AC_log_Analyze_Cls::get_ac_name End\n"), 2)
    return ret
  end
  #----------------------------------------------------------#
  #  TLV Type 名取得                                         #
  #----------------------------------------------------------#
  def get_tlv_name(id)
    log_write(sprintf("AC_log_Analyze_Cls::get_tlv_name Start\n"), 2)

    ret = get_id_match_string(id, TLV_TBL)
    
    if (ret == "NOT_FOUND")
      err("[ERROR] TLV type「0x#{id.to_s(16)}」に対応する TVL 名が #{TLV_TBL} にありません。")
    end

    log_write(sprintf("AC_log_Analyze_Cls::get_tlv_name End\n"), 2)
    return ret
  end

  #----------------------------------------------------------#
  #  ac_tbl/ActionCode一覧＆詳細_X.XX版.gz解凍関数 #14973    #
  #----------------------------------------------------------#
  def tar_expand(dirname)
    log_write(sprintf("AC_log_Analyze_Cls::tar_expand Start\n"))
    wsh = WIN32OLE.new('WScript.Shell')

    # 取得AC定義のパス設定
    dir_tar = $D_ACTBL_PATH + "/" + dirname
    log_write(sprintf("AC_log_Analyze_Cls::tar_expand dir_tar path = %s\n", dir_tar))

    #展開ファイルのパス
    archive_path = "#{dirname}.gz"

    # カレントディレクトリを保存
    current_dir = Dir::pwd

    # カレントディレクトリを変更
    log_write(sprintf("dir_tar = %s\n", dir_tar))
    unless FileTest.exist?(dir_tar)
      msg = sprintf("異常終了")
      msg += sprintf("%s", dir_tar)
      msg += sprintf("上記フォルダが見つかりません。")
      wsh.Popup(msg,0,"ERROR")
      log_write(sprintf("not found => %s\n", dir_tar))
      return 1
    end

    Dir::chdir(File.expand_path(dir_tar))
    log_write(sprintf("chk root\n"))

    # ac_tbl内にActionCode一覧＆詳細_X.XX版.gzが存在するかチェック
    if (!FileTest.exist?(archive_path))
      # 無ければダイアログを表示して終了
      msg = "ac_tbl内に" + dirname + ".gzが存在しません。\n" +
            "TECHINFO解析ツール(ver.4.1)以上の定義データ生成ツールを使用して\n" +
            "作成してください。"
      wsh.Popup(msg,0,"ERROR")

      # カレントディレクトリを復元
      Dir::chdir(current_dir)

      log_write(sprintf("AC_log_Analyze_Cls::tar_expand(ERROR) End\n"))
      return 1
    end

    puts "解凍: ac_tbl/" + dirname

    Zlib::GzipReader.open(archive_path) do |gz|
      Archive::Tar::Minitar::unpack(gz, './')
    end

    # カレントディレクトリを復元
    Dir::chdir(current_dir)

    log_write(sprintf("AC_log_Analyze_Cls::tar_expand End\n"))
    return 0
  end

  #----------------------------------------------------------#
  #  ac_tbl/D_XXXXファイル削除関数 #14973                    #
  #----------------------------------------------------------#
  def tar_delete(dirname)
    log_write(sprintf("AC_log_Analyze_Cls::tar_delete Start\n"))

    # 取得AC定義のパス設定
    dir_tar = $D_ACTBL_PATH + "/" + dirname
    log_write(sprintf("AC_log_Analyze_Cls::tar_delete dir_tar = %s\n", dir_tar))

    # カレントディレクトリを保存
    current_dir = Dir::pwd

    # カレントディレクトリを変更
    log_write(sprintf("AC_log_Analyze_Cls::tar_delete 0\n"), 2)
    log_write(sprintf("dir_tar = %s\n", dir_tar), 2)
    printf("%s\n", dir_tar)
    printf("%s\n", File.expand_path(dir_tar))
    puts dir_tar.encoding
    Dir::chdir(File.expand_path(dir_tar))
    puts dir_tar.encoding
    log_write(sprintf("AC_log_Analyze_Cls::tar_delete 1\n"), 2)

    Dir.glob('./D_*') do |file|
      File.delete(file)
    end

    # カレントディレクトリを復元
    log_write(sprintf("AC_log_Analyze_Cls::tar_delete 2\n"), 2)
    Dir::chdir(current_dir)

    log_write(sprintf("AC_log_Analyze_Cls::tar_delete End\n"))
  end

  #----------------------------------------------------------#
  #  動的バッファの変数があるTLVか判定する関数 #15156        #
  #----------------------------------------------------------#
  def dynamic_judge(tlv_name)
    log_write(sprintf("AC_log_Analyze_Cls::dynamic_judge Start\n"), 2)
    ret = 0

    case tlv_name
    when "D_PULS_TLV_OAM_SND_LBM"
      ret = 1
    when "D_PULS_TLV_OAM_SND_LBR"
      ret = 1
    when "D_PULS_TLV_MTO_SND_LBM"
      ret = 1
    when "D_PULS_TLV_MTO_SND_LBM_R1_2"
      ret = 1
    when "D_PULS_TLV_MTO_SND_LBR"
      ret = 1
    when "D_PULS_TLV_MTO_SND_LBR_R1_2"
      ret = 1
    when "D_PULS_TLV_MTO_SND_DMM"
      ret = 1
    when "D_PULS_TLV_MTO_SND_DMM_R1_2"
      ret = 1
    when "D_PULS_TLV_MTO_SND_LMM"
      ret = 1
    when "D_PULS_TLV_MTO_SND_LMM_R1_2"
      ret = 1
    else
      ret = 0
    end
    log_write(sprintf("AC_log_Analyze_Cls::dynamic_judge End\n"), 2)
    return ret
  end

end

#-----------------------------------------------------------------#
# set_aclog_path
#-----------------------------------------------------------------#
def set_aclog_path(techinfo, logobj)
  logobj.write(sprintf("set_ac_path Start\n"), 0, File.basename(__FILE__), __LINE__)
  outputdir = File::basename(techinfo.encode(Encoding::Windows_31J), '.*')

  # AC解析結果格納フォルダ作成（TECHINFOのファイル名をフォルダ名にする）
  ac_path = $D_RUN_PATH + "/" + outputdir

  logobj.write(sprintf("ac_path = %s\n", ac_path), 0, File.basename(__FILE__), __LINE__)
  FileUtils.mkdir_p(ac_path) unless FileTest.exist?(ac_path)

  # フォルダ配下の全ファイルを削除
  Dir::foreach(ac_path) {|f|
    File::delete(ac_path + "/" + f) if ! (/\.+$/ =~ f)
  }

  logobj.write(sprintf("set_ac_path End\n"), 0, File.basename(__FILE__), __LINE__)
  return ac_path

end
