#coding: windows-31j
#/****************************************************************************
# *  fileName: TECHINFO_Almlog_Analysis.rb
# *
# *               All rights reserved, Copyright (C) FUJITSU LIMITED 2016-2018
# *-----------------------------------------------------------------------------
# *  処理概要: 警報ログ解析
# *            trap&audit/構成情報/version情報/FLT/警報変化点/softエラーなど
# *  引数    : なし
# *  復帰値  : なし
# *---------------------------------------------------------------------------
# *  変更履歴: 2016.11.24 katou     新規作成(Version 1.0)
# *                                 L2の解析ツールをベースとする。
# *            2017.05.19 kinomoto  機能追加(Version 1.1)
# *                                 (1) CUI対応
# *                                 (2) 被疑部品検索機能追加
# *            2017.08.18 kinomoto  温度Over検出機能追加 (Version 1.2)
# * -Version 1.3 -
# *            2018.02.22 kinomoto  機能追加: NBO_PIU_flt_X.log の解析追加
# *            2018.02.26 kinomoto  バグ修正: ruby の delete関数の誤使用
# *                                           FLTログのループデータが正常に読み込めていない
# * -Version 1.3.1 -
# *     2018.06.13 kinomoto  問題対応: "警報変化点ログ解析"シート整形中に実行エラー。
# *            [詳細]
# *             cond_id(警報種別)を文字列化するため、〜cm_name.LOG ファイル内にある
# *            「##### ctneqpt-alm CIR #####」を検索しているが、該当する行が存在しない。
# *             検索ＮＧを想定していないので、後続の処理で実行エラーが発生する。
# *                     ※2014年12月31日の feqp_cm_alm.LOGで発生
# * -Version 1.4 -
# *     2018.10.29 kinomoto  仕様変更: "FLT解析"で参照するFLTテーブルのシートについて
# *                                    C列の値が"D"(半角または全角)の行は無視する
# *            ※「☆S100_故障部品特定ツール(部品対応表)_4.00版_180920.xlsx」対応
# *
# *     2018.10.31 kinomoto  機能追加: --flt_tab_check オプション
# *     2018.11.02 kinomoto  改善    : ログの出力情報追加
# * -Version 1.5 -
# *     2020.12.11 Nishi  機能追加: S150対応
# *                       バグ修正: 警報変化点ログ解析で出力された結果シートの
# *                                 Cond_idの列が一部正しい内容になっていない問題を対処
# * 
# ****************************************************************************/

#----------------------------------------------------------#
# クラス
#----------------------------------------------------------#
class Almlog_Analyze_Cls < Common_Analyze_Cls
    
    # FLTtable の形式
    FLT_TBL_HEAD_ROW = 4        # 見出し行
    FLT_TBL_DATA_ROW = 5        # データ開始行
    FLT_TBL_REPAIR_SIZE = 4     # 被疑部品単位の列数
    FLT_TBL_REPAIR_PAD = 1      # 被疑部品単位間隔(列数)
    
    PARTS_TBL_HEAD_ROW = 3      # 見出し行
    PARTS_TBL_DATA_ROW = 4      # データ開始行
    PARTS_TBL_DATA_COLUMN = 2   # "部分変換表"シートの「Parts#」列
    
    attr_writer :Visible
    
    #----------------------------------------------------------#
    # コンストラクタ
    #----------------------------------------------------------#
    def initialize(in_label) 
        @logobj = Log_class.instance
        @logobj.write("Almlog_Analyze_Cls::initialize Start\n")
        
        @label = in_label
        
        #17920対応 noda
        # 親クラス(Common_Analyze_Cls)のコンストラクタコール
        super()
        
        @logobj = Log_class.instance
        @logobj.write("Almlog_Analyze_Cls::initialize End\n")
        
        @stschgchk_count = 0    #警報変化点log数
        @flt_chk_cnt = 0        #FLTlog数
        @unit_info_chk_cnt = 0  #UnitInfoログ数
        
        @Visible = false

        # S150対応 Add Start 2020/12/11 by Nishi
        # 解析の状態チェック用変数
        @StatusCheckAnalyze = 0
        # オプションの解析の結果チェック用変数
        @CheckResult = 0
        # S150対応 Add End 2020/12/11 by Nishi

    end #initialize
    
    #----------------------------------------------------------#
    #  Excelファイルの作成                                     #
    #----------------------------------------------------------#
    def createExcelWorkBook(files)
        @logobj.write("Almlog_Analyze_Cls::createExcelWorkBook Start\n")
        puts "[debug] createExcelWorkBook: Start" #debug
        
        @xl = WIN32OLE.new('Excel.Application')
        fso = WIN32OLE.new('Scripting.FileSystemObject')
        
        begin
            filename = files
            filename.gsub!("\\","\/")
            renamefilename = File.basename(filename, ".*")
            renamefilename = File.basename(renamefilename, ".*")
            renamedirname  = File.dirname(filename)
            
            # S150対応 Change Start 2020/12/11 by Nishi
            # Typeオプションが"S100"のときはログファイル名_S100_Alm.xlsxで出力する
            if ($g_machine_type == "S100")
                renamefilename2 = renamedirname + "/" + renamefilename + "_S100_Alm.xlsx"
                # S150対応 Add Start 2020/12/25 by Nishi
                popup_msg =  sprintf("%s_S100_Alm.xlsx\n",renamefilename.to_s)
                popup_msg << sprintf("を削除後に再作成しますか？")
                # S150対応 Add End 2020/12/25 by Nishi
            # Typeオプションが"S150"のときはログファイル名_S150_Alm.xlsxで出力する  
            elsif ($g_machine_type == "S150")
                renamefilename2 = renamedirname + "/" + renamefilename + "_S150_Alm.xlsx"
                # S150対応 Add Start 2020/12/25 by Nishi
                popup_msg =  sprintf("%s_S150_Alm.xlsx\n",renamefilename.to_s)
                popup_msg << sprintf("を削除後に再作成しますか？")
                # S150対応 Add End 2020/12/25 by Nishi
            end
            # S150対応 Change End 2020/12/11 by Nishi
            
            puts "Input Base Name: #{renamefilename}"
            puts "Output Path: #{renamefilename2}"
            puts "[debug] createExcelWorkBook: WorkBook creating..."
            
            @logobj.write("Almlog_Analyze_Cls::createExcelWorkBook: filename=#{renamefilename2}\n")

            # S150対応 Add Start 2020/12/25 by Nishi
            # 既にファイルが存在する時、ファイルを再作成するか確認する
            if File.exist?(renamefilename2)
                wsh = WIN32OLE.new('WScript.Shell')
                if wsh.Popup(popup_msg,0,"確認",4).to_i == 6
                    FileUtils.rm(renamefilename2.to_s)
                    puts "[debug] Delete filename: #{renamefilename2}"
                    @logobj.write("Almlog_Analyze_Cls::createExcelWorkBook: Delete filename=#{renamefilename2}\n")
                else
                    @xl.quitS
                    return nil
                end
            end
            # S150対応 Add End 2020/12/25 by Nishi

            @xl.SheetsInNewWorkbook = 1                   # 新bookのsheetsを1枚に設定
            book = @xl.Workbooks.Add
            book.SaveAs(fso.GetAbsolutePathName(renamefilename2))   #ファイルを保存

            if @Visible
                @xl.Visible = true        
                @xl.screenUpdating = true
            end
            
            return book
        # 異常系処理
        rescue
            printf("[debug] rescue at %s:%d\n", File.basename(__FILE__), __LINE__)
            

            # S150対応 Change Start 2020/12/25 by Nishi
            # S150対応 Delete Start 2020/12/11 by Nishi
            #wsh = WIN32OLE.new('WScript.Shell')
            #wsh.Popup("異常終了 出力すべきファイルがExcelでOpenされています\n閉じて実行して下さい。",0, "終了")
            # S150対応 Delete End 2020/12/11 by Nishi
            # S150対応 Change End 2020/12/25 by Nishi
            
            if book != nil
                puts "[debug] createExcelWorkBook: WorkBook Closing..." 
                book.close
            end
            
            @xl.quit
            
            return nil
        # 後始末
        ensure
            puts "[debug] createExcelWorkBook: End"
            
            # 記録
            if book == nil 
                return nil
            end
            @logobj.write("Almlog_Analyze_Cls::createExcelWorkBook End\n")
        end
    end #createExcelWorkBook
    
    #----------------------------------------------------------#
    #  Excelファイルの終了                                     #
    #----------------------------------------------------------#
    def closeExcelWorkbook(book, save_only = true)
        @logobj.write("Almlog_Analyze_Cls::closeExcelWorkbook Start\n")
        
        puts "[debug] closeExcelWorkbook"
        
        # Excelを表示
        if @Visible or save_only
            @xl.screenUpdating = true
            @xl.Visible = true
        end
        puts book
        book.save
        if not save_only
            book.close
            if @xl.Workbooks.Count == 0
                @xl.quit
            end
        end
        @label.text = "TECHINFO.TGZをdrop"
        
        @logobj.write("Almlog_Analyze_Cls::closeExcelWorkbook End\n")
    end #closeExcelWorkbook
    
    def screenUpdate()
        if @Visible
            @xl.screenUpdating = true
            @xl.screenUpdating = false
        end
    end
    
    #----------------------------------------------------------#
    # excelset                                                 #
    #----------------------------------------------------------#
    def excelset(fname, book)
        @logobj.write("Almlog_Analyze_Cls::excelset Start\n")
        
        # S150対応 Add Start 2020/12/11 by Nishi
        # 解析の状態チェック用変数を初期化する
        @StatusCheckAnalyze = 0
        # オプションの解析の結果チェック用変数を初期化する
        @CheckResult = 0
        # S150対応 Add End 2020/12/11 by Nishi

        #解析対象ログ数チェック処理
        begin
            wsh = WIN32OLE.new('WScript.Shell')
            puts "[debug] excelset" #debug

            # S150対応 Add Start 2020/12/11 by Nishi
            # 処理の状態をTrap解析にする
            @StatusCheckAnalyze = PROC_STATUS_TRAP_INFO
            @logobj.write("Almlog_Analyze_Cls::excelset TrapAnalyze\n")
            # S150対応 Add End 2020/12/11 by Nishi
            
            #Trap情報解析
            chk_log_max_set(2,"Trap&auditログ")
            sleep 0.3
            screenUpdate
            
            #エクセルに出力
            trap_analyze(fname,book)
            
            #エクセルから最新状態を割り出す
            # S150対応 Change Start 2020/12/11 by Nishi
            #S100のとき
            if ($g_machine_type == "S100")
                @logobj.write("Almlog_Analyze_Cls::excelset Proc:trap_analyze_from_xls_s100\n")
                trap_analyze_from_xls_s100(book)
             #S150のとき
            elsif ($g_machine_type == "S150")
                @logobj.write("Almlog_Analyze_Cls::excelset Proc:trap_analyze_from_xls_s150\n")
                trap_analyze_from_xls_s150(book)
            end
            # S150対応 Change End 2020/12/11 by Nishi                       
            update_progress()
            sleep 0.001
            screenUpdate

            # S150対応 Add Start 2020/12/11 by Nishi
            # 処理の状態をAudit解析にする
            @StatusCheckAnalyze = PROC_STATUS_AUDIT_INFO
            @logobj.write("Almlog_Analyze_Cls::excelset AuditAnalyze\n")
            # S150対応 Add End 2020/12/11 by Nishi
            
            #audit情報解析
            #エクセルに出力
            audit_analyze(fname,book)
            update_progress()
            sleep 0.3
            screenUpdate
            
            # S150対応 Add Start 2020/12/11 by Nishi
            # 処理の状態を構成情報取得にする
            @StatusCheckAnalyze = PROC_STATUS_CONFIG_INFO
            @logobj.write("Almlog_Analyze_Cls::excelset ConfigInfoAnalyze\n")
            # S150対応 Add End 2020/12/11 by Nishi
            
            chk_log_max_set(1,"構成情報")
            sleep 0.3
            screenUpdate
            
            #構成情報
            # S150対応 Change Start 2020/12/11 by Nishi
            #S100のとき
            if ($g_machine_type == "S100")
                @logobj.write("Almlog_Analyze_Cls::excelset Proc:config_analyze_s100\n")
                config_analyze_s100(fname,book)
            #S150のとき
            elsif ($g_machine_type == "S150")
                @logobj.write("Almlog_Analyze_Cls::excelset Proc:config_analyze_s150\n")
                config_analyze_s150(fname,book)
            end
            # S150対応 Change End 2020/12/11 by Nishi
            update_progress()
            sleep 0.3
            screenUpdate
 
            # S150対応 Add Start 2020/12/11 by Nishi
            # 処理の状態をVersion情報取得にする
            @StatusCheckAnalyze = PROC_STATUS_VERSION_INFO
            @logobj.write("Almlog_Analyze_Cls::excelset VersionInfoAnalyzes\n")
            # S150対応 Add End 2020/12/11 by Nishi
 
            
            #UNIT INFO解析
            chk_log_max_set(1,"Version情報")
            sleep 0.3
            screenUpdate

            # S150対応 Change Start 2020/12/11 by Nishi
            #S100のとき
            if ($g_machine_type == "S100")
                @logobj.write("Almlog_Analyze_Cls::excelset Proc:unitinfo_analyze_s100\n")
                unitinfo_analyze_s100(fname,book)
            #S150のとき
            elsif ($g_machine_type == "S150")
                @logobj.write("Almlog_Analyze_Cls::excelset Proc:unitinfo_analyze_s150\n")
                unitinfo_analyze_s150(fname,book)
            end
            update_progress()
            sleep 0.3
            screenUpdate
            
            #option系機能
            # S150対応 Change Start 2020/12/11 by Nishi
            @CheckResult = select_function(fname,book)
            if @CheckResult == ERR_CHECK_FLT
                return ERR_CHECK_FLT
            elsif @CheckResult == ERR_CHECK_FFANLOG
                return ERR_CHECK_FFANLOG
            end
            # S150対応 Change End 2020/12/11 by Nishi
            
            # プログレスバーを100%で表示させるための待ち時間
            sleep 0.3
            screenUpdate
            
            # 解析終了
            if $exec_CUI_mode == false
                popup_msg =  sprintf("解析完了\n")
                wsh.Popup(popup_msg, 0, "正常通知")
            end
        rescue => e
            printf("[debug] rescue at %s:%d\n", File.basename(__FILE__), __LINE__)
            if @Visible
                @xl.screenUpdating = true
                @xl.Visible = true
            end
            printf("excelset error\n")
            @logobj.write(sprintf("[ERROR]%s\n", $!), 0, File.basename(__FILE__), __LINE__)
            #p $!
            e.backtrace.each{|errdata|
                @logobj.write(sprintf("[ERROR]%s\n", errdata), 0, File.basename(__FILE__), __LINE__)
                p errdata
            }
            
            # S150対応 Add Start 2020/12/11 by Nishi
            if @StatusCheckAnalyze == PROC_STATUS_TRAP_INFO
                @logobj.write("Almlog_Analyze_Cls::excelset TrapAnalyze:NG\n")
                return ERR_CHECK_TRAP_INFO
            elsif @StatusCheckAnalyze == PROC_STATUS_AUDIT_INFO
                @logobj.write("Almlog_Analyze_Cls::excelset AuditAnalyze:NG\n")
                return ERR_CHECK_AUDIT_INFO
            elsif @StatusCheckAnalyze == PROC_STATUS_CONFIG_INFO
                @logobj.write("Almlog_Analyze_Cls::excelset ConfigInfoAnalyze:NG\n")
                return ERR_CHECK_CONFIG_INFO
            else
                @logobj.write("Almlog_Analyze_Cls::excelset VersionInfoAnalyze:NG\n")
                return ERR_CHECK_VERSION_INFO
            end
            # S150対応 Add End 2020/12/11 by Nishi
            
        end
        @logobj.write("Almlog_Analyze_Cls::excelset End\n")
        # S150対応 Add Start 2020/12/11 by Nishi
        return ERR_CHECK_OK
        # S150対応 Add End 2020/12/11 by Nishi
    end #excelset
    
    # S150対応 Change Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # config_analyze
    # 概要：装置構成情報解析(S100向け）
    #       以下の情報から装置構成情報を割り出す
    #       PI >> 各parts(FAN/PIU/Port)の名前
    #       Trap >>各part(FAN/PIU/Port)の警報状態(FLT/RMVD/MEA)
    #----------------------------------------------------------#
    def config_analyze_s100(path,book)
    # S150対応 Change End 2020/12/11 by Nishi
    
        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("Almlog_Analyze_Cls::config_analyze_s100 Start\n")
        # S150対応 Add End 2020/12/11 by Nishi
        sheets = book.sheets(1)
        sheets.Name = "構成情報"                      # シート名を設定
        
        sheets.Cells(1, 1).Value = "種類"
        sheets.Cells(1, 2).Value = "名前1"
        sheets.Cells(1, 3).Value = "名前2"
        sheets.Cells(1, 4).Value = "状態"
        sheets.Cells(2, 4).Value = "FLT"
        sheets.Cells(2, 5).Value = "RMVD"
        sheets.Cells(2, 6).Value = "MEA"
        
        sheets.Cells(3,  1).value = "BLADE"
        sheets.Cells(4,  1).value = "FAN(1)"
        sheets.Cells(5,  1).value = "FAN(2)"
        sheets.Cells(6,  1).value = "FAN(3)"
        sheets.Cells(7,  1).value = "PIU(1)"
        sheets.Cells(8,  1).value = "PIU(2)"
        sheets.Cells(9,  1).value = "PIU(3)"
        sheets.Cells(10, 1).value = "PIU(4)"
        sheets.Cells(11, 1).value = "MDL(1)(1)"
        sheets.Cells(12, 1).value = "MDL(1)(2)"
        sheets.Cells(13, 1).value = "MDL(1)(3)"
        sheets.Cells(14, 1).value = "MDL(1)(4)"
        sheets.Cells(15, 1).value = "MDL(1)(5)"
        sheets.Cells(16, 1).value = "MDL(1)(6)"
        sheets.Cells(17, 1).value = "MDL(1)(7)"
        sheets.Cells(18, 1).value = "MDL(1)(8)"
        sheets.Cells(19, 1).value = "MDL(1)(9)"
        sheets.Cells(20, 1).value = "MDL(1)(10)"
        sheets.Cells(21, 1).value = "MDL(1)(11)"
        sheets.Cells(22, 1).value = "MDL(1)(12)"
        sheets.Cells(23, 1).value = "MDL(2)(1)"
        sheets.Cells(24, 1).value = "MDL(2)(2)"
        sheets.Cells(25, 1).value = "MDL(2)(3)"
        sheets.Cells(26, 1).value = "MDL(2)(4)"
        sheets.Cells(27, 1).value = "MDL(2)(5)"
        sheets.Cells(28, 1).value = "MDL(2)(6)"
        sheets.Cells(29, 1).value = "MDL(2)(7)"
        sheets.Cells(30, 1).value = "MDL(2)(8)"
        sheets.Cells(31, 1).value = "MDL(2)(9)"
        sheets.Cells(32, 1).value = "MDL(2)(10)"
        sheets.Cells(33, 1).value = "MDL(2)(11)"
        sheets.Cells(34, 1).value = "MDL(2)(12)"
        sheets.Cells(35, 1).value = "MDL(3)(1)"
        sheets.Cells(36, 1).value = "MDL(3)(2)"
        sheets.Cells(37, 1).value = "MDL(3)(3)"
        sheets.Cells(38, 1).value = "MDL(3)(4)"
        sheets.Cells(39, 1).value = "MDL(3)(5)"
        sheets.Cells(40, 1).value = "MDL(3)(6)"
        sheets.Cells(41, 1).value = "MDL(3)(7)"
        sheets.Cells(42, 1).value = "MDL(3)(8)"
        sheets.Cells(43, 1).value = "MDL(3)(9)"
        sheets.Cells(44, 1).value = "MDL(3)(10)"
        sheets.Cells(45, 1).value = "MDL(3)(11)"
        sheets.Cells(46, 1).value = "MDL(3)(12)"
        sheets.Cells(47, 1).value = "MDL(4)(1)"
        sheets.Cells(48, 1).value = "MDL(4)(2)"
        sheets.Cells(49, 1).value = "MDL(4)(3)"
        sheets.Cells(50, 1).value = "MDL(4)(4)"
        sheets.Cells(51, 1).value = "MDL(4)(5)"
        sheets.Cells(52, 1).value = "MDL(4)(6)"
        sheets.Cells(53, 1).value = "MDL(4)(7)"
        sheets.Cells(54, 1).value = "MDL(4)(8)"
        sheets.Cells(55, 1).value = "MDL(4)(9)"
        sheets.Cells(56, 1).value = "MDL(4)(10)"
        sheets.Cells(57, 1).value = "MDL(4)(11)"
        sheets.Cells(58, 1).value = "MDL(4)(12)"
        
        #1.PI情報を取得し、刺さっているUnit名を割り出す
        excel_sheet_add("PI情報 (BLADE・FAN・PIU)",book)
        excel_sheet_add("PI情報 (MDL)",book)
        # S150対応 Change Start 2020/12/11 by Nishi
        log_check_PI_s100(path, book)
        # S150対応 Change End 2020/12/11 by Nishi
        pi_sheets = book.sheets("PI情報 (BLADE・FAN・PIU)")
        
        sheets.range(sheets.cells(3,2),sheets.cells(10,2)).value = pi_sheets.range(pi_sheets.cells(3,5),pi_sheets.cells(10,5)).value
        
        pi_sheets = book.sheets("PI情報 (MDL)")
        sheets.range(sheets.cells(11,2),sheets.cells(58,3)).value = pi_sheets.range(pi_sheets.cells(2,2),pi_sheets.cells(49,3)).value
        
        @xl.DisplayAlerts = false
        book.sheets("PI情報 (BLADE・FAN・PIU)").Delete
        book.sheets("PI情報 (MDL)").Delete
        @xl.DisplayAlerts = true
        
        #2.Trapの状態から、Unitの状態を割り出す。
        #BLADE
        if(@blade_data == "critical")
            sheets.Cells(3,  4).value = "○"
            sheets.Range(sheets.cells(3,1),sheets.cells(3,6)).Interior.ColorIndex = 3
        end
        
        #FAN
        for sl in 0...3 do
            pos = 4 + sl
            #MEA
            if(@fan_data[sl][2] == "critical")
                sheets.Cells(pos,  6).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 5
            end
            #FLT
            if(@fan_data[sl][0] == "minor") 
                sheets.Cells(pos,  4).value = "△"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,4)).Interior.ColorIndex = 6
            elsif(@fan_data[sl][0] == "critical")
                sheets.Cells(pos,  4).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 6
            end
            
            #RMVD
            if(@fan_data[sl][1] == "critical")
                sheets.Cells(pos,  5).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 3
            end
        end
        
        #PIU
        for sl in 0...4 do
            pos = 7 + sl
            #MEA
            if(@piu_data[sl][2] == "critical")
                sheets.Cells(pos,  6).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 5
            end
            #FLT
            if(@piu_data[sl][0] == "critical")
                sheets.Cells(pos,  4).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 6
            end
            #RMVD
            if(@piu_data[sl][1] == "critical")
                sheets.Cells(pos,  5).value = "○"
                sheets.Range(sheets.cells(pos,2),sheets.cells(pos,3)).value = "-"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 3
            end
        end
        
        #MDL
        for sl in 0...4 do
            for po in 0...12 do
                pos = 11 + sl*12 + po
                #MEA
                if(@mdl_data[sl][po][2] == "critical")
                    sheets.Cells(pos,  6).value = "○"
                    sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 5
                end
                #FLT
                if(@mdl_data[sl][po][0] == "critical")
                    sheets.Cells(pos,  4).value = "○"
                    sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 6
                end
                #RMVD
                if(@mdl_data[sl][po][1] == "critical")
                    sheets.Cells(pos,  5).value = "○"
                    sheets.Range(sheets.cells(pos,2),sheets.cells(pos,3)).value = "-"
                    sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 3
                end
            end
        end
        
        #セルの結合
        sheets.Range(sheets.cells(1,1), sheets.cells(2,1)).Merge
        sheets.Range(sheets.cells(1,2), sheets.cells(2,2)).Merge
        sheets.Range(sheets.cells(1,3), sheets.cells(2,3)).Merge
        sheets.Range(sheets.cells(1,4), sheets.cells(1,6)).Merge
        
        #中央に配置
        sheets.range(sheets.cells(1,4),sheets.cells(1,6)).HorizontalAlignment = -4108
        
        #罫線
        sheets.Range(sheets.cells(1,1),sheets.cells(58,6)).Borders.LineStyle = 1
        
        # セル色変更
        sheets.Range(sheets.cells(1,1),sheets.cells(2,6)).Interior. ColorIndex = 4
        
        #自動整形
        sheets.Columns.AutoFit
    
        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("Almlog_Analyze_Cls::config_analyze_s100 End\n")
        # S150対応 Add End 2020/12/11 by Nishi
    
    # S150対応 Change Start 2020/12/11 by Nishi    
    end #config_analyze_s100
    # S150対応 Change End 2020/12/11 by Nishi
    
    # S150対応 Add Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # config_analyze
    # 概要：装置構成情報解析(S150向け）
    #       以下の情報から装置構成情報を割り出す
    #       PI >> 各parts(FAN/PIU/Port)の名前
    #       Trap >>各part(FAN/PIU/Port)の警報状態(FLT/RMVD/MEA)
    #----------------------------------------------------------#
    def config_analyze_s150(path,book)
    
        @logobj.write("Almlog_Analyze_Cls::config_analyze_s150 Start\n")
        
        sheets = book.sheets(1)
        sheets.Name = "構成情報"                      # シート名を設定
        
        sheets.Cells(1, 1).Value = "種類"
        sheets.Cells(1, 2).Value = "名前1"
        sheets.Cells(1, 3).Value = "名前2"
        sheets.Cells(1, 4).Value = "状態"
        sheets.Cells(2, 4).Value = "FLT"
        sheets.Cells(2, 5).Value = "RMVD"
        sheets.Cells(2, 6).Value = "MEA"
        
        sheets.Cells(3,  1).value = "BLADE"
        # S150対応 Add Start 2020/12/24 by Nishi
        sheets.Cells(4,  1).value = "CPU"
        # S150対応 Add End 2020/12/24 by Nishi
        # S150対応 Change Start 2020/12/24 by Nishi
        sheets.Cells(5,  1).value = "FAN(1)"
        sheets.Cells(6,  1).value = "FAN(2)"
        sheets.Cells(7,  1).value = "FAN(3)"
        sheets.Cells(8,  1).value = "FAN(4)"
        sheets.Cells(9,  1).value = "FAN(5)"
        sheets.Cells(10, 1).value = "FAN(6)"
        sheets.Cells(11, 1).value = "FAN(7)"
        sheets.Cells(12, 1).value = "FAN(8)"
        sheets.Cells(13, 1).value = "PIU(1)"
        sheets.Cells(14, 1).value = "PIU(2)"
        sheets.Cells(15, 1).value = "PIU(3)"
        sheets.Cells(16, 1).value = "PSU(1)"
        sheets.Cells(17, 1).value = "PSU(2)"
        sheets.Cells(18, 1).value = "PSU(3)"
        sheets.Cells(19, 1).value = "PSU(4)"               
        sheets.Cells(20, 1).value = "MDL(1)(1)"
        sheets.Cells(21, 1).value = "MDL(1)(2)"
        sheets.Cells(22, 1).value = "MDL(1)(3)"
        sheets.Cells(23, 1).value = "MDL(1)(4)"
        sheets.Cells(24, 1).value = "MDL(1)(5)"
        sheets.Cells(25, 1).value = "MDL(1)(6)"
        sheets.Cells(26, 1).value = "MDL(1)(7)"
        sheets.Cells(27, 1).value = "MDL(1)(8)"
        sheets.Cells(28, 1).value = "MDL(1)(9)"
        sheets.Cells(29, 1).value = "MDL(1)(10)"
        sheets.Cells(30, 1).value = "MDL(1)(11)"
        sheets.Cells(31, 1).value = "MDL(1)(12)"
        sheets.Cells(32, 1).value = "MDL(1)(13)"
        sheets.Cells(33, 1).value = "MDL(1)(14)"
        sheets.Cells(34, 1).value = "MDL(1)(15)"
        sheets.Cells(35, 1).value = "MDL(1)(16)"
        sheets.Cells(36, 1).value = "MDL(1)(17)"
        sheets.Cells(37, 1).value = "MDL(1)(18)"      
        sheets.Cells(38, 1).value = "MDL(1)(19)"
        sheets.Cells(39, 1).value = "MDL(1)(20)"
        sheets.Cells(40, 1).value = "MDL(1)(21)"
        sheets.Cells(41, 1).value = "MDL(1)(22)"
        sheets.Cells(42, 1).value = "MDL(1)(23)"
        sheets.Cells(43, 1).value = "MDL(1)(24)"
        sheets.Cells(44, 1).value = "MDL(1)(25)"
        sheets.Cells(45, 1).value = "MDL(1)(26)"
        sheets.Cells(46, 1).value = "MDL(2)(1)"
        sheets.Cells(47, 1).value = "MDL(2)(2)"
        sheets.Cells(48, 1).value = "MDL(2)(3)"
        sheets.Cells(49, 1).value = "MDL(2)(4)"
        sheets.Cells(50, 1).value = "MDL(2)(5)"
        sheets.Cells(51, 1).value = "MDL(2)(6)"
        sheets.Cells(52, 1).value = "MDL(2)(7)"
        sheets.Cells(53, 1).value = "MDL(2)(8)"
        sheets.Cells(54, 1).value = "MDL(2)(9)"
        sheets.Cells(55, 1).value = "MDL(2)(10)"
        sheets.Cells(56, 1).value = "MDL(2)(11)"
        sheets.Cells(57, 1).value = "MDL(2)(12)"
        sheets.Cells(58, 1).value = "MDL(3)(1)"
        sheets.Cells(59, 1).value = "MDL(3)(2)"
        sheets.Cells(60, 1).value = "MDL(3)(3)"
        sheets.Cells(61, 1).value = "MDL(3)(4)"
        sheets.Cells(62, 1).value = "MDL(3)(5)"
        sheets.Cells(63, 1).value = "MDL(3)(6)"
        sheets.Cells(64, 1).value = "MDL(3)(7)"
        sheets.Cells(65, 1).value = "MDL(3)(8)"
        sheets.Cells(66, 1).value = "MDL(3)(9)"
        sheets.Cells(67, 1).value = "MDL(3)(10)"
        sheets.Cells(68, 1).value = "MDL(3)(11)"
        sheets.Cells(69, 1).value = "MDL(3)(12)"
        # S150対応 Change End 2020/12/24 by Nishi
        
        #1.PI情報を取得し、刺さっているUnit名を割り出す
        # S150対応 Change Start 2020/12/24 by Nishi
        excel_sheet_add("PI情報 (BLADE・CPU・FAN・PIU・PSU)",book)
        # S150対応 Change End 2020/12/24 by Nishi
        excel_sheet_add("PI情報 (MDL)",book)
        log_check_PI_s150(path, book)
        # S150対応 Change Start 2020/12/24 by Nishi
        pi_sheets = book.sheets("PI情報 (BLADE・CPU・FAN・PIU・PSU)")
        sheets.range(sheets.cells(3,2),sheets.cells(19,2)).value = pi_sheets.range(pi_sheets.cells(3,5),pi_sheets.cells(19,5)).value
        
        pi_sheets = book.sheets("PI情報 (MDL)")
        sheets.range(sheets.cells(20,2),sheets.cells(69,3)).value = pi_sheets.range(pi_sheets.cells(2,2),pi_sheets.cells(51,3)).value
        # S150対応 Change End 2020/12/24 by Nishi
        
        @xl.DisplayAlerts = false
        # S150対応 Change Start 2020/12/24 by Nishi
        book.sheets("PI情報 (BLADE・CPU・FAN・PIU・PSU)").Delete
        # S150対応 Change End 2020/12/24 by Nishi
       	book.sheets("PI情報 (MDL)").Delete
        @xl.DisplayAlerts = true
        
        #2.Trapの状態から、Unitの状態を割り出す。
        #BLADE
        if(@blade_data == "critical")
            sheets.Cells(3,  4).value = "○"
            sheets.Range(sheets.cells(3,1),sheets.cells(3,6)).Interior.ColorIndex = 3
        end
        # S150対応 Add Start 2020/12/24 by Nishi
        #CPU
        for sl in 0...1 do
            pos = 4 + sl
            #MEA
            if(@cpu_data[sl][2] == "critical")
                sheets.Cells(pos,  6).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 5
            end
            #FLT
            if(@cpu_data[sl][0] == "minor") 
                sheets.Cells(pos,  4).value = "△"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,4)).Interior.ColorIndex = 6
            elsif(@fan_data[sl][0] == "critical")
                sheets.Cells(pos,  4).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 6
            end
            
            #RMVD
            if(@cpu_data[sl][1] == "critical")
                sheets.Cells(pos,  5).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 3
            end
        end
        # S150対応 Add End 2020/12/24 by Nishi

        
        #FAN
        for sl in 0...8 do
            # S150対応 Change Start 2020/12/24 by Nishi
            pos = 5 + sl
            # S150対応 Change End 2020/12/24 by Nishi
            #MEA
            if(@fan_data[sl][2] == "critical")
                sheets.Cells(pos,  6).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 5
            end
            #FLT
            if(@fan_data[sl][0] == "minor") 
                sheets.Cells(pos,  4).value = "△"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,4)).Interior.ColorIndex = 6
            elsif(@fan_data[sl][0] == "critical")
                sheets.Cells(pos,  4).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 6
            end
            
            #RMVD
            if(@fan_data[sl][1] == "critical")
                sheets.Cells(pos,  5).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 3
            end
        end
        
        #PIU
        for sl in 0...3 do
            # S150対応 Change Start 2020/12/24 by Nishi
            pos = 	13 + sl
            # S150対応 Change End 2020/12/24 by Nishi
            #MEA
            if(@piu_data[sl][2] == "critical")
                sheets.Cells(pos,  6).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 5
            end
            #FLT
            if(@piu_data[sl][0] == "critical")
                sheets.Cells(pos,  4).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 6
            end
            #RMVD
            if(@piu_data[sl][1] == "critical")
                sheets.Cells(pos,  5).value = "○"
                sheets.Range(sheets.cells(pos,2),sheets.cells(pos,3)).value = "-"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 3
            end
        end
        
        #PSU
        for sl in 0...4 do
            # S150対応 Change Start 2020/12/24 by Nishi
            pos = 	16 + sl
            # S150対応 Change End 2020/12/24 by Nishi
            #MEA
            if(@psu_data[sl][2] == "critical")
                sheets.Cells(pos,  6).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 5
            end
            #FLT
            if(@psu_data[sl][0] == "critical")
                sheets.Cells(pos,  4).value = "○"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 6
            end
            #RMVD
            if(@psu_data[sl][1] == "critical")
                sheets.Cells(pos,  5).value = "○"
                sheets.Range(sheets.cells(pos,2),sheets.cells(pos,3)).value = "-"
                sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 3
            end
        end
        
        #MDL(1)
        for sl in 0...1 do
            for po in 0...26 do
                # S150対応 Change Start 2020/12/24 by Nishi
                pos = 20 + sl*26 + po
                # S150対応 Change End 2020/12/24 by Nishi
                #MEA
                if(@mdl_data1[sl][po][2] == "critical")
                    sheets.Cells(pos,  6).value = "○"
                    sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 5
                end
                #FLT
                if(@mdl_data1[sl][po][0] == "critical")
                    sheets.Cells(pos,  4).value = "○"
                    sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 6
                end
                #RMVD
                if(@mdl_data1[sl][po][1] == "critical")
                    sheets.Cells(pos,  5).value = "○"
                    sheets.Range(sheets.cells(pos,2),sheets.cells(pos,3)).value = "-"
                    sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 3
                end
            end
        end
        
        #MDL(2,3)
        for sl in 0...2 do
            for po in 0...12 do
                # S150対応 Change Start 2020/12/24 by Nishi
                pos = 46 + sl*12 + po
                # S150対応 Change End 2020/12/24 by Nishi
                #MEA
                if(@mdl_data2[sl][po][2] == "critical")
                    sheets.Cells(pos,  6).value = "○"
                    sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 5
                end
                #FLT
                if(@mdl_data2[sl][po][0] == "critical")
                    sheets.Cells(pos,  4).value = "○"
                    sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 6
                end
                #RMVD
                if(@mdl_data2[sl][po][1] == "critical")
                    sheets.Cells(pos,  5).value = "○"
                    sheets.Range(sheets.cells(pos,2),sheets.cells(pos,3)).value = "-"
                    sheets.Range(sheets.cells(pos,1),sheets.cells(pos,6)).Interior.ColorIndex = 3
                end
            end
        end
        
        #セルの結合
        sheets.Range(sheets.cells(1,1), sheets.cells(2,1)).Merge
        sheets.Range(sheets.cells(1,2), sheets.cells(2,2)).Merge
        sheets.Range(sheets.cells(1,3), sheets.cells(2,3)).Merge
        sheets.Range(sheets.cells(1,4), sheets.cells(1,6)).Merge
        
        #中央に配置
        sheets.range(sheets.cells(1,4),sheets.cells(1,6)).HorizontalAlignment = -4108
        
        #罫線
        # S150対応 Change Start 2020/12/24 by Nishi
        sheets.Range(sheets.cells(1,1),sheets.cells(69,6)).Borders.LineStyle = 1
        # S150対応 Change End 2020/12/24 by Nishi
        
        #セル色変更
        sheets.Range(sheets.cells(1,1),sheets.cells(2,6)).Interior.ColorIndex = 4
        
        #C列を文字列にする
        sheets.Columns(2).NumberFormat = "@"
        sheets.Columns(3).NumberFormat = "@"
        
        #自動整形
        sheets.Columns.AutoFit
    
        @logobj.write("Almlog_Analyze_Cls::config_analyze_s150 Endt\n")
    
    end #config_analyze_s150
    # S150対応 Add End 2020/12/11 by Nishi
 
    #----------------------------------------------------------#
    # audit_analyze
    # 概要：audit情報解析
    #       aduit情報をエクセルに吐き出す
    #----------------------------------------------------------#
    def audit_analyze(path,book)
        sheets = book.Worksheets("Trap情報")
        
        line = @trap_count + 1 #Trap情報＋ヘッダ分
        
        #ファイルの読み込み
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filedata = entry.read
                if (filedata != nil)
                    
                    filename = File.basename(entry.name)
                    
                    if( filename == "audit.log" )
                        data = StringIO.new(filedata)
                        
                        loop_data = ""
                        #エクセルへの吐き出し
                        data.each_line  do |read_data|
                        
                            read_data = read_data.unpack("Z*").join(",").delete("\n")
                            
                            if read_data !~ /CLI/
                                next
                            end
                            
                            #正常データ
                            if /<INFO>/ =~ read_data
                            line += 1
                                sheets.Cells(line, 1).value = read_data 
                            else
                            #異常データ
                                #(1行目だった場合はループしたデータなので保存する)
                                if(line == (@trap_count + 1))
                                    loop_data = read_data
                                else #1行目以外は本当に異常なものなので捨てる
                                end
                            end
                        end
                        #ループデータがあれば、最後の行にくっつける
                        if(loop_data != "")
                            sheets.Cells(line, 1).value += loop_data
                        end
                        
                        if(line == (@trap_count + 1))
                            #一行も処理をしていない場合はそこで終了
                            return
                        end
                        
                        for line_count in (@trap_count + 2)...(line + 1)
                            
                            read_data = sheets.Cells(line_count, 1).value
                            
                            hash = {}
                            hash["Jan"] = "01"
                            hash["Feb"] = "02"
                            hash["Mar"] = "03"
                            hash["Apr"] = "04"
                            hash["May"] = "05"
                            hash["Jun"] = "06"
                            hash["Jul"] = "07"
                            hash["Aug"] = "08"
                            hash["Sep"] = "09"
                            hash["Oct"] = "10"
                            hash["Nov"] = "11"
                            hash["Dec"] = "12"
                            
                            #時刻情報を分解
                            date = "2000/01/01"
                            time = "00:00:00"
                            msecond = ".000"
                            # format: <INFO> 21-Sep-2016::00:34:15.302 fujitsu-s100 confd[4474]: audit user: fujitsu/15 CLI done
                            day = read_data[read_data.index(" ")+1,read_data.index("-") - (read_data.index(" ")+1)]
                            year = read_data.scan(/\d{4}/)
                            month = read_data[read_data.index("-")+1,3]
                            
                            date = year[0] + "/" + hash[month] + "/" + day
                            
                            time_data = read_data.scan(/\d{2}:\d{2}:\d{2}.\d{3}/).join(",")
                            hour      = time_data[0,2]
                            minits    = time_data[3,2]
                            second    = time_data[6,2]
                            msecond   = time_data.scan(/.\d{3}/).join(",")
                            
                            time = hour + ":" + minits + ":" + second
                            
                            #CLIコマンドを分離
                            cmd_top = read_data.index("CLI")+4
                            cmd_data = read_data[cmd_top,read_data.length - cmd_top]
                            
                            sheets.Cells(line_count, 1).value = date
                            sheets.Cells(line_count, 2).value = time
                            sheets.Cells(line_count, 3).value = msecond
                            sheets.Cells(line_count, 4).value = "CLI"
                            sheets.Cells(line_count, 5).value = cmd_data
                            
                        end
                    end
                end
            end
        end
        
        sheets.Range("C:C").NumberFormatLocal = "0.000000_ "
        
        sheets.Range(sheets.cells(2,1), sheets.cells(line,12)).Sort({"key1" => sheets.Range("A2"), 
                                                                     "key2" => sheets.Range("B2"), 
                                                                     "key3" => sheets.Range("C2")})
        #罫線
        sheets.Range(sheets.cells(1,1),sheets.cells(line,12)).Borders.LineStyle = 1
        
        for line_count in 2...line+1 do
            if(sheets.cells(line_count,4).value == "CLI")
                sheets.Range(sheets.cells(line_count,5), sheets.cells(line_count,12)).Merge
                sheets.Range(sheets.cells(line_count,4),sheets.cells(line_count,12)).Interior.ColorIndex = 20
                if(sheets.cells(line_count,5).value == "aborted")
                    sheets.Range(sheets.cells(line_count,5),sheets.cells(line_count,12)).Interior.ColorIndex = 3
                 end
            end
        end
        
    end #audit_analyze
    
    # S150対応 Change Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # unitinfo_analyze_s100
    # 概要：UNIT INFO解析(S100向け)
    #       各種version情報を収集
    #       
    #----------------------------------------------------------#
    def unitinfo_analyze_s100(path,book)
    # S150対応 Change End 2020/12/11 by Nishi
    
        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_s100 Start\n")
        # S150対応 Add End 2020/12/11 by Nishi
        
        sheets = excel_sheet_add("各種version情報",book)
        
        #ヘッダ部分
        sheets.Cells(1, 1).Value = "項目 1"
        sheets.Cells(1, 2).Value = "項目 2"
        sheets.Cells(1, 3).Value = "項目 3"
        sheets.Cells(1, 4).Value = "情報"
        
        #各データ
        sheets.Cells(2, 1).Value  = "boot 情報"
        sheets.Cells(3, 1).Value  = "SW 情報"
        sheets.Cells(3, 2).Value  = "ACTIVE"
        sheets.Cells(4, 2).Value  = "STANDBY"
        sheets.Cells(5, 1).Value  = "Linux 情報"
        sheets.Cells(6, 1).Value  = "Kernel Version"
        sheets.Cells(7, 1).Value  = "DDS 情報"
        sheets.Cells(8, 1).Value  = "CDEC CPLD Version"
        sheets.Cells(9, 1).Value  = "SOAM FPGA Version"
        sheets.Cells(10, 1).Value = "MDEC CPLD Version"
        sheets.Cells(10, 2).Value = "slot 0"
        sheets.Cells(11, 2).Value = "slot 1"
        sheets.Cells(12, 2).Value = "slot 2"
        sheets.Cells(13, 2).Value = "slot 3"
        
        #必要ファイルの読み込み
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filedata = entry.read
                if (filedata != nil)
                    
                    filename = File.basename(entry.name)
                    
                    if( filename == "ud_hw_ver.LOG" or filename == ".kernel_version" or filename == "boot" or filename == "messages" or filename == "ospl-info.log" )
                        data = StringIO.new(filedata)
                        if( filename == "ud_hw_ver.LOG" )
                            unitinfo_analyze_hw_ver(filename,data,sheets)
                        elsif(filename == ".kernel_version")
                            unitinfo_analyze_kernel_ver(filename,data,sheets)
                        elsif (filename == "boot" )
                            unitinfo_analyze_boot_ver(filename,data,sheets)
                        elsif (filename == "messages" )
                            unitinfo_analyze_sw_ver(filename,data,sheets)
                        elsif (filename == "ospl-info.log" )
                            unitinfo_analyze_dds_ver(filename,data,sheets)
                        end
                    end #filename
                end #filedata != nil
            end #do |entry|
        end #do |tgz|
        
        #2.Trapの状態から、Unitの状態を割り出す。
        
        #PIU
        for sl in 0...4 do
            pos = 10 + sl
            #RMVD
            if(@piu_data[sl][1] == "critical")
                sheets.Range(sheets.cells(pos,3),sheets.cells(pos,4)).value = ""
                sheets.Range(sheets.cells(pos,2),sheets.cells(pos,4)).Interior.ColorIndex = 5
            end
        end
        
        #セルの結合
        sheets.Range(sheets.cells(2,1), sheets.cells(2,3)).Merge
        
        sheets.Range(sheets.cells(3,1), sheets.cells(4,1)).Merge
        sheets.Range(sheets.cells(3,2), sheets.cells(3,3)).Merge
        sheets.Range(sheets.cells(4,2), sheets.cells(4,3)).Merge
        
        sheets.Range(sheets.cells(5,1), sheets.cells(5,3)).Merge
        sheets.Range(sheets.cells(6,1), sheets.cells(6,3)).Merge
        sheets.Range(sheets.cells(7,1), sheets.cells(7,3)).Merge
        sheets.Range(sheets.cells(8,1), sheets.cells(8,3)).Merge
        sheets.Range(sheets.cells(9,1), sheets.cells(9,3)).Merge
        
        sheets.Range(sheets.cells(10,1), sheets.cells(13,1)).Merge
        
        #罫線
        sheets.Range(sheets.cells(1,1),sheets.cells(13,4)).Borders.LineStyle = 1
        
        # セル色変更
        sheets.Range(sheets.cells(1,1),sheets.cells(1,4)).Interior.ColorIndex = 3
        
        #自動整形
        sheets.Columns.AutoFit
        
        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_s100 End\n")
        # S150対応 Add End 2020/12/11 by Nishi
        
    # S150対応 Change Start 2020/12/11 by Nishi
    end #unitinfo_analyze_s100
    # S150対応 Change End 2020/12/11 by Nishi
    
    # S150対応 Add Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # unitinfo_analyze_s150
    # 概要：UNIT INFO解析(S150向け)
    #       各種version情報を収集
    #       
    #----------------------------------------------------------#
    def unitinfo_analyze_s150(path,book)
    
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_s150 Start\n")
        
        sheets = excel_sheet_add("各種version情報",book)
        
        #ヘッダ部分
        sheets.Cells(1, 1).Value = "項目 1"
        sheets.Cells(1, 2).Value = "項目 2"
        sheets.Cells(1, 3).Value = "項目 3"
        sheets.Cells(1, 4).Value = "情報"
        
        #各データ
        sheets.Cells(2, 1).Value  = "boot 情報"
        sheets.Cells(3, 1).Value  = "SW 情報"
        sheets.Cells(3, 2).Value  = "ACTIVE"
        sheets.Cells(4, 2).Value  = "STANDBY"
        sheets.Cells(5, 1).Value  = "Linux 情報"
        sheets.Cells(6, 1).Value  = "Kernel Version"
        sheets.Cells(7, 1).Value  = "DDS 情報"
        sheets.Cells(8, 1).Value  = "CPU PLD"
        sheets.Cells(9, 1).Value  = "SYS FPGA"
        sheets.Cells(10, 1).Value  = "Microsemi FPGA"
        sheets.Cells(11, 1).Value = "SOAM FPGA Version"
        sheets.Cells(12, 1).Value = "MDEC CPLD Version"
        sheets.Cells(12, 2).Value = "slot 0"
        sheets.Cells(13, 2).Value = "slot 1"
        sheets.Cells(14, 2).Value = "slot 2"
        
        #必要ファイルの読み込み
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filedata = entry.read
                if (filedata != nil)
                    
                    filename = File.basename(entry.name)
                    
                    if( filename == "ud_hw_ver.LOG" or filename == "dip.log" or filename == "syslog" or filename == "pf_kernel_dbg.log" or filename == "ospl-info.log" )
                        data = StringIO.new(filedata)
                        if (filename == "ud_hw_ver.LOG" )
                            unitinfo_analyze_hw_ver_s150(filename,data,sheets)
                        elsif (filename == "dip.log")
                            unitinfo_analyze_boot_ver_s150(filename,data,sheets)
                        elsif (filename == "syslog" )
                            unitinfo_analyze_sw_ver_s150(filename,data,sheets)
                        elsif (filename == "pf_kernel_dbg.log" )
                            unitinfo_analyze_kernel_ver_s150(filename,data,sheets)
                        elsif (filename == "ospl-info.log" )
                            unitinfo_analyze_dds_ver_s150(filename,data,sheets)
                        end
                    end #filename
                end #filedata != nil
            end #do |entry|
        end #do |tgz|
        
        #2.Trapの状態から、Unitの状態を割り出す。
        
        #PIU
        for sl in 0...3 do
            # S150対応 Change Start 2021/01/04 by Nishi
            pos = 12 + sl
            # S150対応 Change End 2021/01/04 by Nishi
            #RMVD
            if(@piu_data[sl][1] == "critical")
                sheets.Range(sheets.cells(pos,3),sheets.cells(pos,4)).value = ""
                sheets.Range(sheets.cells(pos,2),sheets.cells(pos,4)).Interior.ColorIndex = 5
            end
        end
        
        #セルの結合
        sheets.Range(sheets.cells(2,1), sheets.cells(2,3)).Merge
        
        sheets.Range(sheets.cells(3,1), sheets.cells(4,1)).Merge
        sheets.Range(sheets.cells(3,2), sheets.cells(3,3)).Merge
        sheets.Range(sheets.cells(4,2), sheets.cells(4,3)).Merge
        
        sheets.Range(sheets.cells(5,1), sheets.cells(5,3)).Merge
        sheets.Range(sheets.cells(6,1), sheets.cells(6,3)).Merge
        sheets.Range(sheets.cells(7,1), sheets.cells(7,3)).Merge
        sheets.Range(sheets.cells(8,1), sheets.cells(8,3)).Merge
        sheets.Range(sheets.cells(9,1), sheets.cells(9,3)).Merge
        sheets.Range(sheets.cells(10,1), sheets.cells(10,3)).Merge
        sheets.Range(sheets.cells(11,1), sheets.cells(11,3)).Merge
        
        # S150対応 Change Start 2021/01/04 by Nishi
        sheets.Range(sheets.cells(12,1), sheets.cells(14,1)).Merge
        # S150対応 Change End 2021/01/04 by Nishi
        
        #罫線
        # S150対応 Change Start 2021/01/04 by Nishi
        sheets.Range(sheets.cells(1,1),sheets.cells(14,4)).Borders.LineStyle = 1
        # S150対応 Change End 2021/01/04 by Nishi
        
        # セル色変更
        sheets.Range(sheets.cells(1,1),sheets.cells(1,4)).Interior.ColorIndex = 3
        
        #自動整形
        sheets.Columns.AutoFit
        
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_s150 End\n")
        
    end #unitinfo_analyze_s150
    # S150対応 Add End 2020/12/11 by Nishi

    # S150対応 Add Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # unitinfo_analyze_hw_ver
    # 概要：UNIT INFO解析(S100)
    #       hw ver情報を収集
    #       
    #----------------------------------------------------------#
    # S150対応 Add End 2020/12/11 by Nishi
    def unitinfo_analyze_hw_ver(filename,data,sheets)
    
        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_hw_ver Start\n")
        # S150対応 Add End 2020/12/11 by Nishi
        
        data.each_line  do |read_data|
            #NULL文字を排除したうえで文字列として連結させる
            read_data = read_data.unpack("Z*").join(",").delete("\n")
            
            #日付を削除
            read_data.gsub!(/\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}.\d{6} /,"")
            
            if(read_data =~ /CDEC CPLD/)
                sheets.Cells(8,4).Value = read_data.gsub!("CDEC CPLD  :  ","")
            elsif(read_data=~ /SOAM FPGA/)
                sheets.Cells(9,4).Value = read_data.gsub!("SOAM FPGA  :  ","")
            elsif(read_data =~ /MDEC CPLD/)
                #Unit名(項目3)を検索する
                #MDEC CPLD(PG31 , slot0)  :  0x16042101
                unit_start_pos  = read_data.index("(") + 1
                unit_end_pos    = read_data.index(",") - 2
                unit_name       = read_data[unit_start_pos,(unit_end_pos - unit_start_pos + 1)]
                
                #versionを検索する
                version_start_pos = read_data.index(":") + 3
                version_info      = read_data[version_start_pos,read_data.length - version_start_pos + 1]
                if(read_data     =~ /slot0/)
                    sheets.Cells(10, 3).Value = unit_name
                    sheets.Cells(10, 4).Value = version_info
                elsif(read_data  =~ /slot1/)
                    sheets.Cells(11, 3).Value = unit_name
                    sheets.Cells(11, 4).Value = version_info
                elsif(read_data  =~ /slot2/)
                    sheets.Cells(12, 3).Value = unit_name
                    sheets.Cells(12, 4).Value = version_info
                else #(read_data =~ /slot3/)
                    sheets.Cells(13, 3).Value = unit_name
                    sheets.Cells(13, 4).Value = version_info
                end
            end
        end #do |read_data|
        
        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_hw_ver End\n")
        # S150対応 Add End 2020/12/11 by Nishi
        
    end #unitinfo_analyze_hw_ver

    # S150対応 Add Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # unitinfo_analyze_hw_ver_s150
    # 概要：UNIT INFO解析(S150)
    #       hw ver情報を収集
    #       
    #----------------------------------------------------------#
    def unitinfo_analyze_hw_ver_s150(filename,data,sheets)
        
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_hw_ver_s150 Start\n")
        
        data.each_line  do |read_data|
            #NULL文字を排除したうえで文字列として連結させる
            read_data = read_data.unpack("Z*").join(",").delete("\n")
            
            #日付を削除
            read_data.gsub!(/\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}.\d{6} /,"")
            
            #CPU PLD
            if(read_data =~ /CPU PLD  :  /)
                sheets.Cells(8,4).Value = read_data.gsub!("CPU PLD  :  ","")
            #SYS FPGA
            elsif(read_data=~ /SYS FPGA  :  /)
                sheets.Cells(9,4).Value = read_data.gsub!("SYS FPGA  :  ","")
            #Microsemi FPGA
            elsif(read_data=~ /Microsemi FPGA  :  /)
                sheets.Cells(10,4).Value = read_data.gsub!("Microsemi FPGA  :  ","")
            #SOAM FPGA
            elsif(read_data=~ /SOAM FPGA  :  /)
                sheets.Cells(11,4).Value = read_data.gsub!("SOAM FPGA  :  ","")
            elsif(read_data =~ /MDEC CPLD/)
                #Unit名(項目3)を検索する
                #MDEC CPLD(PG31 , slot0)  :  0x16042101
                unit_start_pos  = read_data.index("(") + 1
                unit_end_pos    = read_data.index(",") - 2
                unit_name       = read_data[unit_start_pos,(unit_end_pos - unit_start_pos + 1)]
                
                #versionを検索する
                version_start_pos = read_data.index(":") + 3
                version_info      = read_data[version_start_pos,read_data.length - version_start_pos + 1]
                if(read_data     =~ /slot0/)
                    sheets.Cells(12, 3).Value = unit_name
                    sheets.Cells(12, 4).Value = version_info
                elsif(read_data  =~ /slot1/)
                    sheets.Cells(13, 3).Value = unit_name
                    sheets.Cells(14, 4).Value = version_info
                elsif(read_data  =~ /slot2/)
                    sheets.Cells(14, 3).Value = unit_name
                    sheets.Cells(14, 4).Value = version_info
                else #(read_data =~ /slot3/)
                    sheets.Cells(15, 3).Value = unit_name
                    sheets.Cells(15, 4).Value = version_info
                end
            end
        end #do |read_data|
        
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_hw_ver_s150 End\n")
        
    end #unitinfo_analyze_hw_ver_s150
    # S150対応 Add End 2020/12/11 by Nishi
    
    # S150対応 Change Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # unitinfo_analyze_kernel_ver
    # 概要：UNIT INFO解析(S100)
    #       kernel ver情報を収集
    #       
    #----------------------------------------------------------#
    # S150対応 Change End 2020/12/11 by Nishi
    def unitinfo_analyze_kernel_ver(filename,data,sheets)

        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_kernel_ver Start\n")
        # S150対応 Add End 2020/12/11 by Nishi
        
        line_count = 0
        
        data.each_line  do |read_data|
            #NULL文字を排除したうえで文字列として連結させる
            read_data = read_data.unpack("Z*").join(",").delete("\n")
            
            # 1行目(kernel version)
            if( line_count == 0 )
                sheets.Cells(6, 4).Value = read_data.gsub!("Version :","")
                line_count = 1
                next
            end
            #2行目(Linux 情報)
            if( line_count == 1 )
                sheets.Cells(5, 4).Value = read_data
                line_count = 2
                next
            end
        end #do |read_data|
        
        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_kernel_ver End\n")
        # S150対応 Add End 2020/12/11 by Nishi
        
    end #unitinfo_analyze_kernel_ver
    
    # S150対応 Add Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # unitinfo_analyze_kernel_ver_s150
    # 概要：UNIT INFO解析(S150)
    #       kernel ver情報を収集
    #       
    #----------------------------------------------------------#
    def unitinfo_analyze_kernel_ver_s150(filename,data,sheets)

        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_kernel_ver_s150 Start\n")
        
        line_count = 0
        str_hostname = ""
        str_osrelease = ""
        str_version = ""
        
        data.each_line  do |read_data|
            #NULL文字を排除したうえで文字列として連結させる
            read_data = read_data.unpack("Z*").join(",").delete("\n")

            #kernel version
            if(read_data =~ /Version :/)
                sheets.Cells(6,4).Value = read_data.gsub!("Version :","")
            end
            
            #kernel.hostname
            if(read_data =~ /kernel.hostname = /)
                str_hostname = read_data.gsub!("kernel.hostname = ","")
            end
            
            #kernel.osrelease
            if(read_data =~ /kernel.osrelease = /)
                str_osrelease = read_data.gsub!("kernel.osrelease = ","")
            end

            #kernel.version
            if(read_data =~ /kernel.version = /)
                str_version = read_data.gsub!("kernel.version = ","")
            end

        end #do |read_data|
        
        sheets.Cells(5,4).Value = str_hostname + " " +  str_osrelease + " " + str_version
        
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_kernel_ver_s150 End\n")
        
    end #unitinfo_analyze_kernel_ver_s150
    # S150対応 Add End 2020/12/11 by Nishi
    
    # S150対応 Change Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # unitinfo_analyze_boot_ver
    # 概要：UNIT INFO解析(S100)
    #       boot ver情報を収集
    #       
    #----------------------------------------------------------#
    # S150対応 Change End 2020/12/11 by Nishi
    def unitinfo_analyze_boot_ver(filename,data,sheets)
    
        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_boot_ver Start\n")
        # S150対応 Add End 2020/12/11 by Nishi
    
        data.each_line  do |read_data|
            #NULL文字を排除したうえで文字列として連結させる
            read_data = read_data.unpack("Z*").join(",").delete("\n")
            if( read_data =~ /: Boot Ver = / )
                sheets.Cells(2, 4).Value = read_data.scan(/: Boot Ver = \w+/).join(",").gsub(": Boot Ver = ", "")
            end
        end #do |read_data|
        
        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_boot_ver End\n")
        # S150対応 Add End 2020/12/11 by Nishi
        
    end #unitinfo_analyze_boot_ver
    
    # S150対応 Add Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # unitinfo_analyze_boot_ver_s150
    # 概要：UNIT INFO解析(S150)
    #       boot ver情報を収集
    #       
    #----------------------------------------------------------#
    def unitinfo_analyze_boot_ver_s150(filename,data,sheets)
    
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_boot_ver_s150 Start\n")
        
        data.each_line  do |read_data|
            #NULL文字を排除したうえで文字列として連結させる
            read_data = read_data.unpack("Z*").join(",").delete("\n")
            if( read_data =~ /: Boot Ver = / )
                version_start_pos = read_data.index("Boot Ver = ") + 11
                version_info      = read_data[version_start_pos,read_data.length - version_start_pos + 1]
                sheets.Cells(2, 4).Value = version_info
            end
        end #do |read_data|
        
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_boot_ver_s150 End\n")
        
    end #unitinfo_analyze_boot_ver_s150
    # S150対応 Add End 2020/12/11 by Nishi
    
    # S150対応 Change Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # unitinfo_analyze_sw_ver
    # 概要：UNIT INFO解析(S100)
    #       sw ver情報を収集
    #       
    #----------------------------------------------------------#
    # S150対応 Change End 2020/12/11 by Nishi
    def unitinfo_analyze_sw_ver(filename,data,sheets)
    
        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_sw_ver Start\n")
        # S150対応 Add End 2020/12/11 by Nishi
    
        data.each_line  do |read_data|
            #NULL文字を排除したうえで文字列として連結させる
            read_data = read_data.unpack("Z*").join(",").delete("\n")
            
            if( read_data =~ /active version: / )
                version_start_pos = read_data.index("active version: ") + 16
                version_info      = read_data[version_start_pos,read_data.length - version_start_pos + 1]
                sheets.Cells(3, 4).Value = version_info
            end
            if( read_data =~ /standby version: / )
                version_start_pos = read_data.index("standby version: ") + 17
                version_info      = read_data[version_start_pos,read_data.length - version_start_pos + 1]
                sheets.Cells(4, 4).Value = version_info
            end
        end # do |read_data|
        
        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_sw_ver End\n")
        # S150対応 Add End 2020/12/11 by Nishi
        
    end #unitinfo_analyze_sw_ver
    
    # S150対応 Add Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # unitinfo_analyze_sw_ver_s150
    # 概要：UNIT INFO解析(S150)
    #       sw ver情報を収集
    #       
    #----------------------------------------------------------#
    def unitinfo_analyze_sw_ver_s150(filename,data,sheets)
 
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_sw_ver_s150 Start\n")
    
        olddate_active_data = 0
        oldtime_active_data = 0
        newdate_active_data = 0
        newtime_active_data = 0
        olddate_backup_data = 0
        oldtime_backup_data = 0
        newdate_backup_data = 0
        newtime_backup_data = 0
    
        data.each_line  do |read_data|
            #NULL文字を排除したうえで文字列として連結させる
            read_data = read_data.unpack("Z*").join(",").delete("\n")
            
            if( read_data =~ / active version: / )
            
                # 日付と時刻を取得する
                split_linedata = read_data.split(" ")
                split_datedata = split_linedata[0].to_s.split("T")
                newdate_active_data =  split_datedata[0].to_s.gsub("-","").to_i
                time_linedata = split_datedata[1].to_s.gsub(":","")
                split_timedata = time_linedata.split("+")
                newtime_active_data = split_timedata[0].to_i
                
                # 最新の日付と時刻ならバージョンを取得する
                if  olddate_active_data < newdate_active_data
                    if  oldtime_active_data < newtime_active_data
                        version_start_pos = read_data.index("active version: ") + 16
                        version_info      = read_data[version_start_pos,read_data.length - version_start_pos + 1]
                        sheets.Cells(3, 4).Value = version_info
                        
                        olddate_active_data = newdate_active_data
                        oldtime_active_data = newtime_active_data
                    end
                end               
            end
            
            if( read_data =~ / backup version: / )
            
                split_linedata = read_data.split(" ")
                split_datedata = split_linedata[0].to_s.split("T")
                newdate_backup_data =  split_datedata[0].to_s.gsub("-","").to_i
                time_linedata = split_datedata[1].to_s.gsub(":","")
                split_timedata = time_linedata.split("+")
                newtime_backup_data = split_timedata[0].to_i
         
                # 最新の日付と時刻ならバージョンを取得する
                if  olddate_backup_data < newdate_backup_data
                    if  oldtime_backup_data < newtime_backup_data
                        version_start_pos = read_data.index("backup version: ") + 16
                        version_info      = read_data[version_start_pos,read_data.length - version_start_pos + 1]
                        sheets.Cells(4, 4).Value = version_info
                        
                        olddate_backup_data = newdate_backup_data
                        oldtime_backup_data = newtime_backup_data
                    end
                end
            end
            
        end # do |read_data|
        
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_sw_ver_s150 End\n")
        
    end #unitinfo_analyze_sw_ver_s150
    # S150対応 Add End 2020/12/11 by Nishi
    
    #----------------------------------------------------------#
    # unitinfo_analyze_dds_ver
    # 概要：UNIT INFO解析
    #       dds ver情報を収集
    #       
    #----------------------------------------------------------#
    def unitinfo_analyze_dds_ver(filename,data,sheets)

        # S150対応 Add Start 2020/12/11 by Nishi    
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_dds_ver Start\n")
        # S150対応 Add End 2020/12/11 by Nishi
    
        data.each_line  do |read_data|
            #NULL文字を排除したうえで文字列として連結させる
            read_data = read_data.unpack("Z*").join(",").delete("\n")
            if( read_data =~ /Internals/ )
                version_start_pos = read_data.index(":") + 2
                version_end_pos = read_data.index("/")
                version_info      = read_data[version_start_pos,version_end_pos - version_start_pos]
                sheets.Cells(7, 4).Value = version_info
            end
        end # do |read_data|
        
        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_dds_ver End\n")
        # S150対応 Add End 2020/12/11 by Nishi
        
    end #unitinfo_analyze_dds_ver
    
    # S150対応 Add Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # unitinfo_analyze_dds_ver_s150
    # 概要：UNIT INFO解析(S150)
    #       dds ver情報を収集
    #       
    #----------------------------------------------------------#
    def unitinfo_analyze_dds_ver_s150(filename,data,sheets)
    
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_dds_ver_s150 Start\n")
    
        data.each_line  do |read_data|
            #NULL文字を排除したうえで文字列として連結させる
            read_data = read_data.unpack("Z*").join(",").delete("\n")
            if( read_data =~ /Internals/ )
                version_start_pos = read_data.index(":") + 2
                version_end_pos = read_data.index("/")
                version_info      = read_data[version_start_pos,version_end_pos - version_start_pos]
                
                if version_info.count('.') == 2
                  sheets.Cells(7, 4).Value = version_info
                end
                
            end
        end # do |read_data|
        
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_dds_ver_s150 End\n")
        
    end #unitinfo_analyze_dds_ver_s150
    # S150対応 Add End 2020/12/11 by Nishi
    
    #----------------------------------------------------------#
    # select_function
    # 概要：警報ログ解析の機能振り分け
    #       CheckBoxの状態を見てexelのシート追加
    #       
    #----------------------------------------------------------#
    def select_function(path,book)
        @logobj.write("Almlog_Analyze_Cls::select_function Start\n")
        
        if($g_step2_log_stschgchk == "する")
            sheets = excel_sheet_add("警報変化点ログ解析",book)
            analyze_stschange(sheets,path,book)
        end #if
        
        if($g_step2_log_fltchk == "する")
            sheets = excel_sheet_add("FLT 解析",book)
            if $g_flt_tab_check != false
                tabSheet = excel_sheet_add("参照FLTテーブル",book)
            else
                tabSheet = nil
            end
                        
            # S150対応 Change Start 2020/12/11 by Nishi
            # Typeオプションが"S100"のとき
            if ($g_machine_type == "S100")
                if flt_analyze_s100(sheets,path,tabSheet) == false
                    return ERR_CHECK_FLT
                end
            # Typeオプションが"S150"のとき
            elsif ($g_machine_type == "S150")
                if flt_analyze_s150(sheets,path,tabSheet) == false
                    return ERR_CHECK_FLT
                end
            end
            # S150対応 Change End 2020/12/11 by Nishi
            
        end #if
        
        if($g_step2_log_eccerrchk == "する")
            sheets = excel_sheet_add("警報メモリECCエラーログ解析",book)
            analyze_memory_ecc_error(sheets,path)
        end #if
        
        if($g_step2_log_softerrchk == "する")
            sheets = excel_sheet_add("FPGAソフトエラーログ解析",book)
            analyze_fpga_soft_error(sheets,path)
        end #if
        
        if($g_step2_log_ffanchk != 0)
            sheets = excel_sheet_add("FFANログ解析",book)
            
            # S150対応 Change Start 2020/12/11 by Nishi
            # Typeオプションが"S100"のとき
            if ($g_machine_type == "S100")
                if analyze_ffan_proc(sheets,path) == false
                    return ERR_CHECK_FFANLOG
                end
            # Typeオプションが"S150"のとき
            elsif ($g_machine_type == "S150")
                if analyze_ffan_proc_s150(sheets,path) == false
                    return ERR_CHECK_FFANLOG
                end
            end
        end #if
        
        @logobj.write("Almlog_Analyze_Cls::select_function End\n")
        # S150対応 Add Start 2020/12/11 by Nishi
        return ERR_CHECK_OK
        # S150対応 Add End 2020/12/11 by Nishi
    end #select_function
    
    #----------------------------------------------------------#
    # flt_analyze
    # 概要：FLT解析
    #       FLT情報を解析
    #       logファイルからの抽出はtechinfo_flt_load
    #       内容の分解はtechinfo_flt_analyze
    #       そのあとにFLTテーブルからregister/bit名を検索する
    #----------------------------------------------------------#
    def flt_analyze_s100(sheets,path,tabSheet)
        @logobj.write("Almlog_Analyze_Cls::flt_analyze Start\n")
        
        write_data = Array.new(2).map{Array.new(15, "")}
        
        write_data[0][0] = "データ数"
        write_data[1][0] = "File名"
        write_data[1][1] = "Date"
        write_data[1][2] = "Time"
        write_data[1][3] = "Time(.sec)"
        
        write_data[1][4] = "Action"
        write_data[1][5]  = "alm_type"
        write_data[1][6] = "dev"
        write_data[1][7] = "slot_no"
        write_data[1][8] = "base_addr"
        write_data[1][9] = "addr"
        write_data[1][10 ]= "val"
        write_data[1][11] = "chip"
        write_data[1][12] = "offset"
        write_data[1][13] = "register名"
        write_data[1][14] = "ON状態のbit名"
        
        if $g_step2_log_repairparts == true
            write_data[1] << "被疑部品"     #15
            write_data[1] << "MAIN/SUB"     #16
            write_data[1] << "部品番号"     #17
            write_data[1] << "実装位置名"   #18
            write_data[1] << "参照シート" #19
            write_data[1] << "参照行"     #20
            write_data[1] << "ON_bit"     #21
            write_data[1] << "Alarmパターン" #22
            write_data[1] << "Clear時間"  #23
            
            alarm_pattern_col = 23
            alarm_clear_col = 24
        end
        max_write_column = write_data[1].size
        
        sheets.Range("D:D").NumberFormatLocal = "0.000000_ "
        
        # Excelに書き込み
        sheets.cells(1,1).resize(2,max_write_column).value = write_data
        @flt_chk_cnt = 0
        
        @logobj.write("Almlog_Analyze_Cls::flt_analyze - GzipReader Start\n")
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filedata = entry.read
                if (filedata != nil)
                    filename = File.basename(entry.name)
                    case filename
                    when "ud_flt.LOG", "ud_warning.LOG", /^NBO_PIU_flt_\d\.log$/ then
                      data = StringIO.new(filedata)
                      techinfo_flt_load(filename,data,sheets)
                    end #filename
                end #filedata != nil
            end #do |entry|
        end #do |tgz|
        @logobj.write("Almlog_Analyze_Cls::flt_analyze - GzipReader End\n")
        
        if(@flt_chk_cnt != 0)
            chk_log_max_set(@flt_chk_cnt,"FLTログ")
            sleep 0.001
            
            techinfo_flt_analyze(sheets)
            
            alm_type_list = []
            alm_type_list << "BLADE FLT"
            alm_type_list << "PIU FLT"
            alm_type_list << "FAN FLT"
            alm_type_list << "FAN モーター1の故障"
            alm_type_list << "FAN モーター2の故障"
            alm_type_list << "-"
            alm_type_list << "MDL FLT"
            alm_type_list << "BLADE-PIU CTNEQPT"
            alm_type_list << "集約レジスタ"
            
            dev_list = Hash::new
            dev_list[3000] = "SOAM FPGA"
            dev_list[3002] = "CDEC CPLD"
            dev_list[3015] = "JERICHO"
            dev_list[3033] = "MDEC CPLD"
            
            #FLTテーブル読み込み
            filepath_flt_table = TECHINFO_Analysis_Common::flt_table_path_get()
            if filepath_flt_table.nil?
                return false
            end
            puts "* FLT table=#{filepath_flt_table}"
            puts "[debug] g_flt_tab_check=#{$g_flt_tab_check}"
            
            @logobj.write("FLT table=#{filepath_flt_table}\n")
            @logobj.write("g_flt_tab_check=#{$g_flt_tab_check}\n")
            
            #local_excel = WIN32OLE.new('Excel.Application'  )
            fltbook = @xl.Workbooks.open(filepath_flt_table, :ReadOnly => true)
            
            #FLTテーブルのシート情報設定
            tab_base = tabSheet.nil? ? nil : tabSheet.cells(1,1)
            alm_type2sheet_name = {}
            fltsheet_table = {}
            
            { '3.代表レジスタ'       => [ "集約レジスタ" ], \
              '4-1.Blade FLT要因'    => [ "BLADE FLT" ], \
              '4-2.PIU FLT要因'      => [ "PIU FLT"  ], \
              '4-3.FAN FLT要因'      => [ "FAN モーター1の故障","FAN モーター2の故障" ], \
              '5.Blade-PIU間CTN要因' => [ "BLADE-PIU CTNEQPT" ] \
            }.each { |sheetname, val|
                puts "[debug] FLT table Check #{sheetname} Start"
                # alm_typeとFLTテーブルのシート名の対応表を作成
                val.each { |alm_type|
                    alm_type2sheet_name[alm_type] = sheetname
                }
                
                #FLTテーブルのシートチェック
                begin
                    readSheets = fltbook.Worksheets(sheetname)
                rescue
                    printf("[debug] rescue at %s:%d\n", File.basename(__FILE__), __LINE__)
                    TECHINFO_Analysis_Common::popup_msg("異常通知", "[ERROR]", \
                            "'#{sheetname}'シートが #{$g_filename_flt_table} にありません。")
                    return false
                end
                
                #FLTテーブルの各シートの「被疑部品」格納列を検出
                fltsheet_info = { :sheetname => sheetname, :table_top => tab_base }
                fltsheet_table[sheetname] = fltsheet_info
                if flt_table_chache(1,fltsheet_info,readSheets) == false
                    return false
                end
                tab_base = fltsheet_info[:table_bottom]
                tab_base = tab_base.Offset(1) unless tab_base.nil?
            }
            if tabSheet != nil
                tabSheet.Columns.AutoFit
                tab_base = nil
            end
            
            puts "[debug] FLT table Check End"
            
            @logobj.write("Almlog_Analyze_Cls::flt_analyze - FLT Table Search Start\n")
            readSheets = fltbook.Worksheets('3.代表レジスタ')
            last_readSheet_name = ""
            
            if $g_step2_log_repairparts == true
                read_size = 13
            else
                read_size = 11
            end
            
            for line in 3...@flt_chk_cnt + 3 do
                #セル値を配列に設定
                #  注意) インデックスは、Excel列番号-1になる
                sheets_data = sheets.Cells(line,1).Resize(1,read_size).Value
                sheets_data.flatten!
                
                #警報対象を文字列に変換
                filename = sheets_data[0]
                alm_type = sheets_data[5]
                if alm_type.nil? then
                    if filename =~ /^NBO_PIU_flt_\d+\.log$/ then               
                        alm_type = "1"
                    end
                end
                alm_type = alm_type.to_i
                alm_type_name = alm_type_list[alm_type]
                sheets.cells(line,6).value = alm_type_name
                
                #検出デバイスを文字列に変換
                dev_type = sheets_data[6].to_i
                dev_name = dev_list[dev_type]
                sheets.cells(line,7).value = dev_name
                
                #Slot番号を文字列に変換
                # 既存バグ(整数が期待なのに小数点になる) Change Start 2020/12/11 by Nishi
                slot_no = sheets_data[7].to_i.to_s
                # 既存バグ(整数が期待なのに小数点になる) Change End 2020/12/11 by Nishi
                if (slot_no == "5")
                    sheets.cells(line,8).value = "BLADE"
                end
                
                #FLTテーブルからレジスタ名/bit名を検索
                #1. alm_type から FLTテーブルの検索シートを決定
                alm_type_defined = alm_type2sheet_name.include?(alm_type_name)
                if alm_type_defined
                    readSheet_name = alm_type2sheet_name[alm_type_name]
                end
                
                #2. base_addr
                base_addr = sheets_data[8]
                base_addr = if base_addr.nil? then "" else base_addr.hex end
                
                #3. CPU Add
                cpu_addr = sheets_data[9]
                cpu_addr = if cpu_addr.nil? then "" else cpu_addr.hex end
                
                if $g_step2_log_repairparts == true
                    alm_pattern = [alm_type, dev_type, slot_no, base_addr, cpu_addr, sheets_data[11], sheets_data[12]].join("-")
                    sheets.cells(line,alarm_pattern_col).value = alm_pattern
                end
                
                if not alm_type_defined
                    update_progress()
                    next
                end
                
                if last_readSheet_name != readSheet_name
                    last_readSheet_name = readSheet_name
                    
                    readSheets = fltbook.Worksheets(readSheet_name)
                    
                    #FLTテーブルのシート情報取り出し
                    fltsheet_info = fltsheet_table[readSheet_name]
                    if $g_step2_log_repairparts == true        
                        ref_repairParts_column = fltsheet_info[:repairPart_column]
                        ref_repairParts_num = fltsheet_info[:repairPart_num]
                        ref_repairParts_size = (ref_repairParts_num * (FLT_TBL_REPAIR_SIZE+FLT_TBL_REPAIR_PAD)) \
                                                - FLT_TBL_REPAIR_PAD
                    end
                    readSheets_data_count = fltsheet_info[:data_num]
                    
                    #検索高速化とチェックのため、キー値を前処理
                    if fltsheet_info.has_key?(:readKeys_base)
                        #printf("[debug] FLT table '%s' use chache\n", readSheet_name)
                    elsif flt_table_chache(2,fltsheet_info,readSheets) == false
                        return false
                    end
                    readKeys_base = fltsheet_info[:readKeys_base]
                    readKeys_cpu = fltsheet_info[:readKeys_cpu]
                    readKeys_bit = fltsheet_info[:readKeys_bit]
                end
                
                if $g_step2_log_repairparts == true
                    #参照シート名を設定
                    sheets.cells(line,20).value = readSheet_name
                end
                
                #FLTテーブルの検索シートを1行ずつ読みだす
                for read_index in 0...readSheets_data_count do
                    read_row = FLT_TBL_DATA_ROW + read_index
                    
                    #読み出し行に何もなければ次の行へ
                    next if(readKeys_base[read_index] == nil)
                    
                    #読み出し行のBase/CPU Addrが一致した場合
                    if( ( readKeys_base[read_index] == base_addr ) && ( readKeys_cpu[read_index] == cpu_addr ) )
                        #レジスタ名設定
                        sheets.cells(line,14).value = readSheets.cells(read_row,10).value
                        
                        reg_val = sheets_data[10].hex
                        
                        #データ側のレジスタ値に値がある場合
                        if(reg_val != 0)
                            #ON bit を抽出
                            reg_bit_list = []
                            for bit in 0...32 do
                                if(reg_val & (1 << bit) != 0)
                                    reg_bit_list << bit
                                end
                            end #for bit
                            
                            #見つかった行から下を検索
                            bit_row_list = []
                            found_bit_list = []
                            for read_index2 in read_index...readSheets_data_count do
                                bit = readKeys_bit[read_index2]
                                next if bit.nil?
                                next unless reg_bit_list.include?(bit)
                                
                                next unless( readKeys_base[read_index2] == base_addr )
                                next unless( readKeys_cpu[read_index2] == cpu_addr )
                                
                                read_row2 = FLT_TBL_DATA_ROW + read_index2
                                found_bit_list <<  bit
                                bit_row_list << read_row2
                            end
                            reg_bit_list.delete_if {|bit| found_bit_list.include?(bit) }
                            
                            #bit名称を設定
                            bit_name = []
                            bit_row_list.each do |read_row2|
                                bit_str = readSheets.cells(read_row2,11).value #bit名称取り出し
                                if(bit_str != nil)
                                    bit_name << bit_str.to_s.gsub(/\s+/," ")
                                end
                            end
                            if bit_name.size == 0
                                sheets.cells(line,15).value = "該当なし"
                            else
                                sheets.cells(line,15).value = bit_name.join("\n")
                            end
                            
                            if $g_step2_log_repairparts == true
                                #検出ＮＧのBitを末尾に追加
                                if reg_bit_list.size > 0
                                    bit_row_list.concat(Array.new(reg_bit_list.size,0))
                                    found_bit_list.concat(reg_bit_list)
                                end
                                
                                #参照行番号を設定
                                sheets.cells(line,21).value = bit_row_list.join("\n")
                                
                                #On Bitを設定
                                sheets.cells(line,22).value = found_bit_list.join("\n")
                                
                                #被疑部品を設定
                                repair_parts = Array.new(FLT_TBL_REPAIR_SIZE).map{Array.new()}
                                bit_row_list.each do |read_row2|
                                    next if read_row2 == 0
                                    
                                    read_data = readSheets.Cells(read_row2,ref_repairParts_column).Resize(1,ref_repairParts_size).Value
                                    read_data.flatten!
                                    read_data.map! { |v| v.to_s.gsub(/\s+/,' ') }
                                    col = 0
                                    ary_o = Array.new(FLT_TBL_REPAIR_SIZE).map{Array.new()}
                                    for i in 0...ref_repairParts_num
                                        ary_i = read_data[col, FLT_TBL_REPAIR_SIZE]
                                        if ( ary_i.any? { |v| v =~ /\S/ } )
                                            for j in 0...FLT_TBL_REPAIR_SIZE
                                                str_i = ary_i[j]
                                                if str_i !~ /\S/
                                                    str_i = '-'
                                                end
                                                ary_o[j] << str_i
                                            end
                                        end
                                        col += FLT_TBL_REPAIR_SIZE + FLT_TBL_REPAIR_PAD
                                    end
                                    for j in 0...FLT_TBL_REPAIR_SIZE
                                        repair_parts[j] << ary_o[j].join(",")
                                    end
                                end
                                
                                for j in 0...FLT_TBL_REPAIR_SIZE
                                    repair_parts[j] = repair_parts[j].join("\n")
                                end
                                sheets.cells(line,16).Resize(1,FLT_TBL_REPAIR_SIZE).Value = repair_parts
                            end
                        else
                            sheets.cells(line,15).value = "ALL Clear"
                        end #if(reg_val != 0)
                        
                        break #ここに来た、ということはFLTテーブルの検索が終わったので read_index のループから出る
                    end #読み出し行のBase/CPU Addrが一致した場合
                end #for read_index
                
                update_progress()
            end #for line
            @logobj.write("Almlog_Analyze_Cls::flt_analyze - FLT Table Search End\n")
            sheets.Cells.Item(1,2).value = @flt_chk_cnt
            
            #時系列にソートする
            sheets.cells(3,1).resize(@flt_chk_cnt,max_write_column).Sort({"key1" => sheets.Range("B3"), "key2" => sheets.Range("C3"), "key3" => sheets.Range("D3")})
            
            if $g_step2_log_repairparts == true
                #alarm毎にClearまでの時間を設定する
                alarm_list = sheets.cells(3,alarm_pattern_col).Resize(@flt_chk_cnt).value.flatten!
                bit_on_list = sheets.cells(3,15).Resize(@flt_chk_cnt).value.flatten!
                bit_on_list.map! {|v| v != "ALL Clear"}
                bit_on_list.each_with_index do |bit_on, i|
                    next unless bit_on
                    pattern = alarm_list[i]
                    for j in (i+1)...@flt_chk_cnt do
                        next if pattern != alarm_list[j]
                        if bit_on_list[j] == false
                            sheets_data = sheets.Cells(3+i,2).Resize(1,2).Value.flatten!
                            alarm_time = sheets_data[0] + sheets_data[1]
                            sheets_data = sheets.Cells(3+j,2).Resize(1,2).Value.flatten!
                            clear_time = sheets_data[0] + sheets_data[1]
                            sheets.cells(3+i,alarm_clear_col).value = clear_time - alarm_time
                        else
                          next
                        end
                        break
                    end
                end
                sheets.Cells(3,alarm_clear_col).Resize(@flt_chk_cnt).NumberFormatLocal = "[h]:mm:ss"
            end
            
            #罫線
            sheets.Cells(1,1).Resize(1,2).Borders.LineStyle = 1
            sheets.Cells(2,1).Resize(1+@flt_chk_cnt,max_write_column).Borders.LineStyle = 1
            
            # セル色変更
            sheets.Cells(1,1).Resize(1,2).Interior.ColorIndex = 6
            sheets.Cells(2,1).Resize(1,15).Interior.ColorIndex = 3
            
            #自動整形
            sheets.Columns.AutoFit
            
            #FLTテーブルClose
            fltbook.Close(:SaveChanges => false)
            #local_excel.quit
            
        end #if(@flt_chk_cnt != 0)
        
        @logobj.write("Almlog_Analyze_Cls::flt_analyze End\n")
        return true
    end #flt_analyze
    
    # S150対応 Add Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # flt_analyze_s150
    # 概要：FLT解析(S150向け)
    #       FLT情報を解析
    #       logファイルからの抽出はtechinfo_flt_load
    #       内容の分解はtechinfo_flt_analyze
    #       そのあとにFLTテーブルからregister/bit名を検索する
    #----------------------------------------------------------#
    def flt_analyze_s150(sheets,path,tabSheet)
        @logobj.write("Almlog_Analyze_Cls::flt_analyze_s150 Start\n")
        
        write_data = Array.new(2).map{Array.new(15, "")}
        
        write_data[0][0] = "データ数"
        write_data[1][0] = "File名"
        write_data[1][1] = "Date"
        write_data[1][2] = "Time"
        write_data[1][3] = "Time(.sec)"
        
        write_data[1][4] = "Action"
        write_data[1][5]  = "alm_type"
        write_data[1][6] = "dev"
        write_data[1][7] = "slot_no"
        write_data[1][8] = "base_addr"
        write_data[1][9] = "addr"
        write_data[1][10 ]= "val"
        write_data[1][11] = "chip"
        write_data[1][12] = "offset"
        write_data[1][13] = "register名"
        write_data[1][14] = "ON状態のbit名"
        
        if $g_step2_log_repairparts == true
            write_data[1] << "被疑部品"     #15
            write_data[1] << "MAIN/SUB"     #16
            write_data[1] << "部品番号"     #17
            write_data[1] << "実装位置名"   #18
            write_data[1] << "参照シート" #19
            write_data[1] << "参照行"     #20
            write_data[1] << "ON_bit"     #21
            write_data[1] << "Alarmパターン" #22
            write_data[1] << "Clear時間"  #23
            
            alarm_pattern_col = 23
            alarm_clear_col = 24
        end
        max_write_column = write_data[1].size
        
        sheets.Range("D:D").NumberFormatLocal = "0.000000_ "
        
        # Excelに書き込み
        sheets.cells(1,1).resize(2,max_write_column).value = write_data
        @flt_chk_cnt = 0
        
        @logobj.write("Almlog_Analyze_Cls::flt_analyze_s150 - GzipReader Start\n")
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filedata = entry.read
                if (filedata != nil)
                    filename = File.basename(entry.name)
                    case filename
                    when "ud_flt.LOG", "ud_warning.LOG", /^NBO_PIU_flt_\d\.log$/ then
                      data = StringIO.new(filedata)
                      techinfo_flt_load(filename,data,sheets)
                    end #filename
                end #filedata != nil
            end #do |entry|
        end #do |tgz|
        @logobj.write("Almlog_Analyze_Cls::flt_analyze_s150 - GzipReader End\n")
        
        if(@flt_chk_cnt != 0)
            chk_log_max_set(@flt_chk_cnt,"FLTログ")
            sleep 0.001
            
            techinfo_flt_analyze(sheets)
            
            alm_type_list = []
            alm_type_list << "BLADE FLT"
            alm_type_list << "PIU FLT"
            alm_type_list << "FAN FLT"
            alm_type_list << "FAN モーター1の故障"
            alm_type_list << "FAN モーター2の故障"
            alm_type_list << "-"
            alm_type_list << "MDL FLT"
            alm_type_list << "BLADE-PIU CTNEQPT"
            alm_type_list << "PSU FLT"
            alm_type_list << "CPU FLT"
            
            # S150対応 Change Start 2020/12/11 by Nishi
            dev_list = Hash::new
            dev_list[3000] = "SOAM FPGA"
            dev_list[3015] = "JERICHO"
            dev_list[3033] = "MDEC CPLD"
            dev_list[3037] = "SYS FPGA"
            dev_list[3038] = "CPU PLD"
            # S150対応 Change Start 2020/12/11 by Nishi
            
            #FLTテーブル読み込み
            filepath_flt_table = TECHINFO_Analysis_Common::flt_table_path_get()
            if filepath_flt_table.nil?
                return false
            end
            puts "* FLT table=#{filepath_flt_table}"
            puts "[debug] g_flt_tab_check=#{$g_flt_tab_check}"
            
            @logobj.write("FLT table=#{filepath_flt_table}\n")
            @logobj.write("g_flt_tab_check=#{$g_flt_tab_check}\n")
            
            #local_excel = WIN32OLE.new('Excel.Application'  )
            fltbook = @xl.Workbooks.open(filepath_flt_table, :ReadOnly => true)
            
            #FLTテーブルのシート情報設定
            tab_base = tabSheet.nil? ? nil : tabSheet.cells(1,1)
            alm_type2sheet_name = {}
            fltsheet_table = {}
            
            { '3.代表レジスタ'       => [ "集約レジスタ" ], \
              '4-1.Blade FLT要因'    => [ "BLADE FLT" ], \
              '4-2.PIU FLT要因'      => [ "PIU FLT"  ], \
              '4-4.PSU FLT要因'      => [ "PSU FLT" ], \
              # S150対応 Add Start 2020/12/25 by Nishi
              '4-5.CPU FLT要因'      => [ "CPU FLT" ], \
              # S150対応 Add End 2020/12/25 by Nishi
              '5.Blade-PIU間CTN要因' => [ "BLADE-PIU CTNEQPT" ] \
            }.each { |sheetname, val|
                puts "[debug] FLT table Check #{sheetname} Start"
                # alm_typeとFLTテーブルのシート名の対応表を作成
                val.each { |alm_type|
                    alm_type2sheet_name[alm_type] = sheetname
                }
                
                #FLTテーブルのシートチェック
                begin
                    readSheets = fltbook.Worksheets(sheetname)
                rescue
                    printf("[debug] rescue at %s:%d\n", File.basename(__FILE__), __LINE__)
                    TECHINFO_Analysis_Common::popup_msg("異常通知", "[ERROR]", \
                            "'#{sheetname}'シートが #{$g_filename_flt_table} にありません。")
                    return false
                end
                
                #FLTテーブルの各シートの「被疑部品」格納列を検出
                fltsheet_info = { :sheetname => sheetname, :table_top => tab_base }
                fltsheet_table[sheetname] = fltsheet_info
                if flt_table_chache(1,fltsheet_info,readSheets) == false
                    return false
                end
                tab_base = fltsheet_info[:table_bottom]
                tab_base = tab_base.Offset(1) unless tab_base.nil?
            }
            if tabSheet != nil
                tabSheet.Columns.AutoFit
                tab_base = nil
            end
            
            puts "[debug] FLT table Check End"
            
            @logobj.write("Almlog_Analyze_Cls::flt_analyze_s150 - FLT Table Search Start\n")
            readSheets = fltbook.Worksheets('3.代表レジスタ')
            last_readSheet_name = ""
            
            if $g_step2_log_repairparts == true
                read_size = 13
            else
                read_size = 11
            end
            
            for line in 3...@flt_chk_cnt + 3 do
                #セル値を配列に設定
                #  注意) インデックスは、Excel列番号-1になる
                sheets_data = sheets.Cells(line,1).Resize(1,read_size).Value
                sheets_data.flatten!
                
                #警報対象を文字列に変換
                filename = sheets_data[0]
                alm_type = sheets_data[5]
                if alm_type.nil? then
                    if filename =~ /^NBO_PIU_flt_\d+\.log$/ then               
                        alm_type = "1"
                    end
                end
                alm_type = alm_type.to_i
                alm_type_name = alm_type_list[alm_type]
                sheets.cells(line,6).value = alm_type_name
                
                #検出デバイスを文字列に変換
                dev_type = sheets_data[6].to_i
                dev_name = dev_list[dev_type]
                sheets.cells(line,7).value = dev_name
                
                #Slot番号を文字列に変換
                # 既存バグ(整数が期待なのに小数点になる) Change Start 2020/12/11 by Nishi
                slot_no = sheets_data[7].to_i.to_s
                # 既存バグ(整数が期待なのに小数点になる) Change End 2020/12/11 by Nishi
                if (slot_no == "5")
                    sheets.cells(line,8).value = "BLADE"
                end
                
                #FLTテーブルからレジスタ名/bit名を検索
                #1. alm_type から FLTテーブルの検索シートを決定
                alm_type_defined = alm_type2sheet_name.include?(alm_type_name)
                if alm_type_defined
                    readSheet_name = alm_type2sheet_name[alm_type_name]
                end
                
                #2. base_addr
                base_addr = sheets_data[8]
                base_addr = if base_addr.nil? then "" else base_addr.hex end
                
                #3. CPU Add
                cpu_addr = sheets_data[9]
                cpu_addr = if cpu_addr.nil? then "" else cpu_addr.hex end
                
                if $g_step2_log_repairparts == true
                    alm_pattern = [alm_type, dev_type, slot_no, base_addr, cpu_addr, sheets_data[11], sheets_data[12]].join("-")
                    sheets.cells(line,alarm_pattern_col).value = alm_pattern
                end
                
                if not alm_type_defined
                    update_progress()
                    next
                end
                
                if last_readSheet_name != readSheet_name
                    last_readSheet_name = readSheet_name
                    
                    readSheets = fltbook.Worksheets(readSheet_name)
                    
                    #FLTテーブルのシート情報取り出し
                    fltsheet_info = fltsheet_table[readSheet_name]
                    if $g_step2_log_repairparts == true        
                        ref_repairParts_column = fltsheet_info[:repairPart_column]
                        ref_repairParts_num = fltsheet_info[:repairPart_num]
                        ref_repairParts_size = (ref_repairParts_num * (FLT_TBL_REPAIR_SIZE+FLT_TBL_REPAIR_PAD)) \
                                                - FLT_TBL_REPAIR_PAD
                    end
                    readSheets_data_count = fltsheet_info[:data_num]
                    
                    #検索高速化とチェックのため、キー値を前処理
                    if fltsheet_info.has_key?(:readKeys_base)
                        #printf("[debug] FLT table '%s' use chache\n", readSheet_name)
                    elsif flt_table_chache(2,fltsheet_info,readSheets) == false
                        return false
                    end
                    readKeys_base = fltsheet_info[:readKeys_base]
                    readKeys_cpu = fltsheet_info[:readKeys_cpu]
                    readKeys_bit = fltsheet_info[:readKeys_bit]
                end
                
                if $g_step2_log_repairparts == true
                    #参照シート名を設定
                    sheets.cells(line,20).value = readSheet_name
                end
                
                #FLTテーブルの検索シートを1行ずつ読みだす
                for read_index in 0...readSheets_data_count do
                    read_row = FLT_TBL_DATA_ROW + read_index
                    
                    #読み出し行に何もなければ次の行へ
                    next if(readKeys_base[read_index] == nil)
                    
                    #読み出し行のBase/CPU Addrが一致した場合
                    if( ( readKeys_base[read_index] == base_addr ) && ( readKeys_cpu[read_index] == cpu_addr ) )
                        #レジスタ名設定
                        sheets.cells(line,14).value = readSheets.cells(read_row,10).value
                        
                        reg_val = sheets_data[10].hex
                        
                        #データ側のレジスタ値に値がある場合
                        if(reg_val != 0)
                            #ON bit を抽出
                            reg_bit_list = []
                            for bit in 0...32 do
                                if(reg_val & (1 << bit) != 0)
                                    reg_bit_list << bit
                                end
                            end #for bit
                            
                            #見つかった行から下を検索
                            bit_row_list = []
                            found_bit_list = []
                            for read_index2 in read_index...readSheets_data_count do
                                bit = readKeys_bit[read_index2]
                                next if bit.nil?
                                next unless reg_bit_list.include?(bit)
                                
                                next unless( readKeys_base[read_index2] == base_addr )
                                next unless( readKeys_cpu[read_index2] == cpu_addr )
                                
                                read_row2 = FLT_TBL_DATA_ROW + read_index2
                                found_bit_list <<  bit
                                bit_row_list << read_row2
                            end
                            reg_bit_list.delete_if {|bit| found_bit_list.include?(bit) }
                            
                            #bit名称を設定
                            bit_name = []
                            bit_row_list.each do |read_row2|
                                bit_str = readSheets.cells(read_row2,11).value #bit名称取り出し
                                if(bit_str != nil)
                                    bit_name << bit_str.to_s.gsub(/\s+/," ")
                                end
                            end
                            if bit_name.size == 0
                                sheets.cells(line,15).value = "該当なし"
                            else
                                sheets.cells(line,15).value = bit_name.join("\n")
                            end
                            
                            if $g_step2_log_repairparts == true
                                #検出ＮＧのBitを末尾に追加
                                if reg_bit_list.size > 0
                                    bit_row_list.concat(Array.new(reg_bit_list.size,0))
                                    found_bit_list.concat(reg_bit_list)
                                end
                                
                                #参照行番号を設定
                                sheets.cells(line,21).value = bit_row_list.join("\n")
                                
                                #On Bitを設定
                                sheets.cells(line,22).value = found_bit_list.join("\n")
                                
                                #被疑部品を設定
                                repair_parts = Array.new(FLT_TBL_REPAIR_SIZE).map{Array.new()}
                                bit_row_list.each do |read_row2|
                                    next if read_row2 == 0
                                    
                                    read_data = readSheets.Cells(read_row2,ref_repairParts_column).Resize(1,ref_repairParts_size).Value
                                    read_data.flatten!
                                    read_data.map! { |v| v.to_s.gsub(/\s+/,' ') }
                                    col = 0
                                    ary_o = Array.new(FLT_TBL_REPAIR_SIZE).map{Array.new()}
                                    for i in 0...ref_repairParts_num
                                        ary_i = read_data[col, FLT_TBL_REPAIR_SIZE]
                                        if ( ary_i.any? { |v| v =~ /\S/ } )
                                            for j in 0...FLT_TBL_REPAIR_SIZE
                                                str_i = ary_i[j]
                                                if str_i !~ /\S/
                                                    str_i = '-'
                                                end
                                                ary_o[j] << str_i
                                            end
                                        end
                                        col += FLT_TBL_REPAIR_SIZE + FLT_TBL_REPAIR_PAD
                                    end
                                    for j in 0...FLT_TBL_REPAIR_SIZE
                                        repair_parts[j] << ary_o[j].join(",")
                                    end
                                end
                                
                                for j in 0...FLT_TBL_REPAIR_SIZE
                                    repair_parts[j] = repair_parts[j].join("\n")
                                end
                                sheets.cells(line,16).Resize(1,FLT_TBL_REPAIR_SIZE).Value = repair_parts
                            end
                        else
                            sheets.cells(line,15).value = "ALL Clear"
                        end #if(reg_val != 0)
                        
                        break #ここに来た、ということはFLTテーブルの検索が終わったので read_index のループから出る
                    end #読み出し行のBase/CPU Addrが一致した場合
                end #for read_index
                
                update_progress()
            end #for line
            @logobj.write("Almlog_Analyze_Cls::flt_analyze_s150 - FLT Table Search End\n")
            sheets.Cells.Item(1,2).value = @flt_chk_cnt
            
            #時系列にソートする
            sheets.cells(3,1).resize(@flt_chk_cnt,max_write_column).Sort({"key1" => sheets.Range("B3"), "key2" => sheets.Range("C3"), "key3" => sheets.Range("D3")})
            
            if $g_step2_log_repairparts == true
                #alarm毎にClearまでの時間を設定する
                alarm_list = sheets.cells(3,alarm_pattern_col).Resize(@flt_chk_cnt).value.flatten!
                bit_on_list = sheets.cells(3,15).Resize(@flt_chk_cnt).value.flatten!
                bit_on_list.map! {|v| v != "ALL Clear"}
                bit_on_list.each_with_index do |bit_on, i|
                    next unless bit_on
                    pattern = alarm_list[i]
                    for j in (i+1)...@flt_chk_cnt do
                        next if pattern != alarm_list[j]
                        if bit_on_list[j] == false
                            sheets_data = sheets.Cells(3+i,2).Resize(1,2).Value.flatten!
                            alarm_time = sheets_data[0] + sheets_data[1]
                            sheets_data = sheets.Cells(3+j,2).Resize(1,2).Value.flatten!
                            clear_time = sheets_data[0] + sheets_data[1]
                            sheets.cells(3+i,alarm_clear_col).value = clear_time - alarm_time
                        else
                          next
                        end
                        break
                    end
                end
                sheets.Cells(3,alarm_clear_col).Resize(@flt_chk_cnt).NumberFormatLocal = "[h]:mm:ss"
            end
            
            #罫線
            sheets.Cells(1,1).Resize(1,2).Borders.LineStyle = 1
            sheets.Cells(2,1).Resize(1+@flt_chk_cnt,max_write_column).Borders.LineStyle = 1
            
            # セル色変更
            sheets.Cells(1,1).Resize(1,2).Interior.ColorIndex = 6
            sheets.Cells(2,1).Resize(1,15).Interior.ColorIndex = 3
            
            #自動整形
            sheets.Columns.AutoFit
            
            #FLTテーブルClose
            fltbook.Close(:SaveChanges => false)
            #local_excel.quit
            
        end #if(@flt_chk_cnt != 0)
        
        @logobj.write("Almlog_Analyze_Cls::flt_analyze_s150 End\n")
        return true
    end #flt_analyze_s150
    # S150対応 Add End 2020/12/11 by Nishi

    #----------------------------------------------------------#
    # flt_table_chache
    # 概要：FLT TABLEの一次解析
    #       列番号取得、キーの抽出
    #----------------------------------------------------------#
    def flt_table_chache(step,fltsheet_info,readSheets)
        
        sheetname = fltsheet_info[:sheetname]
        tab_base = fltsheet_info[:table_top]
        
        if step == 1
            #FLTテーブルの各シートの「被疑部品」格納列を検出
            if $g_step2_log_repairparts == false
                count = 0
            else
                start_col = readSheets.UsedRange.Column
                count = readSheets.UsedRange.Columns.Count
                readHead = readSheets.Cells(FLT_TBL_HEAD_ROW,start_col).resize(1,count).value
                readHead.flatten!
                readHead.map! {|v| v.to_s}
                
                next_idx = 0
                count = 0
                readHead.each_with_index do |name, idx| 
                    next if name != '部品'
                    if count == 0
                        start_col += idx 
                    elsif idx != next_idx
                        TECHINFO_Analysis_Common::popup_msg("異常通知", "[ERROR]", \
                                "'#{sheetname}'シートの「部品」列の位置が不正です。")
                        return false
                    end
                    
                    count += 1
                    next_idx = idx + FLT_TBL_REPAIR_SIZE + FLT_TBL_REPAIR_PAD
                end
                readHead.clear
            end
            if count == 0
                start_col = 0
            end
            
            data_num = @xl.WorksheetFunction.CountA(readSheets.range("e:e")) - 2
            
            fltsheet_info[:data_num] = data_num
            fltsheet_info[:repairPart_column] = start_col
            fltsheet_info[:repairPart_num] = count
        end 
        
        if(step == 2)||(tab_base != nil)
            puts "[debug] FLT table '#{sheetname}' reading..."
            
            data_num = fltsheet_info[:data_num]
            
            readFlags = readSheets.cells(FLT_TBL_DATA_ROW, 3).resize(data_num).value
            readFlags = readFlags.flatten!
            
            readKeys = readSheets.cells(FLT_TBL_DATA_ROW,15).resize(data_num,3).value
            
            readKeys_base = Array.new(data_num,nil)
            readKeys_cpu = Array.new(data_num,nil)
            readKeys_bit = Array.new(data_num,nil)
            
            for i in 0...data_num
                if /^\s*[DＤ]\s*$/ === readFlags[i]
                    # C列の値が"D"(半角または全角)の行は、無視する。
                    readFlags[i] = "D"
                    next
=begin
                #注意) 取り消し線で判定すると、処理時間が少し(数秒？)増える
                elsif readSheets.cells(FLT_TBL_DATA_ROW+i,4).Font.Strikethrough == true
                    # D列(No.)のセルに取り消し線のある行は、無視する。
                    readFlags[i] = "取り消し線"
                    next
=end 
                end
                
                readFlags[i] = "NoAddress"
                next if(readKeys[i][0] == nil)
                next if(readKeys[i][1] == nil)
                
                readFlags[i] = nil
                begin
                    for j in 0..2
                        key_val = readKeys[i][j]
                        if key_val.nil?
                            key_val = ""
                        elsif key_val.to_s =~ /^\s*-*\s*$/
                            key_val = ""
                        else
                            key_val = key_val.to_s
                            case j
                            when 0,1 then
                                if key_val =~ /^(0[xX])?\h+$/
                                    key_val = key_val.hex
                                end
                            else
                                if key_val =~ /^\d+(\.0+)?$/
                                    key_val = key_val.to_i
                                end
                            end #case j
                        end
                        
                        case j
                        when 0 then
                            readKeys_base[i] = key_val
                        when 1 then
                            readKeys_cpu[i] = key_val
                        else
                            readKeys_bit[i] = key_val
                        end #case j
                    end #for j
                rescue
                    TECHINFO_Analysis_Common::popup_msg("異常通知", "[ERROR]", \
                            "'#{sheetname}'シート:#{FLT_TBL_DATA_ROW+i}行目: Base Add/CPU Add/bit 列の値が不正です。")
                    return false
                end
            end #for i
            
            if tab_base != nil
                if tab_base.row == 1
                    tab_base.resize(1,6).value = ["シート名","行","無効","Base Addr","Cpu Addr","Bit"]
                    tab_base = tab_base.Offset(1)
                    fltsheet_info[:table_top] = tab_base
                end
                
                if data_num > 0
                    tab_area = tab_base.Resize(data_num,6)
                    tab_area.columns(1).value = sheetname
                    tab_area.columns(2).value = (0...data_num).map {|v| [FLT_TBL_DATA_ROW + v]}
                    tab_area.columns(3).value = readFlags.map {|v| [v]}
                    tab_area.columns(4).value = readKeys_base.map {|v|[ v.is_a?(Integer)? ("0x" << v.to_s(16)) : v ]}
                    tab_area.columns(5).value = readKeys_cpu.map {|v| [ v.is_a?(Integer)? ("0x" << v.to_s(16)) : v ]}
                    tab_area.columns(6).value = readKeys_bit.map {|v| [v]}
                 end
                 tab_base = tab_base.Offset(data_num-1)
            end
            
            readKeys.clear
            
            fltsheet_info[:table_bottom] = tab_base
            fltsheet_info[:readKeys_base] = readKeys_base
            fltsheet_info[:readKeys_cpu] = readKeys_cpu
            fltsheet_info[:readKeys_bit] = readKeys_bit
        end
        
        return true
    end #flt_table_chache
    
    #----------------------------------------------------------#
    # techinfo_flt_load
    # 概要：FLT情報の解析1
    #       logファイルの読み込みおよびエクセルへの出力
    #----------------------------------------------------------#
    def techinfo_flt_load(filename,data,sheets)
        
        last_type = 0   #最終行の処理種別
        last_data = ""  #最終行のテキスト
        final_data = ""
        line_count = 0
        
        normal_pattern = /(\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}.\d{6} ###\w* \w*###)/o
        
        #cyclicにloggingされるので、log の各データをみる前に
        #1行のデータを完全なものにする
        data.each_line  do |read_data|
            #NULL文字を排除したうえで文字列として連結させる
            read_data = read_data.unpack("Z*").join(",").delete("\n")
            last_data = read_data
            
            #正常データ
            if normal_pattern === read_data
                @flt_chk_cnt += 1
                line_count  = @flt_chk_cnt + 2 #ヘッダ分
                sheets.Cells(line_count, 1).value = filename
                sheets.Cells(line_count, 2).value = read_data
                last_type = 1
            else
                #異常データ
                #logのとり始めのデータは要らない
                #データが多かった場合はcyclicにloggingされる
                #その場合は必ず日付の部分が完全ではない
                #1行目だった場合はループしたデータなので保存する
                if(last_type == 0)
                   final_data = read_data
                   last_type = 2
                elsif read_data != "" 
                   #2行目以降で空でなければ、ループデータに続く可能性がある
                   last_type = 3
                else
                   last_type = 4
                end
            end
        end
        
        #ループデータがあれば、最後の行にくっつける
        if(final_data != "")
            case last_type
            when 1 then
                sheets.Cells(line_count, 2).value += final_data
            when 3 then
                # 最後の行が異常データの場合、
                # ループデータをくっつけて正常になれば、次の行に格納する。
                last_data += final_data
                if normal_pattern === last_data
                    @flt_chk_cnt += 1
                    line_count  = @flt_chk_cnt + 2 #ヘッダ分
                    sheets.Cells(line_count, 1).value = filename
                    sheets.Cells(line_count, 2).value = last_data
                end
            end
        end
        sheets.Cells.Item(1,2).value = @flt_chk_cnt
    end #techinfo_flt_load
    
    #----------------------------------------------------------#
    # techinfo_flt_analyze
    # 概要：FLT情報の解析2
    #       FLT logのデータを分解する
    #----------------------------------------------------------#
    def techinfo_flt_analyze(sheets)
        write_data = Array.new(12)
        for line in 3...@flt_chk_cnt + 3 do
            
            line_data = sheets.Cells(line, 2).value
            
            write_data.fill("")
            line_data.match(/\d{4}\/\d{2}\/\d{2}/) { |md| write_data[0] = md[0] }
            line_data.match(/\d{2}:\d{2}:\d{2}/)   { |md| write_data[1] = md[0] }
            line_data.match(/.\d{6}/)              { |md| write_data[2] = md[0] }
            
            ###Clear Alarm### alm_type = 0, dev = 3015, slot_no = 0, base_addr = 0x0, addr = 0x0, val = 0x1, chip = 0, offset = 0
            write_data[3] = line_data.scan(/(###\w* \w*###)/).join(",").delete("\#")
            write_data[4] = line_data.scan(/alm_type = \d{1,}/).join(",").gsub("alm_type = ","")
            write_data[5] = line_data.scan(/dev = \d{4,}/).join(",").gsub("dev = ","")
            write_data[6] = line_data.scan(/slot_no = \d{1,}/).join(",").gsub("slot_no = ","")
            write_data[7] = line_data.scan(/base_addr = 0x\h{8,}/).join(",").gsub("base_addr = ","")
            write_data[8] = line_data.scan(/ addr = 0x\h{1,}/).join(",").gsub(" addr = ","")
            write_data[9] = line_data.scan(/val = 0x\h{1,}/).join(",").gsub("val = ","")
            write_data[10] = line_data.scan(/chip = \d{1,}/).join(",").gsub("chip = ","")
            write_data[11] = line_data.scan(/offset = \d{1,}/).join(",").gsub("offset = ","")
            
            sheets.Cells(line, 2).resize(1,12).value = write_data
            if(write_data[3] =~ /Detect/)
                sheets.cells(line,1).resize(1,13).Interior.ColorIndex = 4
            end
        end
    end
    
    #----------------------------------------------------------#
    # analyze_stschange
    # 概要：警報変化点log解析
    #       ud_alm_stsch_log_load：ファイルを読み込む。
    #       ud_alm_stsch_log_analyze:結果の並び替えや諸々の後始末。
    #----------------------------------------------------------#
    def analyze_stschange(sheets,path,book)
        @logobj.write("Almlog_Analyze_Cls::analyze_stschange Start\n")
        
        write_data = Array.new(2).map{Array.new(19, "")}
        
        write_data[0][0] = "データ数"
        write_data[1][0] = "File名"
        write_data[1][1] = "Date"
        write_data[1][2] = "Time"
        write_data[1][3] = "Time(.sec)"
        
        write_data[1][4] = "CIR name"
        write_data[1][5] = "shelf"
        write_data[1][6] = "slot"
        write_data[1][7] = "subslot"
        write_data[1][8] = "port"
        write_data[1][9] = "subport"
        write_data[1][10] = "first_index"
        write_data[1][11] = "second_index"
        write_data[1][12] = "pg"
        write_data[1][13] = "type_index"
        write_data[1][14] = "cond_id"
        write_data[1][15] = "prev_set_val"
        write_data[1][16] = "curr_set_val"
        write_data[1][17] = "curr_out_val"
        write_data[1][18] = "発生/回復"
        
        sheets.Range("D:D").NumberFormatLocal = "0.000000_ "

        # Excelに書き込み
        sheets.Range(sheets.cells(1,1), sheets.cells(2,19)).value = write_data
        
        @stschgchk_count = 0
        
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filedata = entry.read
                if (filedata != nil)
                    #変化点log
                    if(entry.name.include?("cm_alm.LOG") or entry.name.include?("cm_name.LOG") )
                        $g_step2_sheetname = "警報変化点ログ解析"
                        
                        @logobj.write(sprintf("変化点log file=%s size=%s\n", entry.name, entry.size))
                        
                        data = StringIO.new(filedata)
                        
                        if entry.name.include?("cm_alm.LOG")
                            ud_alm_stsch_log_load(entry.name,data,book)
                        end
                        if entry.name.include?("cm_name.LOG")
                            ud_alm_stsch_cir_name_check(entry.name,data,book)
                        end
                    end #if g_step2_log_stschgchk
                end #if filedata
            end # do |entry|
        end # do |tgz|
        
        if @stschgchk_count != 0
            #警報変化点log解析後始末
            chk_log_max_set(@stschgchk_count,"警報変化点ログ")
            
            ud_alm_stsch_log_analyze(book)
            #並べ替え、警報名変換
            
        end
        @logobj.write("Almlog_Analyze_Cls::analyze_stschange End\n")
    end #analyze_stschange
    
    #----------------------------------------------------------#
    # ud_alm_stsch_log_analyze
    # 概要：警報変化点log解析
    #       エクセルのデータを整形する
    #----------------------------------------------------------#
    def ud_alm_stsch_log_analyze(book)
        sheets = book.Worksheets($g_step2_sheetname)
        
        cir_errors = {}
        
        write_data = Array.new(18)
        for line in 3...@stschgchk_count + 3 do
            
            line_data = sheets.Cells(line, 2).value
            
            write_data.fill("")
            
            ##########################
            #時刻とほかのデータを分離#
            ##########################
            #ソート用に時刻を分解
            line_data.match(/\d{4}\/\d{2}\/\d{2}/) { |md| write_data[0] = md[0] }
            line_data.match(/\d{2}:\d{2}:\d{2}/)   { |md| write_data[1] = md[0] }
            line_data.match(/.\d{6}/)              { |md| write_data[2] = md[0] }
            
            #データから時刻データを削除
            line_data.gsub!(/\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}.\d{6} /,"")
            
            ##########################
            #各データを分離          #
            ##########################
            line_data.match(/\w*-\w*/) { |md| write_data[3] = md[0] }
            write_data[4] = line_data.scan(/sh=\d{1,}/).join(",").delete("sh=")
            write_data[5] = line_data.scan(/sl=\d{1,}/).join(",").delete("sl=")
            write_data[6] = line_data.scan(/ss=\d{1,}/).join(",").delete("ss=")
            write_data[7] = line_data.scan(/po=\d{1,}/).join(",").delete("po=")
            write_data[8] = line_data.scan(/sp=\d{1,}/).join(",").delete("sp=")
            write_data[9] = line_data.scan(/fi=\d{1,}/).join(",").delete("fi=")
            write_data[10] = line_data.scan(/si=\d{1,}/).join(",").delete("si=")
            write_data[11] = line_data.scan(/pg=\d{1,}/).join(",").delete("pg=")
            write_data[12] = line_data.scan(/ty=\d{1,}/).join(",").delete("ty=")
            write_data[13] = line_data.scan(/cond_id=\d{1,}/).join(",").delete("cond_id=")
            
            prev_val = line_data.scan(/prev=\d{1,}/).join(",").delete("prev=").to_i
            
            set_val  = nil
            out_val  = nil
            
            write_data[14] = "0x" + prev_val.to_s(16)
            if(line_data =~ /set=\d{1,}/)
                set_val  = line_data.scan(/set=\d{1,}/).join(",").delete("set=").to_i
                write_data[15] = "0x" + set_val.to_s(16)
            end
            if(line_data =~ /out=\d{1,}/)
                out_val  = line_data.scan(/out=\d{1,}/).join(",").delete("out=").to_i
                write_data[16] = "0x" + out_val.to_s(16)
            end
            
            #発生/回復
            flag19 = true
            
            if(prev_val == 0)
                flag19 = false
            end
            if(set_val == nil)
                if(prev_val < out_val)
                    flag19 = false
                end
            end
            if(out_val == nil)
                if(prev_val < set_val)
                    flag19 = false
                end
            end
            
            if (flag19)
                write_data[17] = "回復"
            else
                write_data[17] = "発生"
                sheets.Cells(line,1).Resize(1,19).Interior.ColorIndex = 4
            end
            
            #CIR種別ごとに警報名を文字列に変換する
            cir_type = write_data[3]
            cond_type = write_data[13]
            
            if not cir_errors.include?(cir_type) then
                cir_errors[cir_type] = 0
            end
            
            if cir_errors[cir_type] != 1 then
                begin
                    step = 0
                    cir_sheet = book.Worksheets(cir_type + "CIR")
                    
                    # 既存バグ対応 Change Start 2020/12/11 by Nishi
                    # Findメソッドが正しく動かないので列の先頭から順番にCond_idを文字列比較に変更、
                    step = 1
                    checkcount = 1
                    while cir_sheet.Cells(checkcount,1).value != nil do
                    	# 文字列が一致したら抜け出す
                        if (cir_sheet.Cells(checkcount,1).value.floor.to_s == cond_type.to_s) then
                            step = 2
                            cond_id = cir_sheet.Cells(checkcount,2).value
                            break
                        end
                        checkcount = checkcount + 1
                    end
                    # 既存バグ対応 Change End 2020/12/11 by Nishi

                    step = 3                    
                    write_data[13] = cond_id
                rescue
                    #printf("[debug] CIR ERROR step=%d, cir_type=%s, cond_type=%s\n", step, cir_type, cond_type)
                    if(step == 0) then
                        wk_str = sprintf("'%s CIR' not found in *cm_name.LOG", cir_type)
                        cir_errors[cir_type] = 1 # CIRの定義が見つからない
                    elsif(cir_errors[cir_type] == 0) then
                        wk_str = sprintf("*cm_name.LOG: cond_id '%s' not defined in '%s CIR'", cond_type, cir_type)
                        cir_errors[cir_type] = 2 # cond_typeが登録されていない
                                                 # 注意) 登録されていて値が無い場合、ここでは検出できない
                    else
                        wk_str = nil
                    end
                    
                    if not wk_str.nil? then
                        printf("WARNIG!! %s\n", wk_str)
                        @logobj.write(sprintf("WARNIG!! %s\n", wk_str))
                    end
                end
            end
            
            sheets.Cells(line, 2).Resize(1,18).value = write_data
            
            update_progress()
        end
        
        #時系列にソートする
        sheets.Range(sheets.cells(3,1), sheets.cells(line,19)).Sort({"key1" => sheets.Range("B3"), "key2" => sheets.Range("C3"), "key3" => sheets.Range("D3")})
        
        #罫線
        sheets.Range(sheets.cells(1,1),sheets.cells(1,2)).Borders.LineStyle = 1
        sheets.Range(sheets.cells(2,1), sheets.cells(line,19)).Borders.LineStyle = 1
        
        # セル色変更
        sheets.Range(sheets.cells(1,1),sheets.cells(1,2)).Interior.ColorIndex = 6
        sheets.Range(sheets.cells(2,1),sheets.cells(2,19)).Interior.ColorIndex = 3
        
        #自動整形
        sheets.Columns.AutoFit
        
        #不要シート削除
        sheetcount = book.Sheets.Count
        @xl.DisplayAlerts = false
        sheetcount.downto(1) do |i|
            if( book.Sheets(i).Name =~ /CIR/ )
                book.Sheets(i).delete
            end
        end
        
        @xl.DisplayAlerts = true
        
    end
    
    #----------------------------------------------------------#
    # ud_alm_stsch_log_load
    # 概要：警報変化点log解析
    #       ファイルを読み込む。結果をエクセルへ吐き出す
    #----------------------------------------------------------#
    def ud_alm_stsch_log_load(filename,data,book)
        
        sheets = book.Worksheets($g_step2_sheetname)
        first_line = 0
        final_data = ""
        line_count = 0
        file_name = File.basename(filename)
        #cyclicにloggingされるので、log の各データをみる前に
        #1行のデータを完全なものにする
        data.each_line  do |read_data|
            #NULL文字を排除したうえで文字列として連結させる
            read_data = read_data.unpack("Z*").join(",").delete("\n")
            
            first_line += 1
            
            #正常データ
            if /(\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}.\d{6})/ =~ read_data
                @stschgchk_count += 1
                line_count  = @stschgchk_count + 2 #ヘッダ分
                sheets.Cells(line_count, 1).value = file_name
                sheets.Cells(line_count, 2).value = read_data
            else
                #異常データ
                #データが多かった場合はcyclicにloggingされる
                #その場合は必ず日付の部分が完全ではない
                #1行目だった場合はループしたデータなので保存する
                if(first_line == 1)
                    final_data = read_data
                else #1行目以外は本当に異常なものなので捨てる
                end
            end
        end
        if(final_data != "")
            sheets.Cells(line_count, 2).value += final_data
        end
        sheets.Cells.Item(1,2).value = @stschgchk_count
     end
    #----------------------------------------------------------#
    # ud_alm_stsch_cir_name_check
    # 概要：警報変化点log解析
    #       CIR種別を示す各ファイルを読み込み、エクセルに落とす。
    #----------------------------------------------------------#
    def ud_alm_stsch_cir_name_check(filename,data,book)
        check_flag = 0
        
        sheets = nil
        row = 1
        column = 1
        
        data.each_line do |read_data|
        
            #NULL文字を排除したうえで文字列として連結させる
            read_data = read_data.unpack("Z*").join(",").delete("\n")
            
            #logに日付が入っていた場合の対策
            if /(\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}.\d{6})/ =~ read_data
                read_data = read_data.gsub!(/\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}.\d{6}/,"")
            end
            
            if(read_data =~ /####/ )
                check_flag = 1
                row = 1
                sheet_name = read_data.delete("\s#")
#                if(sheet_name == "ctn-almCIR")
#                    sheet_name = "ctneqpt-almCIR"
#                end
                sheets = excel_sheet_add(sheet_name,book)
                next
            end
            
            if(sheets != nil)
              #改行のみの行
              if(read_data == "\n")
                    check_flag = 0
                    sheets = nil
              else
                    sheets.Range(sheets.cells(row,column), sheets.cells(row,column+1)).value = read_data.delete("\s").chomp.split(",")
                    row += 1
              end
            end
        end #do |read_data|
    end #ud_alm_stsch_cir_name_check
    #----------------------------------------------------------#
    # analyze_memory_ecc_error
    # 概要：警報メモリECCエラーログ解析
    #       
    #----------------------------------------------------------#
    def analyze_memory_ecc_error(sheets,path)
        
        data_count = 0
        data_count2 = 0
        
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filedata = entry.read
                if (filedata != nil)
                    #変化点log
                    if(entry.name.include?("ud_softerr.LOG"))
                        
                        @logobj.write(sprintf("警報メモリECCエラー file=%s size=%s\n", entry.name, entry.size))
                        
                        data = StringIO.new(filedata)
                        
                        data_count = sort_data_by_time(data,sheets)
                    end
                end
            end
        end
        
        #そもそもの対象外行を削除
        if(data_count != 0)
            delete_line = 0
            for line in 1...data_count + 1 do
                line_data = sheets.cells(line,4).value
                if line_data =~ /SOAM-FPGA CRAM/ or line_data =~ /####/
                    #CRAM   analyze_fpga_soft_errorで処理
                    #####   一行目
                    sheets.Rows(line).delete
                    delete_line += 1
                    redo
                end
            end
            data_count -= delete_line
             
        end
        
        if(data_count != 0)
            delete_line = 0
            #フォーマットに沿ってとられている先頭までを探す(それまでのデータは捨てる)
            #(WBのところから始まっているような場合を想定)
            for line in 1...data_count + 1 do
                line_data = sheets.cells(line,4).value
                if(line_data =~ /SOAM-FPGA BRAM/)
                    break
                else
                    sheets.Rows(line).delete
                    delete_line += 1
                    redo
                end
            end
            data_count -= delete_line
            
            #正常データについて解析
            chk_log_max_set(data_count,"警報メモリECCエラーログ")
            sleep 0.001
            for line in 1...data_count + 1 do
                line_data = sheets.cells(line,4).value
                
                error_type  = ""
                addr        = ""
                data        = ""
                wb          = ""
                data_before = ""
                data_after  = ""
                
                if line_data =~ /SOAM-FPGA BRAM/
                    #エラー種別
                    end_pos_error_type = line_data.index("[LOG]")
                    error_type = line_data[0,end_pos_error_type]
                    #ADDR
                    addr = line_data.scan(/Addr:0x\w+/).join(",").gsub(/Addr:/,"")
                    #DATA
                    data = line_data.scan(/Data:0x\w+/).join(",").gsub(/Data:/,"")
                    #WB
                    wb = line_data.scan(/\[WB\]\w+/).join(",").gsub(/\[WB\]/,"")
                else #[WB]Yes
                    #エラー種別
                    error_type = "write back"
                    #ADDR
                    addr = line_data.scan(/Addr:0x\w+/).join(",").gsub(/Addr:/,"")
                    #DATA(before)
                    data_before = line_data.scan(/before:0x\w+/).join(",").gsub(/before:/,"")
                    #DATA(After)
                    data_after = line_data.scan(/after:0x\w+/).join(",").gsub(/after:/,"")
                end
                sheets.cells(line, 4).value = error_type
                sheets.cells(line, 5).value = addr
                sheets.cells(line, 6).value = data
                sheets.cells(line, 7).value = wb
                sheets.cells(line, 8).value = data_before
                sheets.cells(line, 9).value = data_after
                
                update_progress()
                sleep 0.001
            end
        end
        
        sheets.Rows(1).Insert
        data_count += 1
        
        sheets.Range("C:C").NumberFormatLocal = "0.000000_ "
        
        #ヘッダ部分
        sheets.Cells(1,  1).Value = "Date"
        sheets.Cells(1,  2).Value = "Time"
        sheets.Cells(1,  3).Value = "Time(.sec)"
        
        sheets.cells(1,  4).value = "エラー種別"
        sheets.cells(1,  5).value = "ADDR"
        sheets.cells(1,  6).value = "DATA"
        sheets.cells(1,  7).value = "WB"
        sheets.cells(1,  8).value = "DATA(before)"
        sheets.cells(1,  9).value = "DATA(After)"
        
        # セル色変更
        sheets.Range(sheets.cells(1,1), sheets.cells(1,9)).Interior.ColorIndex = 6
        
        #罫線
        sheets.Range(sheets.cells(1,1), sheets.cells(data_count,9)).Borders.LineStyle = 1
        #自動整形
        sheets.Columns.AutoFit
        
    end #analyze_memory_ecc_error
    #----------------------------------------------------------#
    # analyze_fpga_soft_error
    # 概要：FPGAソフトエラーログ解析
    #       
    #----------------------------------------------------------#
    def analyze_fpga_soft_error(sheets,path)
        
        data_count = 0
        
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filedata = entry.read
                if (filedata != nil)
                    #変化点log
                    if(entry.name.include?("ud_softerr.LOG"))
                        
                        @logobj.write(sprintf("警報メモリECCエラー file=%s size=%s\n", entry.name, entry.size))
                        
                        data = StringIO.new(filedata)
                        
                        data_count = sort_data_by_time(data,sheets)
                    end
                end
            end
        end
        
        if(data_count != 0)
            delete_line = 0
            for line in 1...data_count + 1 do
                line_data = sheets.cells(line,4).value
                if line_data =~ /SOAM-FPGA BRAM/ or line_data =~ /####/ or line_data =~ /  \[WB\]/
                    #BRAM   analyze_memory_ecc_errorで処理
                    #  [WB] analyze_memory_ecc_errorで処理
                    #####   一行目なのでいらない
                    sheets.Rows(line).delete
                    delete_line += 1
                    redo
                end
            end
            data_count -= delete_line
            
        end
        
        if(data_count != 0)
            chk_log_max_set(data_count,"FPGAソフトエラーログ")
            sleep 0.001
            for line in 1...data_count + 1 do
                line_data = sheets.cells(line,4).value
                
                error_type  = ""
                addr        = ""
                data        = ""
                
                end_pos_error_type = line_data.index("[LOG]")
                error_type = line_data[0,end_pos_error_type]
                #ADDR
                addr = line_data.scan(/Addr:0x\w+/).join(",").gsub(/Addr:/,"")
                #DATA
                start_pos_data = line_data.index("Data:") + 5 
                end_pos_data = line_data.index("\[WB\]") 
                data = line_data[start_pos_data,end_pos_data - start_pos_data].gsub(/ /,"\n").chomp
                
                sheets.cells(line, 4).value = error_type
                sheets.cells(line, 5).value = addr
                sheets.cells(line, 6).value = data
                
                update_progress()
                sleep 0.001
            end
        end
        
        sheets.Rows(1).Insert
        data_count += 1
        
        sheets.Range("C:C").NumberFormatLocal = "0.000000_ "
        
        #ヘッダ部分
        sheets.Cells(1,  1).Value = "Date"
        sheets.Cells(1,  2).Value = "Time"
        sheets.Cells(1,  3).Value = "Time(.sec)"
        
        sheets.cells(1,  4).value = "エラー種別"
        sheets.cells(1,  5).value = "ADDR"
        sheets.cells(1,  6).value = "DATA"
        
        # セル色変更
        sheets.Range(sheets.cells(1,1), sheets.cells(1,6)).Interior.ColorIndex = 6
        
        #罫線
        sheets.Range(sheets.cells(1,1), sheets.cells(data_count,6)).Borders.LineStyle = 1
        
        sheets.Columns(6).ColumnWidth = 18
        #自動整形
        sheets.Columns.AutoFit
        
    end #analyze_fpga_soft_error
    #----------------------------------------------------------#
    # analyze_ffan_proc
    # 概要：FFANログ解析 (温度Over検出)
    #
    # 入力形式:
    # 2017/06/08 10:05:46.847468 [INFO][959be400] void frdm_card_temp_t::print_card_temp_data()(158): \
    #                                    [index:0/slot:1] temp/tresh:[126/125][124/125]〜[89/125][0/0]
    #
    #----------------------------------------------------------#
    def analyze_ffan_proc(sheets,path)
        @logobj.write("Almlog_Analyze_Cls::analyze_ffan_proc Start\n")
        
        data_count = 0
        
        select_p = / temp\/tresh:|[\W]FLT[\W$]/o
        
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filename = File.basename(entry.name)
                if(filename == 'ffan_proc.LOG')
                    filedata = entry.read
                    if (filedata != nil)
                        @logobj.write(sprintf("FFANログ解析 log file=%s size=%s\n", entry.name, entry.size))
                        data = StringIO.new(filedata)
                        data_count = sort_data_by_time(data,sheets) {|v| 
                            # ※抽出条件 block
                            (v !~ /\[DEBUG\]/)&&(select_p === v)
                        }
                    end
                end
                if data_count > 0
                    break
                end
            end
        end
        
        header_data = ["Date","Time","Time(.sec)","Level","Source","Message","index","slot","partsNo"]
        header_size = header_data.size
        write_size = header_size-3
        
        if $g_step2_log_ffanchk == 2
            # 被疑部品も出力する場合
            header_data << "被疑部品"     #repa_data[0]
            header_data << "MAIN/SUB"     #repa_data[1]
            header_data << "部品番号"     #repa_data[2]
            header_data << "実装位置名"   #repa_data[3]
            header_data << "参照シート" #repa_data[4]
            header_data << "参照行"     #repa_data[5]
            
            repa_size = 6
            header_size += repa_size
        else
            repa_size = 0
        end
        
        temp_count = 0
        if(data_count > 0)
            if repa_size > 0
                #FLTテーブルのシート取得
                filepath_flt_table = TECHINFO_Analysis_Common::flt_table_path_get()
                if filepath_flt_table.nil?
                    return false
                end
                puts "* FLT table=#{filepath_flt_table}"
                @logobj.write("FLT table=#{filepath_flt_table}\n")
                fltbook = @xl.Workbooks.open(filepath_flt_table, :ReadOnly => true)
                
                readSheet_name = "部品変換表"
                begin
                    readSheets = fltbook.Worksheets(readSheet_name)
                rescue
                    printf("[debug] rescue at %s:%d\n", File.basename(__FILE__), __LINE__)
                    TECHINFO_Analysis_Common::popup_msg("異常通知", "[ERROR]", \
                            "'#{readSheet_name}'シートが #{$g_filename_flt_table} にありません。")
                    return false
                end
                
                #FLTテーブルシートの「被疑部品」格納列をチェック
                if readSheets.Cells(PARTS_TBL_HEAD_ROW,PARTS_TBL_DATA_COLUMN).value != 'Parts#'
                    TECHINFO_Analysis_Common::popup_msg("異常通知", "[ERROR]", \
                            "'#{readSheet_name}'シートの「Parts#」列の位置が不正です。")
                    return false
                end
                
                readSheets_data_count = @xl.WorksheetFunction.CountA(readSheets.Columns(PARTS_TBL_DATA_COLUMN)) - 2
                if readSheets_data_count < 1
                     TECHINFO_Analysis_Common::popup_msg("異常通知", "[ERROR]", \
                            "'#{readSheet_name}'シートの「Parts#」列に有効なデータがありません。")
                    return false
                end
                readKeys_partsno = readSheets.cells(PARTS_TBL_DATA_ROW,PARTS_TBL_DATA_COLUMN).resize(readSheets_data_count,1).value
                readKeys_partsno.flatten!
            end #if repa_size > 0
            
            #正常データについて解析
            chk_log_max_set(data_count, 'FFANログ')
            sleep 0.001
            write_data = Array.new(write_size)
            for line in 1..data_count do
                write_data.fill("")
                line_data = sheets.cells(line,4).value.to_s.strip
                
                # ログレベル抽出
                if line_data.sub!(/^(\[[^\]]*\])\s*/,'')
                    write_data[0] = $1
                end
                # index, slot 抽出
                if line_data.sub!(/\s*\[index:(\d+)\/slot:(\d+)\]\s*/,' ')
                    write_data[3] = $1
                    write_data[4] = $2
                end
                # 発生元抽出
                if line_data.sub!(/^\s*(.*?[^\:]): \s*/,'')
                    write_data[1] = $1
                end
                # 温度Overデータ抽出
                if line_data.sub!(/^(temp\/tresh):((\[\d+\/\d+\])+)$/,'\1')
                    temp_data = $2.scan(/\[\d+\/\d+\]/)
                    over_data = Array.new()
                    for i in 0...temp_data.size do
                        v = temp_data[i].scan(/\d+/).map {|x| x.to_i}
                        if (v[0] < v[1])
                        elsif (v[0]==0)&&(v[1]==0)
                        else
                            over_data << i
                        end
                    end
                    
                    write_data[5] = over_data.join(',')
                    
                    if repa_size == 0 # 被疑部品を出力しない場合
                    elsif over_data.size == 0 # 温度Overが無い場合
                    else
                        # 被疑部品を出力
                        is_blade = (write_data[3] == "0") #indexが 0(blade)の場合
                        
                        repa_data = Array.new(repa_size).map{Array.new(over_data.size)}
                        for i in 0...over_data.size do
                            parts_no = over_data[i]
                            if is_blade 
                                #FLTテーブルからpart_noを検索
                                j = readKeys_partsno.index(parts_no)
                            else
                                j = nil
                            end
                            
                            if j.nil?
                                read_row = 0
                                wk_str = [ "Parts[#{parts_no}]", '-', '-', '-']
                            else
                                read_row = PARTS_TBL_DATA_ROW + j
                                wk_str = readSheets.cells(read_row,PARTS_TBL_DATA_COLUMN+1).resize(1,4).value
                                wk_str.flatten!
                                wk_str.map! {|v| v.to_s.gsub(/[\s]+/," ")}
                            end
                            
                            for j in 0..3
                                repa_data[j][i] = wk_str[j]
                            end #for j
                            
                            repa_data[5][i] = read_row #参照行
                        end #for i
                        for j in 0...repa_size
                            repa_data[j] = repa_data[j].join("\n")
                        end
                        if is_blade
                            repa_data[4] = readSheet_name #参照シート名
                        else
                            repa_data[4] = ""
                        end
                        
                        sheets.Cells(line, 4+write_size).Resize(1,repa_size).value = repa_data
                    end
                    repa_data = nil
                else
                    temp_data = nil
                end
                write_data[2] = line_data
                
                # 長すぎる文字列は切り詰める
                write_data.map! {|v| (v.size <= 100)? v : v[0..99] << ' ...'}
                if(! temp_data.nil?)
                    temp_data.map! {|v| (v.size <= 100)? v : v[0..99] << ' ...'}
                end
                
                sheets.Cells(line, 4).Resize(1,write_size).value = write_data
                if(! temp_data.nil?)
                    sheets.Cells(line, header_size+1).Resize(1,temp_data.size).value = temp_data
                    temp_count = temp_data.size if(temp_count < temp_data.size)
                    
                    if(! over_data.empty?)
                        sheets.Cells(line, 1).Resize(1,header_size).interior.colorindex = 6 #黄色
                        over_data.map {|i| sheets.Cells(line, header_size+i+1).Font.color = 255} #赤色
                    end
                end
                
                update_progress()
                sleep 0.001
            end #for line
        end #if(data_count > 0)
        
        sheets.Rows(1).Insert
        data_count += 1
        
        sheets.Range("C:C").NumberFormatLocal = "0.000000_ "
        
        #ヘッダ部分出力
        if(temp_count>0)
            header_data.concat((0...temp_count).map {|i| "parts#{i}"})
            header_size = header_data.size
        end
        sheets.Cells(1,1).Resize(1, header_size).value = header_data
        
        #罫線
        sheets.Cells(1,1).Resize(data_count,header_size).Borders.LineStyle = 1
        #自動整形
        sheets.Columns.AutoFit
        for i in 4..6
            if sheets.Columns(i).ColumnWidth > 20
                sheets.Columns(i).ColumnWidth = 20
            end
        end
        @logobj.write("Almlog_Analyze_Cls::analyze_ffan_proc End\n")
    end #analyze_ffan_proc
    
    # S150対応 Add Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # analyze_ffan_proc_s150
    # 概要：FFANログ解析(S150)
    #
    # 入力形式:
    # 2020-07-02T18:37:27+09:00 fujitsu-s150-111 00023 alarm-notification: resource=Slot-1/FAN1 \
    # alarm-type-id=equipmentFault alarm-type-qualifier="not-applicable near-end" \
    # event-time="2020-07-02T18:37:27.497085+09:00" perceived-severity="critical" \
    # alarm-text="Equipment Fault:Type=ACS1-AG11" is-service-affecting=true
    #
    #----------------------------------------------------------#
    def analyze_ffan_proc_s150(sheets,path)
        @logobj.write("Almlog_Analyze_Cls::analyze_ffan_proc_s150 Start\n")
        
        data_count = 0
        write_count = 2
               
        #必要ファイルの読み込み
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filedata = entry.read
                if (filedata != nil)
                    
                    filename = File.basename(entry.name)
                    
                    if ( filename == "customer-syslog.log" )
                        @logobj.write(sprintf("FFANログ解析(S150) log file=%s size=%s\n", entry.name, entry.size))
                        data = StringIO.new(filedata)
                        write_count = unitinfo_analyze_ffan_proc(filename,data,sheets)
                    end
                end
            end
        end
 
        header_data = ["Date","Time","Time(.sec)","resource","alarm-type","alarm-type-qualifier","perceived-severity","alarm-text","is-service-affecting"]
        header_size = header_data.size
        sheets.Cells(1,1).Resize(1, header_size).value = header_data
        
        sheets.Range("C:C").NumberFormatLocal = "0.000000_ "
        
        #罫線
        sheets.Cells(1,1).Resize(write_count,header_size).Borders.LineStyle = 1
        #自動整形
        sheets.Columns.AutoFit
        
        @logobj.write("Almlog_Analyze_Cls::analyze_ffan_proc_s150 End\n")
    end #analyze_ffan_proc_s150
    # S150対応 Add End 2020/12/11 by Nishi
    
    # S150対応 Add Start 2020/12/11 by Nishi
    #----------------------------------------------------------#
    # unitinfo_analyze_ffan_proc
    # 概要：FFANログ情報を収集
    #       
    #----------------------------------------------------------#
    def unitinfo_analyze_ffan_proc(filename,data,sheets)
    
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_ffan_proc Start\n")
        
        write_count = 2
        
        data.each_line  do |read_data|
       
            #NULL文字を排除したうえで文字列として連結させる
            read_data = read_data.unpack("Z*").join(",").delete("\n")
            
            if (read_data =~ /resource=/)
            
                output_flag = false
            
                ffandata_start_pos = read_data.index("resource=")
                ffandata_info      = read_data[ffandata_start_pos,read_data.length - ffandata_start_pos + 1]
                
                split_ffandata = ffandata_info.split(" ")

                split_ffandata.each do |read_ffandata|

                    check_data = read_ffandata.split("=")

                    if (check_data[0] == "resource")
                        sheets.Cells(write_count, 4).Value = check_data[1]
                        if check_data[1].to_s.index("FAN") != nil
                            output_flag = true
                        end
                    elsif (check_data[0] == "alarm-type-id")
                        sheets.Cells(write_count, 5).Value = check_data[1].gsub("\"","")
                    elsif (check_data[0] == "alarm-type-qualifier")
                        sheets.Cells(write_count, 6).Value = check_data[1].gsub("\"","")
                    elsif (check_data[0] == "perceived-severity")
                        sheets.Cells(write_count, 7).Value = check_data[1].gsub("\"","")
                    elsif (check_data[0] == "alarm-text")
                        sheets.Cells(write_count, 8).Value = check_data[1].gsub("\"","")
                    elsif (check_data[0] == "is-service-affecting")
                        sheets.Cells(write_count, 9).Value = check_data[1].gsub("\"","")
                    elsif (check_data[0] == "event-time")
                        datetime_data = check_data[1].to_s.gsub("\"","").split("T")
                        sheets.Cells(write_count, 1).Value = datetime_data[0]
                        times_data = datetime_data[1].to_s.split("+")
                        time_data = times_data[0].to_s.split(".")
                        sheets.Cells(write_count, 2).Value = time_data[0]
                        # S150対応 Change Start 2020/12/22 by Nishi
                        sheets.Cells(write_count, 3).Value = ("0." + time_data[1].to_s)
                        # S150対応 Change End 2020/12/22 by Nishi
                    end
                    
                end #do |read_ffandata|
                
                if output_flag == true
                    write_count = write_count + 1
                else
                    sheets.Range(sheets.Cells(write_count,1), sheets.Cells(write_count,9)).Clear
                end
                   
            end                        

        end #do |read_data|
        
        write_count = write_count - 1
        
        @logobj.write("Almlog_Analyze_Cls::unitinfo_analyze_ffan_proc End\n")
        
        return write_count
        
    end #unitinfo_analyze_ffan_proc
    # S150対応 Add End 2020/12/11 by Nishi
    
end #Almlog_Analyze_Cls
