#coding: windows-31j
#/****************************************************************************
# *  fileName: TECHINFO_Almdump_Analysis.rb
# *
# *                      All rights reserved, Copyright (C) FUJITSU LIMITED 2016
# *-----------------------------------------------------------------------------
# *  処理概要: FLT発生時のDUMPlogを出力する
# *            
# *  引数    : なし
# *  復帰値  : なし
# *---------------------------------------------------------------------------
# *  変更履歴: 2016.11.24 katou     新規作成(Version 1.0)
# *                                 L2の解析ツールをベースとする。
# *
# ****************************************************************************/

#----------------------------------------------------------#
# クラス
#----------------------------------------------------------#
class Almdump_log_Analyze_Cls < Common_Analyze_Cls

    #----------------------------------------------------------#
    # コンストラクタ
    #----------------------------------------------------------#
    def initialize(in_label) 
        @logobj = Log_class.instance
        @logobj.write("Almdump_log_Analyze_Cls::initialize Start\n")
        
        @label = in_label
        @logobj = Log_class.instance
        @logobj.write("Almdump_log_Analyze_Cls::initialize End\n")
    end
    #----------------------------------------------------------#
    #  Excelファイルの作成                                     #
    #----------------------------------------------------------#
    def createExcelWorkBook(files)
        @logobj.write("Almdump_log_Analyze_Cls::createExcelWorkBook Start\n", 0, File.basename(__FILE__), __LINE__)
        puts "[debug] createExcelWorkBook" #debug
        @xl = WIN32OLE.new('Excel.Application')
        fso = WIN32OLE.new('Scripting.FileSystemObject')
        
        begin
            filename = files
            filename.gsub!("\\","\/")
            renamefilename = File.basename(filename, ".*")
            renamefilename = File.basename(renamefilename, ".*")
            renamedirname  = File.dirname(filename)
            renamefilename2 = renamedirname + "/" + renamefilename + "_S100_ALM_DUMP.xlsx"
            
            @xl.SheetsInNewWorkbook = 1                   # 新bookのsheetを1枚に設定
            book = @xl.Workbooks.Add
            book.SaveAs(fso.GetAbsolutePathName(renamefilename2))   #ファイルを保存
            
            sheets = book.sheets(1)
            
            sheets.Name = "FLT DUMP"                      #シート名を設定
            
            return book
        # 異常系処理
        rescue
            wsh = WIN32OLE.new('WScript.Shell')
            wsh.Popup("異常終了 出力すべきファイルがExcelでOpenされています\n閉じて実行して下さい。",0, "終了")
            
            if book != nil
              book.close
            end
            
            @xl.quit
            
            return nil
        # 後始末
        ensure
            # 記録
            if book == nil 
                return nil
            end
            @logobj.write("Almdump_log_Analyze_Cls::createExcelWorkBook End\n", 0, File.basename(__FILE__), __LINE__)
        end #begin
    end #createExcelWorkBook
    #----------------------------------------------------------#
    #  Excelファイルの終了                                     #
    #----------------------------------------------------------#
    def closeExcelWorkbook(book)
        @logobj.write("Almdump_log_Analyze_Cls::closeExcelWorkbook Start\n", 0, File.basename(__FILE__), __LINE__)
        
        # Excelを表示
        @xl.Visible = true
        puts book
        book.save
        
        @label.text = "TECHINFO.TGZをdrop"
        
        @logobj.write("Almdump_log_Analyze_Cls::closeExcelWorkbook End\n", 0, File.basename(__FILE__), __LINE__)
        
    end #closeExcelWorkbook
    #----------------------------------------------------------#
    # excelset                                                 #
    #----------------------------------------------------------#
    def excelset(fname, book)
        @logobj.write("Almdump_log_Analyze_Cls::excelset Start\n", 0, File.basename(__FILE__), __LINE__)
        #18006対応 noda
        begin
            wsh = WIN32OLE.new('WScript.Shell')
            
            fbname = File.basename(fname)
            fbsize = File.size?(fname)
            @logobj.write(sprintf("PIlog_Analyze_Cls::excelset fbname=%s\n", fbname), 0, File.basename(__FILE__), __LINE__)
            @logobj.write(sprintf("PIlog_Analyze_Cls::excelset fbsize=%d\n", fbsize), 0, File.basename(__FILE__), __LINE__)
            
            #ログファイル解析
            
            log_check_S100(fname, book)
            
            # 解析終了
            popup_msg =  sprintf("解析完了\n")
            wsh.Popup(popup_msg, 0, "正常通知")
        #18006対応 noda
        rescue => e
            printf("excelset error\n")
            @logobj.write(sprintf("[ERROR]%s\n", $!), 0, File.basename(__FILE__), __LINE__)
            p $!
            e.backtrace.each{|errdata|
              @logobj.write(sprintf("[ERROR]%s\n", errdata), 0, File.basename(__FILE__), __LINE__)
              p errdata
            }
        end
        @logobj.write("Almdump_log_Analyze_Cls::excelset End\n", 0, File.basename(__FILE__), __LINE__)
    end #excelset
    #----------------------------------------------------------#
    # log_check_S100
    # 概要：解析
    #       
    #       
    #----------------------------------------------------------#
    def log_check_S100(path, book)
        sheets = book.sheets(1)
        data_count = 0
        
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filedata = entry.read
                if (filedata != nil)
                    #DUMPlog
                    if(entry.name.include?("ud_flt_dump.LOG"))
                        data = StringIO.new(filedata)
                        
                        #サイクリック対応
                        data_count = sort_data_by_time(data,sheets)
                        printf("[DBG]%s %s data_count = %d\n",__FILE__,__LINE__,data_count)
                    end
                end
            end
        end
        
        #データが1つ⇒logging開始データなのでそこは消して終わる
        if(data_count == 1)
            sheets.Rows(data_count).delete
        elsif(data_count > 1)
            #フォーマットに沿ってとられている先頭までを探す(それまでのデータは捨てる)
            delete_line = 0
            for line in 1...data_count + 1 do
                line_data = sheets.cells(line,4).value
                if(line_data =~ /##############/)
                    break
                else
                    sheets.Rows(line).delete
                    delete_line += 1
                    redo
                end
            end
            data_count -= delete_line
            
            #正常データについて解析
            for line in 1...data_count + 1 do
                line_data = sheets.cells(line,4).value
                #対象
                if(line_data =~ /##############/)
                    sheets.cells(line,4).value = line_data.gsub(/##############/,"")
                    sheets.Range(sheets.cells(line,1),sheets.cells(line,6)).Interior.ColorIndex = 8
                    sheets.Range(sheets.cells(line,4),sheets.cells(line,6)).Merge
                    sheets.range(sheets.cells(line,4),sheets.cells(line,6)).HorizontalAlignment = -4108
                else
                    length = line_data.index(":")
                    sheets.cells(line,4).value = line_data[0,length].delete(" ")
                    #ADDR
                    sheets.cells(line,5).value  = line_data.scan(/ADDR = 0x\w+/).join(",").gsub(/ADDR = /,"")
                    #value
                    sheets.cells(line,6).value  = line_data.scan(/VALUE = 0x\w+/).join(",").gsub(/VALUE = /,"")
                end
            end
        end
        
        sheets.Range("C:C").NumberFormatLocal = "0.000000_ "
        
        sheets.Rows(1).Insert
        data_count += 1
        
        sheets.Cells(1, 1).Value = "Date"
        sheets.Cells(1, 2).Value = "Time"
        sheets.Cells(1, 3).Value = "Time(.sec)"
        
        sheets.cells(1, 4).value = "対象"
        sheets.cells(1, 5).value = "ADDR"
        sheets.cells(1, 6).value = "VALUE"
        
        # 罫線
        sheets.Range(sheets.cells(1,1), sheets.cells(data_count,6)).Borders.LineStyle = 1
        
        # セル色変更
        sheets.Range(sheets.cells(1,1), sheets.cells(1,6)).Interior.ColorIndex = 4
        
        # Excel表の整形
        sheets.Columns.AutoFit
        
    end #log_check_S100
end #Almdump_log_Analyze_Cls


