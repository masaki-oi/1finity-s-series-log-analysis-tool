#coding: windows-31j
#/****************************************************************************
# *  fileName: TECHINFO_Analysis_CUI.rb
# *
# *                      All rights reserved, Copyright (C) FUJITSU LIMITED 2017-2018
# *-----------------------------------------------------------------------------
# *  処理概要: ツールのCUI
# *            
# *  引数    : なし
# *  復帰値  : なし
# *---------------------------------------------------------------------------
# *
# *  変更履歴: 2017.05.19 kinomoto  新規作成
# *            2017.08.18 kinomoto  温度Over検出機能追加 (Version 1.2)
# * -Version 1.3.1 -
# *     2018.06.13 kinomoto  改善: 存在しない tar.gzファイルに対するエラーメッセージが不適切
# *
# * -Version 1.4 -
# *     2018.10.31 kinomoto  機能追加: --flt_tab_check オプション
# * -Version 1.5 -
# *     2020.12.11 Nishi  機能追加: S150対応
# *                       バグ修正: 警報変化点ログ解析で出力された結果シートの
# *                                 Cond_idの列が一部正しい内容になっていない問題を対処
# *
# ****************************************************************************/

module TECHINFO_Analysis_CUI
    
#----------------------------------------------------------#
# クラス
#----------------------------------------------------------#
class Emit_Label
    @text = ""
    
    def initialize(s="")
        @text = s
    end
    
    attr_accessor :text
    
=begin
    attr_reader :text
    def text=(s)
        if @text != s
            puts "Emit_Label: #{s}"
        end
        @text = s
    end
=end
end #Emit_Label

module_function

#----------------------------------------------------------#
# コマンド引数解析
#----------------------------------------------------------#
def arg_parse(args)
    
    args = args.map {|v| v.encode(Encoding::Windows_31J)}
    input = ""
    exec_str = ""
    default_exec = '11,12a,15a'
    exec_desc = [ "解析対象ログ: 省略時は、-exec=#{default_exec}",
                    '10 : Trap/Audit情報,構成情報,version情報のみ',
                    '11 : 警報変化点',
                    '12 : FLT解析',
                    '12a: FLT解析+被疑部品検索',
                    '15 : FFAN解析(温度上昇FLT検出)',
                    '15a: FFAN解析(温度上昇FLT検出)+被疑部品検索']
    OptionParser.new do |opts|
        opts.separator('')
        opts.on('--log_csv', '実行ログを旧形式(csv)で出力') {|v| $exec_log_tsv = false}
        opts.on('--visible', '実行中Excel可視化')    {|v| $exec_visible = true}
        opts.on('--flt_tab_check', 'FLT解析時に"参照FLTテーブル"シート生成') {|v| $g_flt_tab_check = true}
        opts.on('--flt_table=VALUE', 'FLT table名') {|v| $g_filename_flt_table = v}
        # S150対応 Add Start 2020/12/11 by Nishi
        opts.on('--type=VALUE', '装置種別') {|v| $g_machine_type = v}
        # S150対応 Add End 2020/12/11 by Nishi
        
        opts.on('-x', '--exec=VALUE', '解析対象ログ', ) {|v| exec_str = v}
        opts.on('-i', '--input=VALUE',  '入力ファイル') {|v| input = v}
        opts.on('-h', '--help', 'show this help') { puts opts; exit }
        opts.on('-v', '--version', 'バージョン表示') { puts $log_analyeze_version; exit }
        opts.banner += ' [INPUT]'
        
        opts.on_tail('', 'INPUT: 入力ファイル')
        opts.on_tail('', exec_desc.join("\n" + ' '*4))
        
        opts.summary_indent = ' ' * 2
        opts.summary_width  = 25
        opts.parse!(args)
    end
    
    if (exec_str == "")
        exec_str = default_exec
    end
    $g_step2_log_repairparts = false
    $g_step2_log_fltchk = "Skip"
    $g_step2_log_stschgchk = "Skip"
    $g_step2_log_detailchk = "Skip"
    $g_step2_log_eccerrchk = "Skip"
    $g_step2_log_softerrchk = "Skip"
    $g_step2_log_ffanchk = 0
    $g_step2_log_cyclechgchk = "Skip"
    $g_step2_log_ilanchk = "Skip"
    exec_str.split(/,/).each do |v|
        case v
        when '10'
        when '11'
            $g_step2_log_stschgchk = "する"
        when '12'
            $g_step2_log_fltchk = "する"
        when '12a'
            $g_step2_log_fltchk = "する"
            $g_step2_log_repairparts = true
        when '15'
            $g_step2_log_ffanchk = 1
        when '15a'
            $g_step2_log_ffanchk = 2
        else
            STDERR.print "error: #{v} illegal value for --exec\n"
            # S150対応 Add Start 2020/12/11 by Nishi
            exit(ERR_ARGS_INPUT)
            # S150対応 Add End 2020/12/11 by Nishi
        end
    end
    
    if (args.size == 1) && (input == "")
        input = args[0]
        # S150対応 Add Start 2020/12/11 by Nishi
        # Typeオプションの入力が正しくない時にエラー出力を行う
        if ($g_machine_type != "S100") && ($g_machine_type != "S150")
            # Typeオプションが指定されていない時
            if ($g_machine_type == nil)
                STDERR.print "error: Type Option must need\n"
                exit(ERR_ARGS_INPUT)
            # Typeオプションの入力が正しくない時
            else
                STDERR.print "error: Type Option Error\n"
                exit(ERR_TYPE_OPTION_INPUT)
            end
        end
        # S150対応 Add End 2020/12/11 by Nishi        
    elsif args.size > 0
        STDERR.print "Usage: ruby #{$0} [options] input_file\n"
        STDERR.print "error: Too Many Argument\n"
        # S150対応 Add Start 2020/12/11 by Nishi
        exit(ERR_ARGS_OPTION_INPUT_OVER)
        # S150対応 Add Start 2020/12/11 by Nishi
    end
    
    return input
end #arg_parse

#----------------------------------------------------------#
# 警報ログ解析 CUI
#----------------------------------------------------------#
def Alm_log_analyze(input_path)
        @logobj = Log_class.instance
        @logobj.write("Alm_log_CUI Start\n")
        @logobj.write("Input=#{input_path}\n")
        
        lbl3  = Emit_Label.new("TECHINFO.tar.gzをdrop")
        
        
        # S150対応 Change Start 2020/12/11 by Nishi
        rcode = ERR_CANCEL_WRITE_OUTPUTFILE
        # S150対応 Change End 2020/12/11 by Nishi
        output_path = ""
        begin
            #17920対応 noda
            if $exclusion_flag == 1
                #連続でDROPされた時は何もしない
                puts "[debug] 連続drop" #debug
            else
                $exclusion_flag = 1
                @@almlog_analyze_obj = Almlog_Analyze_Cls.new(lbl3)
                if $exec_visible
                    @@almlog_analyze_obj.Visible = true
                end
                $progress_value = 0
                $progress_max_log_no = 0
                $progress_log_no = 0
                
                lbl3.text = "解析開始"

                    @dropuri = input_path
                    @dropfile = File.basename(@dropuri)
                
                droppass = @dropuri.gsub("file:///", "")
                puts droppass
                
                    if ! (@dropfile =~ /\.tar.gz$/)
                        TECHINFO_Analysis_Common::popup_msg("異常通知", "解析失敗",
                            "入力ファイルがtar.gzファイルではありません。",
                            "  [#{@dropfile}]")
                    elsif  File.file?(droppass)
                        book = @@almlog_analyze_obj.createExcelWorkBook(droppass)
                        
                        if book != nil
                            begin
                                rcode = @@almlog_analyze_obj.excelset(droppass, book)
                                if rcode == 0
                                    output_path = book.FullName
                                end
                            # Excelをクローズ
                            ensure
                                 @@almlog_analyze_obj.closeExcelWorkbook(book, false)
                                 lbl3.text = "TECHINFO.tar.gzをdrop"
                            end
                        else
                            puts "book = nil"
                            lbl3.text = "TECHINFO.tar.gzをdrop"
                        end
                    else
                        TECHINFO_Analysis_Common::popup_msg("異常通知", "解析失敗",
                            "tar.gzファイルが読み込めません。\n",
                            "ファイルが存在していて、かつread権もある場合は", 
                            "フルパスも含めた、TECHINFO名が長すぎる可能性があります。\n",
                            "ファイル名を変更するか、TECHINFO格納場所の階層を浅くしてください。",
                            "  [#{input_path}]")
                        
                        lbl3.text = "TECHINFO.tar.gzをdrop"
                        rcode = 2
                    end
                    #17920対応 noda
                    $exclusion_flag = 0
            end #if $exclusion_flag == 1
        end
        @logobj.write("Alm_log_CUI End\n")
        @logobj.write("Output=#{output_path}\n")
        return rcode
end #Alm_log_analyze
    
end #TECHINFO_Analysis_CUI
