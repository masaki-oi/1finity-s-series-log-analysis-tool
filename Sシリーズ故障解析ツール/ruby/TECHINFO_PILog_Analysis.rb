#coding: windows-31j
#/**********************************************************************************
# *  fileName: TECHINFO_PIlog_Analysis.rb
# *
# *                      All rights reserved, Copyright (C) FUJITSU LIMITED 2016
# *--------------------------------------------------------------------------------
# *  処理概要: PI情報を解析する。
# *            
# *  引数    : なし
# *  復帰値  : なし
# *--------------------------------------------------------------------------------
# *  変更履歴: 2016.11.24 katou     新規作成(Version 1.0)
# *                                 L2の解析ツールをベースとする。
# *
# **********************************************************************************/

#----------------------------------------------------------#
# クラス
#----------------------------------------------------------#
class PIlog_Analyze_Cls < Common_Analyze_Cls

    #----------------------------------------------------------#
    # コンストラクタ
    #----------------------------------------------------------#
    def initialize(in_label) 
        @logobj = Log_class.instance
        @logobj.write("PIlog_Analyze_Cls::initialize Start\n", 0, File.basename(__FILE__), __LINE__)
        
        @label = in_label

        # 親クラス(Common_Analyze_Cls)のコンストラクタコール
        super()

        @logobj.write("PIlog_Analyze_Cls::initialize End\n", 0, File.basename(__FILE__), __LINE__)
    end #initialize
    #----------------------------------------------------------#
    #  Excelファイルの作成                                     #
    #----------------------------------------------------------#
    def createExcelWorkBook(files)
        @logobj.write("PIlog_Analyze_Cls::createExcelWorkBook Start\n", 0, File.basename(__FILE__), __LINE__)
        @xl = WIN32OLE.new('Excel.Application')
        fso = WIN32OLE.new('Scripting.FileSystemObject')
        
        begin
            filename = files
            filename.gsub!("\\","\/")
            renamefilename = File.basename(filename, ".*")
            renamefilename = File.basename(renamefilename, ".*")
            renamedirname  = File.dirname(filename)
            renamefilename2 = renamedirname + "/" + renamefilename + "_S100_PI.xlsx"
            
            @xl.SheetsInNewWorkbook = 1                   # 新bookのsheetを4枚に設定
            book = @xl.Workbooks.Add
            book.SaveAs(fso.GetAbsolutePathName(renamefilename2))   #ファイルを保存
            
            ###########################
            #  BLADE・FAN・PIUの設定  #
            ###########################
            sheets = book.sheets(1)
            
            sheets.Name = "PI情報 (BLADE・FAN・PIU)"                      # シート名を設定
            
            write_data_column = [["対象","unit_code","version_flag","issue_num[3]","abbreviation_name[9]","fc_num[10]","clei_code[10]","product_year_month[5]","location_code[2]","serial_num[5]",
                                  "reserve0[3]","mac_addr_min[6]","mac_addr_max[6]","fwdl_type","reserve1[32]","pcb_revision[5]","reserve2[7]","parts_temp_th[0]","","",
                                  "parts_temp_th[1]","","","parts_temp_th[2]","","","parts_temp_th[3]","","","parts_temp_th[4]",
                                  "","","parts_temp_th[5]","","","parts_temp_th[6]","","","parts_temp_th[7]","",
                                  "","parts_temp_th[8]","","","parts_temp_th[9]","","","min_wavelength[8]","max_wavelength[8]","frec_spacing[6]",
                                  "reserve3","press_sensor_exist","min_fan_step","max_fan_step","min_temp_th","ambient_temp_pitch","fan_ctrl_interval","min_press_th[10]","initial_fan_step[6]","ambient_temp_hysteresis",
                                  "ambient_press_hysteresis","break_temp_over_prot","shelf_temp_alert_prot","shelf_temp_alm_prot","cooling_fail_prot","cooling_fail_clr_prot","reserve4[14]","warmup_timer","reserve5[2]","pd1_init_value",
                                  "pd2_init_value","max_abs_th","voa_adjust_fix_value","voa_setting_value_max","voa_setting_value_min","loop_target_level","loop_target_th_high","loop_target_th_low","max_power_drawn[5]","low_power_level[5]",
                                  "reserve6[18]","check_sum"],
                                 ["","","","","","","","","","",
                                  "","","","","","","","break_temp","max_temp","min_temp",
                                  "break_temp","max_temp","min_temp","break_temp","max_temp","min_temp","break_temp","max_temp","min_temp","break_temp",
                                  "max_temp","min_temp","break_temp","max_temp","min_temp","break_temp","max_temp","min_temp","break_temp","max_temp",
                                  "min_temp","break_temp","max_temp","min_temp","break_temp","max_temp","min_temp","","","",
                                  "","","","","","","","","","",
                                  "","","","","","","","","","",
                                  "","","","","","","","","","",
                                  "",""]]
            
            sheets.Range(sheets.cells(1,1), sheets.cells(2,82)).value = write_data_column
            
            write_data_row = [["BLADE"],["FAN(1)"],["FAN(2)"],["FAN(3)"],["PIU(1)"],["PIU(2)"],["PIU(3)"],["PIU(4)"]]
            
            sheets.Range(sheets.cells(3,1), sheets.cells(10,1)).value = write_data_row
            
            #セルの結合
            sheets.Range(sheets.cells(1,1),sheets.cells(2,1)).Merge
            sheets.Range(sheets.cells(1,2),sheets.cells(2,2)).Merge
            sheets.Range(sheets.cells(1,3),sheets.cells(2,3)).Merge
            sheets.Range(sheets.cells(1,4),sheets.cells(2,4)).Merge
            sheets.Range(sheets.cells(1,5),sheets.cells(2,5)).Merge
            sheets.Range(sheets.cells(1,6),sheets.cells(2,6)).Merge
            sheets.Range(sheets.cells(1,7),sheets.cells(2,7)).Merge
            sheets.Range(sheets.cells(1,8),sheets.cells(2,8)).Merge
            sheets.Range(sheets.cells(1,9),sheets.cells(2,9)).Merge
            sheets.Range(sheets.cells(1,10),sheets.cells(2,10)).Merge
            sheets.Range(sheets.cells(1,11),sheets.cells(2,11)).Merge
            sheets.Range(sheets.cells(1,12),sheets.cells(2,12)).Merge
            sheets.Range(sheets.cells(1,13),sheets.cells(2,13)).Merge
            sheets.Range(sheets.cells(1,14),sheets.cells(2,14)).Merge
            sheets.Range(sheets.cells(1,15),sheets.cells(2,15)).Merge
            sheets.Range(sheets.cells(1,16),sheets.cells(2,16)).Merge
            sheets.Range(sheets.cells(1,17),sheets.cells(2,17)).Merge
            sheets.Range(sheets.cells(1,48),sheets.cells(2,48)).Merge
            sheets.Range(sheets.cells(1,49),sheets.cells(2,49)).Merge
            sheets.Range(sheets.cells(1,50),sheets.cells(2,50)).Merge
            sheets.Range(sheets.cells(1,51),sheets.cells(2,51)).Merge
            sheets.Range(sheets.cells(1,52),sheets.cells(2,52)).Merge
            sheets.Range(sheets.cells(1,53),sheets.cells(2,53)).Merge
            sheets.Range(sheets.cells(1,54),sheets.cells(2,54)).Merge
            sheets.Range(sheets.cells(1,55),sheets.cells(2,55)).Merge
            sheets.Range(sheets.cells(1,56),sheets.cells(2,56)).Merge
            sheets.Range(sheets.cells(1,57),sheets.cells(2,57)).Merge
            sheets.Range(sheets.cells(1,58),sheets.cells(2,58)).Merge
            sheets.Range(sheets.cells(1,59),sheets.cells(2,59)).Merge
            sheets.Range(sheets.cells(1,60),sheets.cells(2,60)).Merge
            sheets.Range(sheets.cells(1,61),sheets.cells(2,61)).Merge
            sheets.Range(sheets.cells(1,62),sheets.cells(2,62)).Merge
            sheets.Range(sheets.cells(1,63),sheets.cells(2,63)).Merge
            sheets.Range(sheets.cells(1,64),sheets.cells(2,64)).Merge
            sheets.Range(sheets.cells(1,65),sheets.cells(2,65)).Merge
            sheets.Range(sheets.cells(1,66),sheets.cells(2,66)).Merge
            sheets.Range(sheets.cells(1,67),sheets.cells(2,67)).Merge
            sheets.Range(sheets.cells(1,68),sheets.cells(2,68)).Merge
            sheets.Range(sheets.cells(1,69),sheets.cells(2,69)).Merge
            sheets.Range(sheets.cells(1,70),sheets.cells(2,70)).Merge
            sheets.Range(sheets.cells(1,71),sheets.cells(2,71)).Merge
            sheets.Range(sheets.cells(1,72),sheets.cells(2,72)).Merge
            sheets.Range(sheets.cells(1,73),sheets.cells(2,73)).Merge
            sheets.Range(sheets.cells(1,74),sheets.cells(2,74)).Merge
            sheets.Range(sheets.cells(1,75),sheets.cells(2,75)).Merge
            sheets.Range(sheets.cells(1,76),sheets.cells(2,76)).Merge
            sheets.Range(sheets.cells(1,77),sheets.cells(2,77)).Merge
            sheets.Range(sheets.cells(1,78),sheets.cells(2,78)).Merge
            sheets.Range(sheets.cells(1,79),sheets.cells(2,79)).Merge
            sheets.Range(sheets.cells(1,80),sheets.cells(2,80)).Merge
            sheets.Range(sheets.cells(1,81),sheets.cells(2,81)).Merge
            sheets.Range(sheets.cells(1,82),sheets.cells(2,82)).Merge
            
            sheets.Range(sheets.cells(1,18),sheets.cells(1,20)).Merge
            sheets.Range(sheets.cells(1,21),sheets.cells(1,23)).Merge
            sheets.Range(sheets.cells(1,24),sheets.cells(1,26)).Merge
            sheets.Range(sheets.cells(1,27),sheets.cells(1,29)).Merge
            sheets.Range(sheets.cells(1,30),sheets.cells(1,32)).Merge
            sheets.Range(sheets.cells(1,33),sheets.cells(1,35)).Merge
            sheets.Range(sheets.cells(1,36),sheets.cells(1,38)).Merge
            sheets.Range(sheets.cells(1,39),sheets.cells(1,41)).Merge
            sheets.Range(sheets.cells(1,42),sheets.cells(1,44)).Merge
            sheets.Range(sheets.cells(1,45),sheets.cells(1,47)).Merge
            
            # 罫線
            sheets.Range(sheets.cells(1,1), sheets.cells(10,82)).Borders.LineStyle = 1
            
            # セル色変更
            sheets.Range(sheets.cells(1,1), sheets.cells(2,82)).Interior.ColorIndex = 6
            
            # Excel表の整形
            sheets.Columns.AutoFit
            
            # 初期値
            sheets.Range(sheets.cells(3,2), sheets.cells(10,82)).Value = "-"
            
            # 文字列として扱う
            sheets.Range(sheets.cells(3,2), sheets.cells(10,82)).NumberFormatLocal = "@"
            
            #######################
            #     MDLの設定       #
            #######################
            sheets = excel_sheet_add("PI情報 (MDL)",book)
            
            write_data_column = [["対象","VENDER_NAME","FC_NUMBER","ISSUE_NUMBER","DOM","SERIAL_NUMBER","CLEI_CODE","LOCATION_CODE"]]
            sheets.Range(sheets.cells(1,1), sheets.cells(1,8)).value = write_data_column
            
            write_data_row = [["MDL(1)(1)"],["MDL(1)(2)"],["MDL(1)(3)"],["MDL(1)(4)"],["MDL(1)(5)"],["MDL(1)(6)"],["MDL(1)(7)"],["MDL(1)(8)"],["MDL(1)(9)"],["MDL(1)(10)"],["MDL(1)(11)"],["MDL(1)(12)"],
                              ["MDL(2)(1)"],["MDL(2)(2)"],["MDL(2)(3)"],["MDL(2)(4)"],["MDL(2)(5)"],["MDL(2)(6)"],["MDL(2)(7)"],["MDL(2)(8)"],["MDL(2)(9)"],["MDL(2)(10)"],["MDL(2)(11)"],["MDL(2)(12)"],
                              ["MDL(3)(1)"],["MDL(3)(2)"],["MDL(3)(3)"],["MDL(3)(4)"],["MDL(3)(5)"],["MDL(3)(6)"],["MDL(3)(7)"],["MDL(3)(8)"],["MDL(3)(9)"],["MDL(3)(10)"],["MDL(3)(11)"],["MDL(3)(12)"],
                              ["MDL(4)(1)"],["MDL(4)(2)"],["MDL(4)(3)"],["MDL(4)(4)"],["MDL(4)(5)"],["MDL(4)(6)"],["MDL(4)(7)"],["MDL(4)(8)"],["MDL(4)(9)"],["MDL(4)(10)"],["MDL(4)(11)"],["MDL(4)(12)"]]
            sheets.Range(sheets.cells(2,1), sheets.cells(49,1)).value = write_data_row
            
            # 罫線
            sheets.Range(sheets.cells(1,1), sheets.cells(49,8)).Borders.LineStyle = 1
            
            # セル色変更
            sheets.Range(sheets.cells(1,1), sheets.cells(1,8)).Interior.ColorIndex = 6
            
            # Excel表の整形
            sheets.Columns.AutoFit
            
            # 初期値
            sheets.Range(sheets.cells(2,2), sheets.cells(49,8)).Value = "-"
            
            # 文字列として扱う
            sheets.Range(sheets.cells(2,2), sheets.cells(49,8)).NumberFormatLocal = "@"
            
            return book
        # 異常系処理
        rescue
            wsh = WIN32OLE.new('WScript.Shell')
            wsh.Popup("異常終了 出力すべきファイルがExcelでOpenされています\n閉じて実行して下さい。",0, "終了")
            
            if book != nil
                book.close
            end
            
            @xl.quit
            
            return nil
            
        # 後始末
        ensure
            # 記録
            if book == nil 
                return nil
            end
            @logobj.write("PIlog_Analyze_Cls::createExcelWorkBook End\n", 0, File.basename(__FILE__), __LINE__)
        end
    end #createExcelWorkBook
    
    #----------------------------------------------------------#
    #  Excelファイルの終了                                     #
    #----------------------------------------------------------#
    def closeExcelWorkbook(book)
        @logobj.write("PIlog_Analyze_Cls::closeExcelWorkbook Start\n", 0, File.basename(__FILE__), __LINE__)
        
        # Excelを表示
        @xl.Visible = true
        book.save
        
        @label.text = "TECHINFO.TGZをdrop"
        
        @logobj.write("PIlog_Analyze_Cls::closeExcelWorkbook End\n", 0, File.basename(__FILE__), __LINE__)
        
    end #closeExcelWorkbook
    
    #----------------------------------------------------------#
    # excelset                                                 #
    #----------------------------------------------------------#
    def excelset(fname, book)
        @logobj.write("PIlog_Analyze_Cls::excelset Start\n", 0, File.basename(__FILE__), __LINE__)
        #18006対応 noda
        begin
            wsh = WIN32OLE.new('WScript.Shell')
            
            fbname = File.basename(fname)
            
            fbsize = File.size?(fname)
            @logobj.write(sprintf("PIlog_Analyze_Cls::excelset fbname=%s\n", fbname), 0, File.basename(__FILE__), __LINE__)
            @logobj.write(sprintf("PIlog_Analyze_Cls::excelset fbsize=%d\n", fbsize), 0, File.basename(__FILE__), __LINE__)
            
            #Trap解析
            #エクセルに出力
            trap_analyze(fname,book)
            #エクセルから最新状態を割り出す
            trap_analyze_from_xls(book)
            
            @xl.DisplayAlerts = false
            book.sheets("Trap情報").Delete
            @xl.DisplayAlerts = true
            
            #PI解析の本体
            log_check_PI(fname, book)
            
            # 解析終了
            popup_msg =  sprintf("解析完了\n")
            wsh.Popup(popup_msg, 0, "正常通知")
        #18006対応 noda
        rescue => e
            printf("excelset error\n")
            @logobj.write(sprintf("[ERROR]%s\n", $!), 0, File.basename(__FILE__), __LINE__)
            p $!
            e.backtrace.each{|errdata|
                @logobj.write(sprintf("[ERROR]%s\n", errdata), 0, File.basename(__FILE__), __LINE__)
                p errdata
            }
        end
        @logobj.write("PIlog_Analyze_Cls::excelset End\n", 0, File.basename(__FILE__), __LINE__)
        
    end #excelset
end #PIlog_Analyze_Cls


