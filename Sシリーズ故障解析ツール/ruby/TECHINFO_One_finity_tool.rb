#coding: windows-31j
#/**********************************************************************************
# *  fileName: TECHINFO_One_finity_tool.rb
# *
# *                      All rights reserved, Copyright (C) FUJITSU LIMITED 2016
# *--------------------------------------------------------------------------------
# *  処理概要: techinfoファイルをドロップして1finity_toolを起動させる。
# *            
# *  引数    : なし
# *  復帰値  : なし
# *--------------------------------------------------------------------------------
# *  変更履歴: 2016.11.24 katou     新規作成(Version 1.0)
# *                                 L2の解析ツールをベースとする。
# *
# **********************************************************************************/

#----------------------------------------------------------#
# クラス
#----------------------------------------------------------#
class One_finity_tool_Cls < Common_Analyze_Cls

    #----------------------------------------------------------#
    # コンストラクタ
    #----------------------------------------------------------#
    def initialize(in_label) 
        @logobj = Log_class.instance
        @logobj.write("One_finity_tool_Cls::initialize Start\n", 0, File.basename(__FILE__), __LINE__)
        
        @label = in_label
        
        @logobj.write("One_finity_tool_Cls::initialize End\n", 0, File.basename(__FILE__), __LINE__)
    end #initialize
    #----------------------------------------------------------#
    #  実処理                                                  #
    #----------------------------------------------------------#
    def Excute(files)
        @logobj.write("One_finity_tool_Cls::createExcelWorkBook Start\n", 0, File.basename(__FILE__), __LINE__)
        wsh = WIN32OLE.new('WScript.Shell')
        
        begin
            exe_flag = 0
            
            filename = files
            filename.gsub!("\\","\/")
            dirname  = File.dirname(filename)
            filename = File.basename(filename, ".*")
            filename = File.basename(filename, ".*")
            
            unpack_dir = dirname + "/" + filename
            if(unpack_dir.index(" "))
                popup_msg =  sprintf("同logフォルダのパスにスペースが入っています。\n")
                wsh.Popup(popup_msg, 0, "異常通知")
                return 
            end
            
            Dir::mkdir(unpack_dir)
            
            puts "フォルダ生成"
            
            Zlib::GzipReader.open(files) do |tgz|
                Minitar.unpack(tgz, unpack_dir)
            end
            
            #解凍dirに移動
            excute_flag = 0
            Dir::chdir(unpack_dir)
            Dir.glob("chal_cm_sig.LOG").each { |file|
                excute_flag = 1
            }
            
            #元のDirに戻る
            Dir::chdir($D_RUN_FULL_PATH)
            
            if( excute_flag == 1 )
                
                #実行dirに移動
                Dir::chdir($D_TOOL_PATH)
                
                #programを実行
                shell_cmd = "1finity_tool.exe " + unpack_dir + "/chal_cm_sig.LOG"
                puts shell_cmd
                wsh.Run(shell_cmd)
                
                #元のDirに戻る
                Dir::chdir($D_RUN_FULL_PATH)
                
                # 起動完了
                popup_msg =  sprintf("起動完了\n")
                wsh.Popup(popup_msg, 0, "正常通知")
                
            else
                popup_msg =  sprintf("chal_cm_sig.LOGがありません\n")
                wsh.Popup(popup_msg, 0, "異常通知")
            end
        rescue => e
            # 解析終了
            popup_msg =  sprintf("同一名フォルダがあります\n")
            wsh.Popup(popup_msg, 0, "異常通知")
        end
        
    end #Excute
    
end #One_finity_tool_Cls



