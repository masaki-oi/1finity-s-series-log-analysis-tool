#coding: windows-31j
#/****************************************************************************
# *  Module  : AC_DefineDataGenarateViewerForm
# *
# *                   All rights reserved, Copyright (C) FUJITSU LIMITED 2013
# *---------------------------------------------------------------------------
# *  処理概要: ActionCode仕様書及びヘッダファイルを取り込み
# *            ActionCode定義データを出力する。
# *  引数    : -
# *  復帰値  : -
# *---------------------------------------------------------------------------
# *  変更履歴: 2013.10.09 QNET）松永    新規作成(Version 1.0)
# *            2014.04.14 QNET）野田    #17152 フォルダ構成変更対応
# *            2014.06.12 QNET）野田    #18273 STEP3 TEHCINFO解析ツール  ACテーブル生成機能追加
# *
# ****************************************************************************/
require 'rubygems'
require 'optparse'
require 'csv'

#----------------------------------------------------------#
# ActionCode定義データ生成 クラス
#----------------------------------------------------------#
class AC_DefineData_Make_Cls < Common_Analyze_Cls

  #----------------------------------------------------------#
  # コンストラクタ
  #----------------------------------------------------------#
  def initialize(label2, logobj)
    @logobj = Log_class.instance
    @aclogobj = logobj
    @logobj.write(sprintf("AC_DefineData_Make_Cls::initialize Start\n"), 0, File.basename(__FILE__), __LINE__)
    @logobj.write(sprintf("AC_DefineData_Make_Cls::initialize End\n"), 0, File.basename(__FILE__), __LINE__)
    @label2 = label2
  end

  #----------------------------------------------------------#
  #  ACログ書き込み
  #----------------------------------------------------------#
  def ac_log_write(msg, level=0)
    file, line, func = parse_caller(caller.first)
    @logobj.write(msg, level, file, line)
    @aclogobj.write(msg, level, file, line)
  end

  #----------------------------------------------------------#
  #  ディレクトリ読み込み                                    #
  #----------------------------------------------------------#
  def readDirectory(dir_ac)
    ac_log_write(sprintf("AC_DefineData_Make_Cls::readDirectory Start\n"), 0)
    ac_log_write(sprintf("ac_tbl 出力先 : %s\n", $D_ACTBL_PATH), 1)

    wsh = WIN32OLE.new('WScript.Shell')
    error_flag = false

    puts "ac_tbl 出力先 : " + $D_ACTBL_PATH

    #includeファイル参照
    puts dir_ac
    dir_tlv = dir_ac + "/" + "include"
    ac_log_write(sprintf("参照先 :%s\n", dir_tlv), 1)
    puts "参照先 : " + dir_tlv

    #TLVデータ格納変数定義
    record_ac_tlv = []
    record_ac_tlv_tmp = []

    #ディレクトリ名取得(パスなし)
    dirname = File.basename(dir_ac)

    #include内ファイル読み込み
    Dir.glob(dir_tlv + "/*.h").each do |file_tlv|
      file_name_tlv = file_tlv
      file_name_tlv = file_name_tlv.gsub(dir_tlv, "")
      file_name_tlv = file_name_tlv.gsub("/", "")
      include_convert(file_tlv, file_name_tlv, record_ac_tlv, dirname)
    end

    #ACファイル参照
    puts "参照先 : " + dir_ac

    #ACID.txt用
    record_num = [] #ActionCode一覧順のACナンバー
    record_name = [] #ActionCode一覧順のAC_NAME
    record_id = [] #ActionCode一覧順のAC_ID

    #TLV_IDファイル用
    tlv_num = 0
    record_file_name = [] #ファイルにあるシート基準のファイル名
    record_sheet_name = [] #1ファイルにあるシート名
    record_sheet = [] #カテゴリファイルシート毎のAC_NAME
    record_sheet_tlv_name = [] #カテゴリファイルシート毎のTLV_NAME
    record_tlv_parameter = [] #TLV_NAME毎のParameter
    record_tlv_name = [] #カテゴリファイル全て(読み込んだ順)のTLV_NAME
    record_tlv_type = [] #カテゴリファイル全て(読み込んだ順)のTLV_NAME毎のパラメータ型
    record_tlv_size = [] #カテゴリファイル全て(読み込んだ順)のTLV_NAME毎のパラメータサイズ
    record_tlv_var = [] #カテゴリファイル全て(読み込んだ順)のTLV_NAME毎のパラメータ変数名

    $delete_check = 0

    #ActionCode一覧＆詳細_X.XX版読み込み
    Dir.glob(dir_ac + "/*.xlsx").each do |file|
      file_name = file
      file_name = file_name.gsub(dir_ac, "")
      file_name = file_name.gsub("/", "")
      tlv_num = excel2csv(file, file_name, dirname, record_num, record_name, record_id, record_file_name, record_sheet_name, record_sheet, record_sheet_tlv_name, record_tlv_name, record_tlv_type, record_tlv_size, record_tlv_var, tlv_num, record_tlv_parameter)
    end

    #CARD.txt作成
    @label2.text = "作成中: CARD.txt"
    card_txt_make(dirname)

    #TLV.txt作成
    @label2.text = "作成中: TLV.txt"
    tlv_txt_make(dirname, record_ac_tlv, record_ac_tlv_tmp, record_tlv_name)

    #ACTION CODE対応表作成
    @label2.text = "作成中: ACTION CODE対応表.xlsx"
    action_code_make(dirname, record_num, record_sheet, record_name, record_id, record_file_name, record_sheet_name, record_sheet_tlv_name, record_ac_tlv, record_tlv_parameter)

    #ac_tbl内のD_XXXXファイルをActionCode一覧＆詳細_X.XX版.gzに圧縮
    @label2.text = "圧縮中: " + $D_ACTBL_PATH + '/' + dirname
    tar_make(dirname)

    #14283 取消線が含まれている場合はポップアップを表示
    if $delete_check == 1
      deleteline_msg(dirname)
    end

    ac_log_write(sprintf("AC_DefineData_Make_Cls::readDirectory End\n"), 0)
    if error_flag
      return false
    else
      return true
    end
  end

  #----------------------------------------------------------#
  #  includeファイル変換                                     #
  #----------------------------------------------------------#
  def include_convert(file_tlv, file_name_tlv, record_ac_tlv, dirname)
    ac_log_write(sprintf("AC_DefineData_Make_Cls::include_convert Start\n"), 0)

    #変数定義
    tlv_tmp = ''

    xlsfile = File.expand_path(file_tlv)
    
    #ファイルの存在を確認
    if (File.exists?(xlsfile))

      #ac/tlvファイルを判定
      if (/ac|tlv/ =~ file_name_tlv)
        puts "File: " + file_name_tlv
        ac_log_write(sprintf("AC_DefineData_Make_Cls::include_convert File: %s\n", file_name_tlv), 0)
        File.open(xlsfile, 'r:utf-8').each do |row|
          if (/^#define D_/ =~ row)
            tlv_tmp = row
            tlv_tmp = tlv_tmp.gsub(/^#define\s+(\w+)\s+(\w+)\s+.*/, '\1 \2')
            if (/^#define/ !~ tlv_tmp)
              if (/^D_+[a-zA-Z0-9]+_+TLV+_+.*/ =~ tlv_tmp || /^D_+[a-zA-Z0-9]+_+TYPE+_+.*/ =~ tlv_tmp)
                record_ac_tlv << tlv_tmp
              end
            end
          end
        end
      else
        puts "File: " + file_name_tlv + " (SKIP)"
      end
    end

    ac_log_write(sprintf("AC_DefineData_Make_Cls::include_convert End\n"), 0)
  end

  #----------------------------------------------------------#
  #  エクセル2csv                                            #
  #----------------------------------------------------------#
  def excel2csv(file, file_name, dirname, record_num, record_name, record_id, record_file_name, record_sheet_name, record_sheet, record_sheet_tlv_name, record_tlv_name, record_tlv_type, record_tlv_size, record_tlv_var, tlv_num, record_tlv_parameter)
    ac_log_write(sprintf("AC_DefineData_Make_Cls::excel2csv Start\n"), 0)

    xlsfile = File.expand_path(file)
    
    #ファイルの存在を確認
    if (File.exists?(xlsfile))

      ac_log_write(sprintf("AC_DefineData_Make_Cls::excel2csv %s analyze Start\n", file_name), 0)

      #ActionCode一覧(Unitドライバ).xlsxの判定
      if (/^ActionCode一覧/ =~ file_name)

        @label2.text = "解析中: " + file_name
        puts "File: " + file_name
        xl = WIN32OLE.new('Excel.Application')
        book = xl.Workbooks.open(xlsfile)
        begin
          book.Worksheets.each do |sheet|

            #ACID.txt作成
            acid_make(sheet, record_name, record_num, record_id, dirname)

          end
        rescue => e
          ac_log_write(sprintf("[ERROR]%s\n", $!), 0)
          p $!
          e.backtrace.each{|errdata|
            ac_log_write(sprintf("[ERROR]%s\n", errdata), 0)
            p errdata
          }
        ensure
          book.Saved = true
          book.Close(false)
          xl.Quit
        end

      #ACカテゴリ毎の詳細ファイルか判定
      elsif (/^カテゴリ/ =~ file_name)
        @label2.text = "解析中: " + file_name
        puts "File: " + file_name
        xl = WIN32OLE.new('Excel.Application')
        book = xl.Workbooks.open(xlsfile)
        begin
          book.Worksheets.each do |sheet|

            #個別パラメータがあるか検索
            $check = 0
            sheet.UsedRange.Rows.each do |row|
              row.Columns("C").each do |cell|
                if cell.Value != nil
                  if (/^Parameter/ =~ cell.Value.to_s)
                    $check = 1
                  end
                end
              end
            end

            if $check == 0
              tmp_sheet = sheet.Cells.Item(3,"D").Value.to_s
              i = 0
              while i < record_name.length
                if record_name[i] == tmp_sheet.chomp
                  $check = 1
                end
                i += 1
              end
            end

            #個別パラメータが無い場合はスキップ
            if $check == 0
              puts "  Sheet: " + sheet.name + " (SKIP)"
              sheet_num = tlv_num

            #個別パラメータが有る場合
            elsif $check == 1
              puts "  Sheet: " + sheet.name

              #ファイル名とシート名を保存
              record_file_name << file_name
              record_sheet_name << sheet.name

              #シート毎のAC_NAME取得
              tmp_sheet = sheet.Cells.Item(3,"D").Value.to_s
              record_sheet << tmp_sheet.chomp
              $tlv_check = 0
              sheet_num = tlv_num

              sheet.UsedRange.Rows.each do |row|

                #tlv_parts_headerの行にヒットしていない
                if $tlv_check == 0

                  #tlv_parts_header検索
                  row.Columns("E").each do |cell|
                    if cell.Value != nil
                      if (/^tlv_parts_header/ =~ cell.Value.to_s)
                        $tlv_check = 1
                      end
                    end
                  end

                #tlv_parts_headerの行にヒットした次の行
                elsif $tlv_check == 1
                  row.Columns("J").each do |cell|
                    if (/^D_/ =~ cell.Value.to_s)

                      #TLV_ID毎に保持するための変数
                      $tmp_file_name = ''
                      $tmp_type = ''
                      $tmp_var = ''
                      $tmp_size = ''
                      $tmp_tmp_size = ''
                      $para_num = 0
                      $record_tlv_parameta_type = [] #カテゴリファイル全て(読み込んだ順)の個別パラメータ型
                      $record_tlv_parameta_size = [] #カテゴリファイル全て(読み込んだ順)の個別パラメータサイズ
                      $record_tlv_parameta_var = [] #カテゴリファイル全て(読み込んだ順)の個別パラメータ変数名
                      $record_tlv_parameta_var_check = [] #カテゴリファイル全て(読み込んだ順)の個別パラメータ変数名の構造体判定

                      #個別パラメータTLV_NAME取得
                      record_tlv_name << cell.Value.to_s
                      $tmp_file_name = cell.Value.to_s
                      puts "   生成: " + $tmp_file_name
                      tlv_num += 1
                    end
                  end

                  #tlv_parts_headerにヒットした行を記憶
                  $row_tmp = row.Row
                  $tlv_check = 2

                #tlv_parts_headerにヒットした後の個別パラメータ行検索
                elsif $tlv_check == 2

                  #tlv_parts_headerから３行目に個別パラメータがあるか判定
                  row_check = 0

                  #tlv_検索
                  row.Columns("E").each do |cell|
                    if row_check == 0
                      if cell.Value != nil
                        if (/^tlv_parts_header/ =~ cell.Value.to_s)
                          $tlv_check = 1
                        elsif (/tlv_/ =~ cell.Value.to_s)
                          $tlv_check = 3
                        end
                      else
                        if (row.Row - $row_tmp) == 3
                          File.open($D_ACTBL_PATH + "/#{dirname}/#{$tmp_file_name}", "w")
                          $tlv_check = 0
                          row_check = 1
                        end
                      end
                    else
                      break
                    end
                  end

                #tlv_parts_headerにヒットした後の個別パラメータ行の次の行以降
                elsif $tlv_check == 3

                  #tlv_parts_headerなら新規個別パラメータとする
                  if (/^tlv_parts_header/ =~ sheet.Cells.Item((row.Row),"E").Value)
                    $tlv_check = 1

                    #TLV_NAME毎の個別パラメータの型取得
                    record_tlv_type << $tmp_type.rstrip

                    #TLV_NAME毎の個別パラメータのサイズ取得
                    record_tlv_size << $tmp_size.rstrip

                    #TLV_NAME毎の個別パラメータの変数名取得
                    record_tlv_var << $tmp_var.rstrip

                    #TLVファイル作成
                    File.open($D_ACTBL_PATH + "/#{dirname}/#{$tmp_file_name}", "w") do |tlvfile|

                      #ファイルに書き込むデータがある場合
                      if $para_num != 0
                        check_struct(tlvfile, $para_num, $record_tlv_parameta_type, $record_tlv_parameta_var, $record_tlv_parameta_var_check, $record_tlv_parameta_size)
                      end
                    end

                    #TLV_NAME毎のParameter
                    tlv_parameter_make($para_num, $tmp_file_name, $record_tlv_parameta_type, $record_tlv_parameta_var, $record_tlv_parameta_var_check, $record_tlv_parameta_size, record_tlv_parameter)
                  else
                    if (sheet.Cells.Item((row.Row),"D").Value != nil)
                      if (/^Note/ !~ sheet.Cells.Item((row.Row),"C").Value)

                        #個別パラメータの変数名取得
                        if (sheet.Cells.Item((row.Row),"F").Value != nil)
                          if ($tmp_var != nil)
                            $tmp_var = $tmp_var + sheet.Cells.Item((row.Row),"F").Value.to_s + " "
                          else
                            $tmp_var = sheet.Cells.Item((row.Row),"F").Value.to_s + " "
                          end
                          $record_tlv_parameta_var << sheet.Cells.Item((row.Row),"F").Value.to_s
                          $record_tlv_parameta_var_check << 1
                        elsif (sheet.Cells.Item((row.Row),"G").Value != nil)
                          $tmp_var = $tmp_var + sheet.Cells.Item((row.Row),"G").Value.to_s + " "
                          $record_tlv_parameta_var << sheet.Cells.Item((row.Row),"G").Value.to_s
                          $record_tlv_parameta_var_check << 2
                        elsif (sheet.Cells.Item((row.Row),"H").Value != nil)
                          $tmp_var = $tmp_var + sheet.Cells.Item((row.Row),"H").Value.to_s + " "
                          $record_tlv_parameta_var << sheet.Cells.Item((row.Row),"H").Value.to_s
                          $record_tlv_parameta_var_check << 3
                        end

                        #個別パラメータの型取得
                        tmp_tmp_type = sheet.Cells.Item((row.Row),"D").Value.to_s.rstrip
                        $tmp_type = $tmp_type + tmp_tmp_type + ""
                        $record_tlv_parameta_type << tmp_tmp_type

                        #個別パラメータのサイズ取得
                        if (sheet.Cells.Item((row.Row),"I").Value != nil)
                          $tmp_tmp_size = sheet.Cells.Item((row.Row),"I").Value.to_s

                          #float型をint型に変換
                          $tmp_tmp_size = $tmp_tmp_size.gsub(/\.0/, "")
                          $tmp_size = $tmp_size + $tmp_tmp_size + " "

                          #14283 取消線判定
                          if sheet.Cells.Item((row.Row),"I").Font.Strikethrough == true || deleteline_check(sheet, row) == 1
                            $delete_check = 1
                            $record_tlv_parameta_size << $tmp_tmp_size + " 取消線"
                          else
                            $record_tlv_parameta_size << $tmp_tmp_size
                          end
                        else

                          #サイズの空白セルを○にする
                          $record_tlv_parameta_size << "○"
                        end

                        $para_num += 1
                      else
                        $tlv_check = 0

                        #TLVファイル作成
                        File.open($D_ACTBL_PATH + "/#{dirname}/#{$tmp_file_name}", "w") do |tlvfile|

                          #ファイルに書き込むデータがある場合
                          if $para_num != 0
                            check_struct(tlvfile, $para_num, $record_tlv_parameta_type, $record_tlv_parameta_var, $record_tlv_parameta_var_check, $record_tlv_parameta_size)
                          end
                        end

                        #TLV_NAME毎のParameter
                        tlv_parameter_make($para_num, $tmp_file_name, $record_tlv_parameta_type, $record_tlv_parameta_var, $record_tlv_parameta_var_check, $record_tlv_parameta_size, record_tlv_parameter)
                      end
                    else
                      $tlv_check = 0

                      #TLVファイル作成
                      File.open($D_ACTBL_PATH + "/#{dirname}/#{$tmp_file_name}", "w") do |tlvfile|

                        #ファイルに書き込むデータがある場合
                        if $para_num != 0
                          check_struct(tlvfile, $para_num, $record_tlv_parameta_type, $record_tlv_parameta_var, $record_tlv_parameta_var_check, $record_tlv_parameta_size)
                        end
                      end

                      #TLV_NAME毎のParameter
                      tlv_parameter_make($para_num, $tmp_file_name, $record_tlv_parameta_type, $record_tlv_parameta_var, $record_tlv_parameta_var_check, $record_tlv_parameta_size, record_tlv_parameter)
                    end
                  end
                end
              end
            end

            #シート毎のTLV_NAME
            n = sheet_num
            while n < tlv_num
              member2 = Member.new(record_tlv_name[n], sheet.name, (tlv_num - sheet_num))
              record_sheet_tlv_name  << member2
              n += 1
            end
          end
        rescue => e
          ac_log_write(sprintf("[ERROR]%s\n", $!), 0)
          p $!
          e.backtrace.each{|errdata|
            ac_log_write(sprintf("[ERROR]%s\n", errdata), 0)
            p errdata
          }
        ensure
          book.Saved = true
          book.Close(false)
          xl.Quit
        end
      else
        puts "File: " + file_name + " (SKIP)"
      end
      ac_log_write(sprintf("AC_DefineData_Make_Cls::excel2csv %s analyze End\n", file_name), 0)

    end

    ac_log_write(sprintf("AC_DefineData_Make_Cls::excel2csv End\n"), 0)
    return tlv_num
  end

  #----------------------------------------------------------#
  #  ACID.txt作成関数                                        #
  #----------------------------------------------------------#
  def acid_make(sheet, record_name, record_num, record_id, dirname)
    ac_log_write(sprintf("AC_DefineData_Make_Cls::acid_make Start\n"), 0)

    #ヒットしたい行検索の変数
    num_column = 0 #No
    ud_column = 0 #UD_AC
    id_column = 0 #AC define

    #ActionCode一覧(Unitドライバ)のAC一覧シートを判定
    if (/^ActionCode.*/ =~ sheet.name)
      i = 0
      sheet.Columns.each do |column|
        if i == 0
          j = 0
          column.Rows.each do |row|
            if j < 50
              row.each do |cell|
                if i == 0
                  if cell.Value != nil
                    #Noのある列を検索
                    if (/No/ =~ cell.Value.to_s)
                      num_column = cell.Column
                      i = 1
                      j = 50
                    end
                  end
                else
                  break
                end
                j += 1
              end
            else
              break
            end
          end
        else
          break
        end
      end

      sheet.Rows(50).Columns.each do |cell|
        #AC名のある列を検索
        if (/^UD_AC_/ =~ cell.Value.to_s)
          ud_column = cell.Column
        end
        #AC_define_IDのある列を検索
        if (/^0[0-9A-Za-z]{7}/ =~ cell.Value.to_s)
          id_column = cell.Column
        end
      end

      i = 0
      sheet.UsedRange.Rows.each do |row|
        row.Columns(ud_column).each do |cell|
          if cell.Value != nil

            #AC_define名を読み込み
            #ACナンバーを読み込み
            if (/^UD_AC_/ =~ cell.Value.to_s)

              #改行取り消し
              tmp_name = cell.Value.to_s
              record_name << tmp_name.chomp
              record_num << sheet.Cells.Item(row.Row, num_column).Value
              i += 1
            end
          end
        end

        row.Columns(id_column).each do |cell|
          if cell.Value != nil
            #AC_define_IDを読み込み
            if (/^0[0-9A-Za-z]{7}/ =~ cell.Value.to_s)

              #改行取り消し
              tmp_id = cell.Value
              record_id << "0x" + tmp_id.chomp

            #ActionCode一覧＆詳細_2.07版対応(ActionCode一覧(Unitドライバ).xlsx)[#14632]
            elsif (/UD_AC_FAN_GET_REG_INFO/ =~ record_name[i - 1])
              record_id[i - 1] = "0x01C01100"
            end
          end
        end
      end

      #ACID.txt作成
      @label2.text = "作成中: ACID.txt"
      puts "  Sheet: " + sheet.name + " (" + "ACID.txt" + ")"
      File.open($D_ACTBL_PATH + "/#{dirname}/ACID.txt", "w") do |acidfile|
        i = 0
        while i < record_name.length
          acidfile.puts record_name[i] + " " + record_id[i]
          i += 1
        end
      end
    else
      puts "  Sheet: " + sheet.name + " (SKIP)"
    end

    ac_log_write(sprintf("AC_DefineData_Make_Cls::acid_make End\n"), 0)
  end

  #----------------------------------------------------------#
  #  TLV個別パラメータが構造体か判定                         #
  #----------------------------------------------------------#
  def check_struct(check_tlvfile, check_para_num, check_record_tlv_parameta_type, check_record_tlv_parameta_var, check_record_tlv_parameta_var_check, check_record_tlv_parameta_size)
    ac_log_write(sprintf("AC_DefineData_Make_Cls::check_struct Start\n"), 0)

    l = 0

    #データ個数分書き込む
    while l < check_para_num

      #データが一つでもある場合
      if check_record_tlv_parameta_type[l] != nil

        #パラメータが構造体名である場合(パラメータのサイズは記載無し)
        if (/^struct/ =~ check_record_tlv_parameta_type[l])

          #一つ目はそのまま記載
          if l == 0
            check_tlvfile.puts check_record_tlv_parameta_type[l] + "," + check_record_tlv_parameta_var[l]

          #二つ目以降はパラメータの構造体が同階層か判定
          elsif l > 0

            #同階層である場合
            if check_record_tlv_parameta_var_check[l - 1] == check_record_tlv_parameta_var_check[l]
              check_tlvfile.puts check_record_tlv_parameta_type[l] + "," + check_record_tlv_parameta_var[l]

            #違う階層である場合
            elsif check_record_tlv_parameta_var_check[l - 1] != check_record_tlv_parameta_var_check[l]

              #新規構造体の宣言時
              if check_record_tlv_parameta_var_check[l - 1] < check_record_tlv_parameta_var_check[l]
                check_tlvfile.puts '{'
                check_tlvfile.puts check_record_tlv_parameta_type[l] + "," + check_record_tlv_parameta_var[l]

              #構造体が一階層終了の場合
              elsif (check_record_tlv_parameta_var_check[l - 1] - check_record_tlv_parameta_var_check[l]) == 1
                check_tlvfile.puts '}'
                check_tlvfile.puts check_record_tlv_parameta_type[l] + "," + check_record_tlv_parameta_var[l]

              #構造体が二階層終了の場合
              elsif (check_record_tlv_parameta_var_check[l - 1] - check_record_tlv_parameta_var_check[l]) == 2
                check_tlvfile.puts '}'
                check_tlvfile.puts '}'
                check_tlvfile.puts check_record_tlv_parameta_type[l] + "," + check_record_tlv_parameta_var[l]
              end
            end
          end

        #パラメータが構造体名でない場合(パラメータのサイズは記載有り)
        else

          #一つ目はそのまま記載
          if l == 0
            check_tlvfile.puts check_record_tlv_parameta_type[l] + "," + check_record_tlv_parameta_var[l] + "=" + check_record_tlv_parameta_size[l]

          #最後のパラメータの場合
          elsif l == (check_para_num - 1)

            #同階層である場合
            if check_record_tlv_parameta_var_check[l - 1] == check_record_tlv_parameta_var_check[l]
              check_tlvfile.puts check_record_tlv_parameta_type[l] + "," + check_record_tlv_parameta_var[l] + "=" + check_record_tlv_parameta_size[l]

              #一階層内の構造体の変数で終了する場合
              if check_record_tlv_parameta_var_check[l] == 2
                check_tlvfile.puts '}'

              #二階層内の構造体の変数で終了する場合
              elsif check_record_tlv_parameta_var_check[l] == 3
                check_tlvfile.puts '}'
                check_tlvfile.puts '}'
              end

            #違う階層である場合
            elsif check_record_tlv_parameta_var_check[l - 1] != check_record_tlv_parameta_var_check[l]

              #新規構造体の宣言時
              if check_record_tlv_parameta_var_check[l - 1] < check_record_tlv_parameta_var_check[l]
                check_tlvfile.puts '{'
                check_tlvfile.puts check_record_tlv_parameta_type[l] + "," + check_record_tlv_parameta_var[l] + "=" + check_record_tlv_parameta_size[l]

                #一階層内の構造体の変数で終了する場合
                if check_record_tlv_parameta_var_check[l] == 2
                  check_tlvfile.puts '}'

                #二階層内の構造体の変数で終了する場合
                elsif check_record_tlv_parameta_var_check[l] == 3
                  check_tlvfile.puts '}'
                  check_tlvfile.puts '}'
                end

              #構造体が一階層終了の場合
              elsif (check_record_tlv_parameta_var_check[l - 1] - check_record_tlv_parameta_var_check[l]) == 1
                check_tlvfile.puts '}'
                check_tlvfile.puts check_record_tlv_parameta_type[l] + "," + check_record_tlv_parameta_var[l] + "=" + check_record_tlv_parameta_size[l]

                #一階層内の構造体の変数で終了する場合
                if check_record_tlv_parameta_var_check[l] == 2
                  check_tlvfile.puts '}'
                end

              #構造体が二階層終了の場合
              elsif (check_record_tlv_parameta_var_check[l - 1] - check_record_tlv_parameta_var_check[l]) == 2
                check_tlvfile.puts '}'
                check_tlvfile.puts '}'
                check_tlvfile.puts check_record_tlv_parameta_type[l] + "," + check_record_tlv_parameta_var[l] + "=" + check_record_tlv_parameta_size[l]
              end
            end

          #二つ目以降、最後のパラメータでない場合
          elsif l > 0

            #同階層である場合
            if check_record_tlv_parameta_var_check[l - 1] == check_record_tlv_parameta_var_check[l]
              check_tlvfile.puts check_record_tlv_parameta_type[l] + "," + check_record_tlv_parameta_var[l] + "=" + check_record_tlv_parameta_size[l]

            #違う階層である場合
            elsif check_record_tlv_parameta_var_check[l - 1] != check_record_tlv_parameta_var_check[l]

              #新規構造体の宣言時
              if check_record_tlv_parameta_var_check[l - 1] < check_record_tlv_parameta_var_check[l]
                check_tlvfile.puts '{'
                check_tlvfile.puts check_record_tlv_parameta_type[l] + "," + check_record_tlv_parameta_var[l] + "=" + check_record_tlv_parameta_size[l]

              #構造体が一階層終了の場合
              elsif (check_record_tlv_parameta_var_check[l - 1] - check_record_tlv_parameta_var_check[l]) == 1
                check_tlvfile.puts '}'
                check_tlvfile.puts check_record_tlv_parameta_type[l] + "," + check_record_tlv_parameta_var[l] + "=" + check_record_tlv_parameta_size[l]

              #構造体が二階層終了の場合
              elsif (check_record_tlv_parameta_var_check[l - 1] - check_record_tlv_parameta_var_check[l]) == 2
                check_tlvfile.puts '}'
                check_tlvfile.puts '}'
                check_tlvfile.puts check_record_tlv_parameta_type[l] + "," + check_record_tlv_parameta_var[l] + "=" + check_record_tlv_parameta_size[l]
              end
            end
          end
        end
        l += 1
      end
    end

    ac_log_write(sprintf("AC_DefineData_Make_Cls::check_struct End\n"), 0)
  end

  #----------------------------------------------------------#
  #  TLV_NAME毎のParameterを構造体化                         #
  #----------------------------------------------------------#
  def tlv_parameter_make(para_num_make, tmp_file_name_make, record_tlv_parameta_type_make, record_tlv_parameta_var_make, record_tlv_parameta_var_check_make, record_tlv_parameta_size_make, record_tlv_parameter)
    ac_log_write(sprintf("AC_DefineData_Make_Cls::tlv_parameter_make Start\n"), 0)

    p_num = 0
    #個別パラメータがある場合
    if para_num_make != 0
      while p_num < para_num_make
        member_tmp = Member_parameter.new(tmp_file_name_make, record_tlv_parameta_type_make[p_num], record_tlv_parameta_var_make[p_num], record_tlv_parameta_var_check_make[p_num], record_tlv_parameta_size_make[p_num], para_num_make)
        record_tlv_parameter  << member_tmp
        p_num += 1
      end

    #個別パラメータがない場合
    else
      member_tmp = Member_parameter.new(tmp_file_name_make, "", "", "", "", 0)
      record_tlv_parameter  << member_tmp
    end

    ac_log_write(sprintf("AC_DefineData_Make_Cls::tlv_parameter_make End\n"), 0)
  end

  #----------------------------------------------------------#
  #  TLV.txtファイル作成                                     #
  #----------------------------------------------------------#
  def tlv_txt_make(tlv_dirname, tlv_record_ac_tlv, tlv_record_ac_tlv_tmp, tlv_record_tlv_name)
    ac_log_write(sprintf("AC_DefineData_Make_Cls::tlv_txt_make Start\n"), 0)

    i = 0
    while i < tlv_record_ac_tlv.length
      tlv_record_ac_tlv_tmp << tlv_record_ac_tlv[i].gsub(/^(\w+)\s+.*/, '\1')
      i += 1
    end

    puts "作成: TLV.txt"
    File.open("#{$D_ACTBL_PATH}/#{tlv_dirname}/TLV.txt", "w") do |tlvfile|
      i = 0
      ac_tmp = []
      while i < tlv_record_tlv_name.length
        j = 0
        while j < tlv_record_ac_tlv_tmp.length
          if (/#{tlv_record_tlv_name[i]}/ =~ tlv_record_ac_tlv_tmp[j])
            if i == 0
              ac_tmp << j
              tlvfile.puts tlv_record_ac_tlv[j]
              break
            else
              k = 0
              l = 0
              while k < ac_tmp.length
                if (ac_tmp[k] == j)
                  l = 1
                end
                k += 1
              end
              if (l == 0)
                ac_tmp << j
                tlvfile.puts tlv_record_ac_tlv[j]
                break
              else
                break
              end
            end
          end
          j += 1
        end
        i += 1
      end
    end

    ac_log_write(sprintf("AC_DefineData_Make_Cls::tlv_txt_make End\n"), 0)
  end

  #----------------------------------------------------------#
  #  CARD.txtファイル作成                                    #
  #----------------------------------------------------------#
  def card_txt_make(card_dirname)
    ac_log_write(sprintf("AC_DefineData_Make_Cls::card_txt_make Start\n"), 0)

    puts "作成: CARD.txt"
    File.open("#{$D_ACTBL_PATH}/#{card_dirname}/CARD.txt", "w") do |cardfile|
      cardfile.puts "D_LIU_BASE 0x00"
      cardfile.puts "D_LIU1 0x01"
      cardfile.puts "D_LIU2 0x02"
      cardfile.puts "D_LIU3 0x03"
      cardfile.puts "D_LIU4 0x04"
      cardfile.puts "D_LIU5 0x05"
      cardfile.puts "D_LIU6 0x06"
      cardfile.puts "D_LIU7 0x07"
      cardfile.puts "D_LIU8 0x08"
      cardfile.puts "D_LIU9 0x09"
      cardfile.puts "D_LIU10 0x0A"
      cardfile.puts "D_LIU11 0x0B"
      cardfile.puts "D_LIU12 0x0C"
      cardfile.puts "D_LIU13 0x0D"
      cardfile.puts "D_LIU14 0x0E"
      cardfile.puts "D_LIU15 0x0F"
      cardfile.puts "D_LIU16 0x10"
      cardfile.puts "D_CPU_BASE 0x20"
      cardfile.puts "D_CPU1 0x21"
      cardfile.puts "D_CPU2 0x22"
      cardfile.puts "D_SWF_BASE 0x30"
      cardfile.puts "D_SWF1 0x31"
      cardfile.puts "D_SWF2 0x32"
      cardfile.puts "D_CLK_BASE 0x40"
      cardfile.puts "D_CLK1 0x41"
      cardfile.puts "D_CLK2 0x42"
      cardfile.puts "D_ALM_BASE 0x50"
      cardfile.puts "D_ALM1 0x51"
      cardfile.puts "D_FAN_BASE 0x60"
      cardfile.puts "D_FAN1 0x61"
      cardfile.puts "D_FAN2 0x62"
      cardfile.puts "D_FAN3 0x63"
      cardfile.puts "D_FAN4 0x64"
      cardfile.puts "D_FAN5 0x65"

      #14884 Trib対応
      cardfile.puts "D_T1LIU_BASE 0x80"
      cardfile.puts "D_T1LIU1 0x81"
      cardfile.puts "D_T1LIU2 0x82"
      cardfile.puts "D_T1LIU3 0x83"
      cardfile.puts "D_T1LIU4 0x84"
      cardfile.puts "D_T1LIU5 0x85"
      cardfile.puts "D_T1LIU6 0x86"
      cardfile.puts "D_T1LIU7 0x87"
      cardfile.puts "D_T1LIU8 0x88"
      cardfile.puts "D_T1LIU9 0x89"
      cardfile.puts "D_T1LIU10 0x8A"
      cardfile.puts "D_T1LIU11 0x8B"
      cardfile.puts "D_T1LIU12 0x8C"
      cardfile.puts "D_T1LIU13 0x8D"
      cardfile.puts "D_T1LIU14 0x8E"
      cardfile.puts "D_T1LIU15 0x8F"
      cardfile.puts "D_T1LIU16 0x90"
      cardfile.puts "D_T1CPU_BASE 0xA0"
      cardfile.puts "D_T1CPU1 0xA1"
      cardfile.puts "D_T1CPU2 0xA2"
      cardfile.puts "D_T1SWF_BASE 0xB0"
      cardfile.puts "D_T1SWF1 0xB1"
      cardfile.puts "D_T1SWF2 0xB2"
      cardfile.puts "D_T1CLK_BASE 0xC0"
      cardfile.puts "D_T1CLK1 0xC1"
      cardfile.puts "D_T1CLK2 0xC2"
      cardfile.puts "D_T1ALM_BASE 0xD0"
      cardfile.puts "D_T1ALM1 0xD1"
      cardfile.puts "D_T1FAN_BASE 0xE0"
      cardfile.puts "D_T1FAN1 0xE1"
      cardfile.puts "D_T1FAN2 0xE2"
      cardfile.puts "D_T1FAN3 0xE3"
      cardfile.puts "D_T1FAN4 0xE4"
      cardfile.puts "D_T1FAN5 0xE5"
    end

    ac_log_write(sprintf("AC_DefineData_Make_Cls::card_txt_make End\n"), 0)
  end

  #----------------------------------------------------------#
  #  ACTION CODE対応表作成                                   #
  #----------------------------------------------------------#
  def action_code_make(action_dirname, action_record_num, action_record_sheet, action_record_name, action_record_id, action_record_file_name, action_record_sheet_name, action_record_sheet_tlv_name, action_record_ac_tlv, action_record_tlv_parameter)
    ac_log_write(sprintf("AC_DefineData_Make_Cls::action_code_make Start\n"), 0)

    #ACTION CODE対応表変数
    ac_record_num = []
    ac_record_name = []
    ac_record_id = []
    ac_record_file_name = []
    ac_record_file_name_tmp = []
    ac_record_sheet_name = []
    ac_record_sheet_name_tmp = []
    ac_record_sheet_tlv = []
    ac_record_sheet_tlv_id = []

    #EXCEL書き込み用変数
    excel_ac_record_num = []
    excel_ac_record_name = []
    excel_ac_record_id = []
    excel_ac_record_file_name = []
    excel_ac_record_sheet_name = []
    excel_ac_record_sheet_tlv = []
    excel_ac_record_sheet_tlv_id = []
    excel_ac_record_tlv_parameter_type = []
    excel_ac_record_tlv_parameter_var_1 = []
    excel_ac_record_tlv_parameter_var_2 = []
    excel_ac_record_tlv_parameter_var_3 = []
    excel_ac_record_tlv_parameter_size = []

    xlendian = WIN32OLE.new('Excel.Application')
    fso = WIN32OLE.new('Scripting.FileSystemObject')
    begin
      xlendian.SheetsInNewWorkbook = 1
      endianbook = xlendian.Workbooks.Add
      endiansheet = endianbook.Worksheets(1)

      #EXCEL初期化
      excel_init(endiansheet)

      #No/AC名/AC_ID/ファイル名/シート名 ソート
      i = 0
      while i < action_record_num.length
        j = 0
        while j < action_record_sheet.length
          if (action_record_sheet[j] == action_record_name[i])
            ac_record_file_name_tmp[i] = action_record_file_name[j]
            ac_record_sheet_name_tmp[i] = action_record_sheet_name[j]
            break
          elsif j == (action_record_sheet.length - 1)
            ac_record_file_name_tmp[i] = ""
            ac_record_sheet_name_tmp[i] = ""
          end
          j += 1
        end
        i += 1
      end

      #シート名/TLV(define名) ソート
      i = 0
      tlv_sheet_num = 0
      while i < action_record_sheet_tlv_name.length
        if i > 0
          if action_record_sheet_tlv_name[i].sheet == action_record_sheet_tlv_name[i - 1].sheet
            tlv_sheet_num += 1
          end
        end
        i += 1
      end

      l = 0
      i = 0
      while l < (action_record_num.length + tlv_sheet_num)
        while i < action_record_num.length
          j = 0
          while j < action_record_sheet_tlv_name.length
            if ac_record_sheet_name_tmp[i] == action_record_sheet_tlv_name[j].sheet
              k = 0
              while k < action_record_sheet_tlv_name[j].num
                ac_record_num[l + k] = action_record_num[i]
                ac_record_name[l + k] = action_record_name[i]
                ac_record_id[l + k] = action_record_id[i]
                ac_record_file_name[l + k] = ac_record_file_name_tmp[i]
                ac_record_sheet_name[l + k] = ac_record_sheet_name_tmp[i]
                ac_record_sheet_tlv[l + k] = action_record_sheet_tlv_name[j + k].name
                k += 1
              end
              l = l + k
              break
            elsif j == (action_record_sheet_tlv_name.length - 1)
              ac_record_num[l] = action_record_num[i]
              ac_record_name[l] = action_record_name[i]
              ac_record_id[l] = action_record_id[i]
              ac_record_file_name[l] = ac_record_file_name_tmp[i]
              ac_record_sheet_name[l] = ac_record_sheet_name_tmp[i]
              ac_record_sheet_tlv[l] = ""
              l += 1
            end
            j += 1
          end
          i += 1
        end
        l += 1
      end

      #TLV_ID ソート
      i = 0
      while i < (action_record_num.length + tlv_sheet_num)
        if ac_record_num[i] != nil
          if ac_record_sheet_tlv[i] == ""
            ac_record_sheet_tlv_id[i] = ""
          else
            j = 0
            while j < action_record_ac_tlv.length
              if (/^#{ac_record_sheet_tlv[i]}/ =~ action_record_ac_tlv[j])
                ac_record_sheet_tlv_id[i] = action_record_ac_tlv[j].gsub(/^\w+\s+(\w)/, '\1')
                if(/\n/ =~ ac_record_sheet_tlv_id[i])
                  ac_record_sheet_tlv_id[i] = ac_record_sheet_tlv_id[i].gsub(/\n/, '')
                end
                break
              elsif j == (action_record_ac_tlv.length - 1)
                ac_record_sheet_tlv_id[i] = ""
              end
              j += 1
            end
          end
        else
          break
        end
        i += 1
      end

      #Parameter ソート
      i = 0
      tlv_parameter_num = 0
      while i < action_record_tlv_parameter.length
        if i > 0
          if action_record_tlv_parameter[i].name == action_record_tlv_parameter[i - 1].name
            tlv_parameter_num += 1
          end
        end
        i += 1
      end

      l = 0
      i = 0
      while l < (action_record_num.length + tlv_sheet_num + tlv_parameter_num)
        while i < action_record_num.length + tlv_sheet_num
          j = 0
          while j < action_record_tlv_parameter.length
            if ac_record_sheet_tlv[i] == action_record_tlv_parameter[j].name
              if action_record_tlv_parameter[j].num == 0
                excel_ac_record_num[l] = ac_record_num[i]
                excel_ac_record_name[l] = ac_record_name[i]
                excel_ac_record_id[l] = ac_record_id[i]
                excel_ac_record_file_name[l] = ac_record_file_name[i]
                excel_ac_record_sheet_name[l] = ac_record_sheet_name[i]
                excel_ac_record_sheet_tlv[l] = ac_record_sheet_tlv[i]
                excel_ac_record_sheet_tlv_id[l] = ac_record_sheet_tlv_id[i]
                excel_ac_record_tlv_parameter_type[l] = ""
                excel_ac_record_tlv_parameter_var_1[l] = ""
                excel_ac_record_tlv_parameter_var_2[l] = ""
                excel_ac_record_tlv_parameter_var_3[l] = ""
                excel_ac_record_tlv_parameter_size[l] = ""
                l += 1
                break
              elsif action_record_tlv_parameter[j].num == 1
                excel_ac_record_num[l] = ac_record_num[i]
                excel_ac_record_name[l] = ac_record_name[i]
                excel_ac_record_id[l] = ac_record_id[i]
                excel_ac_record_file_name[l] = ac_record_file_name[i]
                excel_ac_record_sheet_name[l] = ac_record_sheet_name[i]
                excel_ac_record_sheet_tlv[l] = ac_record_sheet_tlv[i]
                excel_ac_record_sheet_tlv_id[l] = ac_record_sheet_tlv_id[i]
                excel_ac_record_tlv_parameter_type[l] = action_record_tlv_parameter[j].para_type
                excel_ac_record_tlv_parameter_var_1[l] = action_record_tlv_parameter[j].parameter
                excel_ac_record_tlv_parameter_var_2[l] = ""
                excel_ac_record_tlv_parameter_var_3[l] = ""
                excel_ac_record_tlv_parameter_size[l] = action_record_tlv_parameter[j].size
                l += 1
                break
              else
                k = 0
                while k < action_record_tlv_parameter[j].num
                  excel_ac_record_num[l + k] = ac_record_num[i]
                  excel_ac_record_name[l + k] = ac_record_name[i]
                  excel_ac_record_id[l + k] = ac_record_id[i]
                  excel_ac_record_file_name[l + k] = ac_record_file_name[i]
                  excel_ac_record_sheet_name[l + k] = ac_record_sheet_name[i]
                  excel_ac_record_sheet_tlv[l + k] = ac_record_sheet_tlv[i]
                  excel_ac_record_sheet_tlv_id[l + k] = ac_record_sheet_tlv_id[i]
                  excel_ac_record_tlv_parameter_type[l + k] = action_record_tlv_parameter[j + k].para_type
                  if action_record_tlv_parameter[j + k].parameter_check == 1
                    excel_ac_record_tlv_parameter_var_1[l + k] = action_record_tlv_parameter[j + k].parameter
                    excel_ac_record_tlv_parameter_var_2[l + k] = ""
                    excel_ac_record_tlv_parameter_var_3[l + k] = ""
                  elsif action_record_tlv_parameter[j + k].parameter_check == 2
                    excel_ac_record_tlv_parameter_var_1[l + k] = ""
                    excel_ac_record_tlv_parameter_var_2[l + k] = action_record_tlv_parameter[j + k].parameter
                    excel_ac_record_tlv_parameter_var_3[l + k] = ""
                  elsif action_record_tlv_parameter[j + k].parameter_check == 3
                    excel_ac_record_tlv_parameter_var_1[l + k] = ""
                    excel_ac_record_tlv_parameter_var_2[l + k] = ""
                    excel_ac_record_tlv_parameter_var_3[l + k] = action_record_tlv_parameter[j + k].parameter
                  end
                  if (/struct/ =~ action_record_tlv_parameter[j + k].para_type)
                    excel_ac_record_tlv_parameter_size[l + k] = ""
                  else
                    excel_ac_record_tlv_parameter_size[l + k] = action_record_tlv_parameter[j + k].size
                  end
                  k += 1
                end
                l = l + k
                break
              end
            elsif j == (action_record_tlv_parameter.length - 1)
              excel_ac_record_num[l] = ac_record_num[i]
              excel_ac_record_name[l] = ac_record_name[i]
              excel_ac_record_id[l] = ac_record_id[i]
              excel_ac_record_file_name[l] = ac_record_file_name[i]
              excel_ac_record_sheet_name[l] = ac_record_sheet_name[i]
              excel_ac_record_sheet_tlv[l] = ac_record_sheet_tlv[i]
              excel_ac_record_sheet_tlv_id[l] = ac_record_sheet_tlv_id[i]
              excel_ac_record_tlv_parameter_type[l] = ""
              excel_ac_record_tlv_parameter_var_1[l] = ""
              excel_ac_record_tlv_parameter_var_2[l] = ""
              excel_ac_record_tlv_parameter_var_3[l] = ""
              excel_ac_record_tlv_parameter_size[l] = ""
              l += 1
            end
            j += 1
          end
          i += 1
        end
        l += 1
      end

      #EXCEL出力
      puts "作成: ACTION CODE対応表"
      ac_log_write(sprintf("AC_DefineData_Make_Cls::action_code_make EXCEL output Start\n"), 0)
      i = 0
      excel_row = 0
      while i < (action_record_num.length + tlv_sheet_num + tlv_parameter_num)
        if excel_ac_record_num[i] != nil

          #14283 サイズに取消線が含まれている場合はセルを赤くする
          if excel_ac_record_tlv_parameter_size[i] =~ /取消線/
            endiansheet.Cells.Item((i + 5),2).interior.colorindex = 3
            endiansheet.Cells.Item((i + 5),3).interior.colorindex = 3
            endiansheet.Cells.Item((i + 5),4).interior.colorindex = 3
            endiansheet.Cells.Item((i + 5),5).interior.colorindex = 3
            endiansheet.Cells.Item((i + 5),6).interior.colorindex = 3
            endiansheet.Cells.Item((i + 5),7).interior.colorindex = 3
            endiansheet.Cells.Item((i + 5),8).interior.colorindex = 3
            endiansheet.Cells.Item((i + 5),9).interior.colorindex = 3
            endiansheet.Cells.Item((i + 5),10).interior.colorindex = 3
            endiansheet.Cells.Item((i + 5),11).interior.colorindex = 3
            endiansheet.Cells.Item((i + 5),12).interior.colorindex = 3
            endiansheet.Cells.Item((i + 5),13).interior.colorindex = 3
            endiansheet.Cells.Item((i + 5),14).interior.colorindex = 3
          end
          if (excel_ac_record_num[i] == excel_ac_record_num[i - 1] && i > 0)
            endiansheet.Cells.Item((i + 5),2).Value = excel_ac_record_num[i]
            endiansheet.Cells.Item((i + 5),2).Font.colorindex = 15
          else
            endiansheet.Cells.Item((i + 5),2).Value = excel_ac_record_num[i]
          end
          if (excel_ac_record_name[i] == excel_ac_record_name[i - 1] && i > 0)
            endiansheet.Cells.Item((i + 5),3).Value = excel_ac_record_name[i]
            endiansheet.Cells.Item((i + 5),3).Font.colorindex = 15
          else
            endiansheet.Cells.Item((i + 5),3).Value = excel_ac_record_name[i]
          end
          if (excel_ac_record_id[i] == excel_ac_record_id[i - 1] && i > 0)
            endiansheet.Cells.Item((i + 5),4).Value = excel_ac_record_id[i]
            endiansheet.Cells.Item((i + 5),4).Font.colorindex = 15
          else
            endiansheet.Cells.Item((i + 5),4).Value = excel_ac_record_id[i]
          end
          if (excel_ac_record_file_name[i] == excel_ac_record_file_name[i - 1] && i > 0)
            endiansheet.Cells.Item((i + 5),5).Value = excel_ac_record_file_name[i]
            endiansheet.Cells.Item((i + 5),5).Font.colorindex = 15
          else
            endiansheet.Cells.Item((i + 5),5).Value = excel_ac_record_file_name[i]
          end
          if (excel_ac_record_sheet_name[i] == excel_ac_record_sheet_name[i - 1] && i > 0)
            endiansheet.Cells.Item((i + 5),6).Value = excel_ac_record_sheet_name[i]
            endiansheet.Cells.Item((i + 5),6).Font.colorindex = 15
          else
            endiansheet.Cells.Item((i + 5),6).Value = excel_ac_record_sheet_name[i]
          end
          if (excel_ac_record_sheet_tlv[i] == excel_ac_record_sheet_tlv[i - 1] && i > 0)
            endiansheet.Cells.Item((i + 5),7).Value = excel_ac_record_sheet_tlv[i]
            endiansheet.Cells.Item((i + 5),7).Font.colorindex = 15
          else
            endiansheet.Cells.Item((i + 5),7).Value = excel_ac_record_sheet_tlv[i]
          end
          if (excel_ac_record_sheet_tlv_id[i] == excel_ac_record_sheet_tlv_id[i - 1] && i > 0)
            endiansheet.Cells.Item((i + 5),8).Value = excel_ac_record_sheet_tlv_id[i]
            endiansheet.Cells.Item((i + 5),8).Font.colorindex = 15
          else
            endiansheet.Cells.Item((i + 5),8).Value = excel_ac_record_sheet_tlv_id[i]
          end
          endiansheet.Cells.Item((i + 5),9).Value = excel_ac_record_tlv_parameter_type[i]
          endiansheet.Cells.Item((i + 5),10).Value = excel_ac_record_tlv_parameter_var_1[i]
          endiansheet.Cells.Item((i + 5),11).Value = excel_ac_record_tlv_parameter_var_2[i]
          endiansheet.Cells.Item((i + 5),12).Value = excel_ac_record_tlv_parameter_var_3[i]
          endiansheet.Cells.Item((i + 5),13).Value = excel_ac_record_tlv_parameter_size[i]
          if (/struct/ =~ excel_ac_record_tlv_parameter_type[i])
            endiansheet.Cells.Item((i + 5),14).Value = ""
          elsif (/short/ =~ excel_ac_record_tlv_parameter_type[i])
            endiansheet.Cells.Item((i + 5),14).Value = "16bit変換"
          elsif (/long/ =~ excel_ac_record_tlv_parameter_type[i])
            endiansheet.Cells.Item((i + 5),14).Value = "32bit変換"
          elsif excel_ac_record_tlv_parameter_type[i] == ""
            endiansheet.Cells.Item((i + 5),14).Value = ""
          else
            endiansheet.Cells.Item((i + 5),14).Value = "変換なし"
          end
          excel_row += 1
        end
        i += 1
      end

      #セルに境界線を引く
      excel_row = excel_row + 4
      endiansheet.Range("B4:N#{excel_row}").Borders.LineStyle = 1

      xlendian.displayAlerts = false
      endianbook.saveAs(fso.GetAbsolutePathName("#{$D_ACTBL_PATH}/#{action_dirname}/ACTION CODE対応表.xlsx"))
      xlendian.displayAlerts = true
      ac_log_write(sprintf("AC_DefineData_Make_Cls::action_code_make EXCEL output End\n"), 0)
    rescue => e
      ac_log_write(sprintf("[ERROR]%s\n", $!), 0)
      p $!
      e.backtrace.each{|errdata|
        ac_log_write(sprintf("[ERROR]%s\n", errdata), 0)
        p errdata
      }
    ensure
      xlendian.Workbooks.Close
      xlendian.Quit
    end

    ac_log_write(sprintf("AC_DefineData_Make_Cls::action_code_make End\n"), 0)
  end

  #----------------------------------------------------------#
  #  ACTION CODE対応表作成                                   #
  #----------------------------------------------------------#
  def excel_init(endiansheet)
    ac_log_write(sprintf("AC_DefineData_Make_Cls::excel_init Start\n"), 0)

    #シート名を設定
    endiansheet.Name = "ACTION CODE対応表"

    #セル結合
    endiansheet.Range("J4:L4").MergeCells = true

    #タイトル
    endiansheet.Cells.Item(2,3).Value = "■ACTION CODE対応表"
    endiansheet.Cells.Item(4,2).Value = "No"
    endiansheet.Cells.Item(4,3).Value = "ACTION CODE(define名)"
    endiansheet.Cells.Item(4,4).Value = "ACTION CODE(ID)"
    endiansheet.Cells.Item(4,5).Value = "ファイル名"
    endiansheet.Cells.Item(4,6).Value = "シート名"
    endiansheet.Cells.Item(4,7).Value = "TLV(define名)"
    endiansheet.Cells.Item(4,8).Value = "TLV(ID)"
    endiansheet.Cells.Item(4,9).Value = "Parameter(型名)"
    endiansheet.Cells.Item(4,10).Value = "Parameter(変数名)"
    endiansheet.Cells.Item(4,13).Value = "Size"
    endiansheet.Cells.Item(4,14).Value = "Endian有無"

    #タイトルを太字にする
    endiansheet.Cells.Item(2,2).Font.Bold = true
    endiansheet.Cells.Item(4,2).Font.Bold = true
    endiansheet.Cells.Item(4,3).Font.Bold = true
    endiansheet.Cells.Item(4,4).Font.Bold = true
    endiansheet.Cells.Item(4,5).Font.Bold = true
    endiansheet.Cells.Item(4,6).Font.Bold = true
    endiansheet.Cells.Item(4,7).Font.Bold = true
    endiansheet.Cells.Item(4,8).Font.Bold = true
    endiansheet.Cells.Item(4,9).Font.Bold = true
    endiansheet.Cells.Item(4,10).Font.Bold = true
    endiansheet.Cells.Item(4,13).Font.Bold = true
    endiansheet.Cells.Item(4,14).Font.Bold = true

    #タイトルのセルに色をつける
    endiansheet.Cells.Item(4,2).interior.colorindex = 35
    endiansheet.Cells.Item(4,3).interior.colorindex = 35
    endiansheet.Cells.Item(4,4).interior.colorindex = 35
    endiansheet.Cells.Item(4,5).interior.colorindex = 35
    endiansheet.Cells.Item(4,6).interior.colorindex = 35
    endiansheet.Cells.Item(4,7).interior.colorindex = 35
    endiansheet.Cells.Item(4,8).interior.colorindex = 35
    endiansheet.Cells.Item(4,9).interior.colorindex = 35
    endiansheet.Cells.Item(4,10).interior.colorindex = 35
    endiansheet.Cells.Item(4,13).interior.colorindex = 35
    endiansheet.Cells.Item(4,14).interior.colorindex = 35

    #セルの幅を変更
    endiansheet.Cells.Item(4,2).ColumnWidth = 3.8
    endiansheet.Cells.Item(4,3).ColumnWidth = 41.4
    endiansheet.Cells.Item(4,4).ColumnWidth = 17.7
    endiansheet.Cells.Item(4,5).ColumnWidth = 61.9
    endiansheet.Cells.Item(4,6).ColumnWidth = 42.6
    endiansheet.Cells.Item(4,7).ColumnWidth = 51.1
    endiansheet.Cells.Item(4,8).ColumnWidth = 12.3
    endiansheet.Cells.Item(4,9).ColumnWidth = 42.3
    endiansheet.Cells.Item(4,10).ColumnWidth = 3.8
    endiansheet.Cells.Item(4,11).ColumnWidth = 3.8
    endiansheet.Cells.Item(4,12).ColumnWidth = 18.2
    endiansheet.Cells.Item(4,13).ColumnWidth = 4.6
    endiansheet.Cells.Item(4,14).ColumnWidth = 10.2

    ac_log_write(sprintf("AC_DefineData_Make_Cls::excel_init End\n"), 0)
  end

  #----------------------------------------------------------#
  #  ac_tbl/D_XXXXファイル圧縮関数 #14973                    #
  #----------------------------------------------------------#
  def tar_make(dirname)
    ac_log_write(sprintf("AC_DefineData_Make_Cls::tar_make Start\n"), 0)

    puts "圧縮: " + $D_ACTBL_PATH + '/' + dirname

    # 圧縮対象のディレクトリ
    #17152対応 noda
    dir_tar = "#{$D_ACTBL_PATH}/#{dirname}"

    # 圧縮ファイルのパス
    archive_path = "#{dirname}.gz"

    # カレントディレクトリを保存
    current_dir = $D_RUN_FULL_PATH
    # カレントディレクトリを変更
    Dir::chdir(File.expand_path(dir_tar))

    Zlib::GzipWriter.open(archive_path) do |gz|
      out = Archive::Tar::Minitar::Output.new(gz)
      Dir.glob('./D_*') do |file|
        Archive::Tar::Minitar::pack_file(file, out)
        File.delete(file)
      end
      out.close
    end

    # カレントディレクトリを復元
    Dir::chdir(current_dir)

    ac_log_write(sprintf("AC_DefineData_Make_Cls::tar_make End\n"), 0)
  end

  #-------------------------------------------------------------#
  #  Sizeに取り消し線があるか判定する関数                       #
  #-------------------------------------------------------------#
  def deleteline_check(sheet, row)
    ac_log_write(sprintf("AC_DefineData_Make_Cls::deleteline_check Start\n"), 2)

    tmp_check = ""
    tmp_check = sheet.Cells.Item((row.Row),"I").Value.to_s

    ac_log_write(sprintf("AC_DefineData_Make_Cls::deleteline_check End\n"), 2)
    if tmp_check =~ /\w+\s+\w/
      return 1
    end
    return 0

  end

  #-------------------------------------------------------------#
  #  エクセルファイル内の取り消し線がある場合にmsg BOX表示関数  #
  #-------------------------------------------------------------#
  def deleteline_msg(dirname)
    ac_log_write(sprintf("AC_DefineData_Make_Cls::deleteline_msg Start\n"), 0)

    wsh = WIN32OLE.new('WScript.Shell')
    wsh.Popup("参照している" + dirname + "内のファイルに取消線が含まれています。\n送信したいACである場合は、取消線を削除して再度テーブルを作成してください。")

    ac_log_write(sprintf("AC_DefineData_Make_Cls::deleteline_msg End\n"), 0)
  end
end

#----------------------------------------------------------#
#  AC ファイルチェック                                     #
#----------------------------------------------------------#
def file_check(dir_ac, logobj)
  logobj.write(sprintf("file_check Start\n"), 0, File.basename(__FILE__), __LINE__)

  wsh = WIN32OLE.new('WScript.Shell')

  unless FileTest.exist?(dir_ac)
    wsh.Popup("フォルダが指定されていません。\nフォルダを指定して実行をお願いします。",0, "終了")
    logobj.write(sprintf("Folder is not specified\n"), 1, File.basename(__FILE__), __LINE__)
    return false, nil
  else
    #includeファイル参照
    puts dir_ac
    dir_tlv = dir_ac + "/" + "include"
    puts "参照先 : " + dir_tlv

    #ディレクトリ名取得(パスなし)
    dirname = File.basename(dir_ac)

    #includeフォルダが存在しない場合、終了
    unless FileTest.exist?(dir_tlv)
      logobj.write(sprintf("Nothing [%s]\n", dir_tlv), 1, File.basename(__FILE__), __LINE__)
      error_flag = true
      error_line = "指定されたフォルダにincludeフォルダが存在しません\n"
      error_line << "『TECHINFO解析ツール説明書_Step3_verX.X.xlsx』の\n"
      error_line << "シート『『Action Code定義データ生成』ツール説明』を確認し\n"
      error_line << "必要なファイルを準備してください。\n"
      error_line << "その準備したフォルダを指定し再度ツールを実行してください\n"
      wsh.Popup(error_line,0,"Error")
      return false, nil
    end

    #ac_tblディレクトリ作成
    FileUtils.mkdir($D_ACTBL_PATH) unless FileTest.exist?($D_ACTBL_PATH)
    FileUtils.mkdir($D_ACTBL_PATH + "/#{dirname}") unless FileTest.exist?($D_ACTBL_PATH + "/#{dirname}")
  end

  logobj.write(sprintf("file_check End\n"), 0, File.basename(__FILE__), __LINE__)
  return true,$D_ACTBL_PATH + "/#{dirname}"
end




