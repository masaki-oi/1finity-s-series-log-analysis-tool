#coding: windows-31j
#/****************************************************************************
# *  fileName: TECHINFO_Analysis_TypeS.rb
# *
# *               All rights reserved, Copyright (C) FUJITSU LIMITED 2017-2020
# *-----------------------------------------------------------------------------
# *  処理概要: ツールのメイン処理
# *            
# *  引数    : なし
# *  復帰値  : なし
# *---------------------------------------------------------------------------
# *  変更履歴: 2020.12.24 Nishi     新規作成(Version 1.5)
# *
# ****************************************************************************/
#----------------------------------------------------------#
# 基本 require
#----------------------------------------------------------#
if __FILE__ == "TECHINFO_Analysis_TypeS.rb"
  runpath = Dir::pwd
else
  runpath = File.dirname(__FILE__)
end

require 'win32ole'
require 'yaml'
require 'pp'
require 'find'
require 'zlib'
require 'kconv'
require 'archive/tar/minitar'
require 'stringio'
require 'fileutils'
include Archive::Tar
require 'scanf'
require 'singleton'
require File.expand_path(runpath + './TECHINFO_Analysis_Common.rb')
require File.expand_path(runpath + './TECHINFO_AlmLog_Analysis.rb')
require File.expand_path(runpath + './TECHINFO_Almdump_Analysis.rb')
require File.expand_path(runpath + './TECHINFO_PIlog_Analysis.rb')
require File.expand_path(runpath + './TECHINFO_SDH_ASP_Analysis.rb')
#require File.expand_path(runpath + './TECHINFO_ACDefineDataGenarate.rb')
#require File.expand_path(runpath + './TECHINFO_ACLog_Analysis.rb')
require File.expand_path(runpath + './TECHINFO_One_finity_tool.rb')
require File.expand_path(runpath + './TECHINFO_Analysis_CUI.rb')
# require 'green_shoes'
require 'uri'
require 'thread'
require "open3"
require 'optparse'

#----------------------------------------------------------#
# メイン処理
#----------------------------------------------------------#
@logobj = Log_class.instance
@logobj.write("Main Start\n")

print "[debug] ARGV: ", ARGV, "\n"

if ARGV.empty?
    input_path = ""
else
    input_path = TECHINFO_Analysis_CUI.arg_parse(ARGV)
end

if input_path != ""
    $exec_CUI_mode = true
    
    puts "*** CUI Start. "
    puts "* Input=#{input_path}"
    rcode = TECHINFO_Analysis_CUI.Alm_log_analyze(input_path)
    puts "*** CUI End"
    exit(rcode)
end

