#coding: windows-31j
#/****************************************************************************
# *  fileName: TECHINFO_SDH_ASP_Analysis.rb
# *
# *                      All rights reserved, Copyright (C) FUJITSU LIMITED 2016
# *-----------------------------------------------------------------------------
# *  処理概要: 切り替えログを解析する
# *            
# *  引数    : なし
# *  復帰値  : なし
# *---------------------------------------------------------------------------
# *  変更履歴: 2016.11.24 katou     新規作成(Version 1.0)
# *                                 L2の解析ツールをベースとする。
# *
# ****************************************************************************/

#----------------------------------------------------------#
# クラス
#----------------------------------------------------------#
class SDH_ASP_Analyze_Cls < Common_Analyze_Cls

    #----------------------------------------------------------#
    # コンストラクタ
    #----------------------------------------------------------#
    def initialize(in_label)
        @logobj = Log_class.instance
        @logobj.write(sprintf("SDH_ASP_Analyze_Cls::initialize Start\n"), 0, File.basename(__FILE__), __LINE__)
        @label = in_label
        @logobj.write(sprintf("SDH_ASP_Analyze_Cls::initialize End\n"), 0, File.basename(__FILE__), __LINE__)
    end
    
    #----------------------------------------------------------#
    #  Excelファイルの作成                                     #
    #----------------------------------------------------------#
    def createExcelWorkBook(files)
        @logobj.write("SDH_ASP_Analyze_Cls::createExcelWorkBook Start\n", 0, File.basename(__FILE__), __LINE__)
        puts "[debug] createExcelWorkBook" #debug
        @xl = WIN32OLE.new('Excel.Application')
        fso = WIN32OLE.new('Scripting.FileSystemObject')
        
        begin
            filename = files
            filename.gsub!("\\","\/")
            renamefilename = File.basename(filename, ".*")
            renamefilename = File.basename(renamefilename, ".*")
            renamedirname  = File.dirname(filename)
            puts "dir  :" + renamedirname
            puts "file :" + renamefilename
            renamefilename2 = renamedirname + "/" + renamefilename + "_S100_SWITCH.xlsx"
            @xl.SheetsInNewWorkbook = 1                   # 新bookのsheetを1枚に設定
            book = @xl.Workbooks.Add
            book.SaveAs(fso.GetAbsolutePathName(renamefilename2))   #ファイルを保存
            
            return book
            
        # 異常系処理
        rescue => e
            puts $!
            e.backtrace.each{|errdata|
              @logobj.write(sprintf("[ERROR]%s\n", errdata), 0, File.basename(__FILE__), __LINE__)
              p errdata
            }
            
            wsh = WIN32OLE.new('WScript.Shell')
            wsh.Popup("異常終了 出力すべきファイルがExcelでOpenされています\n閉じて実行して下さい。",0, "終了")
            
            if book != nil
                book.close
            end
            
            @xl.quit
            
            return nil
            
        # 後始末
        ensure
            # 記録
            if book == nil 
              return nil
            end
            @logobj.write("SDH_ASP_Analyze_Cls::createExcelWorkBook End\n", 0, File.basename(__FILE__), __LINE__)
            
        end
    end
    
    #----------------------------------------------------------
    # Excel を出力(Saveする)
    #----------------------------------------------------------
    def dispExcelWorkbook(book)
        @logobj.write("SDH_ASP_Analyze_Cls::dispExcelWorkbook Start\n", 0, File.basename(__FILE__), __LINE__)
        
        puts "[debug] dispExcelWorkbook"
        
        # Excelを表示
        @xl.Visible = true
        puts book
        book.save
        
        @logobj.write("SDH_ASP_Analyze_Cls::dispExcelWorkbook End\n", 0, File.basename(__FILE__), __LINE__)
        
    end
    
    #----------------------------------------------------------
    # fail Excel クローズ(Saveする)
    #----------------------------------------------------------
    def closeExcelWorkbook(book)
        @logobj.write("SDH_ASP_Analyze_Cls::closeExcelWorkbook Start\n", 0, File.basename(__FILE__), __LINE__)
        
        puts "[debug] closeExcelWorkbook"
        
        # Excelを表示
        book.Saved = true
        book.Close
        @xl.quit
        
        @logobj.write("SDH_ASP_Analyze_Cls::closeExcelWorkbook End\n", 0, File.basename(__FILE__), __LINE__)
        
        wsh = WIN32OLE.new('WScript.Shell')
        popup_msg =  sprintf("異常終了\nlogのformatが違う可能性があります\nまた、本機能を使いたい場合は一度メイン画面から起動しなおしてください\n")
        wsh.Popup(popup_msg, 0, "異常通知")
    end
    
    #----------------------------------------------------------#
    # analyze                                                  #
    #----------------------------------------------------------#
    def analyze(path, book)
        @logobj.write("SDH_ASP_Analyze_Cls::analyze Start\n", 0, File.basename(__FILE__), __LINE__)
        wsh = WIN32OLE.new('WScript.Shell')
        puts "[debug] analyze" #debug
        ret = true
        
        sheets = book.sheets(1)
        sheets.Name = "切り替えログ"                      # シート名を設定
        
        line_count = 0
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filedata = entry.read
                if (filedata != nil)
                    
                    filename = File.basename(entry.name)
                    
                    if( filename =~ /ELP_MGR/ )
                        data = StringIO.new(filedata)
                        
                        log_start = 0
                        loop_data = ""
                        time_data = ""
                        
                        puts filename
                        #エクセルへの吐き出し
                        data.each_line  do |read_data|
                            
                            read_data = read_data.unpack("Z*").join(",").delete("\n")
                            
                            #日付ありデータ
                            if /(\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}.\d{6})/ =~ read_data
                                if( (time_data != "") && (time_data =~ /process_fsm_ret_data/) )
                                    #次のデータが見つかった時に前のデータを書き込む
                                    line_count += 1
                                    sheets.Cells(line_count, 1).value = filename
                                    sheets.Cells(line_count, 2).value = time_data
                                end
                                
                                log_start = 1
                                time_data = read_data
                            else
                            #日付なしデータ
                                #日付データがまだない場合はためておく
                                if(log_start == 0)
                                    loop_data += "\n"+ read_data
                                else
                                    time_data += "\n"+ read_data
                                end
                            end
                        end
                        #最後のデータを書き込む
                        #ループデータがあれば、最後の行にくっつける
                        final_line = time_data + loop_data
                        if( (final_line != "") && (final_line =~ /process_fsm_ret_data/) )
                            sheets.Cells(line_count, 1).value = filename
                            sheets.Cells(line_count, 2).value = final_line
                        end
                        
                    end #filename =~ /ELP_MGR*.LOG/
                    
                    if( filename == "trap.log" )
                        puts filename
                        data = StringIO.new(filedata)
                        
                        loop_data = ""
                        trap_count = line_count
                        
                        #エクセルへの吐き出し
                        data.each_line  do |read_data|
                            
                            read_data = read_data.unpack("Z*").join(",").delete("\n")
                            
                            #正常データ
                            if /(\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}.\d{6})/ =~ read_data
                                trap_count += 1
                                sheets.Cells(trap_count, 1).value = filename
                                sheets.Cells(trap_count, 2).value = read_data
                            else
                            #異常データ
                                #(1行目だった場合はループしたデータなので保存する)
                                if(trap_count == line_count)
                                    loop_data = read_data
                                else #1行目以外は本当に異常なものなので捨てる
                                end
                            end
                        end
                        #ループデータがあれば、最後の行にくっつける
                        if(loop_data != "")
                            sheets.Cells(trap_count, 2).value += loop_data
                        end
                        
                        if(trap_count > line_count)
                            delete_line = 0
                            for line in line_count+1...trap_count+1
                                line_data = sheets.Cells(line, 2).value
                                if( ( line_data =~ /\[ALM\]:/ ) || ( line_data =~ /\[TCA\]:/ ) )
                                    sheets.Rows(line).delete
                                    delete_line += 1
                                    redo
                                end
                            end
                            line_count = trap_count - delete_line
                        end
                    end
                    
                end # filedata != nil
            end # do |entry|
        end # do |tgz|
        
        chk_log_max_set(line_count,"切り替えログ解析")
        
        #エクセルファイルのデータを整形
        if(line_count > 0)
            #時刻とほかのデータを分離
            for line in 1...line_count + 1 do
                
                line_data = sheets.Cells(line, 2).value
                #時刻データを取得(fileの途中で途中のデータパターンが被る可能性があるので時刻だけ先に取り出す。)
                #(時刻データのパターンは被ることはない。被った場合は別の行にloggingされる)
                time_data = line_data.scan(/\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}.\d{6}/).join(",")
                
                sheets.Cells(line, 2).value = time_data.scan(/(\d{4}\/\d{2}\/\d{2})/)
                sheets.Cells(line, 3).value = time_data.scan(/(\d{2}:\d{2}:\d{2})/)
                sheets.Cells(line, 4).value = time_data.scan(/(.\d{6})/)
                
                #データから時刻データを削除
                line_data.gsub!(/\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}.\d{6} /,"")
                #
                sheets.Cells(line, 5).value = line_data
                
                update_progress()
                sleep 0.01
                
            end
            sheets.Range(sheets.cells(1,1), sheets.cells(line_count,5)).Sort({"key1" => sheets.Range("B1"), 
                                                                              "key2" => sheets.Range("C1"), 
                                                                              "key3" => sheets.Range("D1")})
            
            sheets.Columns(5).ColumnWidth = 80
            
        end
        
        sheets.Rows(1).Insert
        line_count += 1
        
        #ヘッダ部分
        sheets.Cells(1,  1).Value = "file name"
        sheets.Cells(1,  2).Value = "Date"
        sheets.Cells(1,  3).Value = "Time"
        sheets.Cells(1,  4).Value = "Time(.sec)"
        
        sheets.Cells(1,  5).Value = "Data(各ファイルの最後のデータには無効データが入っている可能性があります)"
        
        #罫線
        sheets.Range(sheets.cells(1,1),sheets.cells(line_count,5)).Borders.LineStyle = 1
        
        # セル色変更
        sheets.Range(sheets.cells(1,1),sheets.cells(1,5)).Interior.ColorIndex = 4
        
        #自動整形
        sheets.Columns.AutoFit
        sheets.Rows.AutoFit
        
        sleep 0.3
        
        # 解析終了(正常終了のみ)
        if ret
          popup_msg =  sprintf("解析完了\n")
          wsh.Popup(popup_msg, 0, "正常通知")
        end
        
        return ret
        
    end
end
