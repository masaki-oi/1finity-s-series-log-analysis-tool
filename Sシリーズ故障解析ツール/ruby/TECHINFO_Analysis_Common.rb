#coding: windows-31j
#/****************************************************************************
# *  fileName: TECHINFO_Analysis_Common.rb
# *
# *               All rights reserved, Copyright (C) FUJITSU LIMITED 2016-2018
# *-----------------------------------------------------------------------------
# *  処理概要: 共通処理、グローバル変数の定義
# *            
# *  引数    : なし
# *  復帰値  : なし
# *---------------------------------------------------------------------------
# *  変更履歴: 2016.11.24 katou     新規作成(Version 1.0)
# *                                 L2の解析ツールをベースとする。(不要な処理も残っている)
# *            2017.05.19 kinomoto  機能追加(Version 1.1)
# *                                 (1) CUI対応
# *                                 (2) 被疑部品検索機能対応
# *                                 (3) FLTテーブルファイル名を実行時に取得
# *                                 (4) 実行ログをTSV形式で出力可能にする
# *            2017.08.18 kinomoto  温度Over検出機能追加 (Version 1.2)
# * -Version 1.3 -
# *            2018.02.22 kinomoto  trap.log の新規レコード形式に対応
# *                                   〜 [EVE]:/blade1ram1/TECHINFO_S100_20171111192335.tar.gz,〜
# * -Version 1.4 -
# *            2018.10.31 kinomoto  機能追加: --flt_tab_check オプション
# * -Version 1.5 -
# *     2020.12.11 Nishi  機能追加: S150対応
# *                       バグ修正: 警報変化点ログ解析で出力された結果シートの
# *                                 Cond_idの列が一部正しい内容になっていない問題を対処
# *
# ****************************************************************************/

#----------------------------------------------------------#
# Version変数                                              #
#----------------------------------------------------------#
$log_analyeze_version = "Version 1.5"

#----------------------------------------------------------#
# 共通用 グローバル変数                                    #
#----------------------------------------------------------#
$exclusion_flag = 0

$exec_CUI_mode = false
$exec_log_tsv = true
$exec_visible = false

# S150対応 Change Start 2020/12/09 by Nishi
$g_pattern_flt_table = '☆*_%s*\.xlsx'
# S150対応 Change End 2020/12/09 by Nishi
$g_filename_flt_table = nil # ☆S100_Rls2.1_警報_1.00版_ad170314.xlsx
# S150対応 Add Start 2020/12/09 by Nishi
$g_machine_type = nil 
# S150対応 Add End 2020/12/09 by Nishi

#----------------------------------------------------------#
# Progress変数                                             #
#----------------------------------------------------------#
$progress_value = 0
$progress_max_log_no = 0
$progress_log_no = 0

$progress_value2 = 0
$progress_value3 = 0
$progress_value4 = 0


$progress_funcname = "xxx"
#----------------------------------------------------------#
# 定義
#----------------------------------------------------------#
$D_LOG_MODE = "ON"
$D_LOG_LEVEL = 1
$D_LOG_FILENAME = "THECINFO_Analysis.log"
$D_AC_LOG_FILENAME = "ACLog_Analysis.log"
$D_LOG_LIU_HASH_FILENAME = "THECINFO_g_liu_address_hash.log"

#----------------------------------------------------------#
# パス定義
#----------------------------------------------------------#
$D_RUN_FULL_PATH = Dir.pwd
$D_RUN_PATH = $D_RUN_FULL_PATH.gsub(/\/ruby/,"")
# $D_RUN_PATH = "D:/01_L2_TP_s3/20_作成中文章/24_STEP3_TECHINFO解析ツール/sample_log/パス変数化チェック"

# cfg
$D_CONFIG_PATH = $D_RUN_PATH + "/cfg"

# cfg\ac_tbl
$D_ACTBL_PATH = $D_CONFIG_PATH + "/ac_tbl"

# cfg\flt_table
$D_FLTTBL_PATH = $D_CONFIG_PATH + "/flt_table"

# cfg\step3\almchg
$D_ALMTBL_PATH = $D_CONFIG_PATH + "/step3/almchg"

# cfg\step3\almdump_cfg
$D_ALMDUMP_PATH = $D_CONFIG_PATH + "/step3/almdump_cfg"

# cfg\1finity_tool
$D_TOOL_PATH = $D_CONFIG_PATH + "/1finity_tool"

# doc
$D_DOC_PATH = $D_RUN_PATH + "/doc"

# ruby
$D_SOURCE_PATH = $D_RUN_PATH + "/ruby"

# ruby\installer
$D_INSTALL_PATH = $D_SOURCE_PATH + "/installer"

# デバッグ出力
=begin
puts $D_RUN_FULL_PATH
puts $D_RUN_PATH
puts $D_CONFIG_PATH
puts $D_ACTBL_PATH
puts $D_ALMTBL_PATH
puts $D_ALMDUMP_PATH
puts $D_DOC_PATH
puts $D_SOURCE_PATH
puts $D_INSTALL_PATH
=end

#----------------------------------------------------------#
# Almlog_Analysis用 グローバル変数                         #
#----------------------------------------------------------#
$g_flt_tab_check = false

$g_step2_log_fltonlychk = "Skip"
$g_step2_log_colorchk = "Skip"
$g_step2_log_hexchk     = "Skip"
$g_step2_log_bitanalyzechk = "Skip"
$g_step2_log_macro_run_flag = false
$g_step2_log_unitconfigsheetrow = 2
$g_step2_log_tarfilename = ""
$g_step2_sheetname = ""
$g_liu_alm_tbl = []
$g_swf_address_hash = nil
$g_cpu_address_hash = nil
$g_liu_address_hash = nil

$g_step2_log_repairparts = false
$g_step2_log_fltchk = "Skip"
$g_step2_log_stschgchk = "する"
$g_step2_log_detailchk = "Skip"
$g_step2_log_eccerrchk = "Skip"
$g_step2_log_softerrchk = "Skip"
$g_step2_log_ffanchk = 0 # Skip
$g_step2_log_cyclechgchk = "Skip"
$g_step2_log_ilanchk = "Skip"
$g_step2_log_filechk = "全体"

$deviceName = [[1000,"UM-FPGA,UML-FPGA"], [1001,"CM-FPGA"], [1002,"CM-CPLD,CML-CPLD"], [1003,"NVSRAM"], [1004,"Boot/Apli Flash(BOOT1面)"],
                    [1005,"Boot/Apli Flash(APL0面)"], [1006,"Boot/Apli Flash(APL1面)"], [1007,"Config Flash(0面)"], [1008,"Config Flash(1面)"], [1009,"sRIO Switch,sRIO Switch Ma"],
                    [1010,"EEPROM"], [1011,"RTC"], [1012,"Current"], [1013,"UART"], [1014,"QUAD PHY"],
                    [1015,"Boot/Apli Flash(面情報)"], [1016,"Boot/Apli Flash(Kernel0面)"], [1017,"Boot/Apli Flash(Kernel1面)"], [1018,"Boot/Apli Flash(APL0面)"], [1019,"Boot/Apli Flash(APL1面)"],
                    [1020,"sRIO"], [1021,"統計情報ログ(帯域制限)"], [1022,"ALM PHY"], [1023,"Boot/Apli Flash(BOOT0面)"], [1024,"UMT-FPGA"],
                    [1025,"BRM-FPGA"], [1026,"BRT-FPGA"], [1027,"CMT-CPLD"], [1028,"sRIO Switch Trib"], [1029,"Config Flash(Trib)(0面)"],
                    [1030,"Config Flash(Trib)(1面)"], [2000,"UM-FPGA,UML-FPGA"], [2001,"CM-FPGA"], [2002,"NVSRAM"], [2003,"Boot/Apli Flash(BOOT1面)"], [2004,"Boot/Apli Flash(APL0面)"],
                    [2005,"Boot/Apli Flash(APL1面)"], [2006,"sRIO Switch"], [2007,"Boot/Apli Flash(面情報)"], [2008,"Boot/Apli Flash(Kernel0面)"], [2009,"Boot/Apli Flash(Kernel1面)"],
                    [2010,"Boot/Apli Flash(APL0面)"], [2011,"Boot/Apli Flash(APL1面)"], [2012,"sRIO"], [2013,"Boot/Apli Flash(BOOT0面)"], [2014,"UMT-FPGA"],
                    [2015,"BRM-FPGA"], [2016,"BRT-FPGA"],[3000,"CFG-CPLD"], [3001,"CIF-FPGA,PCIF-FPGA,HCIF-FPGA"], [3002,"NVSRAM"],
                    [3003,"Boot/Apli Flash(BOOT1面)"], [3004,"Boot/Apli Flash(APL0面)"],
                    [3005,"Boot/Apli Flash(APL1面)"], [3006,"Config Flash(0面)"], [3007,"Config Flash(1面)"], [3008,"sRIO Switch"], [3009,"PI EEPROM"],
                    [3010,"温度計(I2C port#1)"], [3011,"温度計(I2C port#2)"], [3012,"温度計(I2C port#4)"], [3013,"電流計"], [3014,"EP-FPGA,SEP-FPGA"],
                    [3015,"PP-FPGA,PPI-FPGA"], [3016,"HL-FPGA"], [3017,"POAM-FPGA,OAM-FPGA"], [3018,"MAC-FPGA"], [3019,"CES-FPGA"],
                    [3020,"META1-FPGA"], [3021,"META2-FPGA"], [3022,"ARAD"], [3023,"PM54201-FPGA"], [3024,"PM54202-FPGA"],
                    [3025,"Boot/Apli Flash(面情報)"], [3026,"Boot/Apli Flash(Kernel0面)"], [3027,"Boot/Apli Flash(Kernel1面)"], [3028,"Boot/Apli Flash(APL0面)"], [3029,"Boot/Apli Flash(APL1面)"],
                    [3030,"sRIO"], [3031,"Boot/Apli Flash(BOOT0面)"], [3032,"GFP Mapper"], [3033,"PCI Express Switch"], [3034,"HPB-FPGA"],
                    [4000,"CPLD,SWC-CPLD"], [4001,"FPGA,SWF-FPGA"], [4002,"NVSRAM"], [4003,"Boot/Apli Flash(BOOT1面)"], [4004,"Boot/Apli Flash(APL0面)"],
                    [4005,"Boot/Apli Flash(APL1面)"], [4006,"Config Flash(0面)"], [4007,"Config Flash(1面)"], [4008,"sRIO Switch"], [4009,"EEPROM(CPLD経由)"],
                    [4010,"EEPROM(FPGA経由)"], [4011,"温度計"], [4012,"電流計"], [4013,"Boot/Apli Flash(面情報)"], [4014,"Boot/Apli Flash(Kernel0面)"],
                    [4015,"Boot/Apli Flash(Kernel1面)"], [4016,"Boot/Apli Flash(APL0面)"], [4017,"Boot/Apli Flash(APL1面)"], [4018,"sRIO"], [4019,"Boot/Apli Flash(BOOT0面)"],
                    [4020,"CXC-CPLD(MAIN)"], [4021,"CXC-CPLD(SUB)"], [4022,"PCIe Switch"], [4023,"FE1600#1"], [4024,"FE1600#2"],
                    [4025,"FE1600#3"], [4026,"FE1600#4"]]


# 解析結果表示用(各Card用) 構造体
$st_result_dsp_card = Struct.new(
                            :analyze_flg,
                            :pf_hw_version_flg,
                            :pf_kernel_dbg_flg,
                            :pf_ud_alm_stschg_flg,
                            :pf_ud_alm_detail_flg,
                            :pf_ud_alm_eccerr_flg,
                            :pf_ud_alm_softerr_flg,
                            :pf_ud_alm_cyclechg_flg,
                            :pf_ilan_flg,
                            :FSWM_VERSION_flg)

# 解析結果表示用(THECINFO.TXT階層用) 構造体
$st_result_dsp_techinfo = Struct.new(:log_name,
                            :syslog_flg,
                            :techinfo_flg,
                            :st_result_dsp_card_hash,
                            :start_time,
                            :end_time)

$result_dsp_techinfo = nil
$result_dsp_card = nil

# スロット情報格納構造体
# THECINFO.TXTの情報を解析し情報を格納する。
# 今後の拡張性を考慮し構造体で作成
$st_slot_info = Struct.new(:type, :alm_table_hash, :flt_unit)
$slot_info_hash_teble = Hash::new
$analysis_slot_info = ""


#----------------------------------------------------------#
# Almdump_Analysis用 グローバル変数                        #
#----------------------------------------------------------#
$g_Almdump_log_unitconfigsheetrow = 2
$g_Almdump_log_flt_liu_file = ""
$g_Almdump_log_flt_target_file = 0
$g_Almdump_LIU = Hash::new
$g_Liu_num = []
$g_Almdump_log_alllogchk = "Skip"
$g_Almdump_drop_file = ""
$g_get_version = ""
$g_Almdump_log_version = ""
$g_Almdump_target_liu_name = ""  # 対象ユニットのLIU種別名
$g_unit_unique_name = ""
$g_hwinf_outdir = ""
$g_csv_file = ""
$g_out_dir = ""
$g_out_main_base_dir = ""
$g_out_main_dir = ""

$g_analyze_almdumplog_allchk = true
$g_analyze_almdumplog_chk = []

$g_tech_almdump_file_name = ""
$g_tech_tech_hw_inf       = ""
$g_tech_file_name         = ""

#----------------------------------------------------------#
# Almdump_Analysis用 define                                #
#----------------------------------------------------------#
  # 検索＆コピー対象ファイル
  TECH_HW_INF_TXT_LOG      = "puls_tech_hw_inf.log"
  TECH_HITLESS_INF_TXT_LOG = "puls_tech_hitless_inf.log"
  ALM_DUMP_BIN_LOG         = "puls_tech_almdump_inf.log"
  ALM_FW_VERSION_TXT_LOG   = "FSWM_VERSION.LOG"

  # UNIT名称排出
  VER_TXT ="UNIT_NAME.txt"

  # hw info情報抽出ファイル
  TECH_HW_INF_TXT_CSV_LOG="puls_tech_hw_inf.csv"

  # hitless_inf情報抽出ファイル
  TECH_HITLESS_INF_TXT_CSV_LOG="puls_tech_hitless_inf.csv"

  MESSAGE_1 ="�@新規Dir作成中"
  MESSAGE_2 ="�Aファイルコピー中"
  MESSAGE_3 ="�BFSWM_VERSIONコピー中"
  MESSAGE_4 ="�CLIU種別判別/INFO・CSV排出中"
  MESSAGE_5 ="�D解析ツールコピー中"

  st_unit_table = Struct.new(:unit_code, :liu_name, :hw_inf_analyze, :tech_almdump_analyze)
    $gst_unit_table = []
    $gst_unit_table << st_unit_table.new("4100","100GE",                "100GE_puls_tech_hw_inf解析.xlsm", "100GE_puls_tech_almdump_inf解析.xlsm")
    $gst_unit_table << st_unit_table.new("4101","100GNBO",              "100GNBO_puls_tech_hw_inf解析.xlsm", "100GNBO_puls_tech_almdump_inf解析.xlsm")
    $gst_unit_table << st_unit_table.new("4102","100GE-HL",             "100GE-HL_100GNBO-HL_puls_tech_hw_inf解析.xlsm", "100GE-HL_100GNBO-HL_puls_tech_almdump_inf解析.xlsm")
    $gst_unit_table << st_unit_table.new("4103","100GNBO-HL",           "100GE-HL_100GNBO-HL_puls_tech_hw_inf解析.xlsm", "100GE-HL_100GNBO-HL_puls_tech_almdump_inf解析.xlsm")
    $gst_unit_table << st_unit_table.new("4200","40GE",                 "40GE_puls_tech_hw_inf解析.xlsm", "40GE_puls_tech_almdump_inf解析.xlsm")
    $gst_unit_table << st_unit_table.new("4300","10GE(10GE2S)",         "", "")
    $gst_unit_table << st_unit_table.new("4301","10G-POS",              "", "")
    $gst_unit_table << st_unit_table.new("4302","10G-CES(10GCES2S)",    "", "")
    $gst_unit_table << st_unit_table.new("4303","10GE1S",               "10GE1S_puls_tech_hw_inf解析.xlsm", "10GE1S_puls_tech_almdump_inf解析.xlsm")
    $gst_unit_table << st_unit_table.new("4304","10GCES1S",             "10GCES1S_puls_tech_hw_inf解析.xlsm", "10GCES1S_puls_tech_almdump_inf解析.xlsm")
    $gst_unit_table << st_unit_table.new("4500","1GE",                  "1GE_puls_tech_hw_inf解析.xlsm", "1GE_puls_tech_almdump_inf解析.xlsm")

  # 解析結果用構造体
  $st_analyze_result = Struct.new(:fw_version, :hw_inf, :hitless_inf, :alm_dump, :almdump_excel, :hw_inf_excel)

  # 改行用セパレータ
  BREAK_SEPARATO = "|"

#----------------------------------------------------------#
# PI_Analysis用 グローバル変数                         #
#----------------------------------------------------------#
$g_pi_log_tarfilename = ""
# カード種別情報
$pi_card_type_teble = Hash::new
$pi_card_type_teble = {"1000" => "-", "1001" => "ALM", "2000" => "CPU-M", "2001" => "CPU-L(Main)", "2002" => "CPU-L(Trib)", "3000" => "SW-M", 
                       "3001" => "SW-L", "3100" => "CLK", "4100" => "100GE", "4101" => "100GNBO", "4102" => "100GE-HL", "4103" => "100GNBO-HL", 
                       "4200" => "40GE", "4300" => "10GE2S", "4301" => "-", "4302" => "10GCES2S", "4303" => "10GE1S", "4304" => "10GCES1S", "4500" => "1GE"}

#----------------------------------------------------------#
# SDH_ASP_Analysis用 グローバル変数                         #
#----------------------------------------------------------#
# decode用ハッシュ
$work_port_hash = Hash::new
$nend_fend_hash = Hash::new
$state_hash = Hash::new
$work_port_hash = {"0" => "Work", "1" => "Prot"}
$nend_fend_hash = {"0" => "NEND", "1" => "FEND"}
$state_hash = {"0" => "未発生", "1" => "発生"}

#18275対応
#----------------------------------------------------------#
# AC用 グローバル変数                         #
#----------------------------------------------------------#
$chk_req = true
$chk_rsp = true
$chk_info = false
$ver_cmbx = ""
$endian_cmbx = ""
$chk_aclog = true
$chk_anatime = false
$start_time = ""
$end_time = ""
$payload = true

#----------------------------------------------------------#
# AC定義生成用グローバル定義
#----------------------------------------------------------#
Member = Struct.new(:name, :sheet, :num)
Member_parameter = Struct.new(:name, :para_type, :parameter, :parameter_check, :size, :num)

#----------------------------------------------------------#
#  AC定義生成用グローバル変数
#----------------------------------------------------------#
$delete_check = 0 #取消線が含まれているか判定する変数

#----------------------------------------------------------#
# AC解析用 定数 (define)
#----------------------------------------------------------#
MAX_SIGLOG_SIZE      = 512*1024
MAX_TOTAL_PARAM_SIZE = 664
BIG_ENDIAN = 0
LITTLE_ENDIAN = 1

# 固定ファイル名定義
ERR_FILE = "ACLog_Analysis_Error.log" # 2013.10.24 Kodama
ACID_TBL = "ACID.txt"
CARD_TBL = "CARD.txt"
TLV_TBL  = "TLV.txt"

# csv カラム（$g_param_data配列）番号
CSV_RESULT   = 0
CSV_SEQNO    = 1
CSV_TIME     = 2
CSV_WAIT     = 3
CSV_IPP      = 4
CSV_ACID     = 5
CSV_ACNAME   = 6
CSV_CARD     = 7
CSV_CARDNAME = 8
CSV_PARAM    = 9

#----------------------------------------------------------#
# AC解析用 定数 グローバル変数
#----------------------------------------------------------#
$g_param_data = []
$g_endian = BIG_ENDIAN
$g_aclogfile = 0
$g_errfile = 0
$g_error_detected = 0
$in_param_analyze = 0
$g_dbg_dump_log = 0
$g_log_rotate = 0
$g_drop_fname = ""
$g_tgz_fname = ""
$g_ud_ac_fname = ""

# S150対応 Change Start 2020/12/11 by Nishi 
#----------------------------------------------------------#
# エラー返り値用 定数 (define)
#----------------------------------------------------------#
ERR_CHECK_OK=0
ERR_CHECK_TRAP_INFO=1
ERR_CHECK_AUDIT_INFO=2
ERR_CHECK_CONFIG_INFO=3
ERR_CHECK_VERSION_INFO=4
ERR_ARGS_OPTION_INPUT_OVER=5
ERR_ARGS_INPUT=6
ERR_TYPE_OPTION_INPUT=7
ERR_CHECK_FLT=8
ERR_CHECK_FFANLOG=9
ERR_CANCEL_WRITE_OUTPUTFILE=10

#----------------------------------------------------------#
# 処理の状態チェック用
#----------------------------------------------------------#
PROC_STATUS_TRAP_INFO=1
PROC_STATUS_AUDIT_INFO=2
PROC_STATUS_CONFIG_INFO=3
PROC_STATUS_VERSION_INFO=4
# S150対応 Change End 2020/12/11 by Nishi 

#----------------------------------------------------------#
# 共通関数 parse_caller
#----------------------------------------------------------#
def parse_caller(at)
  if /^(.+?):(\d+)(?::in `(.*)')?/ =~ at  #'
    file = $1
    line = $2.to_i
    method = $3
    [File::basename(file), line, method]
  end
end


#----------------------------------------------------------#
#  共通関数 Excel CellへのRead/Write
#----------------------------------------------------------#
module Log_Worksheet
  def [] y,x
    cell = self.Cells.Item(y,x)
    if cell.MergeCell
      cell.MergeArea.Item(1,1).Value
    else
      cell.Value
    end
  end

  def []= y,x,value
    cell = self.Cells.Item(y,x)
    if cell.MergeCells
      cell.MergeArea.Item(1,1).Value = value
    else
      cell.Value = value
    end
  end
end

#----------------------------------------------------------#
# Super log Class
#----------------------------------------------------------#
class Super_log_Class

  #----------------------------------------------------------#
  # ファイルオープン
  #----------------------------------------------------------#
  def super_open(pass, tsv_mode=false)
    @tsv_mode = tsv_mode
    @f = open(pass, "w")
    self.write(sprintf("Version = %s\n", $log_analyeze_version), 0, File.basename(__FILE__), __LINE__)
  end

  #----------------------------------------------------------#
  # ファイルクローズ
  #----------------------------------------------------------#
  def file_close()
    @f.close
  end

  #----------------------------------------------------------#
  # 書き込み
  #----------------------------------------------------------#
  def super_write(write_data, filename="", line_no="")
    if filename == ""
      filename, line_no, func =  parse_caller(caller(2).first)
    end

    temp = Time.now.instance_eval { '%s.%03d' % [strftime('%Y/%m/%d|%H:%M:%S'), (usec / 1000.0).round] }
    if @tsv_mode
        temp.gsub!('|',"\t")
        @f.write(sprintf("%s\t%s\t%s\t%s", temp, filename, line_no, write_data))
        return
    end
    @f.write(sprintf("%s|%s|%s|%s", filename, line_no, temp, write_data))
  end

end

#----------------------------------------------------------#
# Common log Class
#----------------------------------------------------------#
class Log_class < Super_log_Class
  include Singleton

  #----------------------------------------------------------#
  # コンストラクタ
  #----------------------------------------------------------#
  def initialize()
    if $D_LOG_MODE == "ON"
      super_open($D_RUN_PATH + "/" + $D_LOG_FILENAME, $exec_log_tsv)
    end
  end

  #----------------------------------------------------------#
  # 書き込み
  #----------------------------------------------------------#
  def write(write_data, level=0, filename="", line_no="")
    if $D_LOG_MODE == "ON"
      if level <= $D_LOG_LEVEL
        super_write(write_data, filename, line_no)
      end
    end
  end

  #----------------------------------------------------------#
  # liu hash 書き込み
  #----------------------------------------------------------#
  def liu_hash_write()
    if $D_LOG_MODE == "ON" and $D_LOG_LEVEL >= 2
      out = open($D_RUN_PATH + "/" + $D_LOG_LIU_HASH_FILENAME, "w")

      $g_liu_address_hash.each{|key, value|
        device_name = value.device_name
        register_name = value.register_name
        address = value.address
        bithash = value.bithash
        bithash.each{|bitkey, bitvalue|
          bit_name = bitvalue.bit_name
          bit      = bitvalue.bit
          out.write(sprintf("%10s %4s %s %s %s\n", key, bitkey, device_name, register_name, bit_name))
        }
      }

      out.close
    end
  end
end

#----------------------------------------------------------#
# AC log Class
#----------------------------------------------------------#
class AC_log_class  < Super_log_Class

  #----------------------------------------------------------#
  # コンストラクタ
  #----------------------------------------------------------#
  def initialize(pass)
    if $D_LOG_MODE == "ON"
      file_pass = pass + "\\" + $D_AC_LOG_FILENAME
      puts "file_pass"
      puts file_pass
      super_open(file_pass)
    end
  end

  #----------------------------------------------------------#
  # 書き込み
  #----------------------------------------------------------#
  def write(write_data, level=0, filename="", line_no="")
    if $D_LOG_MODE == "ON"
      if level <= $D_LOG_LEVEL
        super_write(write_data, filename, line_no)
      end
    end
  end
end

#----------------------------------------------------------#
# 共通クラス
#----------------------------------------------------------#
class Common_Analyze_Cls

  #----------------------------------------------------------#
  # コンストラクタ
  #----------------------------------------------------------#
  def initialize()
    @logobj = Log_class.instance
    @logobj.write("Common_Analyze_Cls::initialize Start\n")

    # input log 管理構造体
    @st_in_log_info = Struct.new(:tar_name, :log_name, :log_obj)

    @in_log_data = []
    @chk_hash = Hash::new
    @chk_log_max = 0
    @progress_cnt = 0

    # S150対応 Change Start 2020/12/11 by Nishi 
    #Trap情報(S100)
    if ($g_machine_type == "S100")
       @blade_data   = "-"
        @fan_data     = Array.new(3).map{Array.new(3, "-")}   #slot(1〜3)/種別(FLT/RMVD/MEA)
        @piu_data     = Array.new(4).map{Array.new(3, "-")}   #slot(1〜4)/種別(FLT/RMVD/MEA)
        @mdl_data     = Array.new(4).map{Array.new(12).map{Array.new(3,"-")}}   #slot(1〜4)/port(1〜12)/種別(FLT/RMVD/MEA)
    #Trap情報(S150)
    elsif ($g_machine_type == "S150")
        @blade_data   = "-"
        # S150対応 Add Start 2020/12/24 by Nishi 
        @cpu_data     = Array.new(1).map{Array.new(3, "-")}   #slot(1〜8)/種別(FLT/RMVD/MEA)
        # S150対応 Add End 2020/12/24 by Nishi 
        @fan_data     = Array.new(8).map{Array.new(3, "-")}   #slot(1〜8)/種別(FLT/RMVD/MEA)
        @piu_data     = Array.new(3).map{Array.new(3, "-")}   #slot(1〜3)/種別(FLT/RMVD/MEA)
        @psu_data     = Array.new(4).map{Array.new(3, "-")}   #slot(1〜4)/種別(FLT/RMVD/MEA)
        @mdl_data1    = Array.new(1).map{Array.new(26).map{Array.new(3,"-")}}   #slot(1)/port(26)/種別(FLT/RMVD/MEA)
        @mdl_data2    = Array.new(2).map{Array.new(12).map{Array.new(3,"-")}}   #slot(2〜3)/port(1〜12)/種別(FLT/RMVD/MEA)
    end
    # S150対応 Change End 2020/12/11 by Nishi

    @logobj.write("Common_Analyze_Cls::initialize End\n")
  end

  #----------------------------------------------------------#
  #   #Worksheet追加 & Tab名前の変更
  #----------------------------------------------------------#
  def excel_sheet_add(tabname,book)
    @logobj.write("Common_Analyze_Cls::excel_sheet_add Start #{tabname}\n")

    if tabname == "SYSLOG"
      sheet     = book.Worksheets.Add({'Before'=>book.Worksheets(1)})
    else
      sheet_end = book.Worksheets(book.Worksheets.count)
      sheet     = book.Worksheets.Add({'After'=>sheet_end})
    end
    sheet.activate 
    sheet.name = File.basename tabname,".TGZ"
    sheet.extend Log_Worksheet

    @logobj.write("Common_Analyze_Cls::excel_sheet_add End\n")
    return sheet

  end
  
  #----------------------------------------------------------#
  #  SYSLOGファイルの書込み                                  #
  #----------------------------------------------------------#
  def syslog_write(book, syslog)
    @logobj.write("Common_Analyze_Cls::syslog_write Start\n")

    p "SYSLOG.TXT"
    #Worksheet追加 & Tab名変更
    sheet = excel_sheet_add("SYSLOG",book)

    write_array = ["no", "date", "time", "ip", "kind", "seqNo", "detail"]
    sheet.Range(sheet.cells(1,2), sheet.cells(1,8)).value = write_array
    l_num = 1
    syslog.each do |line|
      l_num += 1

      #THECINFO解析 SYSLOG シート表示変更対応
      if line.chomp! =~ /<(\d+)>(\w+\s+\d+)\s+(\d+:\d+:\d+)\s+(\d+\.\d+\.\d+\.\d+)\s+(\w+):\s+(\d+)\s+(.*)/
        no   = $1
        date = $2
        time = $3
        ip   = $4
        kind = $5
        seqNo = $6
        detail = $7
        @logobj.write(sprintf("hit \[%s\]\[%s\]\[%s\]\[%s\]\[%s\]\[%s\]\[%s\]\n", no, date, time, ip, kind, seqNo, detail))
        write_array = [no, date, time, ip, kind, seqNo, detail]
        sheet.Range(sheet.cells(l_num,2), sheet.cells(l_num,8)).value = write_array

      else
        @logobj.write(sprintf("no Hit %s", line.chomp!))
        sheet[l_num,2] = line.chomp!
      end

      case line
      when /WARN:/
        sheet.rows(l_num).font.color = 255                #赤色
      when /flt/
        sheet.rows(l_num).font.color = 255                #赤色
      when /TRAP:/
        sheet.rows(l_num).font.color = 16711680           #青色
      when /MORP:/
        sheet.rows(l_num).font.color = 65280              #緑色
      end
    end

    # Excel表の整形
    sheet.Columns.AutoFit

    @logobj.write("Common_Analyze_Cls::syslog_write End\n")
  end
  
  #----------------------------------------------------------#
  #  FSWM_VERSION.LOG ファイルの解析                         #
  #----------------------------------------------------------#
  def fswm_version_log_analyze(fswm_version_log, book)
    @logobj.write("Common_Analyze_Cls::fswm_version_log_analyze Start\n")
    puts "[debug] call fswm_version_log_analyze"
    puts "[debug] call fswm_version_log"
    
    log_fswm_version_log_line = []

    # 配列に格納し、pf_hw_version.LOGでExcel 出力する
    fswm_version_log.each do |line|
      log_fswm_version_log_line << line
    end

    # FSWM_VERSION.LOG ファイルの解析
    log_fswm_version_log_line.each do |line|
      if (line.chomp! != nil)
        if /\s+([0-9A-Za-z_\-\.]+)\s+\|\s+(.+)/ =~ line
          clm2_data = line.gsub(/.*\d{1,}\s+([0-9A-Za-z_\-\.]+)\s+\|\s+(.+)/, '\1')
          clm3_data = line.gsub(/.*\d{1,}\s+([0-9A-Za-z_\-\.]+)\s+\|\s+(.+)/, '\2')

          # UNIT情報をUNIT情報シートに貼り付け
          unit_config_sheet_write(
                                  book,
                                  0,
                                  "FSWM_VERSION.LOG",
                                  clm2_data,
                                  clm3_data,
                                  "",
                                  ""
                                 )

        end
      end
    end
    @logobj.write("Common_Analyze_Cls::fswm_version_log_analyze End\n")
  end


  #----------------------------------------------------------#
  #   chk_log_add
  #----------------------------------------------------------#
  def chk_log_add(log_name_list)
    puts @chk_hash
    log_name_list.each{|log_name|
      @chk_hash[log_name] = true
      @chk_log_max = @chk_log_max + 1
    }
  end


  def chk_log_max_set(max_num,func_name)
      @progress_cnt = 0
      $progress_value = 0
      @chk_log_max = max_num

      $progress_log_no = 0
      $progress_max_log_no = max_num
      $progress_funcname = func_name
      if $exec_CUI_mode then
        puts "* #{$progress_funcname} 解析開始"
      end
  end


  #----------------------------------------------------------#
  #   chk_file
  #----------------------------------------------------------#
  def chk_file(tar_name, log_obj)
    tar_name_dsp = File::basename(tar_name)
    log_name_dsp = File::basename(log_obj.name)

    if @chk_hash[log_name_dsp]
      # printf("[Hit]tar_name=%s, log_name=%s\n", tar_name_dsp, log_name_dsp)
      @in_log_data << @st_in_log_info.new(tar_name_dsp, log_name_dsp, log_obj.read)
      # @in_log_data << @st_in_log_info.new(tar_name_dsp, log_name_dsp, log_obj)
      @chk_log_max = @chk_log_max + 1
    end
  end

  #----------------------------------------------------------#
  #   tar_decompression
  #----------------------------------------------------------#
  def tar_decompression(in_tar_file)
    @chk_log_max = 0

    Zlib::GzipReader.open(in_tar_file) do |tgz1|
      Minitar::Reader.open(tgz1).each do |entry1|
=begin
        # TGZかどうかチェック
        if File::extname(entry1.name) == '.TGZ'
          fpl = StringIO.new(entry1.read)

          Zlib::GzipReader.wrap(fpl) do |tgz2|
            Minitar::Reader.open(tgz2).each do |entry2|
              chk_file(entry1.name, entry2)
            end
          end

        # それ以外のファイル
        else
           chk_file(in_tar_file, entry1)
        end
=end
        chk_file(in_tar_file, entry1)
      end
    end
  end

  #----------------------------------------------------------#
  # get_log_data_obj
  #----------------------------------------------------------#
  def get_log_data_obj()
    return @in_log_data
  end

  #----------------------------------------------------------#
  # update_progress
  #----------------------------------------------------------#
  def update_progress()
    @progress_cnt = @progress_cnt + 1
    $progress_value = ((1.to_f/@chk_log_max.to_f).to_f) * @progress_cnt
    $progress_max_log_no = @chk_log_max
    $progress_log_no = @progress_cnt
    # printf("[%03d][%f]", ($progress_value*100).to_i, $progress_value)
#    @logobj.write(sprintf("update_progress %d/%d %03d％ $progress_value=%f\n", @progress_cnt, @chk_log_max, ($progress_value*100).to_i, $progress_value))
    if $progress_value >= 1.0 then
        puts "* #{$progress_funcname} 解析完了"
    end
  end


    #----------------------------------------------------------#
    # trap_analyze
    # 概要：Trap情報解析
    #       Trap情報をエクセルに吐き出す
    #       吐き出している最中に構成情報のみを検索するのは大変なので
    #       その処理はtrap_analyze_from_xlsに回す
    #----------------------------------------------------------#
    def trap_analyze(path,book)
        sheets = excel_sheet_add("Trap情報",book)
        
        data_count = 0
        @trap_count = 0
        #ファイルの読み込み
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filedata = entry.read
                if (filedata != nil)
                    
                    filename = File.basename(entry.name)
                    
                    if( filename == "trap.log" )
                        data = StringIO.new(filedata)
                        
                        #エクセルへの出力および時刻ソート
                        data_count = sort_data_by_time(data,sheets)
                    end
                end
            end
        end
        
        if(data_count != 0)
            for line in 1...data_count + 1 do
                line_data = sheets.cells(line,4).value
                
                #種別の割り出し
                object_data_start_pos = line_data.index(":")
                type = line_data[0, object_data_start_pos]
                
                sheets.Cells(line, 4).Value = type
                
                #対象の割り出し
                if( ( line_data !~ /\[ALM\]:/ ) && ( line_data !~ /\[TCA\]:/ ) && (line_data !~ /\[EVE\]:/) )
                    @logobj.write(sprintf("[trap_analyze][skip]:%s\n", line_data))
                    next
                end
                object_data_start_pos += 1
                object_data_end_pos = line_data.index(",")
                object_data = line_data[object_data_start_pos, object_data_end_pos - object_data_start_pos]
                if(not object_data.include?("-"))
                    @logobj.write(sprintf("[trap_analyze][skip]:%s\n", line_data))
                    next
                end               
                #対象
                object_name = object_data[0,object_data.rindex("-")]
                #位置の割り出し
                object_pos_info = object_data[object_data.rindex("-") + 1,object_data.length].split("/")
                
                #alarm nameの割り出し
                next_data = line_data[object_data_end_pos + 1, line_data.length]
                alarm_name_end_pos = next_data.index(",")
                alarm_name = next_data[0, alarm_name_end_pos]
                
                #Descriptionの割り出し
                next_data = next_data[alarm_name_end_pos + 1, next_data.length]
                desc_data = ""
                desc_end_pos = 0
                if( ( line_data =~ /\[ALM\]:/ ) || ( line_data =~ /\[TCA\]:/ ) )
                    desc_end_pos = next_data.index(",")
                elsif(line_data =~ /\[EVE\]:/)
                    desc_end_pos = next_data.length
                end
                desc_data = next_data[0, desc_end_pos]
                
                #notification codeの割り出し
                nc_data = ""
                if( ( line_data =~ /\[ALM\]:/ ) || ( line_data =~ /\[TCA\]:/ ) )
                    nc_data = next_data[desc_end_pos + 1, next_data.length - (desc_end_pos + 1)]
                end
                
                sheets.cells(line, 5).Value = object_name
                
                sheets.Range(sheets.cells(line,6), sheets.cells(line,12)).value = ["-","-","-","-"]
                column = 6
                object_pos_info.each { |value|
                  sheets.cells(line,column).value = value
                  column += 1
                }
                
                sheets.Cells(line, 10).Value = alarm_name
                sheets.Cells(line, 11).Value = desc_data
                sheets.Cells(line, 12).Value = nc_data
            end
        end
        
        @trap_count = data_count
        sheets.Rows(1).Insert
        data_count += 1
        
        sheets.Range("C:C").NumberFormatLocal = "0.000000_ "

        #ヘッダ部分
        sheets.Cells(1,  1).Value = "Date"
        sheets.Cells(1,  2).Value = "Time"
        sheets.Cells(1,  3).Value = "Time(.sec)"
        
        sheets.Cells(1,  4).Value = "種別"
        sheets.Cells(1,  5).Value = "対象"
        sheets.Cells(1,  6).Value = "pos1 (shelf)"
        sheets.Cells(1,  7).Value = "pos2 (slot)"
        sheets.Cells(1,  8).Value = "pos3 (?)"
        sheets.Cells(1,  9).Value = "pos4 (port)"
        sheets.Cells(1, 10).Value = "alarm name"
        sheets.Cells(1, 11).Value = "Description"
        sheets.Cells(1, 12).Value = "notification code"
        
        #罫線
        sheets.Range(sheets.cells(1,1),sheets.cells(data_count,12)).Borders.LineStyle = 1
        
        # セル色変更
        sheets.Range(sheets.cells(1,1),sheets.cells(1,12)).Interior.ColorIndex = 4
        
        #自動整形
        sheets.Columns.AutoFit
    end #trap_analyze

    # S150対応 Change Start 2020/12/11 by Nishi    
    #----------------------------------------------------------#
    # trap_analyze_from_xls_s100
    # 概要：Trap情報解析(S100)
    #       エクセルに吐き出したTrap情報から
    #       構成情報のみを検索、解析する
    #----------------------------------------------------------#
    def trap_analyze_from_xls_s100(book)
    # S150対応 Change End 2020/12/11 by Nishi
    
        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("TECHINFO_Analysis_Common::trap_analyze_from_xls_s100 Start\n")
        # S150対応 Add End 2020/12/11 by Nishi
    
        sheets = book.Worksheets("Trap情報")
        
        count = @trap_count
        
        slot_num = 0
        port_num = 0
        type_num = 0
        
        for row in 2...(count+2) do
            type = sheets.cells(row,10).value
            if( (type == "equipmentFault") or
                (type == "equipmentRemoved") or
                (type == "equipmentMismatchAttributes") )
                
                case sheets.cells(row,5).value
                when "BDS1-BEA1"                # Blade
                    if(type == "equipmentFault")
                        @blade_data = sheets.cells(row,12).value
                    end
                when "AC00-F011"                # FAN
                    if(type == "equipmentFault")
                        type_num = 0
                    elsif(type == "equipmentRemoved")
                        type_num = 1
                    else
                        type_num = 2
                    end
                    if(sheets.cells(row,7).value.to_i != 0)
                        slot_num = sheets.cells(row,7).value.to_i - 1
                        @fan_data[slot_num][type_num] = sheets.cells(row,12).value
                    end
                # 新カード対応漏れ対応 Add Start 2020/12/22 by Nishi
                when "PIS1-PG31" , "PIS1-PAC1" , "PIS1-PAC2"  , "PIS1-PD31" , "PIS1-PF11" # PIU
                # 新カード対応漏れ対応 Add End 2020/12/22 by Nishi
                    if(type == "equipmentFault")
                        type_num = 0
                    elsif(type == "equipmentRemoved")
                        type_num = 1
                    else
                        type_num = 2
                    end
                    if( sheets.cells(row,7).value.to_i != 0)
                        slot_num = sheets.cells(row,7).value.to_i - 1
                        @piu_data[slot_num][type_num] = sheets.cells(row,12).value
                    end
                else                            # MDL
                    if(type == "equipmentFault")
                        type_num = 0
                    elsif(type == "equipmentRemoved")
                        type_num = 1
                    else
                        type_num = 2
                    end
                    if( ( sheets.cells(row,7).value.to_i != 0 ) && (sheets.cells(row,9).value.to_i != 0) )
                        slot_num = sheets.cells(row,7).value.to_i - 1
                        port_num = sheets.cells(row,9).value.to_i - 1
                        @mdl_data[slot_num][port_num][type_num] = sheets.cells(row,12).value
                    end
                end #case
            end #if( (type
        end #for row
        
        # S150対応 Add Start 2020/12/11 by Nishi
        @logobj.write("TECHINFO_Analysis_Common::trap_analyze_from_xls_s100 End\n")
        # S150対応 Add End 2020/12/11 by Nishi
        
    # S150対応 Change Start 2020/12/11 by Nishi
    end #trap_analyze_from_xls_s100
    # S150対応 Change End 2020/12/11 by Nishi
    
    # S150対応 Change Start 2020/12/11 by Nishi    
    #----------------------------------------------------------#
    # trap_analyze_from_xls_s150
    # 概要：Trap情報解析(S150)
    #       エクセルに吐き出したTrap情報から
    #       構成情報のみを検索、解析する
    #----------------------------------------------------------#
    def trap_analyze_from_xls_s150(book)

        @logobj.write("TECHINFO_Analysis_Common::trap_analyze_from_xls_s150 Start\n")

        sheets = book.Worksheets("Trap情報")
        
        count = @trap_count
        
        slot_num = 0
        port_num = 0
        type_num = 0
        
        for row in 2...(count+2) do
            type = sheets.cells(row,10)
            .value
            if( (type == "equipmentFault") or
                (type == "equipmentRemoved") or
                (type == "equipmentMismatchAttributes") )
                
                case sheets.cells(row,5).value
                when "BDS1-BEF1"           # Blade
                    if(type == "equipmentFault")
                        @blade_data = sheets.cells(row,12).value
                    end
                # S150対応 Add Start 2020/12/24 by Nishi
                when "ACS1-AQ11"  # CPU
                    if(type == "equipmentFault")
                        type_num = 0
                    elsif(type == "equipmentRemoved")
                        type_num = 1
                    else
                        type_num = 2
                    end
                    if(sheets.cells(row,7).value.to_s.gsub("CPU","").to_i != 0)
                        slot_num = sheets.cells(row,7).value.to_s.gsub("CPU","").to_i - 1
                        @cpu_data[slot_num][type_num] = sheets.cells(row,12).value
                    end
                # S150対応 Add End 2020/12/24 by Nishi
                when "ACS1-AG11"  # FAN
                    if(type == "equipmentFault")
                        type_num = 0
                    elsif(type == "equipmentRemoved")
                        type_num = 1
                    else
                        type_num = 2
                    end
                    # S150対応 Change Start 2020/12/22 by Nishi
                    if(sheets.cells(row,7).value.to_s.gsub("FAN","").to_i != 0)
                        slot_num = sheets.cells(row,7).value.to_s.gsub("FAN","").to_i - 1
                        @fan_data[slot_num][type_num] = sheets.cells(row,12).value
                    end
                    # S150対応 Change End 2020/12/22 by Nishi
                # S150対応 Change Start 2020/12/22 by Nishi
                when "PIS1-FIX" , "PIS1-PD32" , "PIS1-PD31" , "PIS1-PF11"  # PIU
                    if(type == "equipmentFault")
                        type_num = 0
                    elsif(type == "equipmentRemoved")
                        type_num = 1
                    else
                        type_num = 2
                    end
                    if(sheets.cells(row,7).value.to_s.gsub("PIU","").to_i != 0)
                        slot_num = sheets.cells(row,7).value.to_s.gsub("PIU","").to_i - 1
                        @piu_data[slot_num][type_num] = sheets.cells(row,12).value
                    end
                # S150対応 Change End 2020/12/22 by Nishi
                # S150対応 Change Start 2020/12/22 by Nishi
                when "ACS1-AWXX"                # PSU
                # S150対応 Change End 2020/12/22 by Nishi
                    if(type == "equipmentFault")
                        type_num = 0
                    elsif(type == "equipmentRemoved")
                        type_num = 1
                    else
                        type_num = 2
                    end
                    if(sheets.cells(row,7).value.to_s.gsub("PSU","").to_i != 0)
                        slot_num = sheets.cells(row,7).value.to_s.gsub("PSU","").to_i - 1
                        @psu_data[slot_num][type_num] = sheets.cells(row,12).value
                    end
                else                            # MDL
                    if(type == "equipmentFault")
                        type_num = 0
                    elsif(type == "equipmentRemoved")
                        type_num = 1
                    else
                        type_num = 2
                    end
                    if( ( sheets.cells(row,7).value.to_i != 0 ) && (sheets.cells(row,9).value.to_i != 0) )
                        slot_num = sheets.cells(row,7).value.to_i - 1
                        port_num = sheets.cells(row,9).value.to_i - 1
                        if (slot_num == 0) then
                            @mdl_data1[slot_num][port_num][type_num] = sheets.cells(row,12).value
                        else
                            @mdl_data2[slot_num-1][port_num][type_num] = sheets.cells(row,12).value
                        end
                    end
                end #case
            end #if( (type
        end #for row
        
        @logobj.write("TECHINFO_Analysis_Common::trap_analyze_from_xls_s150 End\n")
        
    end #trap_analyze_from_xls_s150
    # S150対応 Change End 2020/12/11 by Nishi
    
    # S150対応 Change Start 2020/12/11 by Nishi 
    #----------------------------------------------------------#
    # log_check_s100
    # 概要：解析
    #       
    #       
    #----------------------------------------------------------#
    def log_check_PI_s100(path,book)
    # S150対応 Change End 2020/12/11 by Nishi 
        
        # S150対応 Change Start 2020/12/11 by Nishi 
        @logobj.write("TECHINFO_Analysis_Common::log_check_PI_s100 Start\n")
        # S150対応 Change End 2020/12/11 by Nishi 
        fan_data = Array.new(3,"-")
        piu_data = Array.new(4,"-")
        mdl_data = Array.new(4).map{Array.new(12,"-")}
        
        data_count = 0
        excel_sheet_add("tmp",book)
        sheets = book.sheets("tmp")
        
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filedata = entry.read
                if (filedata == nil)
                    next
                end
                
                filename = File.basename(entry.name)
                
                #PIlog
                if(filename == "ud_pi.LOG")
                    data = StringIO.new(filedata)
                    #ゴミデータの排除、およびループしていた場合のデータを時系列に並べる
                    data_count = sort_data_by_time(data,sheets)
                end
            end
        end
        # S150対応 Change Start 2020/12/11 by Nishi 
        @logobj.write("TECHINFO_Analysis_Common::log_check_PI_s100 - Zlib::GzipReader End\n")
        # S150対応 Change End 2020/12/11 by Nishi 
        if(data_count != 0)
            #フォーマットに沿ってとられている先頭までを探す(それまでのデータは捨てる)
            delete_line = 0
            for line in 1...data_count + 1 do
                line_data = sheets.cells(line,4).value
                if(line_data =~ /#### PI/)
                    break
                else
                    sheets.Rows(line).delete
                    delete_line += 1
                    redo
                end
            end
            data_count -= delete_line
            #logging開始行を消す(上のfor文で引っかかるはずだが途中に入っていた場合を考慮する(ありえないはずだが))
            delete_line = 0
            for line in 1...data_count + 1 do
                line_data = sheets.cells(line,4).value
                if(line_data =~ / #### UD_FLOG_PI Start ####/)
                    sheets.Rows(line).delete
                    delete_line += 1
                    break
                end
            end
            data_count -= delete_line
            
            pi_log_count = 0
            row_pos = 0
            
            pi_log_type = "NO MDL"
            writesheets = book.sheets("PI情報 (BLADE・FAN・PIU)")
            write_data = ["","","","","","","","","","",
                          "","","","","","","","","","",
                          "","","","","","","","","","",
                          "","","","","","","","","","",
                          "","","","","","","","","","",
                          "","","","","","","","","","",
                          "","","","","","","","","","",
                          "","","","","","","","","","","",]
            
            data_buf = ""
            
            for line in 1...data_count + 1 do
                line_data = sheets.cells(line,4).value
                if(line_data =~ /#### PI/)
                    pi_log_count = 0
                    if(line_data =~ /#### PI MDL/)
                        writesheets = book.sheets("PI情報 (MDL)")
                        pi_log_type = "MDL"
                        key_data_slot = line_data.scan(/slot = \d{1,}/).join(",").delete("slot = ").to_i
                        key_data_port = line_data.scan(/port = \d{1,}/).join(",").delete("port = ").to_i
                        row_pos = 2 + (key_data_slot -1)*12 + (key_data_port -1)
                    else
                        writesheets = book.sheets("PI情報 (BLADE・FAN・PIU)")
                        pi_log_type = "NO MDL"
                        write_data = ["","","","","","","","","","",
                                      "","","","","","","","","","",
                                      "","","","","","","","","","",
                                      "","","","","","","","","","",
                                      "","","","","","","","","","",
                                      "","","","","","","","","","",
                                      "","","","","","","","","","",
                                      "","","","","","","","","","","",]
                        data_buf = ""
                        
                        if(line_data =~ /BLADE/)
                            row_pos = 3
                        elsif(line_data =~ /FAN/)
                            key_data_slot = line_data.scan(/slot = \d{1,}/).join(",").delete("slot = ").to_i
                            row_pos = 3 + key_data_slot
                        else #PIU
                            key_data_slot = line_data.scan(/slot = \d{1,}/).join(",").delete("slot = ").to_i
                            row_pos = 6 + key_data_slot
                        end
                    end
                else
                    pi_log_count += 1
                    if( pi_log_type == "MDL")
                        info_start_pos  = line_data.index("=") + 2
                        info_data       = line_data[info_start_pos,line_data.length - info_start_pos + 1]
                        writesheets.cells(row_pos,pi_log_count + 1).Value = info_data
                    else  # BLADE/FUN/PIU
                        line_data.delete!(" ")
                        data_buf += line_data
                        
                        # 貯めたデータをPIの型に合わせこむ
                        if(pi_log_count == 16)
                            write_data[0] = "0x" + data_buf[0,4]                                                  #unit_code
                            write_data[1] = "0x" +  data_buf[4,2]                                                 #version_flag
                            
                            #      6から2文字ずつ3回飛ばして-1
                            6.step(6+2*3-1, 2){ |num|
                                write_data[2] += data_buf[num,2].hex.chr                                            #issue_num[3]
                            }
                            
                            12.step(12+2*9-1, 2){ |num|
                                write_data[3] += data_buf[num,2].hex.chr                                            #abbreviation_name[9]
                            }
                            
                            30.step(30+2*10-1, 2){ |num|
                                write_data[4] += data_buf[num,2].hex.chr                                            #fc_num[10]
                            }
                            
                            50.step(50+2*10-1, 2){ |num|
                                write_data[5] += data_buf[num,2].hex.chr                                            #clei_code[10]
                            }
                            
                            70.step(70+2*5-1, 2){ |num|
                                write_data[6] += data_buf[num,2].hex.chr                                            #product_year_month[5]
                            }
                            
                            80.step(80+2*2-1, 2){ |num|
                                write_data[7] += data_buf[num,2].hex.chr                                            #location_code[2]
                            }
                            
                            84.step(84+2*5-1, 2){ |num|
                                write_data[8] += data_buf[num,2].hex.chr                                            #serial_num[5]
                            }
                            
                            write_data[9]  = "0x" +  data_buf[94,6]                                               #reserve0[3]
                            write_data[10] = "0x" +  data_buf[100,12]                                             #mac_addr_min[6]
                            write_data[11] = "0x" +  data_buf[112,12]                                             #mac_addr_max[6]
                            write_data[12] = "0x" +  data_buf[124,4]                                              #fwdl_type
                            write_data[13] = "0x" +  data_buf[128,64]                                             #reserve1[32]
                            
                            192.step(192+2*5-1, 2){ |num|
                                write_data[14] += data_buf[num,2].hex.chr                                           #pcb_revision[5]
                            }
                            
                            write_data[15] = "0x" +  data_buf[202,14]                                             #reserve2[7]
                            
                            #parts_temp_th[10];
                            0.step(29,3){ |parts_temp_th|
                                write_data[16 + parts_temp_th] = "0x" +  data_buf[216+ 2*parts_temp_th ,2]          #break_temp
                                write_data[16 + parts_temp_th + 1] = "0x" +  data_buf[216+ 2*parts_temp_th + 2 ,2]  #max_temp
                                write_data[16 + parts_temp_th + 2] = "0x" +  data_buf[216+ 2*parts_temp_th + 4 ,2]  #min_temp
                            }
                            
                            276.step(276+2*8-1, 2){ |num|
                                write_data[46] += data_buf[num,2].hex.chr                                           #min_wavelength[8]
                            }
                            
                            292.step(292+2*8-1, 2){ |num|
                                write_data[47] += data_buf[num,2].hex.chr                                           #max_wavelength[8]
                            }
                            
                            308.step(308+2*6-1, 2){ |num|
                                write_data[48] += data_buf[num,2].hex.chr                                           #frec_spacing[6]
                            }
                            
                            write_data[49] = "0x" +  data_buf[320,2]                                              #reserve3
                            write_data[50] = "0x" +  data_buf[322,2]                                              #press_sensor_exist
                            
                            write_data[51] = "0x" +  data_buf[324,2]                                              #min_fan_step
                            write_data[52] = "0x" +  data_buf[326,2]                                              #max_fan_step
                            write_data[53] = "0x" +  data_buf[328,2]                                              #min_temp_th
                            write_data[54] = "0x" +  data_buf[330,2]                                              #ambient_temp_pitch
                            write_data[55] = "0x" +  data_buf[332,2]                                              #fan_ctrl_inerval
                            
                            write_data[56] = "0x" +  data_buf[334,20] 
#                            334.step(334+2*10-1, 2){ |num|
#                              write_data[56] += data_buf[num,2].hex.chr                                           #min_press_th[10]
#                            }
                            
                            write_data[57] = "0x" +  data_buf[354,12]                                             #initial_fan_step[6]
                            write_data[58] = "0x" +  data_buf[366,2]                                              #ambient_temp_hysteresis
                            write_data[59] = "0x" +  data_buf[368,2]                                              #ambient_press_hysteresis
                            write_data[60] = "0x" +  data_buf[370,2]                                              #break_temp_over_prot
                            write_data[61] = "0x" +  data_buf[372,2]                                              #shelf_temp_alert_prot
                            write_data[62] = "0x" +  data_buf[374,2]                                              #shelf_temp_alm_prot
                            write_data[63] = "0x" +  data_buf[376,2]                                              #cooling_fail_prot
                            write_data[64] = "0x" +  data_buf[378,2]                                              #cooling_fail_clr_prot
                            write_data[65] = "0x" +  data_buf[380,2]                                              #reserve4[14]
                            write_data[66] = "0x" +  data_buf[408,2]                                              #warmup_timer
                            write_data[67] = "0x" +  data_buf[412,2]                                              #reserve5[2]
                            write_data[68] = "0x" +  data_buf[416,2]                                              #pd1_init_value
                            write_data[69] = "0x" +  data_buf[420,2]                                              #pd2_init_value
                            write_data[70] = "0x" +  data_buf[424,2]                                              #max_abs_th
                            write_data[71] = "0x" +  data_buf[428,2]                                              #voa_adjust_fix_value
                            write_data[72] = "0x" +  data_buf[432,2]                                              #voa_setting_value_max
                            write_data[73] = "0x" +  data_buf[436,2]                                              #voa_setting_value_min
                            write_data[74] = "0x" +  data_buf[440,2]                                              #loop_target_level
                            write_data[75] = "0x" +  data_buf[444,2]                                              #loop_target_th_high
                            write_data[76] = "0x" +  data_buf[448,2]                                              #loop_target_th_low
                            
                            452.step(452+2*5-1, 2){ |num|
                                write_data[77] += data_buf[num,2].hex.chr                                           #max_power_drawn[5]
                            }
                            
                            462.step(462+2*5-1, 2){ |num|
                                write_data[78] += data_buf[num,2].hex.chr                                           #low_power_level[5]
                            }
                            
                            write_data[79] = "0x" +  data_buf[472,36]                                             #reserve6[18]
                            write_data[80] = "0x" +  data_buf[508,4]                                              #check_sum
                            
                            writesheets.Range(writesheets.cells(row_pos,2), writesheets.cells(row_pos,82)).value = write_data
                            
                        end # pi_log_count == 16
                    end # pi_log_type == "MDL")
                end #read_data =~ /#### PI/
            end
        end
        
        @xl.DisplayAlerts = false
        book.sheets("tmp").Delete
        @xl.DisplayAlerts = true
        
        #PI情報とTrap情報を組み合わせる。⇒RMVDが発生しているObjectはデータを消す
        #PI情報 (BLADE・FAN・PIU)シート
        sheets = book.sheets("PI情報 (BLADE・FAN・PIU)")
        
        for i in 0...3 
            if(@fan_data[i][1] == "critical")
                sheets.Range(sheets.cells(4+i,2),sheets.cells(4+i,82)).value = "-"
            end
        end
        
        for i in 0...4
            if(@piu_data[i][1] == "critical")
                sheets.Range(sheets.cells(7+i,2),sheets.cells(7+i,82)).value = "-"
            end
        end
        
        # Excel表の整形
        sheets.Columns.AutoFit
        
        #PI情報 (MDL)シート
        sheets = book.sheets("PI情報 (MDL)")
        
        for i in 0...4
            for j in 0...12
                if(@mdl_data[i][j][1] == "critical")
                sheets.Range(sheets.cells(2+i*12+j ,2),sheets.cells(2+i*12+j,8)).value = "-"
                end
            end
        end
        
        # Excel表の整形
        sheets.Columns.AutoFit
        
        # S150対応 Change Start 2020/12/11 by Nishi 
        @logobj.write("TECHINFO_Analysis_Common::log_check_PI_s100 End\n") 
        # S150対応 Change End 2020/12/11 by Nishi       
    # S150対応 Change Start 2020/12/11 by Nishi 
    end #log_check_s100
    # S150対応 Change End 2020/12/11 by Nishi 
    
    # S150対応 Add Start 2020/12/11 by Nishi 
    #----------------------------------------------------------#
    # log_check_s150
    # 概要：解析
    #       
    #       
    #----------------------------------------------------------#
    def log_check_PI_s150(path,book)
        
        @logobj.write("TECHINFO_Analysis_Common::log_check_PI_s150 Start\n")
        fan_data = Array.new(8,"-")
        piu_data = Array.new(4,"-")
        psu_data = Array.new(4,"-")
        mdl_data1 = Array.new(1).map{Array.new(26,"-")}
        mdl_data2 = Array.new(2).map{Array.new(12,"-")}
        
        data_count = 0
        excel_sheet_add("tmp",book)
        sheets = book.sheets("tmp")
        
        Zlib::GzipReader.open(path) do |tgz|
            Minitar::Reader.open(tgz).each do |entry|
                filedata = entry.read
                if (filedata == nil)
                    next
                end
                
                filename = File.basename(entry.name)
                
                #PIlog
                if(filename == "ud_pi.LOG")
                    data = StringIO.new(filedata)
                    #ゴミデータの排除、およびループしていた場合のデータを時系列に並べる
                    data_count = sort_data_by_time(data,sheets)
                end
            end
        end
        @logobj.write("TECHINFO_Analysis_Common::log_check_PI_s150 - Zlib::GzipReader End\n")
        if(data_count != 0)
            #フォーマットに沿ってとられている先頭までを探す(それまでのデータは捨てる)
            delete_line = 0
            for line in 1...data_count + 1 do
                line_data = sheets.cells(line,4).value
                if(line_data =~ /#### PI/)
                    break
                else
                    sheets.Rows(line).delete
                    delete_line += 1
                    redo
                end
            end
            data_count -= delete_line
            #logging開始行を消す(上のfor文で引っかかるはずだが途中に入っていた場合を考慮する(ありえないはずだが))
            delete_line = 0
            for line in 1...data_count + 1 do
                line_data = sheets.cells(line,4).value
                if(line_data =~ / #### UD_FLOG_PI Start ####/)
                    sheets.Rows(line).delete
                    delete_line += 1
                    break
                end
            end
            data_count -= delete_line
            
            pi_log_count = 0
            row_pos = 0
            
            pi_log_type = "NO MDL"
            # S150対応 Change Start 2020/12/24 by Nishi
            writesheets = book.sheets("PI情報 (BLADE・CPU・FAN・PIU・PSU)")
            # S150対応 Change End 2020/12/24 by Nishi
            write_data = ["","","","","","","","","","",
                          "","","","","","","","","","",
                          "","","","","","","","","","",
                          "","","","","","","","","","",
                          "","","","","","","","","","",
                          "","","","","","","","","","",
                          "","","","","","","","","","",
                          "","","","","","","","","","","",]
            
            data_buf = ""
            
            for line in 1...data_count + 1 do
                line_data = sheets.cells(line,4).value
                if(line_data =~ /#### PI/)
                    pi_log_count = 0
                    if(line_data =~ /#### PI MDL/)
                        writesheets = book.sheets("PI情報 (MDL)")
                        pi_log_type = "MDL"
                        key_data_slot = line_data.scan(/slot:\d{1,}/).join(",").delete("slot:").to_i 
                        key_data_port = line_data.scan(/port:\d{1,}/).join(",").delete("port:").to_i
                        # MDL1
                        if (key_data_slot == 1)
                            row_pos = 2 + (key_data_slot -1)*26 + (key_data_port -1)
                        # MDL2,3
                        else
                            row_pos = 28 + (key_data_slot -2)*12 + (key_data_port -1)
                        end
                    else
                        # S150対応 Change Start 2020/12/24 by Nishi
                        writesheets = book.sheets("PI情報 (BLADE・CPU・FAN・PIU・PSU)")
                        # S150対応 Change End 2020/12/24 by Nishi
                        pi_log_type = "NO MDL"
                        write_data = ["","","","","","","","","","",
                                      "","","","","","","","","","",
                                      "","","","","","","","","","",
                                      "","","","","","","","","","",
                                      "","","","","","","","","","",
                                      "","","","","","","","","","",
                                      "","","","","","","","","","",
                                      "","","","","","","","","","","",]
                        data_buf = ""
                        
                        if(line_data =~ /BLADE/)
                            row_pos = 3
                        # S150対応 Change Start 2020/12/24 by Nishi
                        elsif(line_data =~ /CPU/)
                            row_pos = 4
                        elsif(line_data =~ /FAN/)
                            key_data_slot = line_data.scan(/slot = \d{1,}/).join(",").delete("slot = ").to_i
                            row_pos = 4 + key_data_slot
                        elsif(line_data =~ /PIU/)
                            key_data_slot = line_data.scan(/slot = \d{1,}/).join(",").delete("slot = ").to_i
                            row_pos = 12 + key_data_slot
                        elsif(line_data =~ /PSU/)
                            key_data_slot = line_data.scan(/slot = \d{1,}/).join(",").delete("slot = ").to_i
                            row_pos = 15 + key_data_slot
                        end
                        # S150対応 Change End 2020/12/24 by Nishi
                    end
                else
                    pi_log_count += 1
                    if( pi_log_type == "MDL")
                        info_start_pos  = line_data.index("=") + 2
                        info_data       = line_data[info_start_pos,line_data.length - info_start_pos + 1]
                        writesheets.cells(row_pos,pi_log_count + 1).Value = info_data
                    # S150対応 Change Start 2020/12/24 by Nishi
                    else  # BLADE/CPU/FUN/PIU/PSU
                    # S150対応 Change End 2020/12/24 by Nishi
                        line_data.delete!(" ")
                        data_buf += line_data
                        
                        # 貯めたデータをPIの型に合わせこむ
                        if(pi_log_count == 16)
                            write_data[0] = "0x" + data_buf[0,4]                                                  #unit_code
                            write_data[1] = "0x" +  data_buf[4,2]                                                 #version_flag
                            
                            #      6から2文字ずつ3回飛ばして-1
                            6.step(6+2*3-1, 2){ |num|
                                write_data[2] += data_buf[num,2].hex.chr                                            #issue_num[3]
                            }
                            
                            12.step(12+2*9-1, 2){ |num|
                                write_data[3] += data_buf[num,2].hex.chr                                            #abbreviation_name[9]
                            }
                            
                            30.step(30+2*10-1, 2){ |num|
                                write_data[4] += data_buf[num,2].hex.chr                                            #fc_num[10]
                            }
                            
                            50.step(50+2*10-1, 2){ |num|
                                write_data[5] += data_buf[num,2].hex.chr                                            #clei_code[10]
                            }
                            
                            70.step(70+2*5-1, 2){ |num|
                                write_data[6] += data_buf[num,2].hex.chr                                            #product_year_month[5]
                            }
                            
                            80.step(80+2*2-1, 2){ |num|
                                write_data[7] += data_buf[num,2].hex.chr                                            #location_code[2]
                            }
                            
                            84.step(84+2*5-1, 2){ |num|
                                write_data[8] += data_buf[num,2].hex.chr                                            #serial_num[5]
                            }
                            
                            write_data[9]  = "0x" +  data_buf[94,6]                                               #reserve0[3]
                            write_data[10] = "0x" +  data_buf[100,12]                                             #mac_addr_min[6]
                            write_data[11] = "0x" +  data_buf[112,12]                                             #mac_addr_max[6]
                            write_data[12] = "0x" +  data_buf[124,4]                                              #fwdl_type
                            write_data[13] = "0x" +  data_buf[128,64]                                             #reserve1[32]
                            
                            192.step(192+2*5-1, 2){ |num|
                                write_data[14] += data_buf[num,2].hex.chr                                           #pcb_revision[5]
                            }
                            
                            write_data[15] = "0x" +  data_buf[202,14]                                             #reserve2[7]
                            
                            #parts_temp_th[10];
                            0.step(29,3){ |parts_temp_th|
                                write_data[16 + parts_temp_th] = "0x" +  data_buf[216+ 2*parts_temp_th ,2]          #break_temp
                                write_data[16 + parts_temp_th + 1] = "0x" +  data_buf[216+ 2*parts_temp_th + 2 ,2]  #max_temp
                                write_data[16 + parts_temp_th + 2] = "0x" +  data_buf[216+ 2*parts_temp_th + 4 ,2]  #min_temp
                            }
                            
                            276.step(276+2*8-1, 2){ |num|
                                write_data[46] += data_buf[num,2].hex.chr                                           #min_wavelength[8]
                            }
                            
                            292.step(292+2*8-1, 2){ |num|
                                write_data[47] += data_buf[num,2].hex.chr                                           #max_wavelength[8]
                            }
                            
                            308.step(308+2*6-1, 2){ |num|
                                write_data[48] += data_buf[num,2].hex.chr                                           #frec_spacing[6]
                            }
                            
                            write_data[49] = "0x" +  data_buf[320,2]                                              #reserve3
                            write_data[50] = "0x" +  data_buf[322,2]                                              #press_sensor_exist
                            
                            write_data[51] = "0x" +  data_buf[324,2]                                              #min_fan_step
                            write_data[52] = "0x" +  data_buf[326,2]                                              #max_fan_step
                            write_data[53] = "0x" +  data_buf[328,2]                                              #min_temp_th
                            write_data[54] = "0x" +  data_buf[330,2]                                              #ambient_temp_pitch
                            write_data[55] = "0x" +  data_buf[332,2]                                              #fan_ctrl_inerval
                            
                            write_data[56] = "0x" +  data_buf[334,20] 
#                            334.step(334+2*10-1, 2){ |num|
#                              write_data[56] += data_buf[num,2].hex.chr                                           #min_press_th[10]
#                            }
                            
                            write_data[57] = "0x" +  data_buf[354,12]                                             #initial_fan_step[6]
                            write_data[58] = "0x" +  data_buf[366,2]                                              #ambient_temp_hysteresis
                            write_data[59] = "0x" +  data_buf[368,2]                                              #ambient_press_hysteresis
                            write_data[60] = "0x" +  data_buf[370,2]                                              #break_temp_over_prot
                            write_data[61] = "0x" +  data_buf[372,2]                                              #shelf_temp_alert_prot
                            write_data[62] = "0x" +  data_buf[374,2]                                              #shelf_temp_alm_prot
                            write_data[63] = "0x" +  data_buf[376,2]                                              #cooling_fail_prot
                            write_data[64] = "0x" +  data_buf[378,2]                                              #cooling_fail_clr_prot
                            write_data[65] = "0x" +  data_buf[380,2]                                              #reserve4[14]
                            write_data[66] = "0x" +  data_buf[408,2]                                              #warmup_timer
                            write_data[67] = "0x" +  data_buf[412,2]                                              #reserve5[2]
                            write_data[68] = "0x" +  data_buf[416,2]                                              #pd1_init_value
                            write_data[69] = "0x" +  data_buf[420,2]                                              #pd2_init_value
                            write_data[70] = "0x" +  data_buf[424,2]                                              #max_abs_th
                            write_data[71] = "0x" +  data_buf[428,2]                                              #voa_adjust_fix_value
                            write_data[72] = "0x" +  data_buf[432,2]                                              #voa_setting_value_max
                            write_data[73] = "0x" +  data_buf[436,2]                                              #voa_setting_value_min
                            write_data[74] = "0x" +  data_buf[440,2]                                              #loop_target_level
                            write_data[75] = "0x" +  data_buf[444,2]                                              #loop_target_th_high
                            write_data[76] = "0x" +  data_buf[448,2]                                              #loop_target_th_low
                            
                            452.step(452+2*5-1, 2){ |num|
                                write_data[77] += data_buf[num,2].hex.chr                                           #max_power_drawn[5]
                            }
                            
                            462.step(462+2*5-1, 2){ |num|
                                write_data[78] += data_buf[num,2].hex.chr                                           #low_power_level[5]
                            }
                            
                            write_data[79] = "0x" +  data_buf[472,36]                                             #reserve6[18]
                            write_data[80] = "0x" +  data_buf[508,4]                                              #check_sum
                            
                            writesheets.Range(writesheets.cells(row_pos,2), writesheets.cells(row_pos,82)).value = write_data
                            
                        end # pi_log_count == 16
                    end # pi_log_type == "MDL")
                end #read_data =~ /#### PI/
            end
        end
        
        @xl.DisplayAlerts = false
        book.sheets("tmp").Delete
        @xl.DisplayAlerts = true
        
        # S150対応 Change Start 2020/12/24 by Nishi
        #PI情報とTrap情報を組み合わせる。⇒RMVDが発生しているObjectはデータを消す
        #PI情報 (BLADE・FAN・PIU・PSU)シート
        sheets = book.sheets("PI情報 (BLADE・CPU・FAN・PIU・PSU)")
        # S150対応 Change End 2020/12/24 by Nishi
        
        # S150対応 Add Start 2020/12/24 by Nishi
        for i in 0...1
            if(@cpu_data[i][1] == "critical")
                sheets.Range(sheets.cells(4+i,2),sheets.cells(4+i,82)).value = "-"
            end
        end    
        # S150対応 Add End 2020/12/24 by Nishi            
        
        for i in 0...8 
            if(@fan_data[i][1] == "critical")
                # S150対応 Change Start 2020/12/24 by Nishi
                sheets.Range(sheets.cells(5+i,2),sheets.cells(5+i,82)).value = "-"
                # S150対応 Change End 2020/12/24 by Nishi
            end
        end
        
        for i in 0...3
            if(@piu_data[i][1] == "critical")
                # S150対応 Change Start 2020/12/24 by Nishi
                sheets.Range(sheets.cells(13+i,2),sheets.cells(13+i,82)).value = "-"
                # S150対応 Change End 2020/12/24 by Nishi
            end
        end
        
        for i in 0...4
            if(@psu_data[i][1] == "critical")
                # S150対応 Change Start 2020/12/24 by Nishi
                sheets.Range(sheets.cells(16+i,2),sheets.cells(16+i,82)).value = "-"
                # S150対応 Change End 2020/12/24 by Nishi
            end
        end

        # Excel表の整形
        sheets.Columns.AutoFit
        
        #PI情報 (MDL)シート
        sheets = book.sheets("PI情報 (MDL)")
      
        #MDL1
        for i in 0...1
            for j in 0...26
                if(@mdl_data1[i][j][1] == "critical")    
                    sheets.Range(sheets.cells(2+i*26+j ,2),sheets.cells(2+i*26+j,8)).value = "-"
                end
            end
        end
        
        #MDL2,3
        for i in 0...2
            for j in 0...12
                if(@mdl_data2[i][j][1] == "critical")  
                    sheets.Range(sheets.cells(54+i*12+j ,2),sheets.cells(54+i*12+j,8)).value = "-"
                end
            end
        end
        
        # Excel表の整形
        sheets.Columns.AutoFit
        
        @logobj.write("TECHINFO_Analysis_Common::log_check_PI_s150 End\n")
    end #log_check_s150
    # S150対応 Add End 2020/12/11 by Nishi 

    #----------------------------------------------------------#
    # sort_data_by_time
    # 概要：サイクリックにとられたlogに対して時刻でソートする。
    #       時刻のデータはA/B/Cのセルに設定
    #       時刻以外のデータはDのセルに設定
    #
    # ブロック引数がある場合: 行の抽出条件 ※時系列に依存する条件は不可
    #
    #----------------------------------------------------------#
    def sort_data_by_time(data,sheets)
        line_count = 0
        loop_data = ""
        last_type = 0   #最終行の処理種別
        last_data = ""  #最終行のテキスト
        normal_pattern = /\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}.\d{6}/o
        
        #エクセルへの吐き出し
        data.each_line  do |read_data|
            
            read_data = read_data.unpack("Z*").join(",").delete("\n")
            last_data = read_data
            
            if block_given?
                #対象外行は除外
                unless yield(read_data)
                    read_data = ""
                end
            end
            
            #正常データ
            if normal_pattern === read_data
                line_count += 1
                sheets.Cells(line_count, 1).value = read_data
                last_type = 1
            else
            #異常データ
                if(last_type == 0)
                   #1行目だった場合はループしたデータかもしれないので保存する
                   loop_data = last_data
                   last_type = 2
                elsif last_data != "" 
                   #2行目以降で空でなければ、ループデータに続く可能性がある
                   last_type = 3
                else
                   last_type = 4
                end
            end
        end
        #ループデータがあれば、最後の行にくっつける
        if(loop_data != '')
            last_data += loop_data
            if block_given?
                #対象外行は除外
                last_data = '' unless yield(last_data)
            end
            case last_type
            when 1 then
                if normal_pattern === last_data
                    sheets.Cells(line_count, 1).value = last_data
                else
                    sheets.Cells(line_count, 1).clear
                    line_count -= 1
                end
               
            when 3 then
                # 最後の行が異常データの場合、
                # ループデータをくっつけて正常になれば、次の行に格納する。
                if normal_pattern === last_data
                    line_count += 1
                    sheets.Cells(line_count, 1).value = last_data
                end
            end
        end
        
        #エクセルファイルのデータを整形
        if(line_count > 0)
            
            #時刻とほかのデータを分離
            write_data = Array.new(4)  
            for line in 1...line_count + 1 do
                line_data = sheets.Cells(line, 1).value
                write_data.fill("")
                #時刻データを取得
                line_data.match(/\d{4}\/\d{2}\/\d{2}/) {|md| write_data[0] = md[0]}
                line_data.match(/\d{2}:\d{2}:\d{2}/)   {|md| write_data[1] = md[0]}
                line_data.match(/.\d{6}/)              {|md| write_data[2] = md[0]}
                
                #データから時刻データを削除
                line_data.gsub!(/\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}.\d{6} /,"")
                write_data[3] = line_data
                #
                sheets.Cells(line, 1).Resize(1,4).value = write_data
            end
            
            sheets.Range(sheets.cells(1,1), sheets.cells(line_count,4)).Sort({"key1" => sheets.Range("A1"), 
                                                                              "key2" => sheets.Range("B1"), 
                                                                              "key3" => sheets.Range("C1")})
            #自動整形
            sheets.Columns.AutoFit
        end
        return line_count
    end
    

end

#----------------------------------------------------------#
# 警報テーブルデータ                                       #
#----------------------------------------------------------#
$g_liualmtbl = ""
$g_swfalmtbl = ""
$g_cpualmtbl = ""

#----------------------------------------------------------#
# 共通関数                                                 #
#----------------------------------------------------------#
module TECHINFO_Analysis_Common
module_function

    #----------------------------------------------------------#
    #  FLT Tableファイル名取得                                 #
    #----------------------------------------------------------#
    def flt_table_list_get()
        ptn = sprintf($g_pattern_flt_table, '')
        file_list = Pathname.glob(Regexp.escape($D_FLTTBL_PATH) + '/' + ptn).map { |f| File.basename(f) }
        return file_list
    end
    
    def flt_table_name_get()
        if not $g_filename_flt_table.nil?
            if not File.exist?($D_FLTTBL_PATH + "/" + $g_filename_flt_table)
                popup_msg("異常通知", "[ERROR]", $D_FLTTBL_PATH, "\nに FLT Table (#{$g_filename_flt_table}) が存在しません。");
                $g_filename_flt_table = nil
            end
        else
            ptn = sprintf($g_pattern_flt_table, '')
        
            file_list = flt_table_list_get()
            case file_list.length
            when 1
                 $g_filename_flt_table = file_list[0]
            when 0
                ptn.gsub!("\\","")
                popup_msg("異常通知", "[ERROR]", $D_FLTTBL_PATH, "\nに FLT Table (#{ptn}) が存在しません。");
            else
                ptn.gsub!("\\","")
                popup_msg("異常通知", "[ERROR]", $D_FLTTBL_PATH, "\nに FLT Table (#{ptn}) が複数あります。\n",file_list);
            end
        end
        
        return $g_filename_flt_table
    end
    
    def flt_table_path_get()
        name = flt_table_name_get
        if name.nil?
            return nil
        end
        return $D_FLTTBL_PATH + "/" + name
    end
    
    #----------------------------------------------------------#
    #  Popupメッセージ表示                                     #
    #----------------------------------------------------------#
    def popup_msg(title, *descrption)

        puts [ title, descrption ].join(" "), "\n"

        wsh = WIN32OLE.new('WScript.Shell')
        wsh.Popup(descrption.join("\n"), 0, title)
    end

end #TECHINFO_Analysis_Common
