#coding: windows-31j
#/****************************************************************************
# *  fileName: TECHINFO_Analysis_GUI.rb
# *
# *               All rights reserved, Copyright (C) FUJITSU LIMITED 2016-2017
# *-----------------------------------------------------------------------------
# *  処理概要: ツールのGUI生成
# *            
# *  引数    : なし
# *  復帰値  : なし
# *---------------------------------------------------------------------------
# *  変更履歴: 2016.11.24 katou     新規作成(Version 1.0)
# *                                 L2の解析ツールをベースとする。
# *            2017.05.19 kinomoto  機能追加(Version 1.1)
# *                                 (1) CUI追加
# *                                 (2) 被疑部品検索機能追加
# *                                 (3) FLTテーブルファイル名を実行時に取得
# *            2017.08.18 kinomoto  温度Over検出機能追加 (Version 1.2)
# *
# ****************************************************************************/
#----------------------------------------------------------#
# 基本 require
#----------------------------------------------------------#
if __FILE__ == "TECHINFO_Analysis_GUI.rb"
  runpath = Dir::pwd
else
  runpath = File.dirname(__FILE__)
end

require 'win32ole'
require 'yaml'
require 'pp'
require 'find'
require 'zlib'
require 'kconv'
require 'archive/tar/minitar'
require 'stringio'
require 'fileutils'
include Archive::Tar
require 'scanf'
require 'singleton'
require File.expand_path(runpath + './TECHINFO_Analysis_Common.rb')
require File.expand_path(runpath + './TECHINFO_AlmLog_Analysis.rb')
require File.expand_path(runpath + './TECHINFO_Almdump_Analysis.rb')
require File.expand_path(runpath + './TECHINFO_PIlog_Analysis.rb')
require File.expand_path(runpath + './TECHINFO_SDH_ASP_Analysis.rb')
#require File.expand_path(runpath + './TECHINFO_ACDefineDataGenarate.rb')
#require File.expand_path(runpath + './TECHINFO_ACLog_Analysis.rb')
require File.expand_path(runpath + './TECHINFO_One_finity_tool.rb')
require File.expand_path(runpath + './TECHINFO_Analysis_CUI.rb')
# require 'green_shoes'
require 'uri'
require 'gtk3'
#17920対応 noda
require 'thread'
require "open3"
require 'optparse'
#----------------------------------------------------------#
# Log Drop File
# Log解析 GUI Window
#----------------------------------------------------------#

#----------------------------------------------------------#
# 警報ログ解析 GUI Window
#----------------------------------------------------------#
class Alm_log_Window < Gtk::Window
    
    # クラス変数
    @@almlog_analyze_obj = nil
    
    def initialize
        @logobj = Log_class.instance
        @logobj.write("STEP3_Window initialize Start\n")
        
        super("警報ログ解析 " + $log_analyeze_version)
        set_default_size(500, 510)
        vbox = Gtk::VBox.new(false, 5)
        
        lbl2  = Gtk::Label.new("■解析対象ログ選択")
        lbl2.set_alignment(0, 0)
        stschgCheckbox = Gtk::CheckButton.new("警報変化点ログ") 
        fltCheckbox = Gtk::CheckButton.new("FLTログ   (※register/bit名検索のため時間がかかります)")
        repairpartsCheckbox = Gtk::CheckButton.new("FLTログ + 被疑部品検索")
        eccerrCheckbox = Gtk::CheckButton.new("警報メモリECCエラーログ")
        softerrCheckbox = Gtk::CheckButton.new("FPGAソフトエラーログ")
        ffanCheckbox = Gtk::CheckButton.new("FFANログ(温度上昇FLT検出) + 被疑部品検索")
        stschgCheckbox.active = true
=begin
        detailCheckbox = Gtk::CheckButton.new("警報発生ログ")
        cyclechgCheckbox = Gtk::CheckButton.new("警報状態変化定期収集ログ")
        ilanCheckbox = Gtk::CheckButton.new("装置内通信統計ログ")
=end

        lbl4  = Gtk::Label.new("FLT Table ファイル名:")
        lbl4.set_alignment(0, 0)
        fltTableCombobox = Gtk::ComboBox.new
        flt_table_list = TECHINFO_Analysis_Common.flt_table_list_get()
        if flt_table_list.size > 0
            $g_filename_flt_table = flt_table_list[0]
            flt_table_list.each {|val| fltTableCombobox.append_text(val)}
            fltTableCombobox.active = 0
            fltTableCombobox.signal_connect("changed") {|w| $g_filename_flt_table = w.active_iter[0].encode(Encoding::Windows_31J)}
        end
        flt_table_list.clear
        
        lbl3  = Gtk::Label.new("TECHINFO.tar.gzをdrop")
        #17920対応 noda
        $progress_value = 0
        $exclusion_flag = 0
        prg_lbl  = Gtk::Label.new("")
        prg_lbl.text = "警報ログ解析"
        prg  = Gtk::ProgressBar.new
        prg.text = "0%"
        prg.fraction = $progress_value
        dmylbl_array1 = []
        for i in 0..1 do
            dmylbl_array1 << Gtk::Label.new("")
        end
        
        dmylbl_array2 = []
        for i in 0..1 do
            dmylbl_array2 << Gtk::Label.new("")
        end
        
        #17920対応 noda
        dmylbl1  = Gtk::Label.new("")
        
        # 体裁を整えるため空のラベルを挿入
        dmylbl_array1.each{|dmylbl|
            vbox.pack_start(dmylbl, false, true, 0)
        }
        
        vbox.pack_start(lbl2, false, true, 0)
        vbox.pack_start(stschgCheckbox, false, true, 0)
        vbox.pack_start(fltCheckbox, false, true, 0)
        vbox.pack_start(repairpartsCheckbox, false, true, 0)
        vbox.pack_start(lbl4, false, true, 0)
        vbox.pack_start(fltTableCombobox, false, true, 0)
        vbox.pack_start(eccerrCheckbox, false, true, 0)
        vbox.pack_start(softerrCheckbox, false, true, 0)
        vbox.pack_start(ffanCheckbox, false, true, 0)
=begin
        vbox.pack_start(detailCheckbox, false, true, 0)
        vbox.pack_start(cyclechgCheckbox, false, true, 0)
        vbox.pack_start(ilanCheckbox, false, true, 0)
=end
        # 体裁を整えるため空のラベルを挿入
        dmylbl_array2.each{|dmylbl|
            vbox.pack_start(dmylbl, false, true, 0)
        }
        
        vbox.pack_start(lbl3, false, true, 0)
        
        # 体裁を整えるため空のラベルを挿入
        vbox.pack_start(dmylbl1, false, true, 0)
        
        #17920対応 noda
        vbox.pack_start(prg_lbl, false, true, 0)
        vbox.pack_start(prg, false, true, 0)
        
        # 呼び出し元Windowをアクセス禁止にする
        set_modal(true)
        
        add(vbox)
        
        # ファイルをDropしたときの設定
        # URIを取得するように設定
        Gtk::Drag.dest_set(self,
                           Gtk::Drag::DEST_DEFAULT_MOTION | 
                           Gtk::Drag::DEST_DEFAULT_HIGHLIGHT | 
                           Gtk::Drag::DEST_DEFAULT_DROP,
                           [["text/uri-list", Gtk::Drag::TARGET_OTHER_APP, 12345]],
                           Gdk::DragContext::ACTION_COPY | 
                           Gdk::DragContext::ACTION_MOVE)
        
        signal_connect("drag-data-received") { |widget, context, x, y, data, info, time|
            #17920対応 noda
            if $exclusion_flag == 1
                #連続でDROPされた時は何もしない
                puts "[debug] 連続drop" #debug
            else
                # DROPされた時の処理
                @logobj.write("Alm_log_Window signal_connect(\"drag-data-received\") Start\n")
                $exclusion_flag = 1
                puts "[debug] drag & drop" #debug
                #17920対応 noda
                #self._init
                @@almlog_analyze_obj = Almlog_Analyze_Cls.new(lbl3)
                # progressスレッドが起動済みの場合はkillする。
                if @progress != nil then
                    Thread.kill(@progress)
                end
                # ファイルがDropされたため、初期化
                $progress_value = 0
                $progress_max_log_no = 0
                $progress_log_no = 0
                
                lbl3.text = "解析開始"
                
                show_all
                # チェックボックスを非アクティブにする
                fltCheckbox.sensitive = false
                repairpartsCheckbox.sensitive = false
                fltTableCombobox.sensitive = false
                stschgCheckbox.sensitive = false
                eccerrCheckbox.sensitive = false
                softerrCheckbox.sensitive = false
                ffanCheckbox.sensitive = false
=begin
                detailCheckbox.sensitive = false
                cyclechgCheckbox.sensitive = false
                ilanCheckbox.sensitive = false
=end
                # チェックボックスにチェックが入っているか判定する
                if stschgCheckbox.active? then
                    $g_step2_log_stschgchk="する"
                else
                    $g_step2_log_stschgchk="Skip"
                end
                
                $g_step2_log_repairparts = false
                if repairpartsCheckbox.active? then
                    $g_step2_log_fltchk="する"
                    $g_step2_log_repairparts = true
                elsif fltCheckbox.active? then
                    $g_step2_log_fltchk="する"
                else
                    $g_step2_log_fltchk="Skip"
                end
                
                if eccerrCheckbox.active? then
                    $g_step2_log_eccerrchk="する"
                else
                    $g_step2_log_eccerrchk="Skip"
                end
                
                if softerrCheckbox.active? then
                    $g_step2_log_softerrchk="する"
                else
                    $g_step2_log_softerrchk="Skip"
                end
                if ffanCheckbox.active? then
                    $g_step2_log_ffanchk = 2 # する。被疑部品検索も行う。
                else
                    $g_step2_log_ffanchk = 0 # Skip
                end
=begin
                if cyclechgCheckbox.active? then
                    $g_step2_log_cyclechgchk="する"
                else
                    $g_step2_log_cyclechgchk="Skip"
                end
                
                if ilanCheckbox.active? then
                    $g_step2_log_ilanchk="する"
                else
                    $g_step2_log_ilanchk="Skip"
                end
=end
                #警報変化点LOG解析進捗率表示
                @progress = Thread.new(prg, prg_lbl) { |prg, prg_lbl|
                    before_value = $progress_value
                    loop {
                        if before_value != $progress_value
                            before_value = $progress_value
                            prg.fraction = $progress_value
                            prg.text = "#{(prg.fraction * 100).round}%"
                            if prg.fraction >= 1.0 then
                                prg_lbl.text = sprintf("%d/%d %s解析完了", $progress_log_no, $progress_max_log_no, $progress_funcname)
#                                break
                            else
                                prg_lbl.text = sprintf("%d/%d %s解析中", $progress_log_no, $progress_max_log_no, $progress_funcname)
                            end
                        end
                        sleep 0.2
                    }
                }
                
                # DROPされたファイルのファイル名取得
                data.uris.each { |uri|
                    @dropuri = URI.decode(uri)
                    @dropfile = File.basename(@dropuri)
                }
                
                droppass = @dropuri.gsub("file:///", "")
                puts droppass
                
                # 処理中に画面の更新ができないので、別スレッドで処理する
                @excelset = Thread.new() {
                    # パスも含めたファイル名が長い場合、ファイルが正常に取得できないケースがあるため
                    # ここでチェックする。
                    
                    if ! (@dropfile =~ /\.tar.gz$/)
                        popup_msg =  sprintf("解析失敗\n")
                        popup_msg << sprintf("Drag&Dropされたファイルがtar.gzファイルではありません。\n")
                        popup_msg << sprintf("tar.gzファイルをDrag&Dropしてください。\n")
                        wsh = WIN32OLE.new('WScript.Shell')
                        wsh.Popup(popup_msg, 0, "異常通知")
                    elsif  File.file?(droppass)
                        book = @@almlog_analyze_obj.createExcelWorkBook(droppass)
                        
                        if book != nil
                            begin
                                rcode = @@almlog_analyze_obj.excelset(droppass, book)
                                if rcode == 1
                                    exit(1)
                                end
                            # 異常終了した場合 Excelをクローズ
                            ensure
                                @@almlog_analyze_obj.closeExcelWorkbook(book)
                                lbl3.text = "TECHINFO.tar.gzをdrop"
                            end
                        else
                            puts "book = nil"
                            lbl3.text = "TECHINFO.tar.gzをdrop"
                        end
                    else
                        popup_msg =  sprintf("解析失敗\n")
                        popup_msg << sprintf("フルパスも含めた、TECHINFO名が長すぎるため解析に失敗しました。\n")
                        popup_msg << sprintf("ファイル名を変更するか、TECHINFO格納場所の階層を浅くしてください。\n")
                        wsh = WIN32OLE.new('WScript.Shell')
                        wsh.Popup(popup_msg, 0, "異常通知")
                        
                        lbl3.text = "TECHINFO.tar.gzをdrop"
                        
                    end
                    #17920対応 noda
                    $exclusion_flag = 0
                    # チェックボックスをアクティブにする
                    fltCheckbox.sensitive = true
                    repairpartsCheckbox.sensitive = true
                    fltTableCombobox.sensitive = true
                    stschgCheckbox.sensitive = true
                    eccerrCheckbox.sensitive = true
                    softerrCheckbox.sensitive = true
                    ffanCheckbox.sensitive = true
=begin
                    detailCheckbox.sensitive = true
                    cyclechgCheckbox.sensitive = true
                    ilanCheckbox.sensitive = true
=end
                    
                    prg_lbl.text = "警報ログ解析"
                    prg.text = "0%"
                    prg.fraction = 0
                    
                }
                @logobj.write("Alm_log_Window signal_connect(\"drag-data-received\") End\n")
            end #if $exclusion_flag == 1
        }
        
        #self._init
        @@almlog_analyze_obj = Almlog_Analyze_Cls.new(lbl3)
        
        # ×ボタンなど
        signal_connect("destroy") { 
            #17920対応 noda
            if @progress != nil then
                Thread.kill(@progress)
            end
            if @excelset != nil then
                Thread.kill(@excelset)
            end
            Gtk.main_quit
        }
        show_all
        @logobj.write("STEP3_Window initialize End\n")
        
    end #initialize
end #Alm_log_Window

#----------------------------------------------------------#
# alm_dump解析 GUI Window
#----------------------------------------------------------#
class Alm_dump_Window < Gtk::Window
    
    def initialize
        @logobj = Log_class.instance
        @logobj.write("Alm_dump_Window::initialize Start\n")
        super("alm_dump解析 "  + $log_analyeze_version)
        set_default_size(360, 250)
        $exclusion_flag = 0
        @label  = Gtk::Label.new("TECHINFO.tar.gzをdrop")
        
        # 本Window以外アクセス禁止
        set_modal(true)
        
        add(@label)
        
        # ファイルをDropしたときの設定
        # URIを取得するように設定
        Gtk::Drag.dest_set(self,
                           Gtk::Drag::DEST_DEFAULT_MOTION | 
                           Gtk::Drag::DEST_DEFAULT_HIGHLIGHT | 
                           Gtk::Drag::DEST_DEFAULT_DROP,
                           [["text/uri-list", Gtk::Drag::TARGET_OTHER_APP, 1234]],
                           Gdk::DragContext::ACTION_COPY | 
                           Gdk::DragContext::ACTION_MOVE)
        
        signal_connect("drag-data-received") { |widget, context, x, y, data, info, time|
            @logobj.write("Alm_dump_Window signal_connect(\"drag-data-received\") Start\n")
            if $exclusion_flag == 1
                #連続でDROPされた時は何もしない
                puts "[debug] 連続drop" #debug
            else
                $exclusion_flag = 1
                
                # DROPされた時の処理
                data.uris.each { |uri|
                    @dropuri = URI.decode(uri)
                    @dropfile = File.basename(@dropuri)
                }
                dropfile = @dropuri.gsub("file:///", "")
                $g_Almdump_drop_file = dropfile
                
                if ! (@dropfile =~ /\.tar.gz$/)
                    popup_msg =  sprintf("解析失敗\n")
                    popup_msg << sprintf("Drag&Dropされたファイルがtar.gzファイルではありません。\n")
                    popup_msg << sprintf("tar.gzファイルをDrag&Dropしてください。\n")
                    wsh = WIN32OLE.new('WScript.Shell')
                    wsh.Popup(popup_msg, 0, "異常通知")
                elsif  File.file?(dropfile)
                    # パスも含めたファイル名が長い場合、ファイルが正常に取得できないケースがあるため
                    # ここでチェックする。
                    
                    #self._init
                    @@almdump_log_analyze_obj = Almdump_log_Analyze_Cls.new(@label)
                    book = @@almdump_log_analyze_obj.createExcelWorkBook(dropfile)
                    puts book #debug
                    
                    if book != nil
                        begin
                            @@almdump_log_analyze_obj.excelset(dropfile, book)
                        # 異常終了した場合 Excelをクローズ
                        ensure
                            @@almdump_log_analyze_obj.closeExcelWorkbook(book)
                            @label.text = "TECHINFO.tar.gzをdrop"
                        end
                    else
                        puts "book = nil"
                        @label.text = "TECHINFO.tar.gzをdrop"
                    end
                    
                    @logobj.write("Alm_dump_Window signal_connect(\"drag-data-received\") End\n")
                else
                    popup_msg =  sprintf("解析失敗\n")
                    popup_msg << sprintf("フルパスも含めた、TECHINFO名が長すぎるため解析に失敗しました。\n")
                    popup_msg << sprintf("ファイル名を変更するか、TECHINFO格納場所の階層を浅くしてください。\n")
                    wsh = WIN32OLE.new('WScript.Shell')
                    wsh.Popup(popup_msg, 0, "異常通知")
                    
                    @label.text = "TECHINFO.tar.gzをdrop"
                end
                $exclusion_flag = 0
            end #$exclusion_flag == 1
        } #signal_connect("drag-data-received")
        
        # ×ボタンなど
        signal_connect("destroy") { Gtk.main_quit }
        show_all
        @logobj.write("Alm_dump_Window::initialize End\n")
    end #initialize
end #Alm_dump_Window

#----------------------------------------------------------#
# PI解析 GUI Window
#----------------------------------------------------------#
class PI_Window < Gtk::Window
    
    def initialize
        @logobj = Log_class.instance
        @logobj.write("PI_Window::initialize Start\n", 0, File.basename(__FILE__), __LINE__)
        $exclusion_flag = 0
        super("PI解析 "  + $log_analyeze_version)
        set_default_size(360, 250)
        
        @label  = Gtk::Label.new("TECHINFO.tar.gzをdrop")
        
        # 本Window以外アクセス禁止
        set_modal(true)
        
        add(@label)
        
        # ファイルをDropしたときの設定
        # URIを取得するように設定
        Gtk::Drag.dest_set(self,
                           Gtk::Drag::DEST_DEFAULT_MOTION | 
                           Gtk::Drag::DEST_DEFAULT_HIGHLIGHT | 
                           Gtk::Drag::DEST_DEFAULT_DROP,
                           [["text/uri-list", Gtk::Drag::TARGET_OTHER_APP, 1234]],
                           Gdk::DragContext::ACTION_COPY | 
                           Gdk::DragContext::ACTION_MOVE)
        
        signal_connect("drag-data-received") { |widget, context, x, y, data, info, time|
            #18006対応 noda
            if $exclusion_flag == 1
                #連続でDROPされた時は何もしない
                puts "[debug] 連続drop" #debug
            else
                @logobj.write("PI_Window signal_connect(\"drag-data-received\") Start\n", 0, File.basename(__FILE__), __LINE__)
                
                $exclusion_flag = 1
                
                #self._init
                @@pilog_analyze_obj = PIlog_Analyze_Cls.new(@label)
                
                #@logobj.write(@pi_excelData.values, 0, File.basename(__FILE__), __LINE__)
                # DROPされたファイルのファイル名取得
                data.uris.each { |uri|
                    @dropuri = URI.decode(uri)
                    @dropfile = File.basename(@dropuri)
                }
                
                droppass = @dropuri.gsub("file:///", "")
                
                #18006対応 noda
                @excelset = Thread.new() {
                    if ! (@dropfile =~ /\.tar.gz$/)
                        popup_msg =  sprintf("解析失敗\n")
                        popup_msg << sprintf("Drag&Dropされたファイルがtar.gzファイルではありません。\n")
                        popup_msg << sprintf("tar.gzファイルをDrag&Dropしてください。\n")
                        wsh = WIN32OLE.new('WScript.Shell')
                        wsh.Popup(popup_msg, 0, "異常通知")
                    elsif  File.file?(droppass)
                        # パスも含めたファイル名が長い場合、ファイルが正常に取得できないケースがあるため
                        # ここでチェックする。
                        book = @@pilog_analyze_obj.createExcelWorkBook(droppass)
                        
                        if book != nil
                            begin
                              @@pilog_analyze_obj.excelset(droppass, book)
                            # 異常終了した場合 Excelをクローズ
                            ensure
                              @@pilog_analyze_obj.closeExcelWorkbook(book)
                              @label.text = "TECHINFO.tar.gzをdrop"
                            end
                        else
                            puts "book = nil"
                            @label.text = "TECHINFO.tar.gzをdrop"
                        end
                    else
                        popup_msg =  sprintf("解析失敗\n")
                        popup_msg << sprintf("フルパスも含めた、TECHINFO名が長すぎるため解析に失敗しました。\n")
                        popup_msg << sprintf("ファイル名を変更するか、TECHINFO格納場所の階層を浅くしてください。\n")
                        wsh = WIN32OLE.new('WScript.Shell')
                        wsh.Popup(popup_msg, 0, "異常通知")
                        
                        @label.text = "TECHINFO.tar.gzをdrop"
                    end
                    $exclusion_flag = 0
                }
                @logobj.write("PI_Window signal_connect(\"drag-data-received\") End\n", 0, File.basename(__FILE__), __LINE__)
            end #if ! (@dropfile =~ /\.tar.gz$/)
        } #signal_connect("drag-data-received")
        
        # ×ボタンなど
        signal_connect("destroy") { 
          #17920対応 noda
          if @excelset != nil then
            Thread.kill(@excelset)
          end
          Gtk.main_quit
        }
        show_all
        @logobj.write("PI_Window::initialize End\n", 0, File.basename(__FILE__), __LINE__)
    end #initialize
end #PI_Window

#----------------------------------------------------------#
# 1finity tool GUI Window
#----------------------------------------------------------#
class One_fiity_Window < Gtk::Window
    
    def initialize
        @logobj = Log_class.instance
        @logobj.write("One_fiity_Window::initialize Start\n", 0, File.basename(__FILE__), __LINE__)
        $exclusion_flag = 0
        super("1fiity_tool "  + $log_analyeze_version)
        set_default_size(360, 250)
        
        @label  = Gtk::Label.new("TECHINFO.tar.gzをdrop")
        
        # 本Window以外アクセス禁止
        set_modal(true)
        
        add(@label)
        
        # ファイルをDropしたときの設定
        # URIを取得するように設定
        Gtk::Drag.dest_set(self,
                           Gtk::Drag::DEST_DEFAULT_MOTION | 
                           Gtk::Drag::DEST_DEFAULT_HIGHLIGHT | 
                           Gtk::Drag::DEST_DEFAULT_DROP,
                           [["text/uri-list", Gtk::Drag::TARGET_OTHER_APP, 1234]],
                           Gdk::DragContext::ACTION_COPY | 
                           Gdk::DragContext::ACTION_MOVE)
        
        signal_connect("drag-data-received") { |widget, context, x, y, data, info, time|
            #18006対応 noda
            if $exclusion_flag == 1
                #連続でDROPされた時は何もしない
                puts "[debug] 連続drop" #debug
            else
                @logobj.write("One_fiity_Window signal_connect(\"drag-data-received\") Start\n", 0, File.basename(__FILE__), __LINE__)
                
                $exclusion_flag = 1
                
                #self._init
                @@one_finity_tool_obj = One_finity_tool_Cls.new(@label)
                
                #@logobj.write(@pi_excelData.values, 0, File.basename(__FILE__), __LINE__)
                # DROPされたファイルのファイル名取得
                data.uris.each { |uri|
                    @dropuri = URI.decode(uri)
                    @dropfile = File.basename(@dropuri)
                }
                
                droppass = @dropuri.gsub("file:///", "")
                
                #18006対応 noda
                @excelset = Thread.new() {
                    if ! (@dropfile =~ /\.tar.gz$/)
                        popup_msg =  sprintf("起動失敗\n")
                        popup_msg << sprintf("Drag&Dropされたファイルがtar.gzファイルではありません。\n")
                        popup_msg << sprintf("tar.gzファイルをDrag&Dropしてください。\n")
                        wsh = WIN32OLE.new('WScript.Shell')
                        wsh.Popup(popup_msg, 0, "異常通知")
                    elsif  File.file?(droppass)
                        # パスも含めたファイル名が長い場合、ファイルが正常に取得できないケースがあるため
                        # ここでチェックする。
                        @@one_finity_tool_obj.Excute(droppass)
                        
                    else
                        popup_msg =  sprintf("起動失敗\n")
                        popup_msg << sprintf("フルパスも含めた、TECHINFO名が長すぎるため解析に失敗しました。\n")
                        popup_msg << sprintf("ファイル名を変更するか、TECHINFO格納場所の階層を浅くしてください。\n")
                        wsh = WIN32OLE.new('WScript.Shell')
                        wsh.Popup(popup_msg, 0, "異常通知")
                        
                        @label.text = "TECHINFO.tar.gzをdrop"
                    end
                    $exclusion_flag = 0
                }
                @logobj.write("One_fiity_Window signal_connect(\"drag-data-received\") End\n", 0, File.basename(__FILE__), __LINE__)
            end #if ! (@dropfile =~ /\.tar.gz$/)
        } #signal_connect("drag-data-received")
        
        # ×ボタンなど
        signal_connect("destroy") { 
          #17920対応 noda
          if @excelset != nil then
            Thread.kill(@excelset)
          end
          Gtk.main_quit
        }
        show_all
        @logobj.write("One_fiity_Window::initialize End\n", 0, File.basename(__FILE__), __LINE__)
    end #initialize
end #One_fiity_Window


#----------------------------------------------------------#
# 切り替えログ解析 GUI Window
#----------------------------------------------------------#
class SDH_APS_Window < Gtk::Window
  
    def initialize
        @logobj = Log_class.instance
        @logobj.write("SDH_APS_Window::initialize Start\n", 0, File.basename(__FILE__), __LINE__)
        $exclusion_flag = 0
        super("切り替えログ解析 "  + $log_analyeze_version)
        set_default_size(360, 250)
        
        @label  = Gtk::Label.new("TECHINFO.tar.gzをdrop")
        
        vbox = Gtk::VBox.new(false, 0)
        
        dmylbl_array1 = []
        for i in 0..5 do
            dmylbl_array1 << Gtk::Label.new("")
        end
        
        dmylbl_array2 = []
        for i in 0..2 do
            dmylbl_array2 << Gtk::Label.new("")
        end
        
        # 体裁を整えるため空のラベルを挿入
        dmylbl_array1.each{|dmylbl|
            vbox.pack_start(dmylbl, false, true, 0)
        }
        
        $progress_value = 0
        prg_lbl  = Gtk::Label.new("")
        prg_lbl.text = "切り替えログ解析"
        
        prg  = Gtk::ProgressBar.new
        prg.text = "0%"
        prg.fraction = $progress_value
        
        vbox.pack_start(@label, false, true, 0)
        
        dmylbl_array2.each{|dmylbl|
            vbox.pack_start(dmylbl, false, true, 0)
        }
        
        vbox.pack_start(prg_lbl, false, true, 0)
        vbox.pack_start(prg, false, true, 0)
        
        # 本Window以外アクセス禁止
        set_modal(true)
        
        add(vbox)
        
        # ファイルをDropしたときの設定
        # URIを取得するように設定
        Gtk::Drag.dest_set(self,
                           Gtk::Drag::DEST_DEFAULT_MOTION | 
                             Gtk::Drag::DEST_DEFAULT_HIGHLIGHT | 
                             Gtk::Drag::DEST_DEFAULT_DROP,
                           [["text/uri-list", Gtk::Drag::TARGET_OTHER_APP, 1234]],
                           Gdk::DragContext::ACTION_COPY | 
                             Gdk::DragContext::ACTION_MOVE)
        
        signal_connect("drag-data-received") { |widget, context, x, y, data, info, time|
            #18006対応 noda
            if $exclusion_flag == 1
                #連続でDROPされた時は何もしない
                puts "[debug] 連続drop" #debug
            else
                @logobj.write("SDH_APS_Window signal_connect(\"drag-data-received\") Start\n", 0, File.basename(__FILE__), __LINE__)
                $exclusion_flag = 1
                #self._init
                @@sdh_asp_analyze_obj = SDH_ASP_Analyze_Cls.new(@label)
                
                # DROPされたファイルのファイル名取得
                data.uris.each { |uri|
                    @dropuri = URI.decode(uri)
                    @dropfile = File.basename(@dropuri)
                }
                
                droppass = @dropuri.gsub("file:///", "")
                
                #警報変化点LOG解析進捗率表示
                @progress = Thread.new(prg, prg_lbl) { |prg, prg_lbl|
                    before_value = $progress_value
                    loop {
                        if before_value != $progress_value
                            before_value = $progress_value
                            prg.fraction = $progress_value
                            prg.text = "#{(prg.fraction * 100).round}%"
                            if prg.fraction >= 1.0 then
                                prg_lbl.text = sprintf("%d/%d %s解析完了", $progress_log_no, $progress_max_log_no, $progress_funcname)
#                                break
                            else
                                prg_lbl.text = sprintf("%d/%d %s解析中", $progress_log_no, $progress_max_log_no, $progress_funcname)
                            end
                        end
                        sleep 0.2
                    }
                }
                
                @analyze = Thread.new() {
                    if ! (@dropfile =~ /\.tar.gz$/)
                        popup_msg =  sprintf("解析失敗\n")
                        popup_msg << sprintf("Drag&Dropされたファイルがtar.gzファイルではありません。\n")
                        popup_msg << sprintf("tar.gzファイルをDrag&Dropしてください。\n")
                        wsh = WIN32OLE.new('WScript.Shell')
                        wsh.Popup(popup_msg, 0, "異常通知")
                    elsif  File.file?(droppass)
                    # パスも含めたファイル名が長い場合、ファイルが正常に取得できないケースがあるため
                    # ここでチェックする。
                        book = @@sdh_asp_analyze_obj.createExcelWorkBook(droppass)
                        puts book #debug
                        
                        if book != nil
                            begin
                                ret = @@sdh_asp_analyze_obj.analyze(droppass, book)
                            
                            # 全ての処理後
                            ensure
                                # 異常応答の場合 Excelを
                                if ret
                                    @@sdh_asp_analyze_obj.dispExcelWorkbook(book)
                                else
                                    @@sdh_asp_analyze_obj.closeExcelWorkbook(book)
                                end
                                @label.text = "TECHINFO.tar.gzをdrop"
                            end
                        else
                            puts "book = nil"
                            @label.text = "TECHINFO.tar.gzをdrop"
                        end
                        prg_lbl.text = "切り替えログ解析"
                        prg.text = "0%"
                        prg.fraction = 0
                    else
                        popup_msg =  sprintf("解析失敗\n")
                        popup_msg << sprintf("フルパスも含めた、TECHINFO名が長すぎるため解析に失敗しました。\n")
                        popup_msg << sprintf("ファイル名を変更するか、TECHINFO格納場所の階層を浅くしてください。\n")
                        wsh = WIN32OLE.new('WScript.Shell')
                        wsh.Popup(popup_msg, 0, "異常通知")
                        
                        @label.text = "TECHINFO.tar.gzをdropp"
                    end
                    puts "root 1"
                    $exclusion_flag = 0
                }
                @logobj.write("SDH_APS_Window signal_connect(\"drag-data-received\") End\n", 0, File.basename(__FILE__), __LINE__)
            end
        }
        
        # ×ボタンなど
        signal_connect("destroy") { 
            #17920対応 noda
            if @analyze != nil then
                Thread.kill(@analyze)
            end
            Gtk.main_quit
        }
        show_all
        @logobj.write("SDH_APS_Window::initialize End\n", 0, File.basename(__FILE__), __LINE__)
    end
end

=begin
class AC_define_Window < Gtk::Window
  
  # クラス変数
  @@ac_define_obj = nil

  def initialize
    # 現実行ディレクトリパスを取得
    @logobj = Log_class.instance
    @logobj.write("AC_define_Window initialize Start\n")

    puts "ActionCode定義データ生成"
    super("ActionCode定義データ生成 " + $log_analyeze_version)
    set_default_size(500, 300)
    vbox = Gtk::VBox.new(false, 5)
    buttonvbox = Gtk::VBox.new(false, 5)
    buttonhbox = Gtk::HBox.new(false, 0)
    framehbox = Gtk::HBox.new(false, 0)

    # 体裁を整えるため空のラベルを挿入
    dmylabel1 = Gtk::Label.new("")
    vbox.pack_start(dmylabel1, false, true, 0)

    # 選択ボタンの位置設定
    select_button = Gtk::Button.new("  選択  ")
    select_button.set_size_request(50, 30)
    dmylabel2 = Gtk::Label.new("")
    dmylabel3 = Gtk::Label.new("")
    dmylabel2.set_size_request(50, 20)
    dmylabel3.set_size_request(50, 30)
    buttonvbox.pack_start(dmylabel2, false, true, 0)
    buttonvbox.pack_start(select_button, false, true, 0)
    buttonvbox.pack_start(dmylabel3, false, true, 0)

    # ActionCode仕様書/includeファイルフレームの位置設定
    frame = Gtk::Frame.new
    frame.label = "ActionCode仕様書/includeファイル"
    label1  = Gtk::Label.new("")
    label1.set_size_request(430, 80)
    label1.wrap = true
    label1justify = Gtk::JUSTIFY_LEFT
    frame.add(label1)
    framehbox.pack_start(frame, false, true, 0)
    framehbox.pack_start(buttonvbox, true, false, 0)
    vbox.pack_start(framehbox, false, true, 0)
    
    # 体裁を整えるため空のラベルを挿入
    dmylbl_array1 = []
    for i in 0..1 do
      dmylbl_array1 << Gtk::Label.new("")
    end
    dmylbl_array1.each{|dmylbl|
      vbox.pack_start(dmylbl, false, true, 0)
    }

    label2  = Gtk::Label.new("")
    label2.wrap = true
    vbox.pack_start(label2, false, true, 0)

    # 体裁を整えるため空のラベルを挿入
    dmylbl_array2 = []
    for i in 0..1 do
      dmylbl_array1 << Gtk::Label.new("")
    end
    dmylbl_array2.each{|dmylbl|
      vbox.pack_start(dmylbl, false, true, 0)
    }

    # ActionCode定義データ生成ボタンの位置設定
    make_button = Gtk::Button.new("     ActionCode定義データ生成     ")
    buttonhbox.pack_start(make_button, true, false, 0)
    vbox.pack_start(buttonhbox, true, false, 0)

    # 呼び出し元Windowをアクセス禁止にする
    set_modal(true)

    add(vbox)

    select_button.signal_connect("clicked") do |b|
      @logobj.write("AC_define_Window select_button.signal_connect(\"clicked\") Start\n")

      dir = FolderSelectDialog("フォルダを指定してください\nPASS指定もできますが、ツリー画面には反映されません")
      if dir != nil
        label1.text = dir.gsub(/\\/, '/')
      end

      @logobj.write("AC_define_Window select_button.signal_connect(\"clicked\") End\n")
    end

    make_button.signal_connect("clicked") do |b|
      @logobj.write("AC_define_Window make_button.signal_connect(\"clicked\") Start\n")
      wsh = WIN32OLE.new('WScript.Shell')

      result,pass =  file_check(label1.text.encode(Encoding::Windows_31J), @logobj)
      if result == true
        label2.text = "ディレクトリ配下読み込み中"
        # ボタンを非アクティブにする
        select_button.sensitive = false
        make_button.sensitive = false

        @ac_define = Thread.new() {
          begin
            #ログオブジェクト作成
            @aclogobj = AC_log_class.new(pass)
            #★TECHINFO_ACDefineDataGenarateの処理メソッドを呼ぶ
            #★引数はlabel1.text
            acDefineDataMakeObj = AC_DefineData_Make_Cls.new(label2, @aclogobj)
            ret = acDefineDataMakeObj.readDirectory(label1.text.encode(Encoding::Windows_31J))
            if ret
              label2.text = "テーブル出力終了"
              wsh.Popup("定義データの作成に成功しました",0,"正常終了")
            else
              label2.text = "テーブル出力失敗"
            end

          rescue => e
            printf("AC_define_Window error\n")
            @logobj.write(sprintf("[ERROR]%s\n", $!), 0, File.basename(__FILE__), __LINE__)
            p $!
            e.backtrace.each{|errdata|
              @logobj.write(sprintf("[ERROR]%s\n", errdata), 0, File.basename(__FILE__), __LINE__)
              p errdata
            }
            label2.text = "テーブル出力失敗"
            wsh.Popup("定義データの作成に失敗しました",0,"Error")

          ensure
            # ボタンをアクティブにする
            select_button.sensitive = true
            make_button.sensitive = true
            # ACログファイルをクローズ
            @aclogobj.file_close

          end
        }
      end

      @logobj.write("AC_define_Window make_button.signal_connect(\"clicked\") End\n")
    end
    

    # ×ボタンなど
    signal_connect("destroy") { 
      if @ac_define != nil then
        Thread.kill(@ac_define)
      end
      Gtk.main_quit
    }
    show_all
    @logobj.write("AC_define_Window initialize End\n")

  end
end

#----------------------------------------------------------#
# FolderSelectDialog
#----------------------------------------------------------#
def FolderSelectDialog(dialog_str = "フォルダを選択してください", target_path="")
  app =  WIN32OLE.new('Shell.Application')
  puts target_path
  path = app.BrowseForFolder(0, dialog_str, 0x11, target_path)
  if path
    ret_path = path.Items.Item.path
  else
    ret_path = nil
  end
  return ret_path
end

#18275対応 noda
class AC_Window < Gtk::Window
  
  # クラス変数
  @@ac_analyze_obj = nil

  def initialize
    @logobj = Log_class.instance
    @logobj.write("AC_Window initialize Start\n")
    $exclusion_flag = 0

    puts "ActionCode解析"
    super("ActionCode解析 " + $log_analyeze_version)
    set_default_size(450, 530)
    vbox = Gtk::VBox.new(false, 5)
    comboBox1hbox = Gtk::HBox.new(false, 0)
    comboBox2hbox = Gtk::HBox.new(false, 0)
    starttimehbox = Gtk::HBox.new(false, 0)
    endtimehbox = Gtk::HBox.new(false, 0)

    # 体裁を整えるため空のラベルを挿入
    dmylabel1 = Gtk::Label.new("")
    vbox.pack_start(dmylabel1, false, true, 0)

    # 解析対象ActionCodeの位置設定
    label1 = Gtk::Label.new("■解析対象ActionCode")
    label1.set_alignment(0, 0)
    comboBox1 = Gtk::ComboBox.new() 
    comboBox1.set_size_request(350, 25)
    checkbox1 = Gtk::CheckButton.new("要求 (IPPシグナル番号:0x60e00000)")
    checkbox1.active = true
    checkbox2 = Gtk::CheckButton.new("応答 (IPPシグナル番号:0x62e00000)")
    checkbox2.active = true
    checkbox3 = Gtk::CheckButton.new("通知 (IPPシグナル番号:0x60e00001)")
    comboBox1hbox.pack_start(comboBox1, false, true, 0)
    vbox.pack_start(label1, false, true, 0)
    vbox.pack_start(comboBox1hbox, false, true, 0)
    vbox.pack_start(checkbox1, false, true, 0)
    vbox.pack_start(checkbox2, false, true, 0)
    vbox.pack_start(checkbox3, false, true, 0)

    # AC解析用テーブル格納ディレクトリが存在するかチェック
    if (!FileTest.exist?($D_ACTBL_PATH))
      # 無ければダイアログを表示して終了
      msg = "ActionCode解析用テーブル格納ディレクトリ(ac_tbl)が存在しません。\n" +
            "定義データ生成ツールを使用して作成してください。"
      messageBox msg, "ERROR", 0
      return
    else
      # AC version のプルダウンメニューのリストを
      # ",/ac_tbl" ディレクトリ配下のディレクトリ名とする
      Dir::foreach($D_ACTBL_PATH).each do |list|       
        if FileTest.directory?($D_ACTBL_PATH+"/"+list)
          # "." と ".." は無視する。
          if (list == "." || list == "..")
            next
          end
          # プルダウンメニューに追加
          comboBox1.append_text(list)
        end
      end
      # プルダウンメニューのデフォルトを0番目に設定。
      comboBox1.set_active(0)
    end

    # Endian変換モードの位置設定
    label2 = Gtk::Label.new("■Endian変換モード")
    label2.set_alignment(0, 0)
    comboBox2 = Gtk::ComboBox.new() 
    comboBox2.set_size_request(350, 25)
    comboBox2.append_text("変換しない（実機ログ）")
    comboBox2.append_text("変換する（シミュレーションログ）")
    comboBox2.set_active(0)
    comboBox2hbox.pack_start(comboBox2, false, true, 0)
    vbox.pack_start(label2, false, true, 0)
    vbox.pack_start(comboBox2hbox, false, true, 0)

    checkbox4 = Gtk::CheckButton.new("ACのpayloadサイズが0x300を超えた場合、そのACの解析をSkip")
    checkbox4.active = true
    label7  = Gtk::Label.new("     ※ payloadサイズ限界までAC解析したい場合はチェックを外してください")
    label7.set_alignment(0, 0)
    checkbox5 = Gtk::CheckButton.new("解析ログを出力する")
    checkbox5.active = true
    checkbox6 = Gtk::CheckButton.new("解析対象期間を指定する")
    label3 = Gtk::Label.new("      ※ 2013/08/01 12:34:56 -> 20130801123456 と指定")
    label3.set_alignment(0, 0)
    vbox.pack_start(checkbox4, false, true, 0)
    vbox.pack_start(label7, false, true, 0)
    vbox.pack_start(checkbox5, false, true, 0)
    vbox.pack_start(checkbox6, false, true, 0)
    vbox.pack_start(label3, false, true, 0)

    # 時刻設定テキストの位置設定
    label4 = Gtk::Label.new("         解析開始時刻    ")
    entry1 = Gtk::Entry.new
    entry1.set_size_request(200, 25)
    starttimehbox.pack_start(label4, false, true, 0)
    starttimehbox.pack_start(entry1, false, true, 0)
    label5 = Gtk::Label.new("         解析終了時刻    ")
    entry2 = Gtk::Entry.new
    entry2.set_size_request(200, 25)
    endtimehbox.pack_start(label5, false, true, 0)
    endtimehbox.pack_start(entry2, false, true, 0)
    vbox.pack_start(starttimehbox, false, true, 0)
    vbox.pack_start(endtimehbox, false, true, 0)
    
    # 体裁を整えるため空のラベルを挿入
    dmylbl_array1 = []
    for i in 0..1 do
      dmylbl_array1 << Gtk::Label.new("")
    end
    dmylbl_array1.each{|dmylbl|
      vbox.pack_start(dmylbl, false, true, 0)
    }

    label6  = Gtk::Label.new("TECHINFO.tar.gzをdrop")
    vbox.pack_start(label6, false, true, 0)
    prog_label = Gtk::Label.new("")
    vbox.pack_start(prog_label, false, true, 0)

    # 呼び出し元Windowをアクセス禁止にする
    set_modal(true)

    add(vbox)

    # ファイルをDropしたときの設定
    # URIを取得するように設定
    Gtk::Drag.dest_set(self,
                       Gtk::Drag::DEST_DEFAULT_MOTION | 
                         Gtk::Drag::DEST_DEFAULT_HIGHLIGHT | 
                         Gtk::Drag::DEST_DEFAULT_DROP,
                       [["text/uri-list", Gtk::Drag::TARGET_OTHER_APP, 1234]],
                       Gdk::DragContext::ACTION_COPY | 
                         Gdk::DragContext::ACTION_MOVE)
    
    signal_connect("drag-data-received") { |widget, context, x, y, data, info, time|
      wsh = WIN32OLE.new('WScript.Shell')

      #18006対応 noda
      if $exclusion_flag == 1
        #連続でDROPされた時は何もしない
        puts "[debug] 連続drop" #debug
      else
        @logobj.write("AC_Window signal_connect(\"drag-data-received\") Start\n", 0, File.basename(__FILE__), __LINE__)
        $exclusion_flag = 1
        #self._init

        #グローバル変数に画面の設定値を反映
        #要求チェックボックス
        if checkbox1.active? then
          $chk_req = true
        else
          $chk_req = false
        end
        #応答チェックボックス
        if checkbox2.active? then
          $chk_rsp = true
        else
          $chk_rsp = false
        end
        #通知チェックボックス
        if checkbox3.active? then
          $chk_info = true
        else
          $chk_info = false
        end
        #payloadチェックボックス
        if checkbox4.active? then
          $payload = true
        else
          $payload = false
        end
        #ログ出力チェックボックス
        if checkbox5.active? then
          $chk_aclog = true
        else
          $chk_aclog = false
        end
        #解析対象期間チェックボックス
        if checkbox6.active? then
          $chk_anatime = true
        else
          $chk_anatime = false
        end
        #解析対象プルダウン
        $ver_cmbx = comboBox1.active_text
        #エンディアンプルダウン
        $endian_cmbx = comboBox2.active_text
        #解析開始時刻テキスト
        $start_time = entry1.text
        #解析終了時刻テキスト
        $end_time = entry2.text

        label6.text = "解析開始"

        # チェックボックス等を非アクティブにする
        checkbox1.sensitive = false
        checkbox2.sensitive = false
        checkbox3.sensitive = false
        checkbox4.sensitive = false
        checkbox5.sensitive = false
        checkbox6.sensitive = false
        comboBox1.sensitive = false
        comboBox2.sensitive = false
        entry1.sensitive = false
        entry2.sensitive = false

        # DROPされたファイルのファイル名取得
        data.uris.each { |uri|
          @dropuri = URI.decode(uri)
          @dropfile = File.basename(@dropuri)
        }
        
        droppass = @dropuri.gsub("file:///", "")
        
        #18006対応 noda
        @ac_analyze = Thread.new() {
          if ! (@dropfile =~ /\.tar.gz$/)
            popup_msg =  sprintf("解析失敗\n")
            popup_msg << sprintf("Drag&Dropされたファイルがtar.gzファイルではありません。\n")
            popup_msg << sprintf("tar.gzファイルをDrag&Dropしてください。\n")
            wsh.Popup(popup_msg, 0, "異常通知")

          elsif  File.file?(droppass)

              begin
                # 文字コード変換
                $ver_cmbx = $ver_cmbx.encode(Encoding::Windows_31J)
                $endian_cmbx = $endian_cmbx.encode(Encoding::Windows_31J)
                $start_time = $start_time.encode(Encoding::Windows_31J)
                $end_time = $end_time.encode(Encoding::Windows_31J)

                #★TECHINFO_ACLog_Analysisの処理メソッドを呼ぶ
                #★引数はdroppass
                aclog_analyze_out_path = set_aclog_path(droppass, @logobj)
                @logobj.write(sprintf("aclog analyze out path = %s\n", aclog_analyze_out_path), 0, File.basename(__FILE__), __LINE__)

                # ACLog解析トレース用 ログオブジェクト作成
                aclogobj = AC_log_class.new(aclog_analyze_out_path)

                acLogAnalyzeObj = AC_log_Analyze_Cls.new(label6, prog_label, aclogobj, aclog_analyze_out_path)
                acLogAnalyzeObj.techinfo_parse(droppass)

              rescue => e
                printf("AC_Window error\n")
                @logobj.write(sprintf("[ERROR]%s\n", $!), 0, File.basename(__FILE__), __LINE__)
                p $!
                e.backtrace.each{|errdata|
                  @logobj.write(sprintf("[ERROR]%s\n", errdata), 0, File.basename(__FILE__), __LINE__)
                  p errdata
                }
                wsh.Popup("AC解析に失敗しました",0,"Error")

              ensure
                label6.text = "TECHINFO.tar.gzをdrop"
              end

          else
            popup_msg =  sprintf("解析失敗\n")
            popup_msg << sprintf("フルパスも含めた、TECHINFO名が長すぎるため解析に失敗しました。\n")
            popup_msg << sprintf("ファイル名を変更するか、TECHINFO格納場所の階層を浅くしてください。\n")
            wsh = WIN32OLE.new('WScript.Shell')
            wsh.Popup(popup_msg, 0, "異常通知")
            label6.text = "TECHINFO.tar.gzをdrop"
          end
          # チェックボックス等をアクティブにする
          checkbox1.sensitive = true
          checkbox2.sensitive = true
          checkbox3.sensitive = true
          checkbox4.sensitive = true
          checkbox5.sensitive = true
          checkbox6.sensitive = true
          comboBox1.sensitive = true
          comboBox2.sensitive = true
          entry1.sensitive = true
          entry2.sensitive = true

          $exclusion_flag = 0
        }
        @logobj.write("AC_Window signal_connect(\"drag-data-received\") End\n", 0, File.basename(__FILE__), __LINE__)
      end
    }
    
    # ×ボタンなど
    signal_connect("destroy") { 
      if @ac_analyze != nil then
        Thread.kill(@ac_analyze)
      end
      Gtk.main_quit
    }
    show_all
    @logobj.write("AC_Window initialize End\n")

  end
end
=end
# ----------------------------------------------------------
# Main処理
# ----------------------------------------------------------
def mainWindow_create()

window = Gtk::Window.new("1finity S100 TECHINFO解析ツール " + $log_analyeze_version)
window.set_default_size(400,250)
window.border_width = 15

# window.set_window_position(Gtk::Window::POS_MOUSE)
window.signal_connect("delete_event") {
    @logobj.write("Main delete_event\n")
    @logobj.write("Main End\n")
    Gtk::main_quit
}
window.signal_connect("destroy") {
    @logobj.write("Main destroy\n")
    @logobj.write("Main End\n")
    Gtk::main_quit
}

box = Gtk::VBox.new(false, 0)
window.add(box)

# ----------------------------
# 警報ログ解析ボタン
# ----------------------------
button = Gtk::Button.new("警報ログ解析")
button.set_relief(Gtk::RELIEF_HALF)
button.signal_connect("clicked") do
    @logobj.write("Main button clicked [警報ログ解析]\n")
    w = Alm_log_Window.new
    Gtk.main
end
box.pack_start(button, false, false, 5)

# ----------------------------
# alm_dump解析ボタン
# ----------------------------
button = Gtk::Button.new("alm_dump解析", false)
box.pack_start(button, false, false, 5)
button.signal_connect("clicked") do
    @logobj.write("Main button clicked [alm_dump解析]\n")
    w = Alm_dump_Window.new
    Gtk.main
end

# ----------------------------
# PI解析ボタン
# ----------------------------
button = Gtk::Button.new("PI解析", false)
box.pack_start(button, false, false, 5)
button.signal_connect("clicked") do
    @logobj.write("Main button clicked [PI解析]\n")
    w = PI_Window.new
    Gtk.main
end

# ----------------------------
# 切り替えログ解析ボタン
# ----------------------------
button = Gtk::Button.new("切り替えログ解析", false)
box.pack_start(button, false, false, 5)
button.signal_connect("clicked") do
  @logobj.write("Main button clicked [切り替えログ解析]\n")
  w = SDH_APS_Window.new
  Gtk.main
end

=begin
# -------------------------------
# Action Code定義データ生成ボタン
# -------------------------------
button = Gtk::Button.new("Action Code定義データ生成", false)
box.pack_start(button, false, false, 5)
button.signal_connect("clicked") do
  @logobj.write("Main button clicked [Action Code定義データ生成]\n")
  w = AC_define_Window.new
  Gtk.main
end
=end

# -------------------------------
# Action Code解析ボタン
# -------------------------------
button = Gtk::Button.new("Action Code解析(tool単独起動)", false)
box.pack_start(button, false, false, 5)
button.signal_connect("clicked") do
    @logobj.write("Main button clicked [Action Code解析]\n")
    
    shell = WIN32OLE.new("WScript.Shell")
    
    #実行dirに移動
    Dir::chdir($D_TOOL_PATH)
    
    #programを実行
    shell.Run("1finity_tool.exe")
    
    #元のDirに戻る
    Dir::chdir($D_RUN_FULL_PATH)
    
end

# -------------------------------
# Action Code解析ボタン
# -------------------------------
button = Gtk::Button.new("Action Code解析(tool+log)", false)
box.pack_start(button, false, false, 5)
button.signal_connect("clicked") do
    @logobj.write("Main button clicked [Action Code解析(tool+log)]\n")
    
    w = One_fiity_Window.new
    Gtk.main
    
end

# ----------------------------
# INPUT HS仕様書ボタン
# ----------------------------
button = Gtk::Button.new("FLT Table版数")
box.pack_start(button, false, false, 5)
button.signal_connect("clicked") do
    @logobj.write("Main button clicked [FLT Table版数]\n")
    
    flt_table = TECHINFO_Analysis_Common::flt_table_name_get()
    if flt_table.nil?
        next
    end
    wsh = WIN32OLE.new('WScript.Shell')
    popup_msg = flt_table + "\n"
    wsh.Popup(popup_msg, 0, "FLT Table版数")
    
end

window.show_all
end #mainWindow_create

@logobj = Log_class.instance
@logobj.write("Main Start\n")

print "[debug] ARGV: ", ARGV, "\n"

if ARGV.empty?
    input_path = ""
else
    input_path = TECHINFO_Analysis_CUI.arg_parse(ARGV)
end

if input_path != ""
    $exec_CUI_mode = true
    
    puts "*** CUI Start. "
    puts "* Input=#{input_path}"
    rcode = TECHINFO_Analysis_CUI.Alm_log_analyze(input_path)
    puts "*** CUI End"
    exit(rcode)
end

mainWindow_create()

Gtk.main

