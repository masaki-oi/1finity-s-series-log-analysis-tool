Attribute VB_Name = "MyLib"
'-------------------------------------------------------------
' All Rights Reserved, Copyright (C) FUJITSU LIMITED 2017
'
'   汎用ルーチン
'
'   更新日: 2017年 3月 9日
'
'-------------------------------------------------------------
Option Private Module
Option Explicit

Public Function MyFullPathGet(ByVal strPath As String) As String
    Dim objFSO As Object
    
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    If objFSO.GetDriveName(strPath) = "" Then   '相対パスの場合
        MyFullPathGet = objFSO.GetAbsolutePathName(ThisWorkbook.Path & "\" & strPath)
            '注意）GetAbsolutePathNameは、指定したファイル名と実在するファイル名に
            '      大文字小文字の違いがある場合、実在するファイル名を返す。
    Else
        MyFullPathGet = strPath
    End If
    Set objFSO = Nothing
    
End Function

Public Function MyFileNameGet(ByVal strPath As String) As String
    Dim objFSO  As Object
    
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    MyFileNameGet = objFSO.GetFileName(strPath)
    Set objFSO = Nothing

End Function

Public Function MyFolderNameGet(ByVal strPath As String) As String
    Dim objFSO  As Object
    
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    MyFolderNameGet = objFSO.GetParentFolderName(strPath)
    Set objFSO = Nothing

End Function

Public Function MyExtensionName(ByVal strPath As String) As String
    Dim objFSO  As Object
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    MyExtensionName = objFSO.GetExtensionName(strPath)
    Set objFSO = Nothing
End Function

Public Function MyFileType(ByVal strExistPath As String) As String
    Dim objFSO  As Object
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    MyFileType = objFSO.GetFile(strExistPath).Type
    Set objFSO = Nothing
End Function

Public Function MyFileDateGet( _
        ByVal strFullPath As String, _
        ByRef dtLastModified As Date) As Boolean
    Dim objFSO  As Object
    
    On Error Resume Next
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    dtLastModified = objFSO.GetFile(strFullPath).DateLastModified
    If Err.Number = 0 Then
        MyFileDateGet = True
    Else
        MyFileDateGet = False
    End If
    Set objFSO = Nothing
    
End Function

' 注意)
'・Dir(フォルダ名)は、存在していても偽を返す。
'・Dir(〜,vbDirectory)は、ファイルでも真を返す。
'・Dir と GetAttr は、パス名が不正文字を含むとエラーになる
'・GetAttrは、存在しないパス名でエラーになる

Public Function IsExistPath(ByVal strFullPath As String) As Boolean
    IsExistPath = False
    
    On Error GoTo Error_Exit
    If GetAttr(strFullPath) Then
        IsExistPath = True
    End If
    Exit Function
    
Error_Exit:
End Function

Public Function IsFolderName(ByVal strFullPath As String) As Boolean
    IsFolderName = False
    
    If IsExistPath(strFullPath) = False Then
        Exit Function
    ElseIf GetAttr(strFullPath) And vbDirectory Then
        IsFolderName = True
    End If
    
End Function

Public Function IsEnablePath( _
        strFullPath As String, _
        Optional ByVal Writable As Boolean = False) As Boolean
        
    IsEnablePath = False
    
    If IsExistPath(strFullPath) = False Then
        Exit Function
    ElseIf GetAttr(strFullPath) And (vbHidden + vbSystem) Then
        Exit Function
    ElseIf Writable Then
        If GetAttr(strFullPath) And vbReadOnly Then
            Exit Function
        End If
    End If

    IsEnablePath = True
End Function

Sub MyRangeSelect(objRange As Range)
    '注）WorkSheetがActiveでないとCellのSelectが実行エラーになる
    Call MySheetActivate(objRange.Worksheet)
    objRange.Select
End Sub

Sub MySheetActivate(wsTarget As Worksheet)
    '注）BookがActiveでないとWorkSheetのActivateが実行エラーになる
    wsTarget.Parent.Activate
    wsTarget.Select
End Sub

Sub MyScreenUpdate()
    With Application
        If .ScreenUpdating = False Then
            .ScreenUpdating = True
            DoEvents
            .ScreenUpdating = False
        End If
    End With
End Sub

' テーブル取得
Public _
Function GetTableByName( _
        ByVal テーブル名 As String, _
        wsTarget As Worksheet, _
        Optional ByVal NoMsg As Boolean = False) As ListObject
        
    Dim objTmp As ListObject
    Dim objFound As ListObject
    
    Set objFound = Nothing
    For Each objTmp In wsTarget.ListObjects
        If objTmp.Name = テーブル名 Then
            Set objFound = objTmp
            Exit For
        End If
    Next objTmp
    
    If (objFound Is Nothing) And (NoMsg = False) Then
        Call MsgBox(wsTarget.Name & "シートに「" & テーブル名 & "」というテーブルがありません。" _
            , vbCritical)
    End If
    
    Set GetTableByName = objFound
End Function

'=============================================================
'
'    ワークブックの処理
'
'=============================================================
Public _
Function MyBookNameGet(ByVal strPath As String) As String
    Dim strTmp As String
    
    strTmp = MyFileNameGet(strPath)
    
    strTmp = Replace(strTmp, "[", "(")
    strTmp = Replace(strTmp, "]", ")")
    '注）ブック名(WorkBook.Name)の "["または"]"は、Excel 内では "(",")" に変換されている。
    '     WorkBook.Path 及び、WorkBook.FullName では、変換されない。
    
    MyBookNameGet = strTmp
End Function

Public _
Function MyRefBookOpen(ByVal strPath As String) As Workbook
        
    Dim wb As Workbook
    
    Set MyRefBookOpen = Nothing
    
    If MyOpenBookCheck(wb, strPath, ReuseEnable:=True) = False Then
        Exit Function
    ElseIf wb Is Nothing Then
        Set wb = MyBookOpen(strPath, ReadOnly:=True)
    End If
    
    Set MyRefBookOpen = wb
End Function
    
' 指定されたブックがオープンされていれば、取得。
'   ReuseEnable : 取得できた場合にエラーメッセージを表示しない。
'   ReadOnly    : 取得したブックの ReadOnly属性と一致しない場合、エラーメッセージを表示。
Public _
Function MyOpenBookCheck( _
            ByRef wbResult As Workbook, _
            ByVal strPath As String, _
            ByVal ReuseEnable As Boolean, _
            Optional ReadOnly As Variant _
        ) As Boolean
    
    Dim strFullPath As String
    Dim strBookName As String
    Dim objBook As Workbook
    Dim strMsg As String
    
    MyOpenBookCheck = False
    Set wbResult = Nothing
    
    strBookName = MyBookNameGet(strPath)
    
    '既にオープンされているか調べる
    For Each objBook In Workbooks
        If UCase(objBook.Name) = UCase(strBookName) Then
            strFullPath = MyFullPathGet(strPath)
            
            If objBook.FullName <> strFullPath Then    '注）同名の別ファイルかもしれないので完全パスで比較する。
                strMsg = "同じブック名の別ファイル" & vbCrLf _
                    & objBook.FullName & vbCrLf & vbCrLf _
                    & "が開いているため、 & vbCrLf" _
                    & strFullPath & vbCrLf _
                    & "が開けません" & vbCrLf & vbCrLf _
                    & "クローズして再実行して下さい"
                    
            ElseIf ReuseEnable = False Then
                strMsg = strFullPath & vbCrLf & vbCrLf _
                        & "がオープンされています。" & vbCrLf _
                        & "クローズして再実行して下さい"
                        
            ElseIf Not IsMissing(ReadOnly) Then
                If objBook.ReadOnly <> ReadOnly Then
                    strMsg = strFullPath & vbCrLf & vbCrLf _
                            & "がオープンされています。" & vbCrLf _
                            & "クローズして再実行して下さい"
                End If
            End If
            
            If strMsg <> "" Then
                Application.ScreenUpdating = True
                objBook.Activate
                Set objBook = Nothing
                
                Call MsgBox(strMsg, vbCritical)
                
                Exit Function 'ＮＧ
            End If
            
            Set wbResult = objBook
            Exit For ' ＯＫ
        End If
    Next objBook

    MyOpenBookCheck = True
    
End Function

Public _
Function MyBookOpen( _
            ByVal strPath As String, _
            ByVal ReadOnly As Boolean, _
            Optional ByVal UpdateLinks As Boolean = False _
        ) As Workbook
    
    Dim strFullPath As String
    Dim strBookName As String
    Dim objBook As Workbook
    
    Dim booSave As Boolean
    
    Set MyBookOpen = Nothing
        
    strFullPath = MyFullPathGet(strPath)
    strBookName = MyBookNameGet(strPath)
    
    On Error Resume Next
    booSave = Application.ScreenUpdating
    Application.ScreenUpdating = False
    Set objBook = Workbooks.Open(strFullPath, ReadOnly:=ReadOnly, UpdateLinks:=UpdateLinks)
    Application.ScreenUpdating = booSave
    If Err.Number <> 0 Then
        Call MsgBox("'" & strFullPath & "'" & vbCrLf _
            & "がオープンできません" & vbCrLf & vbCrLf _
            & Err.Description, vbExclamation)
            
        Err.Clear
        Exit Function
    End If
    On Error GoTo 0
        
    If objBook.FullName <> strFullPath Then    '注）Excelのバグ(？)対処
        Call MsgBox("'" & strFullPath & "'" & vbCrLf _
            & "の OpenでExcelに異常が発生しました。違うファイル" & vbCrLf _
            & objBook.FullName & vbCrLf _
            & "がOpenされました。" _
            , vbExclamation)
            
        Set objBook = Nothing
        End
    End If

    Set MyBookOpen = objBook
    
End Function

'=============================================================
'
'    ワークシートの処理
'
'=============================================================

'--------------------------------------------------------------
' シート名でワークシートを取り出す
'--------------------------------------------------------------
Public _
Function GetWorkSheetByName( _
            ByVal SearchName As String, _
            ByVal TargetBook As Workbook, _
            Optional ByVal NoMsg As Boolean = False _
        ) As Worksheet
    
    Dim objWS   As Worksheet
    For Each objWS In TargetBook.Worksheets
        If objWS.Name = SearchName Then
            Set GetWorkSheetByName = objWS
            Exit Function
        End If
    Next objWS
    
    If NoMsg = False Then
        Call MsgBox(TargetBook.Name & vbCrLf _
            & "に""" & SearchName & """シートがありません。", vbCritical)
    End If
    
    Set GetWorkSheetByName = Nothing
End Function

'--------------------------------------------------------------
' 保護されたシートをマクロから操作可能にする。
'--------------------------------------------------------------
Public Sub MyProtectReset(ws As Worksheet)
    Dim mySave1  As Protection
    Dim myflag1 As Boolean
    Dim myflag2 As Boolean
    Dim myflag3 As Boolean
    
    myflag1 = ws.ProtectDrawingObjects
    myflag2 = ws.ProtectContents
    myflag3 = ws.ProtectScenarios
    If (myflag1 Or myflag2 Or myflag3) And (ws.ProtectionMode = False) Then
        Set mySave1 = ws.Protection
        ws.Unprotect
        
        With mySave1
            ws.Protect UserInterfaceOnly:=True _
                , DrawingObjects:=myflag1, Contents:=myflag2, Scenarios:=myflag3 _
                , AllowDeletingColumns:=.AllowDeletingColumns _
                , AllowDeletingRows:=.AllowDeletingRows _
                , AllowFiltering:=.AllowFiltering _
                , AllowFormattingCells:=.AllowFormattingCells _
                , AllowFormattingColumns:=.AllowFormattingColumns _
                , AllowFormattingRows:=.AllowFormattingRows _
                , AllowInsertingColumns:=.AllowInsertingColumns _
                , AllowInsertingHyperlinks:=.AllowInsertingHyperlinks _
                , AllowInsertingRows:=.AllowInsertingRows _
                , AllowSorting:=.AllowSorting _
                , AllowUsingPivotTables:=.AllowUsingPivotTables
        End With
        Set mySave1 = Nothing
    End If
End Sub

'=============================================================
'
'    検索処理
'
'=============================================================

'--------------------------------------------------------------
' 指定列の最終データの行番号を返す ※開始セル以降の最初の空白値まで
'--------------------------------------------------------------
Function MyDataEndRowGet(StartCell As Range) As Variant
    
    Dim rngFound As Range
    Set rngFound = MyDataBottomCellGet(StartCell)

    If rngFound Is Nothing Then
        '有効データが無い場合
        MyDataEndRowGet = 0
    Else
        MyDataEndRowGet = rngFound.Row
    End If
End Function

Function MyDataBottomCellGet(StartCell As Range) As Range
    
    '開始セル以降の最初の空白値を検索
    Dim intMaxRow   As Variant
    Dim rngEnd As Range
    
    With StartCell.Worksheet.UsedRange
        intMaxRow = .Cells(.Cells.Count).Row
    End With
    
    Set rngEnd = StartCell
    Do Until IsSpaceString(rngEnd.Value)
        If intMaxRow <= rngEnd.Row Then
            Set MyDataBottomCellGet = rngEnd
            Exit Function
        End If
        Set rngEnd = rngEnd.Offset(1)
    Loop
    
    If rngEnd.Row = StartCell.Row Then
        '有効データが無い場合
        Set MyDataBottomCellGet = Nothing
    Else
        Set MyDataBottomCellGet = rngEnd.Offset(-1)
    End If
End Function

'--------------------------------------------------------------
' 指定列の最終データの行番号を返す ※最後の非空白値まで
'--------------------------------------------------------------
Function MyUsedMaxRowGet(rngTop As Range) As Variant
    
    Dim rngFound    As Range
    rngFound = MyUsedBottomCellGet(rngTop)
    
    If rngFound Is Nothing Then
        '有効データが無い場合
        MyUsedMaxRowGet = 0
    Else
        MyUsedMaxRowGet = rngFound.Row
    End
End Function

'指定列で使用されている最後のセルを取得
Private Function MyUsedBottomCellGet(ByVal rngTop As Range) As Range
    
    Dim intMinRow   As Variant
    Dim rngEnd As Range
    
    intMinRow = rngTop.Row
    
    With rngTop.Worksheet.UsedRange
        Set rngEnd = .Cells(.Rows.Count, rngTop.Column)
    End With
    Do While IsSpaceString(rngEnd.Value)
        If rngEnd.Row <= intMinRow Then
            Set rngEnd = Nothing
            Exit Do
        End If
        Set rngEnd = rngEnd.Offset(-1)
    Loop
    
    Set MyUsedBottomCellGet = rngEnd
End Function

'--------------------------------------------------------------
' 指定行の最終データの列番号を返す ※開始セル以降の最初の空白値まで
'--------------------------------------------------------------
Function MyDataEndColumnGet(StartCell As Range) As Variant
    
    Dim rngFound As Range
    Set rngFound = MyDataRightCellGet(StartCell)

    If rngFound Is Nothing Then
        '有効データが無い場合
        MyDataEndColumnGet = 0
    Else
        MyDataEndColumnGet = rngFound.Column
    End If
End Function

Function MyDataRightCellGet(StartCell As Range) As Range
    
    '開始セル以降の最初の空白値を検索
    Dim intMaxCol   As Variant
    Dim rngEnd As Range
    
    With StartCell.Worksheet.UsedRange
        intMaxCol = .Cells(.Cells.Count).Column
    End With
    
    Set rngEnd = StartCell
    Do Until IsSpaceString(rngEnd.Value)
        If intMaxCol <= rngEnd.Column Then
            Set MyDataRightCellGet = rngEnd
            Exit Function
        End If
        Set rngEnd = rngEnd.Offset(0, 1)
    Loop
    
    If rngEnd.Column = StartCell.Column Then
        '有効データが無い場合
        Set MyDataRightCellGet = Nothing
    Else
        Set MyDataRightCellGet = rngEnd.Offset(0, -1)
    End If
End Function
'--------------------------------------------------------------
' ワークシートの最終データのセルを返す
'--------------------------------------------------------------
Public Function GetUsedRangeEndCell(ws As Worksheet) As Range
    With ws.UsedRange
        Set GetUsedRangeEndCell = .Cells(.Rows.Count, .Columns.Count)
    End With
End Function

'--------------------------------------------------------------
' データシート検索
'--------------------------------------------------------------
Public Function MyCellsValueSearch( _
        varSearch As Variant, _
        rngTarget As Range, _
        Optional ByVal NoMsg As Boolean = False) As Range
        
    Dim c1 As Range
    For Each c1 In rngTarget.Cells
        If Trim(c1.Value) = varSearch Then
            Set MyCellsValueSearch = c1
            Exit Function
        End If
    Next c1
    
    If Not NoMsg Then
        Call MyRangeSelect(rngTarget)
        Call MsgBox(rngTarget.Worksheet.Name & "!" & rngTarget.Address & vbCrLf _
            & "に「" & varSearch & "」がありません", vbCritical)
    End If
    
    Set MyCellsValueSearch = Nothing
End Function

'=============================================================
'
'    名前定義の処理
'
'=============================================================
Public _
Function 名前の参照範囲取得( _
            ByVal ws As Worksheet, _
            ByVal strName As String, _
            Optional NoMsg As Boolean = False) As Range
    
    If ws Is Nothing Then
        Call MsgBox("Bug!!", vbCritical)
        Stop
    End If
    
    On Error Resume Next
    
    Set 名前の参照範囲取得 = ws.Range(strName)
    If Err.Number <> 0 Then
        If Not NoMsg Then
            Call MsgBox("'" & ws.Name & "'シートに'" & strName & "'の名前定義がありません", vbCritical)
        End If
        Err.Clear
        
        Set 名前の参照範囲取得 = Nothing
    End If
    
    On Error GoTo 0
    
End Function

'=============================================================
'
'    文字列処理
'
'=============================================================
Public _
Function IsSpaceString(ByVal 値 As String) As Boolean
    
    ' 空白文字の除去
    If MySpaceCharsReplace(値, "") = "" Then
        IsSpaceString = True
    Else
        IsSpaceString = False
    End If

End Function

Public _
Function MySpaceCharsReplace( _
        ByVal 変換対象 As String, _
        ByVal 置換文字 As String) As String
    
    Dim strTmp As String
    Dim i As Long
        
    strTmp = 変換対象
    
    ' 空白文字を半角空白に置換
    strTmp = Replace(strTmp, "　", " ") '全角空白
    strTmp = Replace(strTmp, vbTab, " ")
    strTmp = Replace(strTmp, vbLf, " ")
    strTmp = Replace(strTmp, vbCr, " ")
    strTmp = Replace(strTmp, vbBack, " ")
    
    '連続する空白を１文字に変換
    Do
        i = Len(strTmp)
        strTmp = Replace(strTmp, "  ", " ")
    Loop While Len(strTmp) <> i
    
    MySpaceCharsReplace = Replace(strTmp, " ", 置換文字)
    
End Function
