Attribute VB_Name = "LogExtract"
'-------------------------------------------------------------
' All Rights Reserved, Copyright (C) FUJITSU LIMITED 2017
'
'   TECHINFO展開
'
'   作成日: 2017年 3月30日
'   更新日: 2017年 7月10日
'
'-------------------------------------------------------------
Option Explicit

Private Const sPasteLogName_base = "A1"   '解凍ログヘッダ設定位置
Private Const sPasteLogText_base = "A2" '解凍ログ貼り付け位置
Private Const sPasteLogHeader1 As String = "## TECHINFOファイル名"
Private Const sPasteLogHeader2 As String = "## TECHINFO解凍ログ"

' S150対応 Add Start 2020/12/11 by Nishi
' エラーメッセージの定義
Private Const ERR_CHECK_OK = 0
Private Const ERR_CHECK_TRAP_INFO = 1
Private Const ERR_CHECK__AUDIT_INFO = 2
Private Const ERR_CHECK_CONFIG_INFO = 3
Private Const ERR_CHECK_VERSION_INFO = 4
Private Const ERR_ARGS_OPTION_INPUT_OVER = 5
Private Const ERR_ARGS_INPUT = 6
Private Const ERR_TYPE_OPTION_INPUT = 7
Private Const ERR_CHECK_FLT = 8
Private Const ERR_CHECK_FFANLOG = 9
Private Const ERR_CANCEL_WRITE_OUTPUTFILE = 10
' S150対応 Add Start 2020/12/11 by Nishi

Public Function FLTLOG解析結果_Read( _
        strInputPath As String) As Boolean
        
    FLTLOG解析結果_Read = False
    
    If Not IsEnablePath(strInputPath) Then
        MsgBox "'" & strInputPath & "'" & vbCrLf _
            & "にアクセスできません。", vbCritical
        Exit Function
    ElseIf IsFolderName(strInputPath) Then
        MsgBox "'" & strInputPath & "'" & vbCrLf _
            & "はフォルダ名です。", vbCritical
        Exit Function
    End If
    
    Const job_name = "TECHINFO解析結果読み込み"
    Dim booResult As Boolean
    
    Call area_clear(99) '出力領域
    Call area_clear(3)  '日時指定クリア
    
    '解凍ログ貼り付け実行
    If swerrlog_paste(strInputPath, sPasteLogHeader1) = False Then
        booResult = False
    ElseIf FLT_log_copy() = False Then
        booResult = False
    Else
        booResult = True
    End If
    
    Call job_end(job_name, booResult)
    
    FLTLOG解析結果_Read = booResult
End Function

Public Function TECHINFO展開_Execute( _
        strInputPath As String, _
        strUnit_figno As String) As Boolean
        
    TECHINFO展開_Execute = False
    
    Const job_name = "TECHINFO展開"
    
    Dim logfile As String
    Dim logtext As String
    Dim strLogHeader As String
    Dim booResult As Boolean
    Dim intRetCode As Long
    
    If Not IsEnablePath(strInputPath) Then
        MsgBox "'" & strInputPath & "'" & vbCrLf _
            & "にアクセスできません。", vbCritical
        Exit Function
    ElseIf IsFolderName(strInputPath) Then
        MsgBox "'" & strInputPath & "'" & vbCrLf _
            & "はフォルダ名です。", vbCritical
        Exit Function
    End If
    
    Call area_clear(99) '出力領域
    Call area_clear(3)  '日時指定クリア
    
    '解凍実行
    logtext = swerr_execute(strInputPath, strUnit_figno, intRetCode)
    If logtext = "" Then
        Exit Function
    End If
    booResult = IIf(intRetCode = 0, True, False)
    
    '解凍ログ貼り付け実行
    If swerrlog_paste(logtext, sPasteLogHeader2) = False Then
        booResult = False
    ElseIf FLT_log_copy() = False Then
        booResult = False
    Else
        Call job_end(job_name, booResult)
    End If
    
    TECHINFO展開_Execute = booResult
End Function

Private Function swerr_execute( _
            ByVal strInputPath As String, _
            ByVal strUnit_figno As String, _
            ByRef intRetCode As Long) As String
            
    Const job_name = "TECHINFOログ解凍"
    Dim WSH As Object
    Dim exefile As String
    Dim strOutFolder  As String
    Dim logtext As String
    Dim strCmd  As String
    Dim rc  As Long
    Dim strTmp As String
    Dim dtStart As Date
    Dim dtLogFile As Date
    Dim strRefTable As String
    
    Call job_begin(job_name)
    
    rc = 2
    logtext = ""
    
    'ツールと出力先のフルパスを取得
    strTmp = toolPara_get("SwerrTool", 省略可:=False, Caption:="実行ファイル")
    If strTmp = "" Then GoTo Exit_Proc
    
    exefile = MyFullPathGet(strTmp)
    If Not IsExistPath(exefile) Then
        MsgBox "'" & exefile & "'" & vbCrLf _
            & "が存在しません。設定を確認して下さい。", vbCritical
        GoTo Exit_Proc
    End If
    
    strOutFolder = MyFolderNameGet(exefile)
    
    ' S150対応 Change Start 2020/12/11 by Nishi
    strRefTable = Module1.GetRefTableFileName(strOutFolder)
    ' S150対応 Change End 2020/12/11 by Nishi
    If strRefTable = "" Then
        GoTo Exit_Proc
    End If
    
    ' S150対応 Add Start 2020/12/11 by Nishi
    Dim strType As String
    strType = BladeName_pickup()
    ' S150対応 Add End 2020/12/11 by Nishi
    
    'DOS窓を開く
    Set WSH = CreateObject("WScript.shell")
    'コマンドを作る
    ' S150対応 Add Start 2020/12/11 by Nishi
    strCmd = """" & exefile & """ ""--flt_table=" & strRefTable & """  ""--type=" & strType & """ """ & strInputPath & """"
    ' S150対応 Add End 2020/12/11 by Nishi
    
    '実行開始日時を取得する
    dtStart = Now
    
    'コマンドをDOS窓に投入
    On Error GoTo Exec_Fail
    rc = WSH.Run(strCmd, IIf(True, vbNormalFocus, vbMinimizedNoFocus), True)
    On Error GoTo 0
    ':: rc【戻り値】
    ':: 0: TECHINFOログファイル抽出 OK
    
    Select Case rc
    ' S150対応 Change Start 2020/12/11 by Nishi
    Case ERR_CHECK_OK
    ' S150対応 Change End 2020/12/11 by Nishi
        '実行ログファイルをチェック
        logtext = strOutFolder & "\THECINFO_Analysis.log"
        If Not IsExistPath(logtext) Then
            'ファイルができていない
            Call MsgBox(job_name & "実行ログファイル" & vbCrLf _
                  & " [" & logtext & "]" & vbCrLf _
                  & "ができていません。" & vbCrLf & vbCrLf _
                  & "実行中に異常が発生していると思われます。" _
                  , vbExclamation)
            rc = 3
        ElseIf MyFileDateGet(logtext, dtLogFile) = False Then
            'ファイルの異常
            If MsgBox(job_name & "実行ログファイル" & vbCrLf _
                    & " [" & logtext & "]" & vbCrLf _
                    & "の更新時刻が取得できません。" & vbCrLf & vbCrLf _
                    & "実行中に異常が発生していると思われますが、" & vbCrLf _
                    & "処理を続けますか？" _
                    , vbExclamation + vbYesNo) = vbNo Then
                rc = 3
            End If
        ElseIf dtLogFile < dtStart Then
            'ファイルが古い
            Call MsgBox(job_name & "実行ログファイル" & vbCrLf _
                  & " [" & logtext & "]" & vbCrLf _
                  & "が更新されていません。" & vbCrLf & vbCrLf _
                  & "実行中に異常が発生していると思われます。" _
                  , vbExclamation)
            rc = 3
        End If
        
    ' S150対応 Change Start 2020/12/11 by Nishi
    Case ERR_CHECK_TRAP_INFO
        Call MsgBox("Trap情報解析時に異常を検出したので強制に終了しました。" & vbCrLf _
                     & "詳細はマニュアルシートのエラー一覧表を確認してください｡ ")
    Case ERR_CHECK__AUDIT_INFO
        Call MsgBox("Audit情報解析時に異常を検出したので強制に終了しました。" & vbCrLf _
                     & "詳細はマニュアルシートのエラー一覧表を確認してください｡ ")
    Case ERR_CHECK_CONFIG_INFO
        Call MsgBox("構成情報取得時に異常を検出したので強制に終了しました。" & vbCrLf _
                     & "詳細はマニュアルシートのエラー一覧表を確認してください｡ ")
    Case ERR_CHECK_VERSION_INFO
        Call MsgBox("Version情報取得時に異常を検出したので強制に終了しました。" & vbCrLf _
                     & "詳細はマニュアルシートのエラー一覧表を確認してください｡ ")
    Case ERR_ARGS_OPTION_INPUT_OVER
        Call MsgBox("オプション指定がない引数が2つ以上が検出されました。" & vbCrLf _
                     & "詳細はマニュアルシートのエラー一覧表を確認してください｡ ")
    Case ERR_ARGS_INPUT
        Call MsgBox("引数の入力エラーを検出したので強制に終了しました。" & vbCrLf _
                     & "詳細はマニュアルシートのエラー一覧表を確認してください｡ ")
    Case ERR_TYPE_OPTION_INPUT
        Call MsgBox("Typeオプションが正しく入力されてません。" & vbCrLf _
                     & "詳細はマニュアルシートのエラー一覧表を確認してください｡ ")
    Case ERR_CHECK_FLT
        Call MsgBox("FLT解析が失敗したので強制に終了しました｡" & vbCrLf _
                     & "詳細はマニュアルシートのエラー一覧表を確認してください｡ ")
    Case ERR_CHECK_FFANLOG
        Call MsgBox("FFANログ解析が失敗したので強制に終了しました。" & vbCrLf _
                     & "詳細はマニュアルシートのエラー一覧表を確認してください｡ ")
    Case ERR_CANCEL_WRITE_OUTPUTFILE
        Call MsgBox("解析の実行をキャンセルしました。")
    ' S150対応 Change End 2020/12/11 by Nishi
        
    Case Else '想定外の戻り値
        Call MsgBox(job_name & "実行結果が判定できません。" & vbCrLf & vbCrLf _
                  & "実行中に異常が発生していると思われます。" & vbCrLf & vbCrLf _
                  & "  ※実行経過表示のウィンドウを[X](閉じる)ボタンで" & vbCrLf _
                  & "    終了させた場合も異常になります。" _
                  , vbExclamation)
        rc = 3
    End Select
    
Exit_Proc:
    On Error GoTo 0
    
    Call job_end(job_name, rc = 0)
    
    Set WSH = Nothing
    
    intRetCode = rc
    swerr_execute = logtext
    Exit Function
    
Exec_Fail:
    MsgBox "'" & exefile & "'" & vbCrLf _
        & "の起動に失敗しました" & vbCrLf & vbCrLf _
        & Err.Description, vbCritical
    
    Resume Exit_Proc
    
End Function

'解凍ログの読込、貼付け
Private Function swerrlog_paste( _
        strFullPath As String, _
        strLogHeader As String) As Boolean
        
    Const job_name = "SWERR解凍ログ貼付け"
    Dim ret_value   As Boolean
    Dim out_ws  As Worksheet
    Dim intFF   As Long  ' FreeFile値
    Dim buf As String
    Dim aryBuf  As Variant
    Dim base_cel    As Range
    Dim c1  As Range
    
    ret_value = False
    
    Call job_begin(job_name)
        
    Set out_ws = GetWorkSheetByName(sPasteLog_shname, ThisWorkbook)
    out_ws.Range(sPasteLogName_base).Value = strLogHeader
    
    If strFullPath = "" Then
        GoTo Exit_Proc
    End If
    
    Set base_cel = out_ws.Range(sPasteLogText_base)
    Set out_ws = Nothing
    
    If strLogHeader = sPasteLogHeader1 Then
        base_cel.NumberFormatLocal = "@"
        base_cel.Value = strFullPath
        ret_value = True
        GoTo Exit_Proc
    End If
    
    On Error GoTo Read_Fail
    intFF = FreeFile
    Open strFullPath For Input As #intFF
        Set c1 = base_cel
        Do Until EOF(intFF)
            Line Input #intFF, buf
            buf = Trim(buf)
            If buf <> "" Then
                aryBuf = Split(buf, vbTab) 'TAB文字で分割
                With c1.Resize(1, UBound(aryBuf) + 1)
                    .NumberFormatLocal = "@"
                    .Value = aryBuf
                End With
                Set c1 = c1.Offset(1)
             End If
        Loop
    Close #intFF
    On Error GoTo 0
    
    'ファイルの中身がなかった（=ROW用のカウンタ(n)が初期値(0)のまま)
    If base_cel.Row = c1.Row Then
        c1.Value = "ログファイルが空です。"
        GoTo Exit_Proc
    End If
    
    base_cel.Resize(c1.Row - base_cel.Row, 4).Columns.AutoFit
    
    ret_value = True
    
Exit_Proc:
    On Error GoTo 0
    Call job_end(job_name, ret_value)
    
    Set base_cel = Nothing
    Set c1 = Nothing
    Set out_ws = Nothing
    
    swerrlog_paste = ret_value
    Exit Function
    
Read_Fail:
    MsgBox "'" & strFullPath & "'" & vbCrLf _
        & "の読み込みに失敗しました" & vbCrLf & vbCrLf _
        & Err.Description, vbCritical
    
    Resume Exit_Proc
End Function

'ログ解析ツールの結果ファイルから"FLT解析"結果シートをコピー
Public Function FLT_log_copy( _
        Optional ByVal NoMsg As Boolean = False) As Boolean
    
    Dim booResult As Boolean
    Dim rngBase As Range
    Dim intMaxRow  As Variant
    
    Dim c1 As Range
    Dim intType  As Long
    Dim strText   As String
    Dim strKey As String
    
    Dim strOutputPath  As String
    
    FLT_log_copy = False
    
    strOutputPath = ""
    
    Dim wsList As Worksheet
    Set wsList = GetWorkSheetByName(sPasteLog_shname, ThisWorkbook)
    If wsList Is Nothing Then Exit Function
    
    Set rngBase = wsList.Range(sPasteLogName_base)
    strText = Trim(rngBase.Value)
    
    '入力データチェック
    If strText = sPasteLogHeader1 Then
        intType = 1    'ログファイル名の指定
        
    ElseIf strText = sPasteLogHeader2 Then
        intType = 2    '解凍ログの始まり
        strKey = "Output="
    
    Else
        If Not NoMsg Then
            Call MyRangeSelect(c1.Offset(1, 0))
            MsgBox "ログテキスト領域に有効なデータがありません"
        End If
        Exit Function
    End If
    
    '解凍ログから解析結果ファイル名を検出
    intMaxRow = GetUsedRangeEndCell(wsList).Row
    Set c1 = rngBase.Offset(1)
    Do While True
        strText = Trim(c1.Value)
        
        If IsSpaceString(strText) Then
            ' 空行は無視
        ElseIf intType = 1 Then
            strOutputPath = strText
            Exit Do
        Else
            strText = c1.Offset(0, 4).Value
            If InStr(strText, strKey) = 1 Then
                strOutputPath = Mid(strText, Len(strKey) + 1)
                Exit Do
            End If
        End If
        
        If c1.Row = intMaxRow Then
            Exit Do
        End If
        Set c1 = c1.Offset(1)
    Loop
    
    If strOutputPath = "" Then
        If Not NoMsg Then
            Call MyRangeSelect(rngBase)
            MsgBox "有効なデータがありません"
            Exit Function
        End If
    End If
    
    Dim wbSrc As Workbook
    Set wbSrc = MyRefBookOpen(strOutputPath)
    If wbSrc Is Nothing Then
        Exit Function
    End If
    
    ' FLT解析結果シートをコピー
    Dim wsDst As Worksheet
    Set wsDst = GetWorkSheetByName(sFLT_Log_shname, ThisWorkbook)
    If wsDst Is Nothing Then GoTo Exit_Proc
    If log_sheet_copy(wsDst, wbSrc, "FLT 解析", True) < 0 Then
        GoTo Exit_Proc
    End If
    
    ' 警報変化点ログ解析結果シートをコピー
    Set wsDst = GetWorkSheetByName(sStschgLog_shname, ThisWorkbook)
    If wsDst Is Nothing Then GoTo Exit_Proc
    If log_sheet_copy(wsDst, wbSrc, "警報変化点ログ解析", True) < 0 Then
        GoTo Exit_Proc
    End If
    
    ' 温度上昇関連ログ解析結果シートをコピー
    Set wsDst = GetWorkSheetByName(sFFAN_Log_shname, ThisWorkbook)
    If wsDst Is Nothing Then GoTo Exit_Proc
    If log_sheet_copy(wsDst, wbSrc, "FFANログ解析", False) < 0 Then
        GoTo Exit_Proc
    End If
    
    ' S150対応 Add Start 2020/12/11 by Nish
    Dim ws As Worksheet
    For Each ws In ThisWorkbook.Worksheets
        Select Case ws.Name
            Case sConfig_Info_shname
                Application.DisplayAlerts = False ' メッセージを非表示
                ThisWorkbook.Sheets(sConfig_Info_shname).Delete
                Application.DisplayAlerts = True  ' メッセージを表示
            Case sTrap_Info_shname
                Application.DisplayAlerts = False ' メッセージを非表示
                ThisWorkbook.Worksheets(sTrap_Info_shname).Delete
                Application.DisplayAlerts = True  ' メッセージを表示
            Case sVersion_Info_shname
                Application.DisplayAlerts = False ' メッセージを非表示
                ThisWorkbook.Worksheets(sVersion_Info_shname).Delete
                Application.DisplayAlerts = True  ' メッセージを表示
        End Select
    Next ws
    
    ' 構成情報シートをコピー
    wbSrc.Worksheets(sConfig_Info_shname).Copy after:=ThisWorkbook.Worksheets(Worksheets.Count)
    
    ' Trap情報シートをコピー
    wbSrc.Worksheets(sTrap_Info_shname).Copy after:=ThisWorkbook.Worksheets(Worksheets.Count)
    
    ' Version情報シートをコピー
    wbSrc.Worksheets(sVersion_Info_shname).Copy after:=ThisWorkbook.Worksheets(Worksheets.Count)
    ' S150対応 Add End 2020/12/11 by Nishi
    
    
    booResult = True
    
Exit_Proc:
    wbSrc.Close
    
    FLT_log_copy = booResult
End Function

Private Function log_sheet_copy( _
        wsDst As Worksheet, _
        wbSrc As Workbook, _
        ByVal src_sheetname As String, _
        ByVal delete_top As Boolean) As Long
    
    Dim rtn_code As Integer: rtn_code = -1
    
    Dim wsSrc As Worksheet
    Set wsSrc = GetWorkSheetByName(src_sheetname, wbSrc)
    If wsSrc Is Nothing Then
        GoTo Exit_Proc
    End If
    
    ' 解析結果シートをコピー
    On Error Resume Next
    wsSrc.Cells.Copy wsDst.Cells(1, 1)
    wsSrc.Cells(1, 1).Copy wsDst.Cells(1, 1)
        '注）図形を含むシートのCopy後に入力側Book閉じる際にエラー（リソース不足とか）になる問題の回避策
        '    使用していないはずのClipboardにデータが生成されているため。Excel2007以降の現象らしい。
    Application.CutCopyMode = False   '不要なはずだが、念のため
    
    If Err.Number <> 0 Then
        GoTo Exit_Proc
    End If
    
    '一行目は削除
    If delete_top Then
        wsDst.Rows(1).Delete
        If Err.Number <> 0 Then
            GoTo Exit_Proc
        End If
    End If
    
    rtn_code = 1
    
Exit_Proc:
    If Err.Number <> 0 Then
        Call MsgBox(Err.Description, vbCritical)
    End If
    On Error GoTo 0
    
    log_sheet_copy = rtn_code
End Function
