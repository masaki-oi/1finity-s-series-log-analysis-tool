Attribute VB_Name = "LogParse"
'-------------------------------------------------------------
' All Rights Reserved, Copyright (C) FUJITSU LIMITED 2017
'
'   ログデータの整形表示
'
'   作成日: 2017年 5月24日
'   更新日: 2017年 7月10日
'   更新日: 2018年 1月25日: 「即時Clear判定条件 」の判定誤りを修正
'   更新日: 2018年 2月26日: "操作"シート出力に「SLOT」列追加
'
'-------------------------------------------------------------
Option Explicit

Public Function FLTログ整形表示_Execute() As Variant
    Const job_name = "FLTログ整形表示"
    
    Call job_begin(job_name)
    
    '出力領域クリア
    Call area_clear(1)
    Call area_clear(2)
    
    Dim intCount As Variant
    Dim data_count(2) As Variant
    If alarmList_write(data_count) = False Then
        intCount = -1
        GoTo Exit_Proc
    End If
    
    ' S150対応 Change Start 2020/12/11 by Nishi
    Dim strType As String
    strType = BladeName_pickup()
    
    If strType = "S100" Then
      intCount = repairParts_write_s100()
    ElseIf strType = "S150" Then
      ' S150対応 Add Start 2020/12/22 by Nishi
      intCount = repairParts_write_s150(True)
      ' S150対応 Add End 2020/12/22 by Nishi
    End If
    ' S150対応 Change End 2020/12/11 by Nishi
    
Exit_Proc:
    Call job_end(job_name, intCount > 0)
    
    FLTログ整形表示_Execute = intCount
End Function

Public Function FLTログ整形結果絞込み_Execute() As Variant
    Const job_name As String = "FLTログ整形結果絞込み"
    Dim date_lower  As Variant
    Dim date_upper  As Variant
    Dim ret_value As Variant
    
    ' 抽出範囲指定取り出し
    If date_get(date_lower, date_upper) = False Then
        Call MsgBox("抽出範囲日時の指定がありません")
        ret_value = -1
        GoTo Exit_Proc
    End If
    
    Call job_begin(job_name)
    
    '依存出力領域クリア
    Call area_clear(2)
    
    ret_value = date_range_apply(date_lower, date_upper)
    If ret_value <= 0 Then
        GoTo Exit_Proc
    End If
    
    ' S150対応 Change Start 2020/12/11 by Nishi
    Dim strType As String
    strType = BladeName_pickup()
    
    If strType = "S100" Then
      ret_value = repairParts_write_s100()
    ElseIf strType = "S150" Then
      ' S150対応 Add Start 2020/12/22 by Nishi
      ret_value = repairParts_write_s150(False)
      ' S150対応 Add End 2020/12/22 by Nishi
    End If
    ' S150対応 Change End 2020/12/11 by Nishi
    
Exit_Proc:
    Dim strMsg  As String
    Select Case ret_value
    Case Is > 0
'        Call MyRangeSelect(wsTarget.Cells(data_base_line, d1_data_base_col))
        strMsg = "完了"
    Case 0
        strMsg = "終了(有効データ無し)"
        
    Case Is < 0
        strMsg = "中止(設定エラー)"
    End Select
        
    Call job_end(job_name, strMsg)
    
    FLTログ整形結果絞込み_Execute = ret_value
End Function

Private Function date_range_apply( _
        ByVal dtLower As Date, _
        ByVal dtUpper As Date) As Variant
        
    Dim ret_code   As Variant
    Dim wsTarget   As Worksheet
    Dim rngHeaderBase As Range
    
    Dim intCount As Variant
    Dim intMaxRow As Variant
    Dim intRow As Variant
    Dim c1 As Range
    
    Set wsTarget = GetWorkSheetByName(control_shname, ThisWorkbook)
    Set rngHeaderBase = wsTarget.Cells(data_base_line, d1_data_base_col)
    
    intMaxRow = MyDataEndRowGet(rngHeaderBase.Offset(1, d1_ID_ofs))
    If intMaxRow <= rngHeaderBase.Row Then
        intCount = 0
        GoTo Exit_Proc
    End If
    
    Application.StatusBar = "抽出範囲適用中..."
    intCount = intMaxRow - rngHeaderBase.Row
    For intRow = intCount To 1 Step -1
        Set c1 = rngHeaderBase.Offset(intRow)
        If Not date_check_d1(c1, dtLower, dtUpper) Then
            intCount = intCount - 1
            c1.EntireRow.Delete
        End If
    Next intRow
    If intCount < 1 Then
        GoTo Exit_Proc
    End If
    Application.StatusBar = "抽出範囲適用 完了"
    Call MyScreenUpdate
    
    '通し番号設定
    Call SerialNoWrite(rngHeaderBase.Offset(0, d1_No_ofs), intCount)
    
Exit_Proc:
    date_range_apply = intCount
End Function

Private Function date_check_d1( _
    rngTarget As Range, _
    date_lower As Date, _
    date_upper As Date) As Boolean
    
    Dim booResult As Boolean
    Dim dtRecord As Date
    Dim varDate As Variant
    Dim varTime As Variant
    
    On Error Resume Next
    With rngTarget
        varDate = .Offset(0, d1_Date_ofs)
        varTime = .Offset(0, d1_Time_ofs)
    End With
    
    If VarType(varDate) <> vbDate Then
        varDate = CDate(varDate)
    End If
    If Not IsDate(varDate & " " & CStr(TimeValue(Now))) Then
        varDate = ""
    End If
    If Err.Number <> 0 Then
        varDate = ""
        Err.Clear
    End If
    
    If VarType(varDate) <> vbDate Then
        varTime = ""
    End If
    
    If VarType(varTime) <> vbDate Then
        varTime = CDate(varTime)
    End If
    If Not IsDate(varDate & " " & varTime) Then
        varTime = ""
    End If
    If Err.Number <> 0 Then
        varTime = ""
        Err.Clear
    End If
    
    On Error GoTo 0
        
    If VarType(varDate) <> vbDate Then
        Call set_error_mark(rngTarget.Offset(0, d1_Date_ofs))
        booResult = True
        
    ElseIf VarType(varTime) <> vbDate Then
        Call set_error_mark(rngTarget.Offset(0, d1_Time_ofs))
        dtRecord = varDate
        booResult = date_check(dtRecord, DateValue(date_lower), DateValue(date_upper))
    Else
        dtRecord = varDate + varTime
        booResult = date_check(dtRecord, date_lower, date_upper)
    End If
    
    date_check_d1 = booResult
End Function

Private Function alarmList_write( _
        ByRef data_count() As Variant) As Boolean
        
    alarmList_write = False
    
    data_count(0) = 0
    data_count(1) = 0
    
    Dim wsSrc As Worksheet
    Dim wsOut As Worksheet
    
    Dim intDataCount As Variant
    Dim intMaxRow As Variant
    Dim intMaxCol As Variant
    Dim rngSrcHeader As Range
    Dim rngOutBase As Range
    
    Dim intRow As Variant
    Dim strName As String
    Dim c0 As Range
    Dim c1 As Range
    Dim i As Long
    
    Set wsSrc = GetWorkSheetByName(sFLT_Log_shname, ThisWorkbook)
    If wsSrc Is Nothing Then Exit Function
    
    Set wsOut = GetWorkSheetByName(control_shname, ThisWorkbook)
    If wsOut Is Nothing Then Exit Function
    
    intMaxRow = MyDataEndRowGet(wsSrc.Cells(1, 1))
    intMaxCol = MyDataEndColumnGet(wsSrc.Cells(1, 1))
    If intMaxCol = 0 Then
        Call MsgBox("ヘッダ不正", vbCritical)
        Exit Function
    End If
    
    '元データシートからコピーする列を検出
    Set rngSrcHeader = wsSrc.Cells(1, 1).Resize(1, intMaxCol)
    
    Dim intSrcCol(d1_data_max_ofs) As Variant
    For i = 0 To d1_data_max_ofs
        Select Case i
        Case d1_Date_ofs: strName = "Date"
        Case d1_Time_ofs: strName = "Time"
        Case d1_data1_ofs: strName = "addr"
        Case d1_data2_ofs: strName = "val"
        Case d1_Info_ofs: strName = "Clear時間"
        Case Else
            strName = ""
        End Select
        If strName <> "" Then
            Set c0 = MyCellsValueSearch(strName, rngSrcHeader)
            If c0 Is Nothing Then Exit Function
            
            intSrcCol(i) = c0.Column
        Else
            intSrcCol(i) = 0
        End If
    Next i
    
    ' 抽出範囲指定取り出し
    Dim date_lower  As Variant
    Dim date_upper  As Variant
    Dim dtLower  As Date
    Dim dtUpper  As Date
    Dim booDateCheck As Boolean
    booDateCheck = date_get(date_lower, date_upper)
    If booDateCheck Then
        dtLower = date_lower
        dtUpper = date_upper
    End If
    
    Dim varDetectTime As Variant
    Dim varClearTime As Variant
    Dim intClearTimeCol As Long
    varDetectTime = detectTime_get()
    If varDetectTime > 0 Then
        intClearTimeCol = intSrcCol(d1_Info_ofs) '"Clear時間"
    End If
    
    '元データシートからコピー
    Dim intBitNameCol As Long
    Set c0 = MyCellsValueSearch("ON状態のbit名", rngSrcHeader)
    If c0 Is Nothing Then Exit Function
    intBitNameCol = c0.Column
    
    Set rngOutBase = wsOut.Cells(data_base_line, d1_data_base_col)
    Set c1 = rngOutBase.Offset(1)
    
    Application.StatusBar = wsSrc.Name & " コピー中..."
    intDataCount = 0
    Set c0 = rngSrcHeader
    Set c1 = rngOutBase
    For intRow = 2 To intMaxRow
        Set c0 = c0.Offset(1)
        If Trim(c0.Columns(intBitNameCol).Value) = "ALL Clear" Then
            GoTo Continue   '"ALL Clear"の行は除外
        End If
        
        If varDetectTime > 0 Then
            varClearTime = c0.Columns(intClearTimeCol).Value
            If IsSpaceString(varClearTime) Then
            ElseIf Not IsNumeric(varClearTime) Then
            ElseIf Val(varClearTime) <= varDetectTime Then
                GoTo Continue
            End If
        End If
        
        Set c1 = c1.Offset(1)
        For i = 0 To d1_data_max_ofs
            If intSrcCol(i) > 0 Then
                c0.Columns(intSrcCol(i)).Copy c1.Offset(0, i)
            End If
        Next i
        
        '日時が抽出範内指定外の行は除外
        If Not booDateCheck Then
            '抽出範内指定なし
        ElseIf Not date_check_d1(c1, dtLower, dtUpper) Then
            Set c1 = c1.Offset(-1)
            c1.Offset(1).EntireRow.Delete
            GoTo Continue
        End If
        
        '行番号設定
        c1.Offset(0, d1_ID_ofs).Value = intRow
        intDataCount = intDataCount + 1
        
Continue:
    Next intRow
    
    data_count(0) = intDataCount
    
    Application.StatusBar = wsSrc.Name & "コピー 完了"
    Call MyScreenUpdate
    
    '警報変化点ログから「温度上昇ログ」を抽出
    data_count(1) = stschgList_write(c1)
    If data_count(1) < 0 Then
        Exit Function
    End If
    
    '------------------------------------------
    ' 後処理
    '------------------------------------------
    intDataCount = intDataCount + data_count(1)
    If intDataCount < 1 Then
        Call MsgBox("有効なデータがありません", vbCritical)
        Exit Function
    End If
    
    '降順に並べ替え
    Dim rngOutHeader As Range
    Set rngOutHeader = rngOutBase.Resize(1, d1_data_max_ofs + 1)
    Call MyRangeSelect(rngOutHeader)   '注）Selectは不要だが、ソート範囲を目視で確認するため。
    
    With wsOut.Sort
        With .SortFields
            .Clear
            .Add Key:=rngOutBase.Offset(0, d1_ID_ofs), Order:=xlDescending
        End With
        
        .SetRange rngOutHeader.Resize(intDataCount + 1)
        .Header = xlYes
        .Orientation = xlTopToBottom
        .Apply
    End With
    
    '通し番号設定
    Call SerialNoWrite(rngOutBase.Offset(0, d1_No_ofs), intDataCount)
    
    '書式設定
    With rngOutHeader.Offset(1).Resize(intDataCount)
        .Borders.LineStyle = xlContinuous
        .Interior.ColorIndex = xlColorIndexNone
    End With
    
    alarmList_write = True
End Function

'警報変化点ログ("警報変化点Log"シート)から「温度上昇ログ」抽出
Private Function stschgList_write( _
        ByVal out_row As Range) As Variant
        
    stschgList_write = -1
    
    Dim wsSrc As Worksheet
    
    Dim data_count As Variant: data_count = 0
    Dim intMaxRow As Variant
    Dim intMaxCol As Variant
    Dim rngSrcHeader As Range
    Dim rngOutBase As Range
    
    Dim intRow As Variant
    Dim strName As String
    Dim c0 As Range
    Dim c1 As Range
    Dim i As Long
    
    Set wsSrc = GetWorkSheetByName(sStschgLog_shname, ThisWorkbook)
    If wsSrc Is Nothing Then Exit Function
    
    intMaxRow = MyDataEndRowGet(wsSrc.Cells(1, 1))
    intMaxCol = MyDataEndColumnGet(wsSrc.Cells(1, 1))
    If intMaxCol = 0 Then
        Call MsgBox("ヘッダ不正", vbCritical)
        Exit Function
    End If
    
    '元データシートからコピーする列を検出
    Set rngSrcHeader = wsSrc.Cells(1, 1).Resize(1, intMaxCol)
    
    Dim intSrcCol(d1_data_max_ofs) As Variant
    For i = 0 To d1_data_max_ofs
        Select Case i
        Case d1_Date_ofs: strName = "Date"
        Case d1_Time_ofs: strName = "Time"
        Case d1_data1_ofs: strName = "cond_id"
        Case Else
            strName = ""
        End Select
        If strName <> "" Then
            Set c0 = MyCellsValueSearch(strName, rngSrcHeader)
            If c0 Is Nothing Then Exit Function
            
            intSrcCol(i) = c0.Column
        Else
            intSrcCol(i) = 0
        End If
    Next i
    
    '元データシートからコピー
    Dim cond_id_column As Long: cond_id_column = intSrcCol(d1_data1_ofs)
    
    Application.StatusBar = wsSrc.Name & " コピー中..."
    data_count = 0
    Set c0 = rngSrcHeader
    Set c1 = out_row
    For intRow = 2 To intMaxRow
        Set c0 = c0.Offset(1)
        Select Case Trim(c0.Columns(cond_id_column).Value)
        Case "BLADE_BREAK_TEMP_STAT"
        Case "PIU_BREAK_TEMP_STAT"
        Case "BLADE_PATH_MON_STAT"
        Case "BLADE_PATH_MON2_STAT"
        Case Else
            GoTo Continue   '上記以外の行は除外
        End Select
        
        Set c1 = c1.Offset(1)
        For i = 0 To d1_data_max_ofs
            If intSrcCol(i) > 0 Then
                c0.Columns(intSrcCol(i)).Copy c1.Offset(0, i)
            End If
        Next i
        
        '行番号設定
        c1.Offset(0, d1_ID_ofs).Value = "S_" & intRow
        data_count = data_count + 1
        
Continue:
    Next intRow
    
    Application.StatusBar = wsSrc.Name & " コピー完了"
    Call MyScreenUpdate
    
    stschgList_write = data_count
End Function

'通し番号設定
Private Sub SerialNoWrite( _
    rngHeader As Range, _
    ByVal intMaxNo As Variant)
    
    Application.StatusBar = "通し番号設定中..."
    Dim intNo As Variant
    Dim c1 As Range
    Set c1 = rngHeader
    For intNo = 1 To intMaxNo
        Set c1 = c1.Offset(1)
        c1.Value = CStr(intNo)
    Next intNo
    
    Application.StatusBar = "通し番号設定 完了"
    DoEvents
End Sub

' S150対応 Change Start 2020/12/11 by Nishi
'Bit毎に被疑部品を出力(S100向け)
Private Function repairParts_write_s100() As Variant
' S150対応 Change End 2020/12/11 by Nish
    
    ' S150対応 Change Start 2020/12/11 by Nishi
    repairParts_write_s100 = -1
    ' S150対応 Change End 2020/12/11 by Nishi
    
    Dim wsSrc As Worksheet
    Dim wsOut As Worksheet
    Dim intDataCount As Variant
    Dim intMaxRow As Variant
    Dim intMaxCol As Variant
    Dim rngSrcHeader As Range
    Dim rngOutBase As Range
    
    Dim intRow As Variant
    Dim intCol As Variant
    Dim strName As String
    Dim c0 As Range
    Dim c1 As Range
    Dim i As Long
    Dim j As Long
    Dim k As Long
    
    Call area_clear(2)
    
    Set wsOut = GetWorkSheetByName(control_shname, ThisWorkbook)
    If wsOut Is Nothing Then Exit Function
    
    Set rngOutBase = wsOut.Cells(data_base_line, d1_data_base_col)
    intMaxRow = MyDataEndRowGet(rngOutBase.Offset(0, d1_ID_ofs))
    If intMaxRow <= rngOutBase.Row Then
        intDataCount = 0
        GoTo Exit_Proc
    End If
    
    ' FFAN_Logシートの情報取得
    Dim wsFFAN As Worksheet
    Dim intFFAN_Row As Variant
    Dim intFFAN_Col(4) As Variant
    
    Set wsFFAN = GetWorkSheetByName(sFFAN_Log_shname, ThisWorkbook)
    If wsFFAN Is Nothing Then Exit Function
    
    intFFAN_Row = MyDataEndRowGet(wsFFAN.Cells(1, 1))
    intMaxCol = MyDataEndColumnGet(wsFFAN.Cells(1, 1))
    If intMaxCol = 0 Then
        Call MsgBox("ヘッダ不正", vbCritical)
        Exit Function
    End If
    
    Set rngSrcHeader = wsFFAN.Cells(1, 1).Resize(1, intMaxCol)
    
    For i = 0 To UBound(intFFAN_Col)
        Select Case i
        Case 0: strName = "Message"
        Case 1: strName = "index"
        Case 2: strName = "slot"
        Case 3: strName = "partsNo"
        Case 4: strName = "被疑部品"
        Case Else
            Call MsgBox("Bug!!", vbCritical)
            Stop
        End Select
        
        Set c0 = MyCellsValueSearch(strName, rngSrcHeader)
        If c0 Is Nothing Then Exit Function
        intFFAN_Col(i) = c0.Column
    Next i
    
    ' FLT_Logシートの情報取得
    Set wsSrc = GetWorkSheetByName(sFLT_Log_shname, ThisWorkbook)
    If wsSrc Is Nothing Then Exit Function
    
    intMaxCol = MyDataEndColumnGet(wsSrc.Cells(1, 1))
    If intMaxCol = 0 Then
        Call MsgBox("ヘッダ不正", vbCritical)
        Exit Function
    End If
    
    Set rngSrcHeader = wsSrc.Cells(1, 1).Resize(1, intMaxCol)
    
    Dim intSrcCol(7) As Variant
    For i = 0 To UBound(intSrcCol)
        Select Case i
        Case 0: strName = "register名"
        Case 1: strName = "ON_bit"
        Case 2: strName = "ON状態のbit名"
        Case 3: strName = "被疑部品"
        Case 4: strName = "MAIN/SUB"
        Case 5: strName = "部品番号"
        Case 6: strName = "実装位置名"
        Case 7: strName = "slot_no"
        Case Else
            Call MsgBox("Bug!!", vbCritical)
            Stop
        End Select
        
        Set c0 = MyCellsValueSearch(strName, rngSrcHeader)
        If c0 Is Nothing Then Exit Function
        intSrcCol(i) = c0.Column
    Next i
    
    Dim intSrcRow As Variant
    Dim c2 As Range
    
    Dim varList As Variant
    Dim intRepeatMax As Long
    Dim strStatus As String
    Dim intDspCount As Variant '実行中表示用カウンタ
    
    Set c1 = rngOutBase
    intMaxRow = intMaxRow - rngOutBase.Row
    
    Set rngOutBase = wsOut.Cells(data_base_line, d2_data_base_col)
    Set c2 = rngOutBase
    
    strStatus = "/" & intMaxRow & " 被疑部品出力中..."
    
    intDspCount = 50
    intDataCount = 0
    For intRow = 1 To intMaxRow
        intDspCount = intDspCount - 1
        If intDspCount < 1 Then
            Application.StatusBar = intRow & strStatus
            DoEvents
            intDspCount = 50
        End If
        
        Set c1 = c1.Offset(1)
        
        intSrcRow = c1.Offset(0, d1_ID_ofs).Value
        If intSrcRow Like "S_*" Then
            '温度上昇の被疑部品出力
            If temp_over_write(c1, c2, wsFFAN, intFFAN_Row, intFFAN_Col) Then
                intDataCount = intDataCount + 1
            End If
            GoTo Continue
        End If
        
        Set c0 = wsSrc.Cells(intSrcRow, 1)
        
        intRepeatMax = 1
        Set c2 = c2.Offset(1)
        For i = 1 To 6
            Select Case i
            Case 1: j = d2_Bit_ofs      '"ON_bit"
            Case 2: j = d2_BitName_ofs  '"ON状態のbit名"
            Case 3: j = d2_RepairPart_ofs   '"被疑部品"
            Case 4: j = d2_MainSub_ofs      '"MAIN/SUB"
            Case 5: j = d2_RepairFigNo_ofs  '"部品番号"
            Case 6: j = d2_RepairPlace_ofs  '"実装位置名"
            End Select
            
            varList = c0.Columns(intSrcCol(i)).Value
            If InStr(varList, vbLf) < 1 Then
                varList = Replace(varList, ",", "," & vbLf)
                c2.Offset(0, j).Value = varList
            Else
                varList = Split(varList, vbLf)
                For k = 0 To UBound(varList)
                    c2.Offset(k, j) = Replace(varList(k), ",", "," & vbLf)
                Next k
            
                k = UBound(varList) + 1
                Erase varList
                
                If intRepeatMax < k Then
                    intRepeatMax = k
                End If
            End If
        Next i
        
        With c2.Resize(intRepeatMax)
            c1.Offset(0, d1_No_ofs).Copy .Offset(0, d2_No_ofs)
            c1.Offset(0, d1_ID_ofs).Copy .Offset(0, d2_ID_ofs)
            c1.Offset(0, d1_data1_ofs).Copy .Offset(0, d2_Addr_ofs)
            
            c0.Columns(intSrcCol(0)).Copy .Offset(0, d2_RegName_ofs)
            Select Case c0.Columns(intSrcCol(7)).Value
            Case 1, 2, 3, 4
                c0.Columns(intSrcCol(7)).Copy .Offset(0, d2_SlotNo_ofs)
            End Select
        End With
        
        Set c2 = c2.Offset(intRepeatMax - 1)
        
        intDataCount = intDataCount + intRepeatMax
Continue:
    Next intRow
    
    Application.StatusBar = "被疑部品出力 完了"
    
    If intDataCount = 0 Then
        GoTo Exit_Proc
    End If
    
    '書式設定
    With rngOutBase.Offset(1).Resize(intDataCount, d2_data_max_ofs + 1)
        .Borders.LineStyle = xlContinuous
        .Columns(1 + d2_Bit_ofs).HorizontalAlignment = xlHAlignRight
    End With
    
Exit_Proc:
    ' S150対応 Change Start 2020/12/11 by Nishi
    repairParts_write_s100 = intDataCount
    ' S150対応 Change End 2020/12/11 by Nishi
End Function

' S150対応 Add Start 2020/12/11 by Nishi
'Bit毎に被疑部品を出力(S150向け)
' S150対応 Add Start 2020/12/22 by Nishi
Private Function repairParts_write_s150(ByVal flag_CheckFFANLog As Boolean) As Variant
' S150対応 Add End 2020/12/22 by Nishi
    
    repairParts_write_s150 = -1
    
    Dim wsSrc As Worksheet
    Dim wsOut As Worksheet
    Dim intDataCount As Variant
    Dim intMaxRow As Variant
    Dim intMaxCol As Variant
    Dim rngSrcHeader As Range
    Dim rngOutBase As Range
    
    Dim intRow As Variant
    Dim intCol As Variant
    Dim strName As String
    Dim c0 As Range
    Dim c1 As Range
    Dim i As Long
    Dim j As Long
    Dim k As Long
    
    Call area_clear(2)
    
    Set wsOut = GetWorkSheetByName(control_shname, ThisWorkbook)
    If wsOut Is Nothing Then Exit Function
    
    Set rngOutBase = wsOut.Cells(data_base_line, d1_data_base_col)
    intMaxRow = MyDataEndRowGet(rngOutBase.Offset(0, d1_ID_ofs))
    If intMaxRow <= rngOutBase.Row Then
        intDataCount = 0
        GoTo Exit_Proc
    End If
    
    ' FLT_Logシートの情報取得
    Set wsSrc = GetWorkSheetByName(sFLT_Log_shname, ThisWorkbook)
    If wsSrc Is Nothing Then Exit Function
    
    intMaxCol = MyDataEndColumnGet(wsSrc.Cells(1, 1))
    If intMaxCol = 0 Then
        Call MsgBox("ヘッダ不正", vbCritical)
        Exit Function
    End If
    
    Set rngSrcHeader = wsSrc.Cells(1, 1).Resize(1, intMaxCol)
    
    Dim intSrcCol(7) As Variant
    For i = 0 To UBound(intSrcCol)
        Select Case i
        Case 0: strName = "register名"
        Case 1: strName = "ON_bit"
        Case 2: strName = "ON状態のbit名"
        Case 3: strName = "被疑部品"
        Case 4: strName = "MAIN/SUB"
        Case 5: strName = "部品番号"
        Case 6: strName = "実装位置名"
        Case 7: strName = "slot_no"
        Case Else
            Call MsgBox("Bug!!", vbCritical)
            Stop
        End Select
        
        Set c0 = MyCellsValueSearch(strName, rngSrcHeader)
        If c0 Is Nothing Then Exit Function
        intSrcCol(i) = c0.Column
    Next i
    
    Dim intSrcRow As Variant
    Dim c2 As Range
    
    Dim varList As Variant
    Dim intRepeatMax As Long
    Dim strStatus As String
    Dim intDspCount As Variant '実行中表示用カウンタ
    
    Set c1 = rngOutBase
    intMaxRow = intMaxRow - rngOutBase.Row
    
    Set rngOutBase = wsOut.Cells(data_base_line, d2_data_base_col)
    Set c2 = rngOutBase
    
    strStatus = "/" & intMaxRow & " 被疑部品出力中..."
    
    intDspCount = 50
    intDataCount = 0
    For intRow = 1 To intMaxRow
        intDspCount = intDspCount - 1
        If intDspCount < 1 Then
            Application.StatusBar = intRow & strStatus
            DoEvents
            intDspCount = 50
        End If
        
        Set c1 = c1.Offset(1)
        
        intSrcRow = c1.Offset(0, d1_ID_ofs).Value
        Set c0 = wsSrc.Cells(intSrcRow, 1)
        
        intRepeatMax = 1
        Set c2 = c2.Offset(1)
        For i = 1 To 6
            Select Case i
            Case 1: j = d2_Bit_ofs      '"ON_bit"
            Case 2: j = d2_BitName_ofs  '"ON状態のbit名"
            Case 3: j = d2_RepairPart_ofs   '"被疑部品"
            Case 4: j = d2_MainSub_ofs      '"MAIN/SUB"
            Case 5: j = d2_RepairFigNo_ofs  '"部品番号"
            Case 6: j = d2_RepairPlace_ofs  '"実装位置名"
            End Select
            
            varList = c0.Columns(intSrcCol(i)).Value
            If InStr(varList, vbLf) < 1 Then
                varList = Replace(varList, ",", "," & vbLf)
                c2.Offset(0, j).Value = varList
            Else
                varList = Split(varList, vbLf)
                For k = 0 To UBound(varList)
                    c2.Offset(k, j) = Replace(varList(k), ",", "," & vbLf)
                Next k
            
                k = UBound(varList) + 1
                Erase varList
                
                If intRepeatMax < k Then
                    intRepeatMax = k
                End If
            End If
        Next i
        
        With c2.Resize(intRepeatMax)
            c1.Offset(0, d1_No_ofs).Copy .Offset(0, d2_No_ofs)
            c1.Offset(0, d1_ID_ofs).Copy .Offset(0, d2_ID_ofs)
            c1.Offset(0, d1_data1_ofs).Copy .Offset(0, d2_Addr_ofs)
            
            c0.Columns(intSrcCol(0)).Copy .Offset(0, d2_RegName_ofs)
            Select Case c0.Columns(intSrcCol(7)).Value
            Case 1, 2, 3, 4
                c0.Columns(intSrcCol(7)).Copy .Offset(0, d2_SlotNo_ofs)
            End Select
        End With
        
        Set c2 = c2.Offset(intRepeatMax - 1)
        
        intDataCount = intDataCount + intRepeatMax
Continue:
    Next intRow

    ' S150対応 Add Start 2020/12/22 by Nishi
    If flag_CheckFFANLog = True Then
        If ffan_log_write_s150 = False Then
            GoTo Exit_Proc
        End If
    End If
    ' S150対応 Add End 2020/12/22 by Nishi
    
    Application.StatusBar = "被疑部品出力 完了"
    
    If intDataCount = 0 Then
        GoTo Exit_Proc
    End If
    
    '書式設定
    With rngOutBase.Offset(1).Resize(intDataCount, d2_data_max_ofs + 1)
        .Borders.LineStyle = xlContinuous
        .Columns(1 + d2_Bit_ofs).HorizontalAlignment = xlHAlignRight
    End With
    
Exit_Proc:
    repairParts_write_s150 = intDataCount
End Function
' S150対応 Add Start 2020/12/11 by Nishi

'温度上昇の被疑部品出力
Private Function temp_over_write( _
        c1 As Range, _
        ByRef c2 As Range, _
        wsFFAN As Worksheet, _
        ByVal intFFAN_Row As Variant, _
        intFFAN_Col() As Variant _
    ) As Long  '出力行数を返す
    
    temp_over_write = 0
    
    Dim cond_id As String
    Dim alm_str As String '設定値
    Dim index_str As String '「FFAN_Log」検索用index値
    Dim slot_str As String  '「FFAN_Log」検索用slot値
    
    cond_id = c1.Offset(0, d1_data1_ofs).Value
    Select Case cond_id
    Case "BLADE_BREAK_TEMP_STAT"
        alm_str = "Blade内部品温度上昇によるFLT"
        index_str = "0"
        slot_str = "1"
    Case "PIU_BREAK_TEMP_STAT"
        alm_str = "PIU内部品温度上昇によるFLT"
        index_str = "1"
        slot_str = "[1234]"
    Case "BLADE_PATH_MON_STAT"
        alm_str = "PFで検出している制御系装置内監視によるFLT"
        index_str = ""  '「FFAN_Log」検索不要
    Case "BLADE_PATH_MON2_STAT"
        alm_str = "UDで検出している制御系装置内監視によるFLT"
        index_str = ""  '「FFAN_Log」検索不要
    Case Else
        Exit Function
    End Select
    
    Dim c0 As Range
    Dim row_no As Variant
    Dim parts_no As String
    Dim parts_str As String '設定値(被疑部品)
    Dim out_count As Long
    
    Dim c2_save As Range: Set c2_save = c2
    
    If index_str = "" Then
        out_count = 0
        parts_str = ""
    Else
        '「FFAN_Log」検索
        Set c0 = wsFFAN.Cells(1, 1)
        For row_no = 2 To intFFAN_Row
            Set c0 = c0.Offset(1)
            If Trim(c0.Columns(intFFAN_Col(0))) <> "temp/tresh" Then
            ElseIf Trim(c0.Columns(intFFAN_Col(1))) <> index_str Then
            ElseIf Trim(c0.Columns(intFFAN_Col(2))) Like slot_str Then
                parts_no = c0.Columns(intFFAN_Col(3)).Value
                
                '温度Overしたデバイスがあれば、被疑部品情報をコピー
                If Not IsSpaceString(parts_no) Then
                    Set c2 = c2.Offset(1)
                    With c0.Columns(intFFAN_Col(4))
                        Call set_result(c2.Offset(0, d2_RepairPart_ofs), .Value)
                        Call set_result(c2.Offset(0, d2_MainSub_ofs), .Offset(0, 1).Value)
                        Call set_result(c2.Offset(0, d2_RepairFigNo_ofs), .Offset(0, 2).Value)
                        Call set_result(c2.Offset(0, d2_RepairPlace_ofs), .Offset(0, 3).Value)
                    End With
                End If
            End If
        Next row_no
        
        out_count = c2.Row - c2_save.Row
        If out_count = 0 Then
            parts_str = "温度上昇ログなし"
        End If
    End If
    
    'メッセージ列出力
    If (cond_id = "BLADE_BREAK_TEMP_STAT") And (out_count > 0) Then
        'BLADE_BREAK_TEMP_STATで、かつ部品特定が出来た場合は、メッセージ列出力無し。
    Else
        c1.Offset(0, d1_Etc_ofs).Value = "開発部門へ問い合わせお願いします"
    End If
    
    ' 被疑部品の検出が無い場合の出力
    If out_count = 0 Then
        Set c2 = c2.Offset(1)
        c2.Offset(0, d2_RepairPart_ofs).Value = parts_str
        out_count = 1
    End If
    
    With c2_save.Offset(1).Resize(out_count)
        c1.Offset(0, d1_No_ofs).Copy .Offset(0, d2_No_ofs)
        c1.Offset(0, d1_ID_ofs).Copy .Offset(0, d2_ID_ofs)
        c1.Offset(0, d1_data1_ofs).Copy .Offset(0, d2_Addr_ofs)
        
        .Offset(0, d2_RegName_ofs).Value = alm_str
    End With
    
    temp_over_write = out_count
End Function

Private Function date_get( _
        ByRef date_lower As Variant, _
        ByRef date_upper As Variant) As Boolean
    
    date_get = False
    
    Dim date_str As String  '日付指定
    Dim date_num As String    '日付範囲指定
    Dim date_range As String    '日付範囲単位
    Dim time_str    As String   '時刻指定
    Dim time_range  As String    '時刻範囲指定
    Dim ss  As String
    
    ''日付指定取得
    date_str = toolPara_get("TargetDate") '日付指定
    If Not IsDate(date_str) Then    '日付指定がない場合
        date_upper = Null
        date_lower = Null
        Exit Function
    End If
    
    date_upper = CDate(date_str)
    date_lower = date_upper
    
    date_num = toolPara_get("TargetDateRange") '日付範囲指定
    If date_num = "" Then
    ElseIf IsNumeric(date_num) Then  '日付範囲指定がある場合
        
        date_range = toolPara_get("TargetDateUnit") '日付範囲単位
        Select Case date_range
        Case "日前後"
            date_lower = DateAdd("d", -1 * date_num, date_upper)
            date_upper = DateAdd("d", date_num, date_upper)
        Case "日前まで"
            date_lower = DateAdd("d", -1 * date_num, date_upper)
        Case "週前まで"
            date_lower = DateAdd("ww", -1 * date_num, date_upper)
        Case "月前まで"
            date_lower = DateAdd("m", -1 * date_num, date_upper)
        Case "年前まで"
            date_lower = DateAdd("yyyy", -1 * date_num, date_upper)
        End Select
    End If
    
    ''時刻指定取得
    If date_lower <> date_upper Then    '複数日指定の場合は時刻指定を無視する
        time_str = ""
    Else
        time_str = toolPara_get("TargetTime") '時刻指定
        If time_str = "" Then
        ElseIf Not IsDate(time_str) Then    '不正フォーマット
            time_str = ""
        End If
    End If
    
    If time_str <> "" Then  '時刻指定がある場合
        date_upper = date_upper + TimeValue(time_str)
        date_lower = date_upper
        
        time_range = toolPara_get("TargetTimeRange") '時刻範囲指定
        If time_range Like "*秒前後" Then
            ss = Left(time_range, Len(time_range) - 3)  '秒数取り出し
            date_lower = DateAdd("s", 0 - CLng(ss), date_lower)
            date_upper = DateAdd("s", CLng(ss), date_upper)
        ElseIf time_range Like "*秒前まで" Then
            ss = Left(time_range, Len(time_range) - 4)  '秒数取り出し
            date_lower = DateAdd("s", 0 - CLng(ss), date_lower)
        End If
    Else
        date_lower = date_lower + TimeValue("00:00:00")
        date_upper = date_upper + TimeValue("23:59:59")
    End If
    
    date_get = True
End Function

Private Function date_check(rec_date As Variant, date1 As Variant, date2 As Variant) As Boolean
    date_check = False
    
    If Not IsNull(rec_date) Then
        If Not IsNull(date1) Then
            If rec_date < date1 Then
                Exit Function
            End If
        End If
        
        If Not IsNull(date2) Then
            If rec_date > date2 Then
                Exit Function
            End If
        End If
    End If
    
    date_check = True
End Function

' S150対応 Add Start 2020/12/22 by Nishi
' FFAN_logの内容を整形表示する
Private Function ffan_log_write_s150() As Boolean
        
    ffan_log_write_s150 = False
    
    Dim wsSrc As Worksheet
    Dim wsOut As Worksheet
    
    Dim intDataCount As Variant
    Dim intMaxRow As Variant
    Dim intMaxCol As Variant
    Dim rngSrcHeader As Range
    Dim rngOutBase As Range
    
    Dim intRow As Variant
    Dim strName As String
    Dim c0 As Range
    Dim c1 As Range
    Dim i As Long
    
    Dim intSrcCol(d1_data_max_ofs) As Variant
    Dim wsFFAN As Worksheet
    Dim intFFAN_Row As Variant
    Dim intFFAN_Col(8) As Variant
    
    Dim varDetectTime As Variant
    Dim varClearTime As Variant
    Dim intClearTimeCol As Long
    Dim longNewMaxRow As Long
    
    Dim date_lower  As Variant
    Dim date_upper  As Variant
    Dim dtLower  As Date
    Dim dtUpper  As Date
    Dim booDateCheck As Boolean
    
    Dim OutputFlag As Boolean
    
    Set wsSrc = GetWorkSheetByName(sFFAN_Log_shname, ThisWorkbook)
    If wsSrc Is Nothing Then Exit Function
    
    Set wsOut = GetWorkSheetByName(control_shname, ThisWorkbook)
    If wsOut Is Nothing Then Exit Function
    
    intMaxRow = MyDataEndRowGet(wsSrc.Cells(1, 1))
    intMaxCol = MyDataEndColumnGet(wsSrc.Cells(1, 1))
    If intMaxCol = 0 Then
        Call MsgBox("ヘッダ不正", vbCritical)
        Exit Function
    End If
    
    ' FFAN_Logシートの情報取得
    Set wsFFAN = GetWorkSheetByName(sFFAN_Log_shname, ThisWorkbook)
    If wsFFAN Is Nothing Then Exit Function
    
    intFFAN_Row = MyDataEndRowGet(wsFFAN.Cells(1, 1))
    intMaxCol = MyDataEndColumnGet(wsFFAN.Cells(1, 1))
    If intMaxCol = 0 Then
        Call MsgBox("ヘッダ不正", vbCritical)
        Exit Function
    End If
    
    Set rngSrcHeader = wsFFAN.Cells(1, 1).Resize(1, intMaxCol)
    
    For i = 0 To UBound(intFFAN_Col)
        Select Case i
        Case 0: strName = "Date"
        Case 1: strName = "Time"
        Case 2: strName = "Time(.sec)"
        Case 3: strName = "resource"
        Case 4: strName = "alarm-type"
        Case 5: strName = "alarm-type-qualifier"
        Case 6: strName = "perceived-severity"
        Case 7: strName = "alarm-text"
        Case 8: strName = "is-service-affecting"
        Case Else
            Call MsgBox("Bug!!", vbCritical)
            Stop
        End Select
        
        Set c0 = MyCellsValueSearch(strName, rngSrcHeader)
        If c0 Is Nothing Then Exit Function
        intFFAN_Col(i) = c0.Column
    Next i
    
    ' 抽出範囲指定取り出し
    booDateCheck = date_get(date_lower, date_upper)
    If booDateCheck Then
        dtLower = date_lower
        dtUpper = date_upper
    End If
    
    varDetectTime = detectTime_get()
    
    ' 最新の最大行を求める
    longNewMaxRow = wsOut.Cells(data_base_line, d1_data_base_col).End(xlDown).Row
    If data_base_line < longNewMaxRow Then
        intDataCount = wsOut.Cells(longNewMaxRow, d1_data_base_col).Value
    Else
        intDataCount = 0
    End If
    
    Set rngOutBase = wsOut.Cells(longNewMaxRow, d1_data_base_col)
    Set c1 = rngOutBase.Offset(1)
    
    Application.StatusBar = wsSrc.Name & " コピー中..."
    Set c0 = rngSrcHeader
    Set c1 = rngOutBase
    For intRow = 2 To intMaxRow
    
        Set c0 = c0.Offset(1)
        If varDetectTime > 0 Then
            If IsSpaceString(varClearTime) Then
            ElseIf Not IsNumeric(varClearTime) Then
            ElseIf Val(varClearTime) <= varDetectTime Then
                GoTo Continue
            End If
        End If
        
        OutputFlag = False
        Set c1 = c1.Offset(1)
        
        For i = 2 To 3
            ' alarm-type列で"equipmentFault" の時は出力する
            If c0.Columns(intFFAN_Col(4)) = "equipmentFault" Then
                c0.Columns(intFFAN_Col(i - 2)).Copy c1.Offset(0, i)
                If i = 3 Then
                    ' その他列にFFAN_logの結果を出力
                    c1.Offset(0, 7) = MakeFFANResultOutputStr(c0.Columns(intFFAN_Col(3)), c0.Columns(intFFAN_Col(6)))
                    ' 行番号設定
                    c1.Offset(0, d1_ID_ofs).Value = intRow
                    ' FFAN_logの内容を出力したことを示すフラグをONにする
                    OutputFlag = True
                End If
            End If
        Next i
        
        '日時が抽出範内指定外の行は除外
        If Not booDateCheck Then
            '抽出範内指定なし
        ElseIf Not date_check_d1(c1, dtLower, dtUpper) Then
            Set c1 = c1.Offset(-1)
            For i = d1_No_ofs To d1_Etc_ofs
              c1.Offset(1, i).Delete (xlShiftUp)
            Next i
            GoTo Continue
        End If
        
        ' FFAN_logの内容を出力したことを示すフラグをチェックする
        If OutputFlag = True Then
            intDataCount = intDataCount + 1
        Else
            Set c1 = c1.Offset(-1)
        End If
        
Continue:
    Next intRow
    
    Application.StatusBar = wsSrc.Name & "コピー 完了"
    Call MyScreenUpdate
    
    Set rngOutBase = wsOut.Cells(data_base_line, d1_data_base_col)
    
    '------------------------------------------
    ' 後処理
    '------------------------------------------
    intDataCount = intDataCount
    If intDataCount < 1 Then
        Call MsgBox("有効なデータがありません", vbCritical)
        Exit Function
    End If
    
    '降順に並べ替え
    Dim rngOutHeader As Range
    Set rngOutHeader = rngOutBase.Resize(1, d1_data_max_ofs + 1)
    Call MyRangeSelect(rngOutHeader)   '注）Selectは不要だが、ソート範囲を目視で確認するため。
    
    With wsOut.Sort
        With .SortFields
            .Clear
            .Add Key:=rngOutBase.Offset(0, d1_Date_ofs), Order:=xlDescending
            .Add2 Key:=rngOutBase.Offset(0, d1_Time_ofs), Order:=xlDescending
        End With
        
        .SetRange rngOutHeader.Resize(intDataCount + 1)
        .Header = xlYes
        .Orientation = xlTopToBottom
        .Apply
    End With
    
    '通し番号設定
    Call SerialNoWrite(rngOutBase.Offset(0, d1_No_ofs), intDataCount)
    
    '書式設定
    With rngOutHeader.Offset(1).Resize(intDataCount)
        .Borders.LineStyle = xlContinuous
        .Interior.ColorIndex = xlColorIndexNone
    End With
    
    ' その他列の幅を自動調整する
    Worksheets(control_shname).Columns(d1_Etc_ofs + 2).AutoFit
    
    ffan_log_write_s150 = True
End Function

' その他列に出力するFFANの結果生成する
Private Function MakeFFANResultOutputStr(ByVal strDataResource As String, _
                                         ByVal strDataPerceivedSeverity) As String

  ' perceived-severity列が"critical"の時
  If strDataPerceivedSeverity = "critical" Then
      MakeFFANResultOutputStr = strDataResource & " FLT(発生)"
  ' perceived-severity列が"clear"の時
  Else
      MakeFFANResultOutputStr = strDataResource & " FLT(回復)"
  End If

End Function
' S150対応 Add End 2020/12/22 by Nishi






