Attribute VB_Name = "Module1"
'-------------------------------------------------------------
' All Rights Reserved, Copyright (C) FUJITSU LIMITED 2017
'
' 1FINITYログ解析ツール(Sシリーズ) - メインモジュール
'
'   作成日: 2017年 5月23日
'   更新日: 2017年 7月10日
'
'-------------------------------------------------------------
Option Explicit

Public verbose_flag    As Boolean

Public Const control_shname = "操作"
Public Const deftab_shname = "参照ファイル定義"

'参照ファイル定義
Private Const strTabName_UnitNoList = "UNIT図番テーブル"

Private Const strDefName_ExtractTool = "解凍ツール"
Private Const strDefName_ExtractInput = "解凍元ファイル_初期値"
'Private Const strDefName_ExtractOutput = "解凍先フォルダ"

'Private Const strDefName_FLTLOG = "FLTログ_初期値"
Private Const strDefName_MacroFlag = "マクロ終了フラグ"
Private Const strDefName_SaveFile = "保存先ファイル名"

'解析パラメータの位置
Private Const para1_base = "抽出範囲_日付"
Private Const para2_base = "抽出範囲_時刻"
Private Const para3_base = "UNIT図番指定"
Private Const para4_base = "出力フィルタ"
' S150対応 Add Start 2020/12/11 by Nishi
Private Const para5_base = "Blade名"
' S150対応 Add End 2020/12/11 by Nishi

'swerr自動実行＆貼り付け用
Public Const sPasteLog_shname = "LogText"
Public Const sFLT_Log_shname = "FLT_Log"
Public Const sStschgLog_shname = "警報変化点Log"
Public Const sFFAN_Log_shname = "FFAN_Log"
' S150対応 Add Start 2020/12/11 by Nishi
Public Const sConfig_Info_shname = "構成情報"
Public Const sTrap_Info_shname = "Trap情報"
Public Const sVersion_Info_shname = "各種version情報"
' S150対応 Add End 2020/12/11 by Nishi


'データ領域の定義
Public Const data_base_line = 19   '出力項目名の行

Public Const d1_data_base_col = "B"    'ログテキストからの抽出データ
Public Const d1_No_ofs = 0     '遠し番号の列
Public Const d1_ID_ofs = 1     '「ID」列
Public Const d1_Date_ofs = 2
Public Const d1_Time_ofs = 3
Public Const d1_data1_ofs = 4  '「Addr」列
Public Const d1_data2_ofs = 5  '「data2」列
Public Const d1_Info_ofs = 6   '「Clear時間」列
Public Const d1_Etc_ofs = 7     '「その他」列
Public Const d1_data_max_ofs = 7  '最終列

Public Const d2_data_base_col = "K"    '検索結果
Public Const d2_No_ofs = 0     '遠し番号の列
Public Const d2_ID_ofs = 1     '「ID」列
Public Const d2_Addr_ofs = 2    '「Addr」列
Public Const d2_RegName_ofs = 3    '「register名」列
Public Const d2_Bit_ofs = 4        '「Bit」列
Public Const d2_BitName_ofs = 5    '「通知bit名」列
Public Const d2_SlotNo_ofs = 6     '「SLOT」列
Public Const d2_MainSub_ofs = 7  '「MAIN/SUB」列
Public Const d2_RepairPart_ofs = 8     '「被疑部品」列
Public Const d2_RepairFigNo_ofs = 9    '「部品番号」列
Public Const d2_RepairPlace_ofs = 10   '「実装位置名」列
                               '= 11 ※Reserve
Public Const d2_data_max_ofs = 11  '最終列

Public Sub 未実装()
    MsgBox "未実装"
End Sub

Public Sub 終了_Click()
    Application.StatusBar = False
    
    If Workbooks.Count <= 1 Then
        Application.Quit
    Else
        ThisWorkbook.Close
    End If
End Sub

Public Sub 一括実行_Click()

    ' S150対応 Add Start 2020/12/11 by Nishi
    ' 選択されたログ解析モード（S100,S150）を確認する
    Dim strType As String
    Dim ResultCode As Integer
    Dim strCheckMsg As String
    
    strType = BladeName_pickup()
    strCheckMsg = strType & "のログ解析を行いますか？"
    ResultCode = MsgBox(strCheckMsg, vbYesNo + vbQuestion, "確認")
    If ResultCode = vbNo Then
        Call MsgBox("ログ解析を中断しました", vbOKOnly)
        Exit Sub
    End If
    ' S150対応 Add End 2020/12/11 by Nishi

    If toolPara_get("UnitFigNo", 省略可:=False, Caption:="UNIT図番") = "" Then
        Exit Sub
    End If
    
    If MsgBox("一括実行を開始します。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    
    Dim strInputPath As String
    strInputPath = TECHINFOログファイル名取得()
    If strInputPath = "" Then
        Exit Sub
    End If
    
    ' S150対応 Add Start 2020/12/11 by Nishi
    Call CheckAddSheet
    ' S150対応 Add End 2020/12/11 by Nishi
    
    Dim intResult    As Variant '被疑部品検出件数
    intResult = 一括実行_Execute(strInputPath)
    If intResult > 0 Then
        MsgBox "一括実行終了 (" & intResult & "件検出)"
    End If
End Sub

Public Function 一括実行_Execute(ByVal strInputPath As String) As Variant
    Const job_name = "一括実行"
    Dim varResult   As Variant '実行結果
    Dim intFound    As Variant '被疑部品検出件数
    
    Call job_begin(job_name)
    
    varResult = ""
    
    'UNIT図番取得
    Dim unit_figno  As String
    unit_figno = toolPara_get("UnitFigNo", 省略可:=False, Caption:="UNIT図番")
    
    If unit_figno = "" Then
    ElseIf TECHINFO展開_Execute(strInputPath, unit_figno) = False Then
    Else
        intFound = FLTログ整形表示_Execute()
        If intFound > 0 Then
            varResult = True
        End If
    End If
    
    Call job_end(job_name, varResult)
    
    一括実行_Execute = intFound
End Function

'--------------------------------------------------------------
' データ領域クリア
'--------------------------------------------------------------
Private Sub range_clear(rng As Range)
    rng.Clear
    rng.Locked = False
End Sub

'整形表示エリアのクリア
Public Sub 整形表示クリア_Click()
    Application.StatusBar = False
    If MsgBox("整形表示結果をクリアします。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    Call area_clear(1)
    Call area_clear(2)
End Sub

'日時指定エリアのクリア
Public Sub 日時指定クリア_Click()
    Application.StatusBar = False
    If MsgBox("日時指定をクリアします。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    Call area_clear(3)
End Sub

'一括クリア
Public Sub 一括クリア_Click()
    Application.StatusBar = False
    If MsgBox("全ての実行結果をクリアします。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    Call area_clear(99)
    Call area_clear(3)
End Sub

Public Sub area_clear(area_no As Long)
    Dim ws  As Worksheet
    Dim r1  As Range
    Dim r2  As Range
    Dim max_row As Long
    Dim max_col As Long
    
    Set ws = GetWorkSheetByName(control_shname, ThisWorkbook)
    max_row = ws.Rows.Count
    
    Call MyProtectReset(ws)
    
    Select Case area_no
    Case 0, 99  'ログテキスト領域
        Dim i As Long
        Dim sheet_name As String
        Dim wsText As Worksheet
        ' S150対応 Change Start 2020/12/11 by Nishi
        For i = 0 To 6
        ' S150対応 Change End 2020/12/11 by Nishi
            Select Case i
            Case 0: sheet_name = sPasteLog_shname
            Case 1: sheet_name = sFLT_Log_shname
            Case 2: sheet_name = sStschgLog_shname
            Case 3: sheet_name = sFFAN_Log_shname
            
            ' S150対応 Change Start 2020/12/11 by Nishi
            Case 4: sheet_name = sConfig_Info_shname
            Case 5: sheet_name = sTrap_Info_shname
            Case 6: sheet_name = sVersion_Info_shname
            ' S150対応 Add End 2020/12/11 by Nishi
            End Select
            Set wsText = GetWorkSheetByName(sheet_name, ThisWorkbook)
            If Not wsText Is Nothing Then
                wsText.AutoFilterMode = False
                wsText.Rows.Hidden = False
                wsText.Columns.Hidden = False
                
                wsText.Cells.Clear
            End If
        Next i
        Set wsText = Nothing
    End Select
    
    Select Case area_no
    Case 1, 99 'ログ解析結果エリア
        max_col = ws.Columns(d2_data_base_col).Offset(0, -1).Column
        Set r1 = ws.Cells(data_base_line + 1, 1)
        Set r2 = ws.Cells(max_row, max_col)
        Call range_clear(ws.Range(r1, r2))
    End Select
    
    Select Case area_no
    Case 2, 99  'データ検索結果エリア
        If area_no = 2 Then
            Set r1 = ws.Cells(data_base_line + 1, d1_data_base_col)
            Set r2 = ws.Cells(max_row, r1.Column + d1_data_max_ofs)
            ws.Range(r1, r2).Interior.ColorIndex = xlNone
        End If
        max_col = ws.Columns(d2_data_base_col).Column + d2_data_max_ofs + 1
        Set r1 = ws.Cells(data_base_line + 1, d2_data_base_col)
        Set r2 = ws.Cells(max_row, max_col)
        Call range_clear(ws.Range(r1, r2))
    End Select
    
    Select Case area_no
    Case 0, 1, 2, 99
        
    Case 3  '日時指定
        Call toolPara_set("TargetDate") '日付
        Call toolPara_set("TargetDateRange") '日数
        
        Call toolPara_set("TargetTime") '時刻
        Call toolPara_set("TargetTimeRange", "３秒前まで")
        
    Case 4  'UNIT図番
        Call toolPara_set("UnitFigNo")
        
    Case Else
        MsgBox "BUG!", vbCritical
    End Select
    
    Call MyScreenUpdate
    
    Set r1 = Nothing
    Set r2 = Nothing
    Set ws = Nothing
End Sub

'--------------------------------------------------------------
' エラーデータのマーク
'--------------------------------------------------------------
Public Sub set_error_mark(r As Range)
    r.Borders.LineStyle = xlContinuous
    r.Interior.Color = vbRed
End Sub

'--------------------------------------------------------------
' 検出結果を設定
'--------------------------------------------------------------
Public Sub set_result(r As Range, v As Variant _
        , Optional strForm As String = "@", Optional bWrapText As Boolean = True)
    r.Borders.LineStyle = xlContinuous
    r.HorizontalAlignment = xlLeft
    r.VerticalAlignment = xlTop
    r.WrapText = bWrapText
    r.NumberFormatLocal = strForm
    
    r.Value = v
End Sub

'--------------------------------------------------------------
' ジョブ開始処理
'--------------------------------------------------------------
Public Sub job_begin(job_name As String)
    Application.StatusBar = job_name & " 開始"
    Application.EnableEvents = False
    Application.ScreenUpdating = False
    verbose_flag = False
'    verbose_flag = True '試験用
End Sub

'--------------------------------------------------------------
' ジョブ終了処理
'--------------------------------------------------------------
Public Sub job_end(job_name As String, ret_code As Variant)
    Dim strMsg As String
    If VarType(ret_code) <> vbBoolean Then
        strMsg = ret_code
    ElseIf ret_code Then
        strMsg = "完了"
    Else
        strMsg = "ＮＧ"
    End If
    
    Application.ScreenUpdating = True
    Application.EnableEvents = True
    If strMsg <> "" Then
        Application.StatusBar = job_name & " " & strMsg
    End If
    On Error Resume Next
    Call MySheetActivate(ThisWorkbook.Worksheets(control_shname))
    On Error GoTo 0
End Sub

'--------------------------------------------------------------
'
'   FLTLOG解析結果読み込み
'
'--------------------------------------------------------------
Public Sub FLTLOG解析結果読み込み_Click()

    ' S150対応 Add Start 2020/12/11 by Nishi
    ' 選択されたログ解析モード（S100,S150）を確認する
    Dim strType As String
    Dim ResultCode As Integer
    Dim strCheckMsg As String
    
    strType = BladeName_pickup()
    strCheckMsg = strType & "解析結果の読み込みを行いますか？"
    ResultCode = MsgBox(strCheckMsg, vbYesNo + vbQuestion, "確認")
    If ResultCode = vbNo Then
        Call MsgBox("読込みを中断しました", vbOKOnly)
        Exit Sub
    End If
    ' S150対応 Add End 2020/12/11 by Nishi


    Dim strInputPath As String
    strInputPath = TECHINFO解析結果ファイル名取得()
    If strInputPath = "" Then
        Exit Sub
    End If
    
    ' S150対応 Add Start 2020/12/11 by Nishi
    Call CheckAddSheet
    ' S150対応 Add End 2020/12/11 by Nishi
        
    If FLTLOG解析結果_Read(strInputPath) Then
        Call MsgBox("解析結果読み込み 完了")
    End If
End Sub

Private Function TECHINFO解析結果ファイル名取得() As String
    
    ' 初期フォルダ名取得
    Dim strInit As String
    strInit = toolPara_get("SwerrIn")
    If strInit <> "" Then
        strInit = MyFullPathGet(strInit)
        If IsFolderName(strInit) Then
            If Right(strInit, 1) <> "\" Then
                strInit = strInit & "\"
            End If
        ElseIf Not IsExistPath(strInit) Then
            strInit = MyFolderNameGet(strInit)
            If IsExistPath(strInit) Then
                strInit = strInit & "\"
            Else
                strInit = ""
            End If
        Else
            strInit = MyFolderNameGet(strInit) & "\"
        End If
    End If
    
    If strInit = "" Then
        strInit = ThisWorkbook.Path & "\"
    End If
    
    ' ファイル選択
    Dim fd As FileDialog
    Dim strInputPath   As String
    Dim vrtSelectedItem As Variant
    
    strInputPath = ""
    Set fd = Application.FileDialog(msoFileDialogFilePicker)
    With fd
        .Title = "TECHINFO解析結果(FLT解析を含む)ログファイルを指定"
        
        .Filters.Clear
        .Filters.Add "TECHINFO解析結果", "*.xlsx"
        .Filters.Add "すべてのファイル", "*.*"
        .FilterIndex = 1
        .InitialFileName = strInit
        .AllowMultiSelect = False
        If .Show = -1 Then
            For Each vrtSelectedItem In .SelectedItems
                strInputPath = vrtSelectedItem
            Next vrtSelectedItem
        End If
    End With
    Set fd = Nothing
    
    TECHINFO解析結果ファイル名取得 = strInputPath
End Function

'--------------------------------------------------------------
'
'   ログ解析処理
'
'--------------------------------------------------------------
Public Sub 整形表示_Click()

    ' S150対応 Change Start 2020/12/22 by Nishi
    ' 選択されたログ解析モード（S100,S150）を確認する
    Dim strType As String
    Dim ResultCode As Integer
    Dim strCheckMsg As String
    
    strType = BladeName_pickup()
    strCheckMsg = strType & "のFLTログ解析結果を整形表示しますか？"
    ResultCode = MsgBox(strCheckMsg, vbYesNo + vbQuestion, "確認")
    If ResultCode = vbNo Then
        Call MsgBox("整形表示を中断しました", vbOKOnly)
        Exit Sub
    End If
    ' S150対応 Change End 2020/12/22 by Nishi
    
    Dim intResult As Variant '検出件数
    intResult = FLTログ整形表示_Execute()
    Select Case intResult
    Case 0
        Call MsgBox("有効なログデータがありません", vbCritical)
        
    Case Is > 0
        Call MsgBox("FLTログ整形表示 完了(" & intResult & "件検出)")
    End Select
End Sub

Public Sub 整形結果絞込み_Click()
    ' S150対応 Change Start 2020/12/22 by Nishi
    ' 選択されたログ解析モード（S100,S150）を確認する
    Dim strType As String
    Dim ResultCode As Integer
    Dim strCheckMsg As String
    
    strType = BladeName_pickup()
    strCheckMsg = strType & "の整形結果絞込みを行います。" & vbCrLf & _
                  "また、整形表示の結果から指定日時範囲外のデータを削除します。"
    ResultCode = MsgBox(strCheckMsg, vbYesNo + vbQuestion, "確認")
    If ResultCode = vbNo Then
        Call MsgBox("整形結果絞込みを中断しました", vbOKOnly)
        Exit Sub
    End If
    ' S150対応 Change End 2020/12/22 by Nishi
    
    Dim intResult As Variant 'エラーログ検出件数
    intResult = FLTログ整形結果絞込み_Execute()
    If intResult > 0 Then
        MsgBox "完了(" & intResult & "件抽出)"
    End If
End Sub

Public Function detectTime_get() As Variant
    Dim ss As String
    Dim vv As Variant
    Dim unit_str As String
    Dim i As Long
    
    ss = toolPara_get("DetectFilter")
    vv = 0 '未指定、または指定誤りの場合は、0 を返す。
    For i = 0 To 2
        Select Case i
        Case 0
            unit_str = "秒"
        Case 1
            unit_str = "分"
        Case 2
            unit_str = "hour"
        End Select
        If Right(ss, Len(unit_str)) = unit_str Then
            ss = Left(ss, Len(ss) - Len(unit_str))
            If IsNumeric(ss) Then
                vv = Val(ss)
                'TimeSerial値に変換
                vv = vv / 24
                If i < 2 Then vv = vv / 60
                If i < 1 Then vv = vv / 60
            End If
            Exit For
        End If
    Next i
    
    detectTime_get = vv
End Function

' S150対応 Change Start 2020/12/11 by Nishi
'--------------------------------------------------------------
' 参照ファイルの定義を返す
'--------------------------------------------------------------
Public Function RepairPartTableGet( _
        ByVal unit_figno As String, _
        Optional OnlyBladeName As Boolean = False) As Variant
    
    Dim varRefTable(1) As Variant
    Dim tabList As ListObject
    Dim tabRow As ListRow
    Dim tabCol As ListColumn
    Dim intFound As Long
    
    Dim wbRef As Workbook
    Dim c1 As Range
    Dim strName As String
    
    RepairPartTableGet = Empty
    
    Set tabList = toolParaUnitNoTable_get()
    
   intFound = 0
    Set tabCol = tabList.ListColumns("UNIT図番")
    For Each c1 In tabCol.DataBodyRange.Cells
        If Trim(c1.Value) = unit_figno Then
            intFound = c1.Row - tabCol.DataBodyRange.Row + 1
            Exit For
        End If
    Next c1
    
    If intFound = 0 Then
        Set c1 = tabCol.Range.Cells(1)
        Call MyRangeSelect(c1)
        Call MsgBox("設定エラー at " & c1.Address & vbCrLf & vbCrLf _
                   & "'" & unit_figno & "' が未登録です。", vbCritical)
        GoTo Exit_Proc
    End If
    
    Set tabRow = tabList.ListRows(intFound)
    
    Dim strBladeName As String
    Set tabCol = tabList.ListColumns("Blade名")
    Set c1 = tabRow.Range.Columns(tabCol.Index)
    strBladeName = MySpaceCharsReplace(c1.Value, "")
    If strBladeName = "" Then
        Call MyRangeSelect(c1)
        Call MsgBox("設定エラー at " & c1.Address & vbCrLf & vbCrLf _
                  & "'" & unit_figno & "' の" & tabCol.Name & "が未設定です。", vbCritical)
        GoTo Exit_Proc
    End If
    varRefTable(1) = strBladeName
    
    If OnlyBladeName Then
        GoTo Normal_End
    End If
    
    Set tabCol = tabList.ListColumns("ファイル名")
    Set c1 = tabRow.Range.Columns(tabCol.Index)
    Call MyRangeSelect(c1)
    
    strName = Trim(c1.Value)
    If IsSpaceString(strName) Then
        Call MsgBox("設定エラー at " & c1.Address & vbCrLf & vbCrLf _
                  & "'" & unit_figno & "' の" & tabCol.Name & "が未設定です。", vbCritical)
        GoTo Exit_Proc
    End If

    varRefTable(0) = strName

Normal_End:
    RepairPartTableGet = varRefTable
    
Exit_Proc:
    Erase varRefTable
    Set tabRow = Nothing
    Set tabCol = Nothing
    Set tabList = Nothing
End Function
' S150対応 Change End 2020/12/11 by Nishi

'--------------------------------------------------------------
'   ツール設定
'--------------------------------------------------------------
Private Function toolParaArea_get(sKey As String) As Range
    
    Set toolParaArea_get = Nothing
    
    Dim strSheetName As String
    If sKey Like "Swerr*" Then
        strSheetName = deftab_shname
    ElseIf sKey Like "Auto*" Then
        strSheetName = deftab_shname
    Else
        strSheetName = control_shname
    End If
    
    Dim ws  As Worksheet
    Set ws = GetWorkSheetByName(strSheetName, ThisWorkbook)
    If ws Is Nothing Then Exit Function
    
    Dim rngResult As Range
    Select Case sKey
    Case "SwerrTool"    'SWERRログ解凍ツールパス
        Set rngResult = 名前の参照範囲取得(ws, strDefName_ExtractTool)
        
    Case "SwerrIn"      'SWERRログ解凍元ファイル格納フォルダ
        Set rngResult = 名前の参照範囲取得(ws, strDefName_ExtractInput)
        
    Case "AutoMacroFlag"
        Set rngResult = 名前の参照範囲取得(ws, strDefName_MacroFlag)
        
    Case "AutoSaveFile"
        Set rngResult = 名前の参照範囲取得(ws, strDefName_SaveFile)
        
    Case "TargetDate", "TargetDateRange", "TargetDateUnit"
        '抽出範囲_日付
        Set rngResult = 名前の参照範囲取得(ws, para1_base)
        If rngResult Is Nothing Then Exit Function
        
        Select Case sKey
        Case "TargetDate":      Set rngResult = rngResult.Cells(1)
        Case "TargetDateRange": Set rngResult = rngResult.Cells(2)
        Case "TargetDateUnit":  Set rngResult = rngResult.Cells(2).Offset(0, 1)
        Case Else
            Call MsgBox("Bug!!", vbCritical)
            Stop
            Exit Function
        End Select
        
    Case "TargetTime", "TargetTimeRange"
        '抽出範囲_時刻
        Set rngResult = 名前の参照範囲取得(ws, para2_base)
        If sKey = "TargetTime" Then
            Set rngResult = rngResult.Cells(1)
        Else
            Set rngResult = rngResult.Cells(2)
        End If
        
    Case "UnitFigNo"  'UNIT図番
        Set rngResult = 名前の参照範囲取得(ws, para3_base)
        
    ' S150対応 Add Start 2020/12/11 by Nishi
    Case "BladeName"   'Blade名
        Set rngResult = 名前の参照範囲取得(ws, para5_base)
    ' S150対応 Add End 2020/12/11 by Nishi

    Case "DetectFilter" '出力フィルタ
        Set rngResult = 名前の参照範囲取得(ws, para4_base)
        
    Case Else
        Call MsgBox("Bug!!", vbCritical)
        Stop
        Exit Function
    End Select
    
    Set toolParaArea_get = rngResult
End Function

Private Sub toolPara_set( _
        strKey As String, _
        Optional varValue As Variant)
        
    Dim rngPara As Range
    Set rngPara = toolParaArea_get(strKey)
    If rngPara Is Nothing Then Exit Sub
    
    Call MyProtectReset(rngPara.Worksheet)
    
    If IsMissing(varValue) Then
        rngPara.ClearContents
    Else
        If VarType(varValue) = vbString Then
            rngPara.NumberFormatLocal = "@"
        End If
        rngPara.Value = varValue
    End If
End Sub

Public Function toolPara_get( _
        sKey As String, _
        Optional ByVal 省略可 As Boolean = True, _
        Optional ByVal Caption As String = "") As String
            
    toolPara_get = ""
    
    Dim rngPara As Range
    Set rngPara = toolParaArea_get(sKey)
    If rngPara Is Nothing Then Exit Function
    
    Dim strValue As String
    Select Case sKey
    Case "TargetDate", "TargetTime"
        strValue = Trim(rngPara.Text) '注意) TargetTimeは、.Textで取り出さないと時刻範囲の判定ができない
    Case Else
        strValue = Trim(rngPara.Value)
    End Select
    If IsSpaceString(strValue) Then
        strValue = ""
    End If
    
    If Not 省略可 And (strValue = "") Then
        Dim strMsg As String
        With rngPara
            strMsg = "'" & .Worksheet.Name & "シートの'" & .Address(False, False)
        End With
        If Caption <> "" Then
            strMsg = Caption & " (" & strMsg & ")" & vbCrLf
        End If
        Call MyRangeSelect(rngPara)
        Call MsgBox(strMsg & " が未設定です。", vbCritical)
        Exit Function
    End If
    
    toolPara_get = strValue
End Function

'UNIT図番テーブル取得
Private Function toolParaUnitNoTable_get() As ListObject
    Set toolParaUnitNoTable_get = Nothing
    
    Dim ws  As Worksheet
    Set ws = GetWorkSheetByName(deftab_shname, ThisWorkbook)
    If ws Is Nothing Then Exit Function
    
    Set toolParaUnitNoTable_get = GetTableByName(strTabName_UnitNoList, ws)
        
End Function

Public Sub TECHINFO解凍ツール設定_Click()
    
    'SWERRログ解凍ツールのフルパスを指定する
    Dim fd As FileDialog
    Dim strInit   As String
    Dim vrtSelectedItem As Variant
    Const strSuffix As String = "*.bat"
    
    strInit = toolPara_get("SwerrTool")
    If strInit <> "" Then
        strInit = MyFullPathGet(strInit)
        If Not IsExistPath(strInit) Then
            strInit = ""
        End If
    End If
    
    If strInit = "" Then
        strInit = ThisWorkbook.Path & "\"
    End If
    
    Set fd = Application.FileDialog(msoFileDialogFilePicker)
    With fd
        .Title = "TECHINFO解凍ツールを指定"
        
        .Filters.Clear
        .Filters.Add "実行ファイル", strSuffix
        .Filters.Add "すべてのファイル", "*.*"
        .FilterIndex = 1
        .InitialFileName = strInit
        .AllowMultiSelect = False
        If .Show = -1 Then
            For Each vrtSelectedItem In .SelectedItems
                Call toolPara_set("SwerrTool", vrtSelectedItem)
            Next vrtSelectedItem
        End If
    End With
    Set fd = Nothing

End Sub

Public Sub TECHINFOログファイル名設定_Click()
    
    Dim strResult As String
    strResult = TECHINFOログファイル名取得(ForSetting:=True)
    If strResult <> "" Then
        Call toolPara_set("SwerrIn", strResult)
    End If

End Sub

Public Sub TECHINFO格納フォルダ設定_Click()
    
    Dim strInit   As String
    Dim strResult As String
    
    strInit = toolPara_get("SwerrIn")
    strResult = フォルダ選択("TECHINFO格納フォルダ", strInit)
    If strResult <> "" Then
        Call toolPara_set("SwerrIn", strResult)
    End If

End Sub

Private Function フォルダ選択( _
        ByVal strTitle As String, _
        Optional ByVal strInitPath As String = "") As String
    
    フォルダ選択 = ""
    
    Dim fd As FileDialog
    Dim strFolderPath As String
    
    If IsSpaceString(strInitPath) Then
        strFolderPath = ""
    Else
        strFolderPath = 有効なフォルダ名を取得(MyFullPathGet(strInitPath))
    End If
    
    If strFolderPath = "" Then
        strFolderPath = ThisWorkbook.Path
    End If
    
    Set fd = Application.FileDialog(msoFileDialogFolderPicker)
    With fd
        .Title = strTitle & "を指定"
        .InitialFileName = strFolderPath & "\"
        If .Show = -1 Then
            フォルダ選択 = .SelectedItems(1)
        End If
    End With
    Set fd = Nothing

End Function

Private _
Function 有効なフォルダ名を取得(strFullPath As String) As String
    Dim strFolderPath As String
    Dim strSave As String
        
    strFolderPath = MyFolderNameGet(strFullPath)
    Do Until IsExistPath(strFolderPath)
        strSave = strFolderPath
        strFolderPath = MyFolderNameGet(strFolderPath)
        If strFolderPath = strSave Then
            strFolderPath = ""
            Exit Do
        End If
    Loop
    
    有効なフォルダ名を取得 = strFolderPath
End Function

'--------------------------------------------------------------
'
' TECHINFOログ解凍
'
'--------------------------------------------------------------
Public Sub TECHINFO展開_Click()
    
    ' S150対応 Add Start 2020/12/11 by Nishi
    ' 選択されたログ解析モード（S100,S150）を確認する
    Dim strType As String
    Dim ResultCode As Integer
    Dim strCheckMsg As String
    
    strType = BladeName_pickup()
    strCheckMsg = strType & "のログ解析を行いますか？"
    ResultCode = MsgBox(strCheckMsg, vbYesNo + vbQuestion, "確認")
    If ResultCode = vbNo Then
        Call MsgBox("ログ解析を中断しました", vbOKOnly)
        Exit Sub
    End If
    ' S150対応 Add End 2020/12/11 by Nishi
    
    'UNIT図番取得
    Dim unit_figno  As String
    unit_figno = toolPara_get("UnitFigNo", 省略可:=False, Caption:="UNIT図番")
    If unit_figno = "" Then
        Exit Sub
    End If
    
    Dim strInputPath As String
    strInputPath = TECHINFOログファイル名取得()
    If strInputPath = "" Then
        Exit Sub
    End If
    
    ' S150対応 Add Start 2020/12/11 by Nishi
    Call CheckAddSheet
    ' S150対応 Add End 2020/12/11 by Nishi
    
    Call toolPara_set("AutoMacroFlag", "")
    
    If TECHINFO展開_Execute(strInputPath, unit_figno) Then
        Call MsgBox("TECHINFO展開 完了")
    End If
    
    Call toolPara_set("AutoMacroFlag", "1")
End Sub

Public Function TECHINFOログファイル名取得( _
        Optional ForSetting As Boolean = False) As String
    
    Const strTarGz As String = ".tar.gz"
    
    ' 初期フォルダ名取得
    Dim strInit As String
    strInit = toolPara_get("SwerrIn")
    If strInit <> "" Then
        strInit = MyFullPathGet(strInit)
        If IsFolderName(strInit) Then
            If Right(strInit, 1) <> "\" Then
                strInit = strInit & "\"
            End If
        ElseIf Not IsExistPath(strInit) Then
            strInit = MyFolderNameGet(strInit)
            If IsExistPath(strInit) Then
                strInit = strInit & "\"
            Else
                strInit = ""
            End If
        ElseIf ForSetting Then
            strInit = MyFolderNameGet(strInit) & "\"
        Else
            'ファイル名が指定されている場合
            If LCase(Right(strInit, Len(strTarGz))) = strTarGz Then
                Select Case MsgBox("'" & strInit & "'" & vbCrLf & vbCrLf _
                                 & "から抽出します。", vbYesNoCancel)
                Case vbYes
                    TECHINFOログファイル名取得 = strInit
                    Exit Function
                Case vbCancel
                    TECHINFOログファイル名取得 = ""
                    Exit Function
                End Select
            End If
            strInit = MyFolderNameGet(strInit) & "\"
        End If
    End If
    
    If strInit = "" Then
        strInit = ThisWorkbook.Path & "\"
    End If
    
    ' ファイル選択
    Dim fd As FileDialog
    Dim strInputPath   As String
    Dim vrtSelectedItem As Variant
    
    strInputPath = ""
    Set fd = Application.FileDialog(msoFileDialogFilePicker)
    With fd
        .Title = "TECHINFOログファイルを指定"
        
        .Filters.Clear
        .Filters.Add "TECHINFOログファイル", "*" & strTarGz
        .Filters.Add "すべてのファイル", "*.*"
        .FilterIndex = 1
        .InitialFileName = strInit
        .AllowMultiSelect = False
        If .Show = -1 Then
            For Each vrtSelectedItem In .SelectedItems
                strInputPath = vrtSelectedItem
            Next vrtSelectedItem
        End If
    End With
    Set fd = Nothing
    
    TECHINFOログファイル名取得 = strInputPath
End Function

Sub 実行結果保存_Click()
    If MsgBox("実行結果を保存します。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    
    If 実行結果保存_Execute(DoClose:=True) Then
        Call MsgBox("実行結果保存 完了")
    End If
End Sub

Sub 名前を付けて保存()
'
' 名前を付けて保存 Macro
'
    Call toolPara_set("AutoMacroFlag", "")

    Worksheets(control_shname).Activate
    
    Application.DisplayAlerts = False '問い合せダイアログの表示をOFFにします
    
    ActiveWorkbook.SaveAs Filename:= _
        toolPara_get("AutoSaveFile") _
        , FileFormat:=xlOpenXMLWorkbookMacroEnabled, CreateBackup:=False '名前を付けて保存
        
    Application.DisplayAlerts = True '問い合せダイアログの表示をONにします
    
    Call toolPara_set("AutoMacroFlag", "1")
   
End Sub


Sub EXCEL終了()

    Call toolPara_set("AutoMacroFlag", "")
    
    Application.DisplayAlerts = False
    
    'Worksheets("操作").Activate
          
    Application.Quit                     'Excelを終了します

End Sub

' S150対応 Add Start 2020/12/11 by Nishi
'--------------------------------------------------------------
' シート変更イベントの処理
'--------------------------------------------------------------
Public Sub control_ws_change(Target As Range)
    Dim r1 As Range
    Set r1 = toolParaArea_get("UnitFigNo")
    If r1 Is Nothing Then Exit Sub
                                        
    If Target.Address(External:=True) = r1.Address(External:=True) Then
        Dim varSave As Variant: varSave = Application.EnableEvents
        Dim varBladeName As Variant
        varBladeName = BladeName_pickup()
        Application.EnableEvents = False
        If IsEmpty(varBladeName) Then
            Call toolPara_set("BladeName")
        Else
            Call toolPara_set("BladeName", varBladeName)
        End If
        Application.EnableEvents = varSave
    End If
    Set r1 = Nothing
End Sub

'Blade名取り出し
Public Function BladeName_pickup( _
        Optional ByVal 省略可 As Boolean = True) As Variant
                                            
    BladeName_pickup = Empty
                                        
    'UNIT図番取り出し
    Dim unit_figno As String
    unit_figno = toolPara_get("UnitFigNo", 省略可:=省略可)
    If unit_figno = "" Then Exit Function
                                        
    Dim varRefTable As Variant
    varRefTable = RepairPartTableGet(unit_figno, OnlyBladeName:=True)
    If IsEmpty(varRefTable) Then Exit Function
                                        
    BladeName_pickup = varRefTable(1)
End Function

Public Function GetRefTableFileName(strFolderPath As String) As String
                                            
    GetRefTableFileName = Empty
                                        
    'UNIT図番取り出し
    Dim unit_figno As String
    unit_figno = toolPara_get("UnitFigNo")
    If unit_figno = "" Then Exit Function
                                        
    Dim varRefTable As Variant
    Dim RefTableFileName As String
    Dim strCheckFolderPath As String
    
    varRefTable = RepairPartTableGet(unit_figno, OnlyBladeName:=False)
    If IsEmpty(varRefTable) Then Exit Function
                                        
    RefTableFileName = varRefTable(0)
    strCheckFolderPath = strFolderPath & "\cfg\flt_table\" & RefTableFileName
    
    If Dir(strCheckFolderPath, vbNormal) <> "" Then
      GetRefTableFileName = RefTableFileName
    Else
      MsgBox "'" & RefTableFileName & "'" & vbCrLf _
          & "が存在しません。設定を確認して下さい。", vbCritical

      GetRefTableFileName = ""
    End If

End Function

' 必要なシートをチェックして追加する
Public Sub CheckAddSheet()

    Dim ws As Worksheet
    Dim flag_PasteLogSheet As Boolean
    Dim flag_LogSheet As Boolean
    Dim flag_StschgLogSheet As Boolean
    Dim flag_FFANLogSheet As Boolean
    Dim flag_ConfigInfoSheet As Boolean
    Dim flag_TrapInfoSheet As Boolean
    Dim flag_VersionInfoSheet As Boolean
    
    ' シートが存在するかチェックする
    For Each ws In ThisWorkbook.Worksheets
        Select Case ws.Name
            Case sPasteLog_shname
                flag_PasteLogSheet = True
            Case sFLT_Log_shname
                flag_LogSheet = True
            Case sStschgLog_shname
                flag_StschgLogSheet = True
            Case sFFAN_Log_shname
                flag_FFANLogSheet = True
            Case sConfig_Info_shname
                flag_ConfigInfoSheet = True
            Case sTrap_Info_shname
                flag_TrapInfoSheet = True
            Case sVersion_Info_shname
                flag_VersionInfoSheet = True
        End Select
    Next ws
    
    'LogTextシートを追加する
    If flag_PasteLogSheet = False Then
        Debug.Print ("[Module1:sFFAN_Log_shname] Add ""LogText"" Sheet  ")
        Worksheets.Add after:=Worksheets(Worksheets.Count)
        ActiveSheet.Name = sPasteLog_shname
    End If
    
    'FLT_Logシートを追加する
    If flag_LogSheet = False Then
        Debug.Print ("[Module1:sFFAN_Log_shname] Add ""FLT_Log"" Sheet  ")
        Worksheets.Add after:=Worksheets(Worksheets.Count)
        ActiveSheet.Name = sFLT_Log_shname
    End If
    
    '警報変化点Logシートを追加する
    If flag_StschgLogSheet = False Then
        Debug.Print ("[Module1:sFFAN_Log_shname] Add ""警報変化点Log"" Sheet  ")
        Worksheets.Add after:=Worksheets(Worksheets.Count)
        ActiveSheet.Name = sStschgLog_shname
    End If
    
    'FFAN_Logシートを追加する
    If flag_FFANLogSheet = False Then
        Debug.Print ("[Module1:sFFAN_Log_shname] Add ""FFAN_Log"" Sheet  ")
        Worksheets.Add after:=Worksheets(Worksheets.Count)
        ActiveSheet.Name = sFFAN_Log_shname
    End If
    
    '構成情報シートを追加する
    If flag_ConfigInfoSheet = False Then
        Debug.Print ("[Module1:sFFAN_Log_shname] Add ""構成情報"" Sheet  ")
        Worksheets.Add after:=Worksheets(Worksheets.Count)
        ActiveSheet.Name = sConfig_Info_shname
    End If
    
    'Trap情報シートを追加する
    If flag_TrapInfoSheet = False Then
        Debug.Print ("[Module1:sFFAN_Log_shname] Add ""Trap情報"" Sheet  ")
        Worksheets.Add after:=Worksheets(Worksheets.Count)
        ActiveSheet.Name = sTrap_Info_shname
    End If
    
    '各種version情報シートを追加する
    If flag_VersionInfoSheet = False Then
        Debug.Print ("[Module1:sFFAN_Log_shname] Add ""各種version情報"" Sheet  ")
        Worksheets.Add after:=Worksheets(Worksheets.Count)
        ActiveSheet.Name = sVersion_Info_shname
    End If

End Sub
' S150対応 Add End 2020/12/11 by Nishi




